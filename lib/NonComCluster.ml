open Core
open Algebra

type tIndex = N of int | T of int [@@deriving compare,sexp]
let transpose_index i =
  match i with
  | N(i) -> T(i)
  | T(i) -> N(i)
let i_of v =
  match v with
  | N(i) -> i
  | T(i) -> i
let iequal i u = Int.equal (i_of u) i
let match_trans u v =
  match (u,v) with
  |(N _, N _) -> false
  |(T _, T _) -> false
  |(N i, T j) -> Int.equal i j
  |(T i, N j) -> Int.equal i j

type position = BEFORE | AFTER
(* Interpret TN((i,ipos),(o,opos)) as edge from i to o with tail colored (T,ipos) and head covered (N,opos) *)
type qEdge =
  | TN of (int*position) * (int*position)
  | NT of (int*position) * (int*position)


module Make(R : sig include Ring.S val id : t -> string val transpose : t -> t end) (* : Cluster.S with module Vertex = V *) =
  struct
    module Vertex = R
    open Cluster
    (* (InBefore, InAfter), (OutBefore, OutAfter) *)
    (* Befores are stored in reverse order *)
    type nbrs = (Vertex.t list * Vertex.t list) * (Vertex.t list * Vertex.t list) [@@deriving compare,sexp]

    (* If u -> N v then v <- T u
     * If u -> T v then v <- N u  *)
    type nbrsI = (tIndex list * tIndex list) * (tIndex list * tIndex list) [@@deriving compare,sexp]
    type graph =  nbrsI Int.Map.t [@@deriving compare,sexp]
    type cluster = {graph:graph;
                    label:Vertex.t Int.Map.t;
                    index:int list Vertex.Map.t;
                    frozen: Int.Set.t;
                    unfrozen: Int.Set.t} [@@deriving compare,sexp]

    module T = struct
      type t = cluster
      let sexp_of_t = sexp_of_cluster
      let t_of_sexp = cluster_of_sexp
      (* Clusters are identified by the multiset of a coordinates up to transpose regardless of index *)
      let compare c1 c2  =
        let addTrans index =
          index |> Vertex.Map.to_alist
          |> List.concat_map ~f:(fun (v,l) -> if Vertex.(transpose v = v) then [v,List.length l] else [v,List.length l; Vertex.transpose v, List.length l])
          |> Vertex.Map.of_alist_reduce ~f:Int.(+) in
       Vertex.Map.compare_direct Int.compare (addTrans c1.index) (addTrans c2.index)
    end



    include T

    let at c i = Map.find c.label i
    let at_exn c i =
      match  Map.find c.label i with
      | Some v -> v
      | None -> invalid_arg ("Clusters doesn't have a vertex indexed "^ Int.to_string i)

    let atT c i =
      match i with
      | N i -> Map.find c.label i
      | T i -> Option.map ~f:(R.transpose) (Map.find c.label i)

    let atT_exn c i =
      match atT c i with
      | Some v -> v
      | None -> invalid_arg ("Clusters doesn't have a vertex indexed "^ Int.to_string (i_of i))

    let index_of c v =
      match Map.find c.index v with
      | Some [i] -> Some i
      | Some [] -> None
      | Some _ -> invalid_arg ("Vertex " ^ Vertex.to_string v ^ " occurs multiple times")
      | None -> None

    let index_of_exn c v =
      match index_of c v with
      | None -> invalid_arg ("Vertex " ^ Vertex.to_string v ^ " isn't in the cluster")
      | Some i -> i

    let mem ?vkind:(vkind=AnyKind) c v =
      match (vkind, Map.find c.index v) with
      | (_, None) -> false
      | (Frozen, Some is) -> List.for_all ~f:(Set.mem c.frozen) is
      | (Unfrozen, Some is) -> List.for_all ~f:(Set.mem c.unfrozen) is
      | (AnyKind, Some _) -> true
    let memI ?vkind:(vkind=AnyKind) c i =
      match vkind with
      | Frozen -> Set.mem c.frozen i
      | Unfrozen -> Set.mem c.unfrozen i
      | AnyKind -> Set.mem c.unfrozen i || Set.mem c.frozen i
    let verts ?vkind:(vkind=AnyKind) c =
      match vkind with
      | Frozen -> Vertex.Set.map c.frozen ~f:(Map.find_exn c.label)
      | Unfrozen -> Vertex.Set.map c.unfrozen ~f:(Map.find_exn c.label)
      | AnyKind -> Vertex.Set.union (Vertex.Set.map c.frozen ~f:(Map.find_exn c.label)) (Vertex.Set.map c.unfrozen ~f:(Map.find_exn c.label))

    let map c ~f = {graph = c.graph
                   ;label = c.label |> Int.Map.map ~f:f
                   ;index = c.index |> Map.to_alist |> List.map ~f:(fun (v,is) -> (f v, is)) |> Vertex.Map.of_alist_reduce ~f:(@)
                   ;frozen = c.frozen
                   ;unfrozen = c.unfrozen}


    let filteri c ~f =
      {frozen = c.frozen |> Int.Set.filter ~f:(fun i -> f i (at_exn c i))
      ;unfrozen= c.unfrozen |> Int.Set.filter ~f:(fun i -> f i (at_exn c i))
      ;label = c.label |> Int.Map.filteri ~f:(fun ~key:i ~data:v -> f i v)
      ;index = c.index |> Vertex.Map.mapi ~f:(fun ~key:v ~data:is -> List.filter is ~f:(fun i -> f i v)) |> Vertex.Map.filter ~f:(Fn.non List.is_empty)
      ;graph = Int.Map.filter_mapi c.graph ~f:(fun ~key:vi ~data:((iB,iA),(oB,oA)) ->
                   if f vi (at_exn c vi)
                   then Some((List.filter iB ~f:(fun wi -> f (i_of wi) (at_exn c (i_of wi))),List.filter iA ~f:(fun wi -> f (i_of wi) (at_exn c (i_of wi)))),
                             (List.filter oB ~f:(fun wi -> f (i_of wi) (at_exn c (i_of wi))),List.filter oA ~f:(fun wi -> f (i_of wi) (at_exn c (i_of wi)))))
                   else None)}
    let filter c ~f = filteri c ~f:(fun _(*i*) v -> f v)

    let exists ?vkind:(vkind = AnyKind) (c) ~f =  Map.existsi c.index ~f:(fun ~key:v ~data:_ -> (mem ~vkind:vkind c v) && f v)
    let for_all ?vkind:(vkind = AnyKind) (c) ~f = Map.for_alli c.index ~f:(fun ~key:v ~data:_ -> not(mem ~vkind:vkind c v) || f v)
    let count ?vkind:(vkind = AnyKind) (c:cluster) ~f = Map.fold c.index ~init:0 ~f:(fun ~key:v ~data:is acc -> List.count is ~f:(fun i -> (memI ~vkind:vkind c i) && f v) + acc)
    let find ?vkind:(vkind = AnyKind) (c:cluster) ~f = Set.find (verts ~vkind:vkind c) ~f:f
    let find_exn ?vkind:(vkind = AnyKind) (c:cluster) ~f = Set.find_exn (verts ~vkind:vkind c) ~f:f


    (* CREATE *)

    (* Each element of the list is of form (v, (inBefore inAfter),(OutBefore, OutAfter))). Repeated elements create multi-edges*)
     let of_nbrlist  ~(graph : (int * nbrsI) list) ~label  ~frozen =
       {graph = Int.Map.of_alist_exn graph
        ; label = Int.Map.of_alist_exn label
        ; index = Vertex.Map.of_alist_multi (List.map ~f:(fun (i,v)-> (v,i)) label)
        ; frozen = frozen
        ; unfrozen = Int.Set.of_list(List.filter_map label ~f:(fun (key,_) -> if not (Set.mem frozen key) then Some key else None))}


     let cyc l i =
       let x = i mod (List.length l) in
       List.nth_exn l (if x >= 0 then x else x+List.length l)
     let rec tripair_to_edges (t1, t2) =
       match (List.findi t1 ~f:(fun _ u -> List.exists t2 ~f:(match_trans u)),List.findi t2 ~f:(fun _ u -> List.exists t1 ~f:(match_trans u))) with
       | (Some(i,N u),Some(j,T _)) -> (u, (([(cyc t2 (j-1))],[transpose_index (cyc t1 (i-1))]),([transpose_index (cyc t2 (j+1))],[ (cyc t1 (i+1))])))
       | (Some(_,T _),Some(_, N _)) -> tripair_to_edges (t2, t1)
       | _-> invalid_arg ( "cycles should match with one transpose and one not"
                           ^ Sexp.to_string(List.sexp_of_t sexp_of_tIndex t1) ^ " "
                           ^ Sexp.to_string(List.sexp_of_t sexp_of_tIndex t2))
     let pairTris t l = List.filter_map l ~f:(fun s -> if List.exists t ~f:(fun u -> List.exists s ~f:(match_trans u)) then Some(t,s) else None)
     let of_tris ~graph:test ~frozen ~label =
       of_nbrlist
         ~graph:(List.concat_mapi test ~f:(fun i t -> pairTris t (List.drop test i)) |> List.map ~f:tripair_to_edges) ~frozen:frozen ~label:label;;

     let of_edgelist ~graph ~frozen ~label =
       let pos i pos = match pos with BEFORE -> ([i],[]) | AFTER -> ([],[i]) in
       {graph = graph |> List.concat_map ~f:(function NT((i,ipos),(o,opos)) -> [(i, (([],[]),pos (N o) ipos)); (o, (pos (T i) opos,([],[])))]
                                                    | TN((i,ipos),(o,opos)) -> [(i, (([],[]),pos (T o) ipos)); (o, (pos (N i) opos,([],[])))])
                |> Int.Map.of_alist_reduce ~f:(fun ((accIB,accIA),(accOB,accOA)) ((iB,iA),(oB,oA)) -> ((accIB@iB,accIA@iA),(accOB@oB,accOA@oA)))
       ; label = Int.Map.of_alist_exn label
       ; index = Vertex.Map.of_alist_multi (List.map ~f:(fun (i,v)-> (v,i)) label)
       ; frozen = frozen
       ; unfrozen = Int.Set.of_list(List.filter_map label ~f:(fun (key,_) -> if not (Set.mem frozen key) then Some key else None))}


(*    let of_quiver ~(quiver:graph)  ~(frozen : Vertex.Set.t) : cluster =
      { graph = quiver
      ; frozen = frozen
      ; unfrozen = Vertex.Set.of_list( Map.keys (Map.filteri quiver ~f:(fun ~key ~data -> not (Set.mem frozen key))))}
*)
    let freeze c l =
      { graph = c.graph
      ; label = c.label
      ; index = c.index
      ; frozen = List.fold ~init:c.frozen ~f:(fun s v -> List.fold ~init:s ~f:Int.Set.add (Map.find_multi c.index v)) l
      ; unfrozen = List.fold ~init:c.unfrozen ~f:(fun s v -> List.fold ~init:s ~f:Int.Set.remove (Map.find_multi c.index v))  l}

    let unfreeze c l =
      { graph = c.graph
      ; label = c.label
      ; index = c.index
      ; frozen = List.fold ~init:c.frozen ~f:(fun s v -> List.fold ~init:s ~f:Int.Set.remove (Map.find_multi c.index v)) l
      ; unfrozen = List.fold ~init:c.unfrozen ~f:(fun s v -> List.fold ~init:s ~f:Int.Set.add (Map.find_multi c.index v)) l}

    let freezeI c l =
      {graph = c.graph
      ; label = c.label
      ; index = c.index
      ; frozen = List.fold ~init:c.frozen ~f:Int.Set.add l
      ; unfrozen = List.fold ~init:c.unfrozen ~f:Int.Set.remove l}
    let unfreezeI c l =
      {graph = c.graph
      ; label = c.label
      ; index = c.index
      ; frozen = List.fold ~init:c.frozen ~f:Int.Set.remove l
      ; unfrozen = List.fold ~init:c.unfrozen ~f:Int.Set.add l}


    let tauI c i =
      match at c i with
      | None -> c
      | Some v ->
         {graph = (* Transpose references to i at other places. At i swap before after and transpose to each spot (transpose is antihomomorphsim *)
            c.graph |> Map.map ~f:(fun ((iB,iA),(oB,oA)) -> ((List.map ~f:(fun j -> if iequal i j then transpose_index j else j) iB
                                                             ,List.map ~f:(fun j -> if iequal i j then transpose_index j else j) iA)
                                                            ,(List.map ~f:(fun j -> if iequal i j then transpose_index j else j) oB
                                                             ,List.map ~f:(fun j -> if iequal i j then transpose_index j else j) oA)))
            |> (fun graph -> Map.update graph i ~f:(function None -> invalid_arg "Already checked i was in the graph here"
                                                           | Some ((iB,iA),(oB,oA)) -> ((List.map ~f:transpose_index iA,List.map ~f:transpose_index iB)
                                                                                       ,(List.map ~f:transpose_index oA,List.map ~f:transpose_index oB))))
         ;label = c.label |> Map.mapi ~f:(fun ~key ~data -> if Int.equal i key then Vertex.transpose data else data)
         ;index = Map.change c.index v ~f:(function None -> None | Some is -> match List.filter is ~f:(Fn.non (Int.equal i)) with [] -> None | l -> Some l)
                  |> Map.add_multi ~key:(Vertex.transpose v) ~data:i
         ;frozen = c.frozen
         ;unfrozen = c.unfrozen}

    (* OUTPUT *)
    let frozen c = Vertex.Set.map ~f:(Map.find_exn c.label) c.frozen
    let unfrozen c = Vertex.Set.map ~f:(Map.find_exn c.label) c.unfrozen
    let unfrozenList c = c.unfrozen |> Core.Set.to_list |> List.map ~f:(Map.find_exn c.label)
    let verts ?frozen:(include_frozen = true) c =
      (if include_frozen then Set.union c.frozen c.unfrozen else c.unfrozen) |> Vertex.Set.map ~f:(Map.find_exn c.label)

    let quiver c = c.graph

    let nbrI ?frozen:(frozen = true) c i  =
    match Map.find c.graph i with
    | None -> (([],[]),([],[]))
    | Some ((iB,iA),(oB,oA)) -> ((List.filter iB ~f:(fun key -> (frozen || Set.mem c.unfrozen (i_of key))),
                                  List.filter iA ~f:(fun key -> (frozen || Set.mem c.unfrozen (i_of key))))
                                ,(List.filter oB ~f:(fun key -> (frozen || Set.mem c.unfrozen (i_of key))),
                                  List.filter oA ~f:(fun key -> (frozen || Set.mem c.unfrozen (i_of key)))))
    let nbr ?frozen:(frozen = true) c v =  nbrI ~frozen:frozen c (index_of_exn c v)


    let combineEdgeInfo =
      function ((T v,upos),(N u,vpos)) -> NT((v,vpos),(u,upos))
              |((N v,upos),(T u,vpos)) -> TN((v,vpos),(u,upos))
              | _ -> invalid_arg "All edges should be NT or TN"
    let rec edgesI c u v =
      match (Map.find c.graph u, Map.find c.graph v) with
      | (None,None) -> []
      | (None, Some ((viB,viA),(voB,voA))) ->
         if Set.mem c.frozen u
         then
           let vos = List.map ~f:(fun x -> (x,BEFORE)) voB @ List.map ~f:(fun x -> (x,AFTER)) voA
                     |> List.filter ~f:(fun (x,_) -> iequal u x)
                     |> List.map ~f:(function (N _,pos) -> NT((v,pos),(u,pos))
                                             |(T _,pos) -> TN((v,pos),(u,pos))) in
           let vis = List.map ~f:(fun x -> (x,BEFORE)) viB @ List.map ~f:(fun x -> (x,AFTER)) viA
                     |> List.filter ~f:(fun (x,_) -> iequal u x)
                     |> List.map ~f:(function (N _,pos) -> TN((u,pos),(v,pos))
                                            | (T _,pos) -> NT((u,pos),(v,pos)))in
           vos @ vis
         else []
      | (Some _, None) -> edgesI c v u
      | (Some ((uiB,uiA),(uoB,uoA)),Some ((viB,viA),(voB,voA))) ->
         let uis = List.map ~f:(fun x -> (x,BEFORE)) uiB @ List.map ~f:(fun x -> (x,AFTER)) uiA in
         let vos = List.map ~f:(fun x -> (x,BEFORE)) voB @ List.map ~f:(fun x -> (x,AFTER)) voA in
         let vToU = List.zip_exn (List.filter uis ~f:(fun (x,_) -> iequal v x)) (List.filter vos ~f:(fun (x,_) -> iequal u x))
                    |> List.map ~f:combineEdgeInfo in
         let uos = List.map ~f:(fun x -> (x,BEFORE)) uoB @ List.map ~f:(fun x -> (x,AFTER)) uoA in
         let vis = List.map ~f:(fun x -> (x,BEFORE)) viB @ List.map ~f:(fun x -> (x,AFTER)) viA in
         let uToV = List.zip_exn (List.filter vis ~f:(fun (x,_) -> iequal u x)) (List.filter uos ~f:(fun (x,_) ->  iequal v x))
                    |> List.map ~f:combineEdgeInfo in
         vToU @ uToV
    let edge_list c =
      let is = (Map.keys c.label) in
      is  |> List.concat_mapi ~f:(fun i u -> List.concat_map (List.drop is i ) ~f:(fun v -> edgesI c u v))

    let frozen_color = "#d1f2f2"
    let graphviz_of_cluster
        vlabel
        c
        (explicit)
        (scheme : (int*Vertex.t) -> int)
        (colors : int -> string): string =
    let tab = "    " in
    let n_color = "black" in
    let t_color = "purple" in
    let string_of_edge edge =
      let (i,ipos,o,opos,isTN) =
        match edge with
        | TN((i,ipos),(o,opos)) -> (i,ipos,o,opos,true)
        | NT((i,ipos),(o,opos)) -> (i,ipos,o,opos,false) in
      String.concat ~sep:""
                    [ tab
                    ; "\""
                    ; Int.to_string i
                    ; "\""
                    ; " -> "
                    ; "\""
                    ; Int.to_string o
                    ; "\""
                    ; " ["
                    ; "len = 2.0, "
                    ; "dir=both,"
                    ; " color = \"" ^ (if isTN then t_color else n_color) ^ ":" ^ (if isTN then n_color else t_color)  ^ ";0.5\","
                    ; if explicit then " headlabel = \" " ^ (if isTN then "N" else "T") ^ (match opos with BEFORE -> "1"|AFTER->"2") ^"\"," else ""
                    ; if explicit then " taillabel = \" " ^ (if isTN then "T" else "N") ^ (match ipos with BEFORE -> "1"|AFTER->"2") ^"\"," else ""
                    ; " arrowhead = \"" ^ (match opos with BEFORE -> "onormal" | AFTER ->  "normal") ^ "\","
                    ; " arrowtail = \"" ^ "none" ^ (match ipos with BEFORE -> "obox" | AFTER -> "box") ^ "\""
                    ; "]"
                    ] in
    let vs =  Map.keys c.label in
    let edges = vs
                |> List.concat_mapi ~f:(fun i x -> List.map (List.drop vs i) ~f:(fun y -> (x,y)))
                |> List.concat_map ~f:(fun (u,v) -> edgesI c u v)
                |> List.map ~f:(string_of_edge)
    in
    String.concat ~sep:"\n"
                  [ "digraph G {"
                  ; "overlap=false"
                  ; String.concat ~sep:"\n" edges
                  ; ""
                  ; String.concat ~sep:"\n" (List.map ~f:(fun v -> String.concat ~sep:""
                                                                                 [ tab
                                                                                 ; "\"" ^ Int.to_string v ^ "\""
                                                                                 ; " [shape=box, color = \""^frozen_color^"\", "
                                                                                 ; "label=\"" ^ vlabel v (at_exn c v) ^"\","
                                                                                 ; "penwidth=\"5\""
                                                                                 ; "]"])
                                                      (Set.to_list c.frozen))
                  ; String.concat ~sep:"\n" (List.map ~f:(fun v -> String.concat ~sep:"" [ tab
                                                                                         ; "\"" ^ Int.to_string v ^ "\""
                                                                                         ; " [color = "
                                                                                         ; colors (scheme (v, at_exn c v))
                                                                                         ; ",penwidth=\"5\""
                                                                                         ; ", label = \"" ^ vlabel v (at_exn c v) ^ "\""
                                                                                         ; "]" ])
                                                      (Set.to_list (c.unfrozen)))
                  ; "}"]

    let write_cluster ?image_size:(isize = None)
          ?node_color:(scheme =(fun _ -> 0))
          ?vlabel:(vlabel=(fun _-> Vertex.to_string))
          ?explicit:(explicit=false)
          (filename : string)
          (cluster) =
      let () =  Out_channel.write_all filename ~data:(graphviz_of_cluster vlabel  cluster explicit scheme Color.classic) in
      match isize with
      | None -> let _:int = Caml.Sys.command ("neato " ^ "\"" ^ filename ^ "\"" ^ " -Tpng -O") in ()
      | Some(xin,yin,dpi) ->
         let _:int =
           Caml.Sys.command ("neato \"" ^ filename ^ "\" -Gsize="^Int.to_string xin^","^Int.to_string yin ^ "\\! -Gdpi=" ^ Int.to_string dpi ^ " -Tpng -O") in
         let _:int =
           Caml.Sys.command ("convert \"" ^ filename^".png\" -gravity center -background white -extent "
                        ^ Int.to_string (xin*dpi)^"x"^Int.to_string(yin*dpi) ^ " \"" ^ filename^".png\"") in
         ()


  let write_tikz ?node_color:(scheme =(fun _ -> 0))
        ?vlabel:(vlabel=(fun _-> Vertex.to_string))
        (filename : string)
        (cluster : cluster) =
    let () = Out_channel.write_all filename ~data:(graphviz_of_cluster vlabel cluster false scheme Color.classic) in
    let colors = cluster.unfrozen |> Int.Set.map ~f:(fun i -> scheme (i,at_exn cluster i))  in
    let cMut = "vc" in
    let cFrozen = "vf" in
    let ()  = print_endline (colors |> Set.to_list |> List.map ~f:Int.to_string |> String.concat ~sep:" ") in
    let process_color color =
      let comp c1 c2 = ['0';'x';c1;c2] |> String.of_char_list |> Int.of_string |> Int.to_string in
      match String.to_list (String.strip ~drop:(function '"' -> true | c -> Char.is_whitespace c) color) with
      | [_;r1;r2;g1;g2;b1;b2] ->  comp r1 r2^"," ^ comp g1 g2^","^comp b1 b2
      | _ -> invalid_arg ("Color must be cRRGGBB not: "^ color) in
    let process_line l =
      match l with
      | "graph" :: _(*scale*) :: _(*width*) :: _(*height*) :: []-> ""
      | "node" :: name :: x :: y :: _(*width*) :: _(*height*) :: label :: _(*style*) :: _(*shape*) :: _(*color*) :: _(*fillcolor*) :: [] ->
         let vi = Int.of_string name in
         String.concat ~sep:" " [ "\\node[]"
                                ; "at"
                                ; "(" ^ Float.to_string (Float.of_string x *. 0.5) ^ ","
                                ; Float.to_string (Float.of_string y *. 0.5) ^")"
                                ; "(" ^  name ^ ")"
                                ; "["
                                ; "fill=" ^ (if Set.mem cluster.unfrozen vi then cMut^Int.to_string (scheme (vi, at_exn cluster vi)) else cFrozen) ^ ","
                                ; "label=" ^ label ^ ""
                                ; "]"
                                ; "{}"
                                ; ";\n"]
      | "edge" :: tail :: head :: _ ->
         let es = edgesI cluster  (Int.of_string head) (Int.of_string tail) in
         let posstr pos = match pos with BEFORE->",open"  |AFTER->"" in
         es |> List.map ~f:(function TN((_,ipos),(_,opos)) ->
                                      "\\path[{Square[transpose"^posstr ipos^"]}-{Latex[normal"^ posstr opos^"]}]"^" (" ^tail^") edge [] node {} ("^head^")"
                                   | NT((_,ipos),(_,opos)) ->
                                      "\\path[{Square[normal"^posstr ipos^"]}-{Latex[transpose"^ posstr opos^"]}]"^" (" ^tail^") edge [] node {} ("^head^")")
         |> String.concat ~sep:";\n"
         |> fun s -> s ^ ";\n"
      | ["stop"] -> ""
      | [] -> ""
      | s -> invalid_arg ("Unknown line type in plain file:" ^ String.concat ~sep:" " s) in
    let process_plain csv =
      Out_channel.with_file (filename^".tex") ~f:(fun file ->
          let () = Out_channel.output_string file "\\begin{tikzpicture}\n" in
          let () = Set.iter colors ~f:(fun i -> Out_channel.output_string file
                                                   ("\\definecolor{"^cMut^Int.to_string i^"}{RGB}{"^process_color(Color.choose Color.Category i)^"}\n")) in
          let () = Out_channel.output_string file ("\\definecolor{"^cFrozen^"}{RGB}{"^process_color(frozen_color)^"}\n") in
          let () = Out_channel.output_string file ("\\definecolor{"^"transpose"^"}{named}{"^"purple"^"}\n") in
          let () = Out_channel.output_string file ("\\definecolor{"^"normal"^"}{named}{"^"black"^"}\n") in
          let () = Csv.iter csv ~f:(fun line -> line |> process_line |> Out_channel.output_string file) in
          let () = Out_channel.output_string file "\\end{tikzpicture}" in ()) in
    match Caml.Sys.command ("neato " ^ "\"" ^ filename ^ "\"" ^ " -Tplain -O") with
    | 0 ->
       let plain = (filename ^ ".plain") |> In_channel.create |> Csv.of_channel ~separator:' ' in
       let () = process_plain plain in
       (match Caml.Sys.command ("rm \"" ^ filename ^ ".plain\"") with
        | 0 -> ()
        | _ -> raise (Sys_error "Failed to delete .plain file"))
    | _ -> raise (Sys_error "Failed to produce .plain file")


    type foundTrans =
      Found
    | Transpose
    | Missing
    let del l i = List.take l i @ List.drop l (i+1)
    let memberTrans l v =
      match List.find l ~f:(fun u -> i_of u = v) with
        None -> Missing
      | Some (N _) ->  Found
      | Some (T _) ->  Transpose
    let rec removeTrans l1 l2 =
      match l1 with
      | [] -> []
      | x::xs -> (match List.findi l2 ~f:(fun _ u -> i_of u = i_of x) with
                  | None -> x::(removeTrans xs l2)
                  | Some(i,_) -> removeTrans xs (del l2 i))
    let replaceTrans l v ~withL =
      List.concat_map l ~f:(fun w ->
          match w with
          | N i -> if i = v then withL else [w]
          | T i -> if i = v then List.rev_map withL ~f:transpose_index else [w])

    let mutate_coord c i =
      let v = at_exn c i in
      let ((inBefore,inAfter),(outBefore, outAfter)) =  Map.find_exn c.graph i in
      let vinv = match R.inv v with Some vinv -> vinv | None -> invalid_arg "Must be able to invert variables"  in

      let inp = List.fold inAfter ~init:(List.fold inBefore ~init:[vinv] ~f:(fun acc x -> (atT_exn c x):: acc) |> List.rev) ~f:(fun acc x ->  (atT_exn c x) :: acc) |> List.rev in
      let out =
        List.fold outAfter ~init:(List.fold outBefore ~init:([R.transpose vinv]) ~f:(fun acc x -> (atT_exn c x)::acc) |> List.rev) ~f:(fun acc x ->(atT_exn c x)::acc)|> List.rev in
      (inp,out)

    let mutateI_name c i =
      match at c i with
      | None -> (c, None)
      | Some v ->
         if Set.mem c.frozen i
         then (c,Some v)
         else
           let ((inBefore,inAfter),(outBefore, outAfter)) =  Map.find_exn c.graph i in
           let vinv = match R.inv v with Some vinv -> vinv | None -> invalid_arg "Must be able to invert variables"  in
           let vnew =
             R.add
               (List.fold inAfter ~init:(List.fold inBefore ~init:vinv ~f:(fun acc x -> R.mult (atT_exn c x) acc)) ~f:(fun acc x -> R.mult acc (atT_exn c x)))
               (List.fold outAfter ~init:(List.fold outBefore ~init:(R.transpose vinv)
                                            ~f:(fun acc x -> R.mult (atT_exn c x) acc)) ~f:(fun acc x -> R.mult acc (atT_exn c x))) in
        let vNbrNew:nbrsI =((outAfter, outBefore), (List.rev_map ~f:transpose_index inBefore, List.rev_map ~f:transpose_index inAfter)) in
        let vI = List.rev_append inBefore inAfter in
        let vO = List.rev_append outBefore outAfter in
        let flowEdges ((uIB,uIA),(uOB,uOA): nbrsI) =
          let ((uIBNew,uIANew),(uOBNew,uOANew)) =
          ((replaceTrans uIB i ~withL:(List.rev vI), replaceTrans uIA i ~withL:vI),
           (replaceTrans uOB i ~withL:(List.rev_map ~f:transpose_index vO), replaceTrans uOA i ~withL:(List.map ~f:transpose_index vO)))
          in
          (( (match memberTrans uOB i with Missing -> uIBNew | Transpose ->  (N i)::uIBNew | Found ->  (T i) :: uIBNew)
           , (match memberTrans uOA i with Missing -> uIANew | Transpose ->  (N i)::uIANew | Found ->  (T i)::uIANew))
          ,( (match memberTrans uIB i with Missing -> uOBNew | Transpose -> (T i)::uOBNew | Found ->  (N i)::uOBNew)
           , (match memberTrans uIA i with Missing -> uOANew | Transpose -> (T i)::uOANew | Found ->  (N i)::uOANew))) in

        let removeTwoCycles((uIB,uIA),(uOB,uOA)) = ((removeTrans uIB uOB, removeTrans uIA uOA), (removeTrans uOB uIB, removeTrans uOA uIA)) in
        let removeTwoCyclesCross((uIB,uIA),(uOB,uOA)) = ((removeTrans uIB uOA, removeTrans uIA uOB), (removeTrans uOB uIA, removeTrans uOA uIB)) in
        let newGraph : nbrsI Int.Map.t =
          Map.update c.graph i ~f:(function None -> invalid_arg "Shouldn't mutate at vertex not in graph"| Some _ -> vNbrNew)
          |> Map.map ~f:flowEdges
          |> Map.map ~f:removeTwoCycles
          |> Map.map ~f:removeTwoCyclesCross in
        ({ graph= newGraph
         ; frozen= c.frozen
         ; unfrozen= c.unfrozen
         ; label = Map.update c.label i ~f:(function None -> invalid_arg "already checked i in graph" | Some _ -> vnew)
         ; index =  Map.change c.index v ~f:(function None -> invalid_arg "mutating at v in index"
                                                      | Some l -> let lnew = List.filter l ~f:(Fn.non (Int.equal i))
                                                                  in (match lnew with
                                                                      | [] -> None
                                                                      | _ -> Some lnew))
                   |> (fun imap -> Map.change imap  vnew ~f:(function None -> Some [i] | Some l -> Some(i::l)))}, Some vnew)
    let mutateI c i = fst(mutateI_name c i)

    (* mutate_name c v == (c', w) where c' is the result of mutating in the direction of v, and w replaces v
     * since mutation is an involution: (uncurry mutate) (mutate_name c v) == c *)
    let mutate_name c v =
      match Map.find c.index v with
      | Some [i] ->
         (match mutateI_name c i with
          | (cnew,Some vnew) -> (cnew, vnew)
          | (_,None) -> invalid_arg "Cluster index updated incorrectly")
      | Some [] -> (c,v)
      | Some _ -> invalid_arg ("Vertex " ^ Vertex.to_string v ^ "occurs multiple times")
      | None -> (c,v)

(*
      if not(Set.mem c.unfrozen v)
      then (c,v)
      else
        let ((inBefore,inAfter),(outBefore, outAfter)) =  Map.find_exn c.graph v in
        let vinv = match R.inv v with Some vinv -> vinv | None -> invalid_arg "Must be able to invert variables"  in
        let vnew =
          R.add
            (List.fold inAfter ~init:(List.fold inBefore ~init:vinv ~f:(fun acc x -> R.mult x acc)) ~f:(fun acc x -> R.mult acc x))
            (List.fold outAfter ~init:(List.fold outBefore ~init:(R.transpose vinv) ~f:(fun acc x -> R.mult x acc)) ~f:(fun acc x -> R.mult acc x)) in
        let vNbrNew:nbrs =((outAfter, outBefore), (List.rev_map ~f:R.transpose inBefore, List.rev_map ~f:R.transpose inAfter)) in
        let vI = List.rev_append inBefore inAfter in
        let vO = List.rev_append outBefore outAfter in
        let fixV ((uIB,uIA),(uOB,uOA)) =
          (( (match memberTrans uOB v with Missing -> uIB | Transpose ->  vnew::uIB | Found ->  R.transpose vnew :: uIB)
           , (match memberTrans uOA v with Missing -> uIA | Transpose ->  vnew::uIA | Found ->  R.transpose vnew ::uIA))
           ,( (match memberTrans uIB v with Missing -> uOB | Transpose -> R.transpose vnew::uOB | Found ->  vnew::uOB)
           , (match memberTrans uIA v with Missing -> uOA | Transpose -> R.transpose vnew::uOA | Found ->  vnew::uOA))) in

        let flowEdges ((uIB,uIA),(uOB,uOA): nbrs) =
          ((replaceTrans uIB v ~withL:(List.rev vI), replaceTrans uIA v ~withL:vI),
           (replaceTrans uOB (R.transpose v) ~withL:(List.rev vO), replaceTrans uOA (R.transpose v) ~withL:vO))  in

        let removeTwoCycles((uIB,uIA),(uOB,uOA)) = ((removeTrans uIB uOB, removeTrans uIA uOA), (removeTrans uOB uIB, removeTrans uOA uIA)) in
        let newGraph : nbrs Vertex.Map.t =
          c.graph
          |> (fun graph -> Map.remove graph v)
          |> Map.add_exn ~key:vnew ~data:vNbrNew
          |> Map.map ~f:fixV
          |> Map.map ~f:flowEdges
          |> Map.map ~f:removeTwoCycles in
        let unfrozenNew = c.unfrozen |> (fun s -> Set.remove s v) |> (fun s -> Set.add s vnew) in
        ({graph= newGraph; frozen= c.frozen; unfrozen= unfrozenNew}, vnew)
 *)
    let mutate c v = mutate_name c v |> fst

    let mutate_multi c l = List.fold ~init:c ~f:mutate l
    let mutate_multi_name c path =
      List.fold path ~init:(c,[]) ~f:(fun (c,names) v -> let (cnew, vnew) = mutate_name c v in (cnew, vnew::names))
    let mutateI_multi c l= List.fold ~init:c ~f:mutateI l
    let mutateI_multi_name c path =
      List.fold path ~init:(c,[]) ~f:(fun (c,names) v -> match mutateI_name c v with | (_,None) -> (c,names) | (cnew,Some vnew)-> (cnew, vnew::names))



    type edge = int [@@deriving sexp,equal]
    let follow c es = (mutateI_multi c es, List.rev es)
    let n c = c.unfrozen |> Int.Set.to_list |> List.map ~f:(fun i -> (mutateI c i, i))
    let label c =
      c.unfrozen
      |> Set.to_list
      |> List.map ~f:(fun i -> at_exn c i)
      |> List.sort ~compare:Vertex.compare
      |> List.map ~f:Vertex.to_string
      |> String.concat ~sep:"\n"

    let id c =
      c.unfrozen
      |> Set.to_list
      |> List.map ~f:(fun i -> at_exn c i)
      |> List.concat_map ~f:(fun v -> [v;Vertex.transpose v])
      |> List.sort ~compare:Vertex.compare
      |> List.map ~f:Vertex.to_string
      |> String.concat ~sep:""

    let trianglesI c i =
      let ((iB,iA),(oB,oA)) = Map.find_exn c.graph i in
      ([List.rev iB; [T i]; List.rev_map ~f:transpose_index oB]
      ,[List.map ~f:transpose_index iA; [N i]; oA] )

    let triangles c i =
      let (t1,t2) = trianglesI c i in
      (t1 |> List.map ~f:(List.map ~f:(atT_exn c)) |> List.map ~f:(List.fold ~init:Vertex.one ~f:Vertex.mult)
      ,t2 |> List.map ~f:(List.map ~f:(atT_exn c)) |> List.map ~f:(List.fold ~init:Vertex.one ~f:Vertex.mult))

    let all_trianglesI c =
       c.unfrozen |> Set.to_list |> List.map ~f:(trianglesI c) |> List.concat_map ~f:(fun (t1,t2) -> [t1;t2])

    let all_triangles c =
       c.unfrozen |> Set.to_list |> List.map ~f:(triangles c) |> List.concat_map ~f:(fun (t1,t2) -> [t1;t2])

    module RMat = ListMatrix.Make(R)
    let convertTri f l =
      match l with
      | [r;start;l] -> f start ~l:l ~r:r
      | _ -> invalid_arg "Every triangle should have 3 coordinates"
    let rotClockTri l =
      match l with
      | [r;start;l] -> [start;l;r]
      | _ -> invalid_arg "Every triangle should have 3 coordinates"
    let rotCounterClockTri l =
      match l with
      | [r;start;l] -> [l;r;start]
      | _ -> invalid_arg "Every triangle should have 3 coordinates"

    let gLeft start ~l ~r =
      match R.inv l with
      | None -> invalid_arg (R.to_string l ^ " as left turn must be invertible")
      | Some linv -> R.[[mult linv (transpose r); one]
                       ;[neg (mult (transpose linv) (start)); zero]] |> RMat.of_rows
    let gRight start ~l ~r =
      match (R.inv r) with
      | (Some rinv) -> R.[[zero; neg(mult (rinv) (transpose start))]
                         ;[one; mult (transpose rinv) l]] |> RMat.of_rows |> RMat.neg
      | _ -> invalid_arg "Need to invert start or right"

    let gCross = R.[[zero; one];[neg one; zero]] |> RMat.of_rows
    let gLeftTri = convertTri gLeft
    let gRightTri = convertTri gRight


    let angle cycle =
      match cycle with
      | [x1;x2;x3] ->
         (match (R.inv x1, R.inv x3) with
          | (Some x1inv, Some x3inv) -> R.(mult (transpose x1inv) (mult x2 (transpose x3inv)))
          | _ -> invalid_arg "Need to be able to invert x1 and/or x3")
      | _ -> invalid_arg "Can only compute angle from orriented cycle of 3 edges"

    let badTriangle c = c |> all_triangles |> List.filter ~f:(fun tri -> not(R.equal (R.transpose (angle tri)) (angle tri)))
(*
                                                Map.filter_mapi c.graph ~f:(fun ~key:a ~data:((iB,iA),(oB,oA)) ->
                     match ((iB,iA),(oB,oA)) with
                     | (([iB],[iA]),([oB],[oA])) ->
                        (match R.inv (at_exn c a) with
                         | None -> invalid_arg "Cluster Variables must be invertible"
                         | Some ainv ->
                            let x = R.mult (R.mult (atT_exn c iB) ainv) (R.transpose (atT_exn c oB)) in
                            let y = R.mult (R.transpose (atT_exn c iA)) (R.mult (R.transpose ainv) (atT_exn c oA))
                            in (match (R.equal x (R.transpose x)) ,(R.equal y (R.transpose y)) with
                                | (true,true) -> None
                                | (false,true) -> Some([x])
                                | (true, false) -> Some([y])
                                | (false,false) -> Some([x;y])
                        ))
                     | _ -> invalid_arg "ONly can check 2 in 2 out ")
             |> Map.to_alist
 *)
    let checkTriangle c = c |> all_triangles |> List.for_all ~f:(fun tri -> R.equal(R.transpose (angle tri)) (angle tri))




    let qA1_double mA mB mC =
      of_nbrlist ~graph:[ (1,(([T 2],[T 2]),([N 3],[N 3])))
                        ; (2,(([T 3],[T 3]),([N 1],[N 1])))
                        ; (3,(([T 1],[T 1]),([N 2],[N 2])))]
        ~frozen:(Int.Set.of_list [])
        ~label:[1,mA; 2, mB; 3,mC]

    let qA1_affine f1 f2 mA mB = of_edgelist
        ~graph:[ NT((1,AFTER), (2,AFTER)); NT((2,AFTER),(-2,AFTER)); NT((-2,AFTER),(1,AFTER))
               ; NT((1,BEFORE),(2,BEFORE)); NT((2,BEFORE),(-1,BEFORE)); NT((-1,BEFORE),(1,BEFORE))]
        ~frozen:(Int.Set.of_list [-1;-2])
        ~label:[-1,f1;-2,f2; 1,mA; 2, mB]

    include Comparable.Make(T)
  end
