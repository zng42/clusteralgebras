open Core

module Make(R: Algebra.SemiRing.S) : Vertex.S with type t = R.t =
  struct
    include R
    let weight _ = 1
    let canExchange v ~inp:_ ~out:_ =
      match inv v with
      | Some _ -> true
      | None -> false

    let mutate v ~inp ~out =
      let rhs =  add (Map.fold inp ~init:R.one ~f:(fun ~key:r ~data:n acc -> mult (pow r (Int.abs n)) acc))
                   (Map.fold out ~init:R.one ~f:(fun ~key:r ~data:n acc -> mult (pow r n) acc)) in
      match inv v with
      | Some vinv -> R.mult vinv rhs
      | None -> invalid_arg ("Must be able to invert " ^ R.to_string v )
    let id = to_string

  end
