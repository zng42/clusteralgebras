open Core

(* Convention is that inner boundary labeled clockwise and outer boundary labeled ccw. So:
 * Inner(i,j) goes clockwise from i to j
 * Outer(i,j) goes counterclockwise from i to j *)
type arc =
  Inner of int * int
  | Outer of int * int
  | Cross of int * int * int  [@@deriving compare,sexp]

let cyc_between ~l ~r i =
  if l < r
  then l < i  && i < r
  else i > l || i < r

module Arc =
  struct
  type t =arc

  let sexp_of_t = sexp_of_arc
  let t_of_sexp = arc_of_sexp
  let compare = compare_arc
  let int_to_digit i =
    if Int.between ~low:0 ~high:9 i then Char.of_string(Int.to_string i)
    else if Int.between ~low:10 ~high:35 i then Char.of_int_exn (Char.to_int 'a' + (i-10))
    else invalid_arg ("Can't process int larger than 35 "^ Int.to_string i)

  let digit_to_int c =
    if Char.is_digit c then Int.of_string (Char.to_string c)
    else if Char.is_lowercase c then Char.to_int c - Char.to_int 'a' + 10
    else invalid_arg ("Only mapped 0-9, a-z to int" ^ Char.to_string c)

  let to_string v =
    match v with
    | Inner (i,j) -> "i" ^ String.of_char_list (List.map ~f:int_to_digit [i;j])
    | Outer (i,j) -> "o" ^ String.of_char_list (List.map ~f:int_to_digit [i;j])
    | Cross (i,j,wind) -> "c" ^ String.of_char_list (List.map ~f:int_to_digit [i;j]) ^ "w" ^ Int.to_string(wind)
  let id (v : t) = to_string v
  let pp ppf v = Format.fprintf ppf "%s" (to_string v)
  let of_string s =
    match String.to_list s with
    | ['i';di;dj] -> Inner(digit_to_int di, digit_to_int dj)
    | ['o';di;dj] -> Outer(digit_to_int di, digit_to_int dj)
    | 'c'::di::dO::'w'::dwind -> Cross(digit_to_int di, digit_to_int dO, Int.of_string (String.of_char_list dwind))
    | _ -> invalid_arg ("String doesn't describe an arc in an annulus " ^ s)

  let hash v = String.hash (to_string v)

  let winding v =
    match v with
    | Cross(_,_,w) -> w
    | Inner(_,_) -> 0
    | Outer(_,_) -> 0

  let surrounds v w =
    match (v,w) with
    | (Inner(i,j),Inner(it,jt)) -> (cyc_between ~l:i ~r:j it || i = it) && (cyc_between ~l:i ~r:j jt || j = jt)
    | (Outer(o,p),Outer(ot,pt)) -> (cyc_between ~l:o ~r:p ot || o = ot) && (cyc_between ~l:o ~r:p pt || p = pt)
    | _ -> false

  let rotateInner p v =
    match v with
    | Inner(i,j) -> Inner((i+1)mod p, (j+1)mod p)
    | Outer(_,_) -> v
    | Cross(o,i,w) -> if i < p-1 then Cross(o,i+1,w) else Cross(o,0,w+1)

  let rotateOuter q v =
    match v with
    | Inner(_,_) -> v
    | Outer(o,p) -> Outer((o+1)mod q, (p+1) mod q)
    | Cross(o,i,w) -> if o < q-1 then Cross(o+1,i,w) else Cross(0,i,w+1)
end

module Vert : sig include Vertex.S with type t = arc end =
  struct

    include Arc
    module Comp = Comparable.Make(Arc)

    let weight _ = 1
    let canExchange v ~inp ~out =
      match v with
      | Inner (_,_) -> (2= Comp.Map.length inp) && (2 = Comp.Map.length out)
      | Outer(_,_) ->  (2= Comp.Map.length inp) && (2 = Comp.Map.length out)
      | Cross(_,_,_) -> true


    let flatten_map map =
      List.concat_map (Map.to_alist map) ~f:(fun (x,i) -> List.init (Int.abs i) ~f:(fun _ -> x))

    let mutate v ~(inp: int Comp.Map.t) ~(out: int Comp.Map.t) =
      let nbrs = List.sort ~compare:compare_arc (flatten_map out @ flatten_map inp) in
      match (v,nbrs) with
      | (Outer(o,p), [Outer(p1,p2);Outer(p3,p4);Outer(p5,p6);Outer(p7,p8)]) ->
         let (o,p) = if o < p then (o,p) else (p,o) in
         let (o1,o2,o3,o4) =
           match List.sort ~compare:Int.compare [p1;p2;p3;p4;p5;p6;p7;p8] with
           | [o1;_;o2;_;o3;_;o4;_] -> (o1,o2,o3,o4)
           | _ -> invalid_arg "List has 8 elements" in
         let (oNew,pNew) = if o = o1 && p= o3 then (o2,o4) else (o1,o3) in
         if List.count nbrs ~f:(Arc.surrounds (Outer(oNew,pNew))) = 2
         then Outer(oNew,pNew)
         else Outer(pNew,oNew)

      | (Cross(_(*o*),_(*i*),_(*w*)), [Outer(_,_) as out1;Outer(_,_);Cross(o0,_,_);Cross(o1,_,_)]) ->
         if Arc.surrounds (Outer(o0,o1)) out1 then Outer(o0,o1) else Outer(o1,o0)

      | (Outer(o,_(*p*)), [Outer(a,b);Outer(c,d);Cross(o0,i,w0);Cross(_(*o1*),_,w1)]) ->
         let oc =
           match List.find_all_dups ~compare:Int.compare [a;b;c;d] with
           | [oc] -> oc
           | [oA;oB] -> if o = oA then oB else oA
           | _ -> invalid_arg ("2 outer edges must overlap " ^ (Arc.to_string (Outer(a,b))) ^ " " ^ Arc.to_string((Outer(c,d))) ) in
         let wO = if o = o0 then w0 else w1 in
         if oc > o then Cross(oc,i,wO) else Cross(oc,i,wO+1)

      | (Inner(i,j), [Inner(j1,j2);Inner(j3,j4);Inner(j5,j6);Inner(j7,j8)]) ->
         let (i,j) = if i < j then (i,j) else (j,i) in
         let (i1,i2,i3,i4) =
           match List.sort ~compare:Int.compare [j1;j2;j3;j4;j5;j6;j7;j8] with
           | [j1;_;j2;_;j3;_;j4;_] -> (j1,j2,j3,j4)
           | _ -> invalid_arg "List has 8 elements" in
         let (iNew,jNew) = if i = i1 && j= i3 then (i2,i4) else (i1,i3) in
         if List.count nbrs ~f:(Arc.surrounds (Inner(iNew,jNew))) = 2
         then Inner(iNew,jNew)
         else Outer(jNew,iNew)

      | (Cross(_(*o*),_(*i*),_(*w*)), [Inner(_,_) as inn1;Inner(_,_);Cross(_,i0,_);Cross(_,i1,_)]) ->
         if Arc.surrounds (Inner(i0,i1)) inn1 then Inner(i0,i1) else Inner(i1,i0)

      | (Inner(i,_(*j*)), [Inner(a,b);Inner(c,d);Cross(o,i0,w0);Cross(_,_(*i1*),w1)]) ->
         let ic =
           match List.find_all_dups ~compare:Int.compare [a;b;c;d] with
           | [ic] -> ic
           | [iA;iB] -> if i = iA then iB else iA
           | _ -> invalid_arg "2 inner edges overlap exactly once" in
         let wO = if i = i0 then w0 else w1 in
         if ic > i then Cross(o,ic,wO) else Cross(o,ic,wO+1)

      | (Cross(o,i,w), [Inner(i1,j1);Outer(o2,p2);Cross(o3,_(*i3*),w3);Cross(_(*o4*),i4,w4)]) ->
         let oNew = if o = o2 then p2 else o2 in
         let iNew = if i = i1 then j1 else i1 in
         let wNew =
           (match (Ordering.of_int (Int.compare o2 p2), Ordering.of_int(Int.compare i1 j1)) with
            | (Less,Less) -> w

            | (Less, Greater) ->  if o = o3 then w3 else w4
            | (Less, Equal) -> if o = o3 then w3 else w4

            | (Greater, Less) -> if i = i4 then w4 else w3
            | (Equal, Less) -> if i = i4 then w4 else w3

            | (Greater, Greater) -> if w < w3 then w+2 else w-2
            | (Equal, Greater) -> if w < w3 then w+2 else w-2
            | (Greater, Equal) -> if w < w3 then w+2 else w-2
            | (Equal, Equal) -> if w < w3 then w+2 else w-2
           ) in
         Cross(oNew,iNew,wNew)
      | _ -> invalid_arg ("Bad mutation at " ^ to_string v ^" surrounded by " ^ String.concat ~sep:" " (List.map ~f:to_string nbrs))

    include Comp
  end

module Clust =  MapCluster.Make(Vert)

let frozen p q = Vert.Set.of_list (List.init p ~f:(fun i -> Inner(i,(i+1)mod p))
                               @ List.init q ~f:(fun o -> Outer(o,(o+1) mod q)))

let qA_cycle p q =
  Clust.of_edgelist
    ~frozen:(frozen p q)
    ~graph:(List.concat_no_order (List.init p ~f:(function i -> [ (Cross(0,i,0), Arc.rotateInner p (Cross(0,i,0)))
                                                             ; (Arc.rotateInner p (Cross(0,i,0)), Inner(i, (i+1) mod p))
                                                             ; (Inner(i, (i+1) mod p), Cross(0,i,0))]))
            @ List.concat_no_order (List.init q ~f:(fun o -> [ (Cross(o,0,0), Arc.rotateOuter q (Cross(o,0,0)))
                                                          ; (Arc.rotateOuter q (Cross(o,0,0)), Outer(o, (o+1) mod q))
                                                          ; (Outer(o,(o+1) mod q), Cross(o,0,0))])) )
let max_wind c =
  Set.fold ~f:(fun acc v -> Int.max acc (Int.abs (Arc.winding v))) ~init:0 (Clust.unfrozen c)
  * if Set.exists (Clust.unfrozen c) ~f:(fun v -> Arc.winding v < 0) then -1 else 1
(*
let qA_tails p q =
  Clust.of_edgelist
    ~frozen:(frozen p q)
    ~graph:([(Cross(0,0,0),Cross(0,0,1)); (Cross(0,0,1),Outer(0,0,true)); (Outer(0,0,true),Cross(0,0,0))
             ;(Cross(0,0,0),Cross(0,0,1)); (Cross(0,0,1),Inner(0,0,true)); (Inner(0,0,true),Cross(0,0,0))]
            @ List.concat(List.init p (fun i -> [Inner()) )
 *)

module CSearch = ClusterSearch.Make(Clust)
module XVert = CSearch.XVert
