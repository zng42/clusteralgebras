open Core
module Make(BaseNode : Search.NodeS) =
  struct
    module Node =
      struct
        module T =
          struct
            type t = BaseNode.t * (BaseNode.edge list list) [@@deriving sexp]
            let compare (n1,_) (n2,_) = BaseNode.compare n1 n2
          end
        include T
        include Comparable.Make(T)

        let label (node, _(*groups*)) = BaseNode.label node
        let id (node, _(*groups*)) =
          BaseNode.id node
        (* ^ (String.concat ~sep:";" (List.map ~f:(fun g -> g |> List.map ~f:Cluster.Vertex.to_string |> String.concat ~sep:",") groups)) *)

        type edge = BaseNode.edge list [@@deriving sexp,equal]

        let replace_i l i a  = (List.take l i) @ a::(List.drop l (i+1))
        let safe_follow (node,groups) group =
          match List.findi groups ~f:(fun _ -> List.equal BaseNode.equal_edge group) with
          |Some (i,_) -> let (newNode,newGroup) = BaseNode.follow node group in ((newNode,replace_i groups i newGroup), newGroup)
          |None -> invalid_arg "Can only follow path in specified groups"

        let follow (node,groups) egs =
          List.fold egs ~init:((node,groups),[])
            ~f:(fun ((curC,curGroups),l) (group) -> let (newc,newgroup) = safe_follow (curC,curGroups) group in (newc,newgroup::l))


        let n (node,groups) =
          List.mapi groups ~f:(fun i group -> let (newNode,newgroup) = BaseNode.follow node group in ((newNode, replace_i groups i newgroup),newgroup))
      end
    include SearchBase.Make(Node)
  end
