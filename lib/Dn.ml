open Core

(* Convention is that inner boundary labeled clockwise and outer boundary labeled ccw. So:
 * Cross(i,tag) goes from i to the puncture. tag = true if arc is tagged
 * Boundary(i,j) goes counterclockwise from i to j *)
type arc =
  | Boundary of int * int
  | Puncture of int * bool  [@@deriving compare,sexp]

let pmod i n =
  let out = i mod n in
  if out < 0 then out + n else out

let cyc_between ~l ~r i =
  if l < r
  then l < i  && i < r
  else i > l || i < r

module Arc =
  struct
  type t =arc

  let sexp_of_t = sexp_of_arc
  let t_of_sexp = arc_of_sexp
  let compare = compare_arc
  let int_to_digit i =
    if Int.between ~low:0 ~high:9 i then Char.of_string(Int.to_string i)
    else if Int.between ~low:10 ~high:35 i then Char.of_int_exn (Char.to_int 'a' + (i-10))
    else invalid_arg ("Can't process int larger than 35 "^ Int.to_string i)

  let digit_to_int c =
    if Char.is_digit c then Int.of_string (Char.to_string c)
    else if Char.is_lowercase c then Char.to_int c - Char.to_int 'a' + 10
    else invalid_arg ("Only mapped 0-9, a-z to int" ^ Char.to_string c)

  let to_string v =
    match v with
    | Boundary (i,j) -> "b" ^ String.of_char_list (List.map ~f:int_to_digit [i;j])
    | Puncture (i,false) -> "u" ^ String.of_char (int_to_digit i)
    | Puncture (i,true) -> "t" ^ String.of_char (int_to_digit i)
  let pp ppf v = Format.fprintf ppf "%s" (to_string v)
  let id (v : t) = to_string v
  let of_string s =
    match String.to_list s with
    | ['b';di;dj] -> Boundary(digit_to_int di, digit_to_int dj)
    | ['u';di] -> Puncture(digit_to_int di, false)
    | ['t';di] -> Puncture(digit_to_int di,true)
    | _ -> invalid_arg ("String doesn't describe an arc in a punctured n gon " ^ s)

  let hash v = String.hash (to_string v)

  let surrounds v w =
    match (v,w) with
    | (Boundary(i,j),Boundary(it,jt)) -> (cyc_between ~l:i ~r:j it || i = it) && (cyc_between ~l:i ~r:j jt || j = jt)
    | _ -> false

  let rotate p v =
    match v with
    | Boundary(i,j) -> Boundary((i+1)mod p, (j+1)mod p)
    | Puncture(i,tag) -> Puncture((i+1) mod p, tag)
end

module Vert : sig include Vertex.S with type t = arc end =
  struct

    include Arc
    module Comp = Comparable.Make(Arc)

    let weight _ = 1
    let canExchange _ ~inp:_ ~out:_ = true

    let flatten_map map =
      List.concat_map (Map.to_alist map) ~f:(fun (x,i) -> List.init (Int.abs i) ~f:(fun _ -> x))

    let mutate v ~(inp: int Comp.Map.t) ~(out: int Comp.Map.t) =
      let nbrs = List.sort ~compare:compare_arc (flatten_map out @ flatten_map inp) in
      match (v,nbrs) with
      | (Boundary(o,p), [Boundary(p1,p2);Boundary(p3,p4);Boundary(p5,p6);Boundary(p7,p8)]) ->
         let (o,p) = if o < p then (o,p) else (p,o) in
         let (o1,o2,o3,o4) =
           match List.sort ~compare:Int.compare [p1;p2;p3;p4;p5;p6;p7;p8] with
           | [o1;_;o2;_;o3;_;o4;_] -> (o1,o2,o3,o4)
           | _ -> invalid_arg "List has 8 elements" in
         let (oNew,pNew) = if o = o1 && p= o3 then (o2,o4) else (o1,o3) in
         if List.count nbrs ~f:(Arc.surrounds (Boundary(oNew,pNew))) = 2
         then Boundary(oNew,pNew)
         else Boundary(pNew,oNew)

      | Boundary(o,p), [Boundary(p1,p2);Boundary(p3,p4);Boundary(p5,p6); Puncture(p7,false);Puncture(_,true)] ->
         let (o,p) = if o < p then (o,p) else (p,o) in
         let (o1,o2,o3,o4) =
           match List.sort ~compare:Int.compare [p1;p2;p3;p4;p5;p6;p7;p7] with
           | [o1;_;o2;_;o3;_;o4;_] -> (o1,o2,o3,o4)
           | _ -> invalid_arg "List has 8 elements" in
         let (oNew,pNew) = if o = o1 && p= o3 then (o2,o4) else (o1,o3) in
         if List.count nbrs ~f:(Arc.surrounds (Boundary(oNew,pNew))) = 2
         then Boundary(oNew,pNew)
         else Boundary(pNew,oNew)

      | (Puncture(_(*o*),_), [Boundary(_,_) as out1;Boundary(_,_);Puncture(o0,_);Puncture(o1,_)]) ->
         if Arc.surrounds (Boundary(o0,o1)) out1 then Boundary(o0,o1) else Boundary(o1,o0)

      | (Boundary(_(*o*),_(*p*)), [Boundary(a,b);Boundary(c,d);Puncture(_(*o0*),tag0);Puncture(_(*o1*),_(*tag1*))]) ->
         let oc =
           match List.find_all_dups ~compare:Int.compare [a;b;c;d] with
           | [oc] -> oc
           | _ -> invalid_arg ("2 outer edges must overlap " ^ (Arc.to_string (Boundary(a,b))) ^ " " ^ Arc.to_string((Boundary(c,d))) ) in
          Puncture(oc,tag0)
      | (Puncture(o,tag), [Boundary(a,b);Boundary(_,_)]) ->
         if a = o then Puncture(b,not tag) else Puncture(a,not tag)
      | _ -> invalid_arg ("Bad mutation at " ^ to_string v ^" surrounded by " ^ String.concat ~sep:" " (List.map ~f:to_string nbrs))

    include Comp
  end

module Clust =
  struct
    include MapCluster.Make(Vert)

    let squish_a ~at ~tag n edge =
      let (i,j) = match List.sort ~compare:Int.compare (Int.Set.to_list edge) with
        | [i;j] -> (i,j)
        | _ -> invalid_arg "Can only squish Gr(2,n) (type A_{n+3}) to type D_n" in
      match (Ordering.of_int(Int.compare i at), Ordering.of_int(Int.compare j at)) with
      | (Less, Less) -> Boundary(pmod (i-1) n, pmod(j-1) n)
      | (Less, Equal) -> Puncture(pmod (i-1) n, tag)
      | (Equal,Less) -> Puncture(pmod (i-1) n, tag)
      | (Greater,Equal) -> Puncture(pmod (i-2) n, tag)
      | (Equal,Greater) -> Puncture(pmod (i-2) n, tag)
      | (Less, Greater) -> if Int.equal (pmod (j-2) n) i then Puncture(pmod (i-1) n, not tag) else Boundary(pmod (j-3) n, pmod (i-1) n)
      | (Greater, Less) -> if Int.equal (pmod (i-2) n) j then Puncture(pmod (j-1) n, not tag) else Boundary(pmod (i-3) n, pmod (j-1) n)
      | (Greater,Greater) -> Boundary(pmod(i-3) n, pmod (j-3) n)
      | (Equal,Equal) -> invalid_arg (Int.to_string i ^ " can't be equal to "^ Int.to_string j ^ "in a plucker vertex")


    let of_pcluster ~at ~tag n c =
      let conv e = squish_a ~at:at ~tag:tag n e in
      of_edgelist ~frozen:(Vert.Set.map ~f:conv (Plucker.Clust.frozen c))
        ~graph:(List.map ~f:(fun (e1,e2) -> (conv e1, conv e2)) (Plucker.Clust.edgelist c))

  end

let frozen n = Vert.Set.of_list (List.init n ~f:(fun o -> Boundary(o,(o+1) mod n)) )

let qD_cycle n =
  Clust.of_edgelist
    ~frozen:(frozen n)
    ~graph:(List.concat_no_order (List.init n ~f:(function i -> [ (Puncture(i,false), Arc.rotate n (Puncture(i,false)))
                                                                ; (Arc.rotate n (Puncture(i,false)), Boundary(i, (i+1) mod n))
                                                                ; (Boundary(i, (i+1) mod n), Puncture(i,false))])))

(*
let qA_tails p q =
  Clust.of_edgelist
    ~frozen:(frozen p q)
    ~graph:([(Puncture(0,0,0),Puncture(0,0,1)); (Puncture(0,0,1),Boundary(0,0,true)); (Boundary(0,0,true),Puncture(0,0,0))
             ;(Puncture(0,0,0),Puncture(0,0,1)); (Puncture(0,0,1),Inner(0,0,true)); (Inner(0,0,true),Puncture(0,0,0))]
            @ List.concat(List.init p (fun i -> [Inner()) )
 *)

include CAlg.Extend(struct module Vert = Vert module Clust = Clust end)
