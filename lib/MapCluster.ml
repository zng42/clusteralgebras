open Core
module Heap = Pairing_heap

module Make (V : Vertex.S) : (Cluster.S with module Vertex = V) =
struct
  module Vertex = V
  type nbrs = int Vertex.Map.t [@@deriving compare, sexp]

  type graph = nbrs Vertex.Map.t [@@deriving compare, sexp]
  type cluster =  {graph: graph ; frozen : Vertex.Set.t; unfrozen : Vertex.Set.t} [@@deriving compare, sexp]


  (* Output *)
  let frozen cluster = cluster.frozen
  let unfrozen cluster = cluster.unfrozen
  let verts ?frozen:(include_frozen = true) cluster =
    if include_frozen then Set.union cluster.frozen cluster.unfrozen else cluster.unfrozen

  let  name cluster =
    "gr" (* TODO: Change to name valid for all clusters *)
    ^ (String.concat ~sep:"_"
                     (List.map ~f:Vertex.id (Set.to_list (unfrozen cluster))))

  let label cluster =
    String.concat ~sep:"\n" (List.map ~f:Vertex.to_string (Set.to_list (unfrozen cluster)))
  let id cluster =
    String.concat ~sep:"" (List.map ~f:Vertex.id (Set.to_list (unfrozen cluster)))

  module T = struct
    type t = cluster
    let sexp_of_t = sexp_of_cluster
    let t_of_sexp = cluster_of_sexp
    let compare = compare_cluster
  end

  include T
  module Cluster =Comparable.Make(T)

  (* INPUT *)
  let of_nbrlist ~(graph) ~(frozen) : cluster =
    { graph =  Vertex.Map.of_alist_exn (List.map graph ~f:(fun (v, (inbr, onbr)) ->
                                                   (v, Vertex.Map.of_alist_fold ~init:0 ~f:(+)
                                                                                (List.append (List.map onbr ~f:(fun w -> (w,1)))
                                                                                             (List.map inbr ~f:(fun w -> (w,-1)))))))
    ; frozen = frozen
    ; unfrozen = Vertex.Set.of_list(List.filter_map graph ~f:(fun (key,_) -> if not (Set.mem frozen key) then Some key else None))}

  let of_list ~graph ~frozen : cluster =
    { graph = Vertex.Map.of_alist_exn graph
    ; frozen = frozen
    ; unfrozen = Vertex.Set.of_list(List.filter_map graph ~f:(fun (key,_) -> if not (Set.mem frozen key) then Some key else None))}

  let of_quiver ~(quiver:graph)  ~(frozen : Vertex.Set.t) : cluster =
    { graph = quiver
    ; frozen = frozen
    ; unfrozen = Vertex.Set.of_list( Map.keys (Map.filteri quiver ~f:(fun ~key ~data:_ -> not (Set.mem frozen key))))}

  let of_edgelist ~graph ~frozen : cluster =
    let nbrlist = Vertex.Map.of_alist_fold (List.concat_map graph ~f:(fun (i,j) -> [(i,(j,1));(j,(i,-1))]))
                                           ~init:Vertex.Map.empty
                                           ~f:(fun nbr -> fun (j, dir) -> Map.change nbr j ~f:(function None ->  Some dir | Some old -> Some(dir + old)))
    in
    of_quiver ~quiver:nbrlist ~frozen:frozen

  let freeze c l =
    { graph = c.graph
    ; frozen = List.fold ~init:c.frozen ~f:Vertex.Set.add l
    ; unfrozen = List.fold ~init:c.unfrozen ~f:Vertex.Set.remove l}

  let unfreeze c l =
    { graph = c.graph
    ; frozen = List.fold ~init:c.frozen ~f:Vertex.Set.remove l
    ; unfrozen = List.fold ~init:c.unfrozen ~f:Vertex.Set.add l}

  let edgelist c =
    Vertex.Map.fold
      c.graph
      ~init:[]
      ~f:(fun ~key:v ~data:nbr acc -> Vertex.Map.fold
                                        nbr
                                        ~init:acc
                                        ~f:(fun ~key:w ~data:d acc -> if d > 0 then List.append (List.init (Int.abs d) ~f:(fun _ -> (v,w)) ) acc else acc))

  let rev c =
    { graph  = Map.map (c.graph) ~f:(Map.map ~f:(fun dir -> -dir))
    ; frozen = c.frozen
    ; unfrozen = c.unfrozen}

  let removeFrozen c =
    of_list ~frozen:Vertex.Set.empty
      ~graph:(List.filter_map (Map.to_alist c.graph)
                ~f:(fun (key,data) -> if Set.mem c.unfrozen key
                                   then Some(key, Map.filteri data ~f:(fun ~key ~data:_ -> Set.mem c.unfrozen key))
                                   else None))

  let memUnfrozen c v = Vertex.Set.mem (c.unfrozen) v
  let memFrozen c v = Vertex.Set.mem (c.frozen) v
  let mem c v = memUnfrozen c v || memFrozen c v

  let exists ?frozen:(frozen = true) (c:cluster) ~f = Set.exists (verts ~frozen:frozen c) ~f:f
  let for_all ?frozen:(frozen = true) (c:cluster) ~f = Set.for_all (verts ~frozen:frozen c) ~f:f
  let count ?frozen:(frozen = true) (c:cluster) ~f = Set.count (verts ~frozen:frozen c) ~f:f
  let find ?frozen:(frozen = true) (c:cluster) ~f = Set.find (verts ~frozen:frozen c) ~f:f
  let find_exn ?frozen:(frozen = true) (c:cluster) ~f = Set.find_exn (verts ~frozen:frozen c) ~f:f
  let filter c ~f =
    {frozen = Vertex.Set.filter c.frozen ~f:f
    ;unfrozen= Vertex.Set.filter c.unfrozen ~f:f
    ;graph = Vertex.Map.filter_mapi c.graph ~f:(fun ~key ~data -> if f key then Some(Vertex.Map.filter_keys data ~f:f) else None)}

  let exists_edge ?frozen:(frozen = true) (c:cluster) ~f =
    Map.existsi c.graph ~f:(fun ~key:v ~data -> (frozen || memUnfrozen c v)
                                             && Map.existsi data ~f:(fun ~key:w ~data:weight -> (frozen || memUnfrozen c w) && f(v,w,weight)))

  let for_all_edge ?frozen:(frozen = true) (c:cluster) ~f =
    Map.for_alli c.graph ~f:(fun ~key:v ~data -> (not frozen &&  memFrozen c v)
                                              || Map.for_alli data ~f:(fun ~key:w ~data:weight -> (not frozen && memFrozen c w) || f(v,w,weight)))

  let count_edge ?frozen:(frozen = true) (c:cluster) ~f =
    Map.fold ~init:0 c.graph ~f:(fun ~key:v ~data acc -> if (frozen || memUnfrozen c v)
                                                         then acc+Map.counti data ~f:(fun ~key:w ~data:weight -> (frozen || memUnfrozen c w) && f(v,w,weight))
                                                         else acc)
  let find_edge ?frozen:(frozen=true) (c:cluster) ~f =
    Map.fold ~init:None c.graph ~f:(fun ~key:v ~data acc ->
          match acc with
          | Some ans -> Some ans
          | None ->
             if frozen || memUnfrozen c v
             then List.find_map (Map.to_alist data) ~f:(fun (w,weight) -> if (frozen || memUnfrozen c w) && f(v,w,weight) then Some (v,w,weight) else None)
             else None)

  let find_edge_exn ?frozen:(frozen=true) (c:cluster) ~f =
    match find_edge ~frozen:frozen c ~f:f with
    | Some ans -> ans
    | None -> invalid_arg "There is no edge in the cluster satisfying f"

  (* Note: By checking f (v,w, d) and f(w,v,-d) we ensure that both copies of each edge are kept/removed together.*)
  let filter_edge c ~f =
    {frozen = c.frozen
    ;unfrozen= c.unfrozen
    ;graph = Vertex.Map.mapi c.graph ~f:(fun ~key:v ~data:nbr -> Vertex.Map.filteri nbr ~f:(fun ~key:w ~data:d -> f(v,w,d) || f(w,v,-d)))}




  let nbr ?frozen:(frozen = true) c (v : Vertex.t) =
    match Map.find c.graph v with
    | None -> Vertex.Map.empty
    | Some nbrs -> Map.filter_keys nbrs ~f:(fun key -> (frozen || Set.mem c.unfrozen key))

  let oNbr ?frozen:(frozen = true) c v =
    match Map.find c.graph v with
    | None -> Vertex.Map.empty
    | Some nbrs -> Map.filteri nbrs ~f:(fun ~key ~data:d -> d > 0 && (frozen || Set.mem c.unfrozen key))

  let iNbr ?frozen:(frozen = true) c v =
    match Map.find c.graph v with
    | None -> Vertex.Map.empty
    | Some nbrs -> Map.filteri nbrs ~f:(fun ~key ~data:d -> d < 0 && (frozen || Set.mem c.unfrozen key))

  let nbrSet ?frozen:(frozen = true) c v = Vertex.Set.of_list (Map.keys (nbr ~frozen:frozen c v))
  let oNbrSet ?frozen:(frozen = true) c v = Vertex.Set.of_list (Map.keys (oNbr ~frozen:frozen c v))
  let iNbrSet ?frozen:(frozen = true) c v = Vertex.Set.of_list (Map.keys (iNbr ~frozen:frozen c v))

  let nbrList ?frozen:(frozen = true) c v = List.concat_map (Map.to_alist (nbr ~frozen:frozen c v)) ~f:(fun (w,d) -> List.init (Int.abs d) ~f:(fun _ -> w))
  let oNbrList ?frozen:(frozen = true) c v = List.concat_map (Map.to_alist (oNbr ~frozen:frozen c v)) ~f:(fun (w,d) -> List.init (Int.abs d) ~f:(fun _ -> w))
  let iNbrList ?frozen:(frozen = true) c v = List.concat_map (Map.to_alist (iNbr ~frozen:frozen c v)) ~f:(fun (w,d) -> List.init (Int.abs d) ~f:(fun _ -> w))

  let dim c = Set.length(c.unfrozen)

  let edge_weight c ~inp ~out =
    match Map.find c.graph inp with
    | None -> 0
    | Some nbr ->
       (match Map.find nbr out with
        | None -> 0
        | Some d -> d)

  let fEdgeAbove ?frozen:(frozen = true) c ~w =
    if frozen
    then Map.exists c.graph ~f:(fun nbr -> Map.exists nbr ~f:(fun i -> (Int.abs i) > w))
    else Set.exists c.unfrozen ~f:(fun v -> Map.exists (nbr ~frozen:false c v) ~f:(fun i -> (Int.abs i) > w))

  let oDegree ?frozen:(frozen = true) (c) (v: Vertex.t) =
    Map.fold (oNbr ~frozen:frozen c v) ~init:0 ~f:(fun ~key:_ ~data  -> fun acc -> acc + data)

  let iDegree ?frozen:(frozen = true) (c) (v: Vertex.t) =
    Map.fold (iNbr ~frozen:frozen c v) ~init:0 ~f:(fun ~key:_ ~data -> fun acc -> acc + data)

  let oDegreeWeighted ?frozen:(frozen = true) (c) (v: Vertex.t) =
    Map.fold (oNbr ~frozen:frozen c v) ~init:0 ~f:(fun ~key ~data  -> fun acc -> acc + (Vertex.weight key * data))

  let iDegreeWeighted ?frozen:(frozen = true) (c) (v: Vertex.t) =
    Map.fold (iNbr ~frozen:frozen c v) ~init:0 ~f:(fun ~key ~data -> fun acc -> acc + (Vertex.weight key * data))

  let cVector ?frozenlist:(frozenlist = None) c u =
    let frozenlist = match frozenlist with Some l -> l | None -> List.sort ~compare:Vertex.compare (Set.to_list (frozen c)) in
    List.map frozenlist ~f:(fun v -> edge_weight c ~inp:v ~out:u)

  let cVectors ?frozenlist:(frozenlist = None) c =
    let frozenlist = match frozenlist with Some l -> l | None -> List.sort ~compare:Vertex.compare (Set.to_list (frozen c)) in
    let unfrozenlist = Set.to_list (unfrozen c) in
    let vectors = List.map unfrozenlist ~f:(cVector ~frozenlist:(Some frozenlist) c) in
    List.sort ~compare:(List.compare Int.compare) vectors

  let fCVectorAbove ?frozenlist:(frozenlist = None) c  ~w =
    List.exists (cVectors ~frozenlist:frozenlist c) ~f:(List.exists ~f:(fun i -> (Int.abs i) > w))


  (* A c-vector is green if all entries are nonpositive and red if all entries are nonnegative*)
  let is_vec_green = List.for_all ~f:(fun i -> Int.(i <= 0))
  let is_green ?frozenlist:(frozenlist=None) c = List.for_all ~f:is_vec_green (cVectors ~frozenlist:frozenlist c)
  let is_vec_red = List.for_all ~f:(fun i -> Int.(i>=0))
  let is_red ?frozenlist:(frozenlist=None) c = List.for_all ~f:is_vec_red (cVectors ~frozenlist:frozenlist c)

  let matrix ?(vorder=None) c =
    let verts = match vorder with None -> List.append (Set.to_list c.unfrozen) (Set.to_list c.frozen) | Some verts -> verts in
    List.map verts ~f:(fun v -> List.map verts ~f:(fun w -> edge_weight c ~inp:v ~out:w))
  let transvection ?(vorder=None) c v =
    let vs = match vorder with None -> verts c |> Set.to_list | Some order -> order in
    List.map vs ~f:(fun vi -> if Vertex.equal v vi
                              then List.map vs ~f:(fun vj -> if Vertex.equal vi vj then 1 else Algebra.Load.cmod (edge_weight c ~inp:vj ~out:vi) 2)
                              else List.map vs ~f:(fun vj -> if Vertex.equal vi vj then 1 else 0) )


  let casimir_vectors ?(vorder=None) q =
    let vorder = match vorder with None -> Some(q |> unfrozen |> Set.to_list) | Some l -> Some l in
    matrix ~vorder:(vorder) q |> Flint.BigIntMat.of_rows_int |> Flint.BigIntMat.nullspace
    |> (fun (i,m) -> List.init (Signed.Long.to_int i) ~f:(fun j -> Flint.BigIntMat.col m j))
    |> List.map ~f:(List.map ~f:(fun i -> i |> Flint.BigInt.to_string |> Int.of_string))


  let two_color c =
    let comparedist (i, _) (j, _) = Int.compare i j in
    let q = (Heap.of_list [(0, Set.min_elt_exn (unfrozen c))] ~cmp:comparedist) in
    let cadd (dist, w) = if Set.mem c.frozen w then () else Heap.add q (dist, w) in
    let rec loop (d) =
      match Heap.pop q with
      | None -> Some d
      | Some (dist, v) ->
         match Map.find d v with
         | Some i ->
            if i = (dist mod 2)
            then loop d
            else None
         | None ->
            if List.exists (Map.keys (nbr c v)) ~f:(fun w -> match Map.find d w with None -> false | Some i -> i = (dist mod 2))
            then None
            else
              let () = Map.iter_keys (nbr c v) ~f:(fun key -> cadd (dist+1, key)) in
              loop (Map.add_exn d ~key:v ~data:(dist mod 2))
    in
    match loop (Vertex.Map.empty) with
    | None -> None
    | Some dict ->
       Some (Map.keys (Map.filter dict ~f:(fun data -> data = 0)), Map.keys (Map.filter dict ~f:(fun data -> 1 = data)))


  let sources c = Set.filter (unfrozen c) ~f:(fun v -> Vertex.Set.is_subset (iNbrSet c v) ~of_:c.frozen)
  let sinks c = Set.filter (unfrozen c) ~f:(fun v -> Vertex.Set.is_subset (oNbrSet c v) ~of_:c.frozen)
  let sources_sinks_neithers c =
    let (sources, rest) = Vertex.Set.partition_tf ~f:(fun v -> Vertex.Set.is_subset (iNbrSet c v) ~of_:c.frozen) (unfrozen c) in
    let (sinks, neithers) = Vertex.Set.partition_tf ~f:(fun v -> Vertex.Set.is_subset (oNbrSet c v) ~of_:c.frozen) rest in
    (sources, sinks, neithers)

  let is_SourceSink c =
    let unfrozen = unfrozen c
    in Vertex.Set.for_all unfrozen ~f:(fun v -> let ins = iNbrSet ~frozen:false c v in
                                             let out = oNbrSet ~frozen:false c v in
                                             (Vertex.Set.is_empty (Set.inter ins unfrozen))
                                             || (Vertex.Set.is_empty (Set.inter out unfrozen)))


  let degree ?frozen:(frozen = true) c v = (oDegree ~frozen:frozen c v, iDegree ~frozen:frozen c v)
  let valence ?frozen:(frozen = true) c v = oDegree ~frozen:frozen c v + Int.abs(iDegree ~frozen:frozen c v)
  let degreeWeighted ?frozen:(frozen = true) c v = (oDegreeWeighted ~frozen:frozen c v, iDegreeWeighted ~frozen:frozen c v)

  let quiver c = c.graph

  let graphviz_of_cluster_color
        (vlabel : Vertex.t -> string)
        (cluster :cluster)
        (scheme : Vertex.t -> int)
        (colors : int -> string): string =
    let tab = "    " in
    let edge_color = "black" in
    let string_of_edge ~inp ~out mult =
      String.concat ~sep:""
                    [ tab
                    ; "\""
                    ; Vertex.id inp
                    ; "\""
                    ; " -> "
                    ; "\""
                    ; Vertex.id out
                    ; "\""
                    ; " ["
                    ; "len = 2.0, "
                    ; " color = \"" ^ edge_color ^ (String.concat (List.map (List.range 1 mult) ~f:(fun _ -> ":invis:" ^ edge_color))) ^ "\""
                    ; "]"
                    ] in
    let strings_of_nbrs ~key:v ~data:nbrs  =
      List.map (Map.to_alist (Map.filter nbrs ~f:(fun w -> w > 0))) ~f:(fun (w, mult) -> string_of_edge ~inp:v ~out:w mult) in
    let edges =
      Map.fold cluster.graph
          ~init:[]
          ~f:(fun ~key -> fun ~data -> fun l -> List.append (strings_of_nbrs ~key:key ~data:data)  l)
    in
    String.concat ~sep:"\n"
                  [ "digraph G {"
                  ; ""
                  ; String.concat ~sep:"\n" edges
                  ; ""
                  ; String.concat ~sep:"\n" (List.map ~f:(fun v -> String.concat ~sep:""
                                                                                 [ tab
                                                                                 ; "\"" ^ Vertex.id v ^ "\""
                                                                                 ; " [shape=box, color = \"#d1f2f2\", "
                                                                                 ; "label=\"" ^ vlabel v ^"\","
                                                                                 ; "penwidth=\"5\""
                                                                                 ; "]"])
                                                      (Set.to_list cluster.frozen))
                  ; String.concat ~sep:"\n" (List.map ~f:(fun v -> String.concat ~sep:"" [ tab
                                                                                         ; "\"" ^ Vertex.id v ^ "\""
                                                                                         ; " [color = "
                                                                                         ; colors (scheme v)
                                                                                         ; ",penwidth=\"5\""
                                                                                         ; ", label = \"" ^ vlabel v ^ "\""
                                                                                         ; "]" ])
                                                      (Set.to_list cluster.unfrozen))
                  ; "}"]


  let default_colors i =
    match i with
    | -1 -> "salmon"
    | 0 -> "\"#000000\""
    | 1 -> "cyan"
    | 2 -> "purple"
    | 3 -> "red"
    | 4 -> "orange"
    | 5 -> "yellow"
    | 6 -> "green"
    | 7 -> "blue"
    | 8 -> "magenta"
    | i -> invalid_arg ("Need a color for " ^ Int.to_string i)

  let write_cluster ?image_size:(isize = None)
        ?node_color:(scheme =(fun _ -> 0))
        ?vlabel:(vlabel=Vertex.to_string)
        (filename : string)
        (cluster : cluster)  =
    let () =  Out_channel.write_all filename ~data:(graphviz_of_cluster_color vlabel cluster scheme default_colors) in
    match isize with
    | None -> let _:int = Caml.Sys.command ("neato " ^ "\"" ^ filename ^ "\"" ^ " -Tpng -O") in ()
    | Some(xin,yin,dpi) ->
       let _:int =
         Caml.Sys.command ("neato \"" ^ filename ^ "\" -Gsize="^Int.to_string xin^","^Int.to_string yin ^ "\\! -Gdpi=" ^ Int.to_string dpi ^ " -Tpng -O") in
       let _:int = Caml.Sys.command ("convert \"" ^ filename^".png\" -gravity center -background white -extent "
                                ^ Int.to_string (xin*dpi)^"x"^Int.to_string(yin*dpi) ^ " \"" ^ filename^".png\"") in
       ()

  let write_clav (filename : string) (cluster : cluster) =
    let () = Out_channel.write_all filename ~data:(graphviz_of_cluster_color Vertex.to_string cluster (fun _(*v*) -> 0) default_colors) in
    let pad0 l s = (String.make (l - String.length s) '0' ) ^ s in
    let max_len = String.length(Int.to_string (Map.length cluster.graph)) in
    let vid_to_v id =
      let vid_map = String.Map.of_alist_exn (List.map (Map.keys cluster.graph) ~f:(fun v -> (Vertex.id v, v))) in
      match Map.find vid_map (String.strip ~drop:(function '"' -> true | _ -> false) id) with
      | Some v -> v
      | None ->  invalid_arg ("Missing " ^ id ^ "in " ^ String.concat ~sep:"," (Map.keys vid_map)) in
    let vid_to_index id =
      let vid_map = String.Map.of_alist_exn (List.mapi (Map.keys cluster.graph) ~f:(fun i -> fun v -> (Vertex.id v, pad0 max_len (Int.to_string i)) )) in
      match Map.find vid_map (String.strip ~drop:(function '"' -> true | _ -> false) id) with
      | Some i -> i
      | None ->  invalid_arg ("Missing " ^ id ^ "in " ^ String.concat ~sep:"," (Map.keys vid_map)) in
    let process_line l =
      match l with
      | "graph" :: _(*scale*) :: _(*width*) :: _(*height*) :: []-> ""
      | "node" :: name :: x :: y :: _(*width*) :: _(*height*) :: label :: _(*style*) :: _(*shape*) :: _(*color*) :: fillcolor :: [] ->
         String.concat ~sep:" " [ "v[" ^ vid_to_index name ^ "]"
                                ; "f:1"
                                ; "x:" ^ Int.to_string(Float.to_int (Float.of_string x *. 50.))
                                ; "y:" ^ Int.to_string(Float.to_int (Float.of_string y *. 50.))
                                ; "c:" ^ "0x" ^ (String.drop_prefix fillcolor 1)
                                ; "l:" ^ Int.to_string(String.length label)
                                ; "n:" ^ label
                                ; "\n"]
      | "edge" :: tail :: head :: _ ->
         let weight = Int.abs(edge_weight cluster ~inp:(vid_to_v head) ~out:(vid_to_v tail)) in
         String.concat ~sep:""
           [ "e[" ; vid_to_index tail ; "][" ; vid_to_index head ; "]" ; " " ; Int.to_string(weight);  "/1"; "\n"
           ; "e[" ; vid_to_index head ; "][" ; vid_to_index tail ; "]" ; " " ; Int.to_string(-weight); "/1"; "\n"]
      | ["stop"] -> ""
      | _ -> invalid_arg "Unknown line type in plain file " in
    let process_plain l =
      let (verts, edges) = List.split_while (List.map (List.drop l 1) ~f:(fun line -> process_line (String.split line ~on:' ')))
                                            ~f:(String.is_prefix ~prefix:"v") in
      (Int.to_string (Map.length cluster.graph) ^  " Vertices" ^ "\n")
      ^ String.concat ~sep:"" (List.append (List.sort ~compare:String.compare verts) edges)

    in match Caml.Sys.command ("neato " ^ "\"" ^ filename ^ "\"" ^ " -Tplain -O") with
       | 0 -> let () = Out_channel.write_all (filename ^ ".clav") ~data:(process_plain (In_channel.read_lines (filename ^ ".plain"))) in
              (match Caml.Sys.command ("rm \"" ^ filename ^ ".plain\"") with
               | 0 -> ()
               | _ -> raise (Sys_error "Failed to delete .plain file"))
       | _ -> raise (Sys_error "Failed to produce .plain file")


  (* Functions on clusters *)
(*
  let subset_map m1 m2 =
    Map.fold m1 ~init:true ~f:(fun ~key -> fun ~data:d1 -> fun acc -> match Map.find m2 key with Some d2 -> (Int.abs(d1) < Int.abs(d2)) && acc | None -> false)
  let subset_nbrset (nbr1 :nbrs) (nbr2 : nbrs) =
    subset_map nbr1 nbr2
*)
  let is_subset ?unfrozen:(unfrozen=true) c1 ~of_:c2 =
    let frozen = not unfrozen in
    let check ~key:u ~data:weight acc =
      acc && (match weight with
             | `Both(w1,w2) -> w1 = w2
             | `Left(_) -> false
             | `Right (_) -> not(Vertex.Set.mem (verts ~frozen:frozen c1) u)) in
    Set.for_all (verts ~frozen:frozen c1) ~f:(fun v -> Map.fold2 (nbr ~frozen:frozen c1 v) (nbr ~frozen:frozen c2 v) ~init:true ~f:check)

  let dfsnbr c parent v =
    Map.to_alist(Map.filter_keys (nbr c v) ~f:(fun key -> not(Vertex.compare key parent = 0) && not(Set.mem (c.frozen) key)))


  let rank_pi1 (c : cluster) : int =
    let dfs start =
      let rec loop l parent visited count =
        match l with
        | [] -> (count, visited)
        | (v, num_edge) :: vs ->
           if Set.mem visited v
           then loop vs parent visited (count + (abs num_edge))
           else
             let (count_rec, visited_new) = loop (dfsnbr c parent v) v (Set.add visited v) (count + (abs num_edge) - 1)
             in loop (List.filter vs ~f:(fun (w,_) -> not(Set.mem (Set.diff visited_new visited) w))) parent visited_new count_rec
      in loop (dfsnbr c start start) start (Vertex.Set.singleton start) 0 in
    fst(dfs (Set.min_elt_exn (unfrozen c)))

  let count_oriented_cycles (c : cluster) : int =
    let dfs () =
      let rec loop l visited stack count =
        match l with
        | [] -> (count, visited)
        | (v, num_edge) :: vs ->
           if Set.mem visited v
           then if Set.mem stack v
                then loop vs visited stack (count + (abs num_edge))
                else loop vs visited stack count
           else
             let (count_rec, visited_new) = loop (Map.to_alist (oNbr ~frozen:false c v)) (Set.add visited v) (Set.add stack v) (count)
             in loop (List.filter vs ~f:(fun (w,_) -> not(Set.mem (Set.diff visited_new visited) w))) visited_new (stack) count_rec
      in
      c |> unfrozen |> Set.fold ~init:(0,Vertex.Set.empty)
                         ~f:(fun (count,visited) v -> if Set.mem visited v then (count,visited)
                                                      else loop (Map.to_alist (oNbr ~frozen:false c v)) (Set.add visited v) (Vertex.Set.singleton v) count)
    in
    fst(dfs ())

  let is_acylic c = count_oriented_cycles c = 0

  let is_abundant c=
    for_all c ~frozen:false ~f:(fun v -> for_all c ~frozen:false ~f:(fun w -> Vertex.equal v w || Int.abs(edge_weight c ~inp:v ~out:w) >=2))
  let is_fork c =
    if not(is_abundant c)
    then None
    else find ~frozen:false c ~f:(fun r ->
           let iNbr = iNbrSet ~frozen:false c r in
           let oNbr = oNbrSet ~frozen:false c r in
           Set.for_all iNbr ~f:(fun i -> Set.for_all oNbr ~f:(fun j -> Int.(edge_weight c ~inp:j ~out:i > edge_weight c ~inp:i ~out:r)
                                                                       && Int.(edge_weight c ~inp:j ~out:i > edge_weight c ~inp:r ~out:j)))
           && is_acylic (filter c ~f:(Set.mem iNbr))
           && is_acylic (filter c ~f:(Set.mem oNbr))
           )

  let is_key c =
    let vlist = c |> unfrozen |> Set.to_list |> List.filter ~f:(fun v -> let c' = freeze c [v] in is_abundant c' && is_acylic c') in
    let vpairs = List.concat_mapi vlist ~f:(fun i v -> List.map (List.drop vlist (i+1)) ~f:(fun w -> (v,w))) in
    vpairs |> List.find ~f:(fun (k,k') -> Set.equal (Set.remove (oNbrSet c k) k') (Set.remove (oNbrSet c k') k)
                                          && Set.equal (Set.remove (iNbrSet c k) k') (Set.remove (iNbrSet c k') k))

  let is_prefork c =
    let vlist = c |> unfrozen |> Set.to_list |> List.filter_map ~f:(fun v -> freeze c [v] |> is_fork |> Option.map ~f:(fun r -> (v,r))) in
    let vpairs = List.concat_mapi vlist ~f:(fun i v -> List.map (List.drop vlist (i+1)) ~f:(fun w -> (v,w))) in
    vpairs |> List.find ~f:(fun ((k,r),(k',r')) -> Vertex.equal r r'
                                                 && Set.equal (Set.remove (oNbrSet c k) k') (Set.remove (oNbrSet c k') k)
                                                 && Set.equal (Set.remove (iNbrSet c k) k') (Set.remove (iNbrSet c k') k))
    |> Option.map ~f:(fun ((k,r),(k',_)) -> (r,(k,k')) )

  let component_of c v =
    let comp =
      let rec loop visited queue  =
        match Vertex.Set.min_elt queue with
        | None -> visited
        | Some w  ->
           if Vertex.Set.mem visited w
           then loop visited (Vertex.Set.remove queue w)
           else loop (Vertex.Set.add visited w) (Vertex.Set.union queue (Vertex.Set.diff (nbrSet ~frozen:false c w) visited))
      in loop Vertex.Set.empty (Vertex.Set.singleton v)
    in { graph = c.graph
       ; unfrozen = comp
       ; frozen = Vertex.Set.diff (verts c) comp}

  let components c =
    let rec loop queue comps  =
      match Vertex.Set.min_elt queue with
      | None -> comps
      | Some v ->
         let comp = component_of c v
         in loop (Vertex.Set.diff queue (unfrozen comp)) (comp::comps)
    in loop (unfrozen c) []

  let fConnected c = List.length (components c) = 1

  (* Requires: f to be injective on integers appearing in cluster*)
  let map (cluster : cluster) ~(f : Vertex.t -> Vertex.t) : cluster =
    let mapkeys m ~f = Vertex.Map.of_alist_exn (List.map (Map.to_alist m) ~f:(fun (k, d) -> (f k , d))) in
    let fseed (v, nbr) = (f v, mapkeys nbr ~f:f)
    in { frozen = Vertex.Set.map cluster.frozen ~f:f
       ; unfrozen = Vertex.Set.map cluster.unfrozen ~f:f
       ; graph = match Vertex.Map.of_alist (List.map (Vertex.Map.to_alist cluster.graph) ~f:fseed) with
                 | `Duplicate_key _ -> invalid_arg "f is not injective on integers of cluster"
                 | `Ok (seed) -> seed
       }

  let non_zero i = if i = 0 then None else Some i

  let geometric_exchange c (v : Vertex.t) : Vertex.t * cluster =
    let formula ~bxy ~bxv ~bvy = non_zero(bxy + ((Int.abs(bxv) * bvy + bxv * Int.abs(bvy)) / 2)) in
    match Map.find c.graph v with
    | None -> (v, c)
    | Some vnbrs ->
       let vnew = Vertex.mutate v ~inp:(iNbr c v) ~out:(oNbr c v) in
       let mutate_edge ~key:_ ~data:nbr =
         match Map.find nbr v with
         | None -> nbr
         | Some bxv ->
            (* Map.add_exn ~key:vnew ~data:(-bxv) (Map.remove nbr v)*)
            Map.add_exn (Map.merge (Map.remove nbr v) vnbrs ~f:(fun ~key:_(*y*) -> function `Left bxy -> formula ~bxy:bxy ~bxv:bxv ~bvy:0
                                                                              | `Right bvy -> formula ~bxy:0 ~bxv:bxv ~bvy:bvy
                                                                              | `Both(bxy, bvy) -> formula ~bxy:bxy ~bxv:bxv ~bvy:bvy))
                    ~key:vnew
                    ~data:(-bxv) in

       let seed_fix_edges = Map.mapi (Map.remove c.graph v) ~f:mutate_edge in
       let seed_repv = Map.add_exn seed_fix_edges ~key:vnew ~data:(Map.map vnbrs ~f:(fun d -> -d)) in
       (vnew , {graph = seed_repv; frozen = c.frozen; unfrozen = Set.add (Set.remove c.unfrozen v) vnew})


  let mutate_name c v =
    let (vnew,cnew) = geometric_exchange c v
    in (cnew, vnew)

  let mutate cluster v = let (cnew,_) = mutate_name cluster v in cnew
  let mutate_multi cluster  = List.fold ~init:cluster ~f:mutate
  let mutate_multi_name c path =
    List.fold path ~init:(c,[]) ~f:(fun (c, names) v -> let (cnew, vnew) = mutate_name c v in (cnew, vnew::names))


  let canExchange (frozen : Vertex.Set.t) (v : Vertex.t) (seed) =
    not (Set.mem frozen v ) && (Vertex.canExchange v ~inp:(iNbr seed v) ~out:(oNbr seed v))

  type edge = Vertex.t [@@deriving sexp]
  let equal_edge (v1) (u1) = Vertex.equal v1 u1 
  let follow c es = mutate_multi_name c es
  let n (cluster) : (cluster * edge) list =
    Map.fold
      (Map.filter_keys
         cluster.graph
         ~f:(fun key-> canExchange cluster.frozen key cluster))
      ~init:[]
      ~f:(fun ~key ~data:_ l -> let (nbr,w) = mutate_name cluster key in (nbr, w) :: l)


  let follow_two_color_path start count =
    let two_color_nbrs c =
      match two_color c with
      | None -> invalid_arg "Cluster isn't two colorable"
      | Some (odd, even) -> ( mutate_multi (mutate_multi c odd) even
                            , mutate_multi (mutate_multi c even) odd) in
    let rec loop start count current path =
      match count with
      | 0 -> path
      | _ -> let (next1,next2) = two_color_nbrs current in
             match path with
             | [] -> loop start (count - 1) next1 [current]
             | (prev :: _ ) ->
                let next = if Cluster.equal next1 prev
                           then (print_endline "right"; next2)
                           else (print_endline "left" ; next1)
                in if Cluster.equal next start
                   then path
                   else loop start (count - 1) next (current :: path)
    in List.rev(loop start count start [])

  let follow_sources_path start max_count =
    let hashtbl=  String.Table.create () in
    let rec loop start count current path =
      match count <= max_count with
      | false -> (max_count + 1, path)
      | true -> let next = mutate_multi current (Vertex.Set.to_list (sources current)) in
             match Hashtbl.find hashtbl (id next) with
             | Some i -> (i, current::path)
             | None ->
                let () = Hashtbl.add_exn hashtbl ~key:(id current) ~data:count in
                loop start (count + 1) next (current :: path)
    in
    let (i, path) = loop start 0 start [] in
    (i, List.rev path)


  let write_path dirname start path =
    let (cend, vend) =
      List.foldi
        ~init:(start,None)
        ~f:(fun i -> fun (c, vprev) -> fun v ->
                                       let vnew = Vertex.mutate v ~inp:(iNbr c v) ~out:(oNbr c v) in
                                       let cnew = mutate c v in
                                       let () = write_cluster
                                                 (String.concat [dirname ; Int.to_string i; "_" ; Vertex.to_string v ; "_" ; name c])
                                                 c
                                                 ~node_color:(fun w -> if Vertex.equal v w
                                                           then 3
                                                           else match vprev with
                                                                | None -> 0
                                                                | Some vprev -> if Vertex.equal vprev w then 2 else 0)
                                       in (cnew, Some vnew))
        path
    in write_cluster
         (String.concat [dirname; Int.to_string (List.length path); "_End_"; name cend])
         cend
         ~node_color:(fun w -> match vend with None -> 0 | Some vend -> if Vertex.equal vend w then 2 else 0)

  let write_path_condense dirname start path =
    let rec loop l i current =
      match l with
      | [] -> ()
      | _ -> let norepeat = List.take_while l ~f:(Set.mem (unfrozen current)) in
             let () = write_cluster
                       (dirname
                        ^ Int.to_string i
                        ^ "-"
                        ^ Int.to_string (i + List.length norepeat)
                        ^ "_"
                        ^ String.concat ~sep:"_" (List.map ~f:Vertex.to_string norepeat))
                       current
                       ~node_color:(fun v -> match List.findi norepeat ~f:(fun _ -> Vertex.equal v) with
                                 | None -> 0
                                 | Some (i,_) -> i+1)
             in loop (List.drop l (List.length norepeat)) (i + List.length norepeat) (mutate_multi current norepeat)
    in loop path 0 start

  (* Helper functions for Isomorphism Checking *)
  let group_by_degree directed (m : nbrs) =
    Int.Map.of_alist_fold
      ~init:Vertex.Set.empty
      ~f:Vertex.Set.add
      (Map.fold m ~init:[] ~f:(fun ~(key:Vertex.t) ~(data:int) acc -> ((if directed then data else Int.abs data), key)::acc))
  let compatible m1 m2 =
    Map.fold2 m1 m2 ~init:true
      ~f:(fun ~key:_ ~data acc -> match data with `Both (d1,d2) -> Set.length d1 = Set.length d2 && acc | `Right _ -> false | `Left _ -> false)
  let least_choices ~key ~data acc =
    match acc with
    | None -> Some(key, data)
    | Some (v,l) -> if Set.length data < Set.length l
                    then Some (key, data)
                    else Some (v,l)

  let isomorphism ?unfrozen:(unfrozen = true) ?directed:(directed = true) c1 c2 =
    let frozen = not unfrozen in
    let c2_verts = verts ~frozen:frozen c2 in
    let c2_by_degree = Set.to_map c2_verts ~f:(fun v -> group_by_degree directed (nbr ~frozen:frozen c2 v)) in
    let c1_verts = verts ~frozen:frozen c1 in
    let iso_possible =
      Set.to_map c1_verts ~f:(fun v -> Set.filter c2_verts
                                      ~f:(fun w -> compatible (group_by_degree directed (nbr ~frozen:frozen c1 v)) (Map.find_exn c2_by_degree w))) in
    let prune possible v w =
      let wnbr = Map.find_exn c2_by_degree w in
      Map.mapi (Map.remove possible v) ~f:(fun ~key:v' ~data:set -> match edge_weight c1 ~inp:v ~out:v' with
                                                                  | 0 -> Set.remove set w
                                                                  | weight -> Set.inter (Set.remove set w)
                                                                           (Map.find_exn wnbr (if directed then weight else Int.abs weight))) in
    let rec loop possible =
      match Map.fold possible ~init:None ~f:least_choices with
      | None -> Some []
      | Some(v,s) -> choose possible v (Set.to_list s)
    and choose possible v l =
      match l with
      | [] -> None
      | w::ws ->
         (match loop (prune possible v w) with
          | None ->  choose possible v ws
          | Some partial -> Some( (v,w) :: partial) )
    in Option.map (loop iso_possible) ~f:(fun l -> let m = Vertex.Map.of_alist_exn l in (fun v -> match Map.find m v with | Some w -> w | None -> v))

  let compatible_subgraph m1 m2 =
    Map.fold2 m1 m2 ~init:true
      ~f:(fun ~key:_ ~data acc -> match data with `Both (d1,d2) -> Set.length d1 <= Set.length d2 && acc | `Right _ -> true | `Left _ -> false)

  let all_embeddings ?unfrozen:(unfrozen = true) ?directed:(directed = true) c1 ~of_:c2 =
    let frozen = not unfrozen in
    let c2_verts = verts ~frozen:frozen c2 in
    let c2_by_degree = Set.to_map c2_verts ~f:(fun v -> group_by_degree directed (nbr ~frozen:frozen c2 v)) in
    let c1_verts = verts ~frozen:frozen c1 in
    let iso_possible =
      Set.to_map c1_verts ~f:(fun v -> Set.filter c2_verts
                                      ~f:(fun w -> compatible_subgraph (group_by_degree directed (nbr ~frozen:frozen c1 v)) (Map.find_exn c2_by_degree w))) in
    let prune possible v w =
      let wnbr = Map.find_exn c2_by_degree w in
      Map.mapi (Map.remove possible v) ~f:(fun ~key:v' ~data:set -> match edge_weight c1 ~inp:v ~out:v' with
                                                                  | 0 -> Set.remove set w
                                                                  | weight -> Set.inter (Set.remove set w)
                                                                           (Map.find_exn wnbr (if directed then weight else Int.abs weight))) in
    let rec loop possible =
      match Map.fold possible ~init:None ~f:least_choices with
      | None -> [[]]
      | Some (v,s) -> List.concat_map (Set.to_list s) ~f:(fun w -> List.map ~f:(fun partial -> (v,w) :: partial) (loop (prune possible v w))) in
    let subgraphs =
      List.map (loop iso_possible) ~f:(fun l -> let m = Vertex.Map.of_alist_exn l in (fun v -> match Map.find m v with | Some w -> w | None -> v)) in
    List.filter subgraphs ~f:(fun f -> is_subset (map ~f:f c1) ~of_:c2)

  let is_subgraph  ?unfrozen:(unfrozen = true) ?directed:(directed = true) c1 ~of_:c2 =
    not(List.is_empty (all_embeddings ~unfrozen:(unfrozen) ~directed:directed c1 ~of_:c2))

  let is_isomorphic ?unfrozen:(unfrozen = true) ?directed:(directed = true) c1 c2 =
    match isomorphism ~unfrozen:unfrozen ~directed:directed c1 c2 with
    | Some _ -> true
    | None -> false

  let automorphisms ?unfrozen:(unfrozen = true) ?directed:(directed = true) c =
    let frozen = not unfrozen in
    let c_verts = verts ~frozen:frozen c in
    let c_by_degree = Set.to_map c_verts ~f:(fun v -> group_by_degree directed (nbr ~frozen:frozen c v)) in
    let iso_possible =
      Map.mapi c_by_degree ~f:(fun ~key:_ ~data:vgroups -> Set.filter c_verts ~f:(fun w -> compatible vgroups (Map.find_exn c_by_degree w))) in
    let prune possible v w =
      let wnbr = Map.find_exn c_by_degree w in
      Map.mapi (Map.remove possible v) ~f:(fun ~key:v' ~data:set -> match edge_weight c ~inp:v ~out:v' with
                                                                    | 0 -> Set.remove set w
                                                                    | weight -> Set.inter (Set.remove set w)
                                                                                  (Map.find_exn wnbr (if directed then weight else Int.abs weight))) in
    let rec loop possible =
      match Map.fold possible ~init:None ~f:least_choices with
      | None -> [[]]
      | Some (v,s) -> List.concat_map (Set.to_list s) ~f:(fun w -> List.map ~f:(fun partial -> (v,w) :: partial) (loop (prune possible v w))) in
    List.map (loop iso_possible) ~f:(fun l -> let m = Vertex.Map.of_alist_exn l in (fun v -> match Map.find m v with | Some w -> w | None -> v))


  let tail c v =
    let rec loop prev w =
      let nbrs = nbrList ~frozen:false c w in
      if List.length nbrs = 2 then
        match List.find nbrs ~f:(fun u -> not(Vertex.equal prev u) && not(Vertex.equal v u)) with
        | Some u -> prev:: loop w u
        | None -> invalid_arg "Tail shouldn't loop"
      else  [prev;w]
    in match nbrList ~frozen:false c v with
       | [w] -> loop v w
       | _ -> []

  let tail_lengths c =
    List.map (Set.to_list (unfrozen c)) ~f:(fun v -> List.length (tail c v))
    |> List.filter ~f:(fun len -> len > 0)
    |> List.sort ~compare:Int.compare

  let fAffineDynkin c =
    let test c =
      (rank_pi1 c = 0 && is_SourceSink c && Vertex.Set.length (sources c ) >= (Vertex.Set.length (sinks c)))
      || (rank_pi1 c = 1 && for_all ~frozen:false c ~f:(fun v -> valence ~frozen:false c v = 2)
          && Vertex.Set.length(sources c) = 1 && Vertex.Set.length(sinks c) = 1)
    in List.for_all (components c) ~f:test

  let is_Tpqr c =
    match find_edge ~frozen:false c ~f:(fun (_,_,w) -> w = 2) with
    | Some(u,v,_) -> is_SourceSink (freeze c [u])
                     && count_edge ~frozen:false c ~f:(fun (_,_,w)-> w > 1) = 1
                     && Set.equal (oNbrSet ~frozen:false c v) (iNbrSet ~frozen:false c u)
    | None -> false

  let dynkin_type c =
    let comp_name c =
      let dim = dim c in
      if fAffineDynkin c then
        (match tail_lengths c with
         | [_;_] ->  "A"^(Int.to_string dim)
         | [2;2;2] -> "D4"
         | [2;2;_] -> "D"^(Int.to_string dim)
         | [2;3;3] -> "E6"
         | [2;3;4] -> "E7"
         | [2;3;5] -> "E8"
         | [3;3;3] -> "E6affine"
         | [2;4;4] -> "E7affine"
         | [2;3;6] -> "E8affine"
         | [2;2;2;2] -> "D" ^ (Int.to_string(dim - 1)) ^ "affine"
         | [] ->
            let rec out_len v =
              match oNbrList c v with
              | [u] -> 1 + out_len u
              | _ -> 1
            in
            if dim = 1 then "A1" else
            (match Set.to_list (sources c) with
             | [so] -> "A" ^ Int.to_string (dim - 1) ^ "-"
                       ^ String.concat ~sep:"-" (List.map (oNbrList ~frozen:false c so) ~f:(fun v -> Int.to_string(out_len v)))
                       ^ "affine"
             | _ -> invalid_arg "Defining quiver for Apq affine has a single source"
            )
         | _ -> "Unknown"
        )
      else if is_Tpqr c then
        match find_edge c ~f:(fun (_,_,w) -> Int.equal w 2) with
        | None -> invalid_arg "Every Tqr quiver has a double edge"
        | Some (u,_,2) ->
           (match tail_lengths (freeze c [u]) with
            | [] -> "A1affine"
            | [_;_] ->
               (match tail_lengths c with
                | [p] -> String.concat ["A" ; Int.to_string (dim-1) ;"-"; Int.to_string (dim-p-1) ; "-" ; Int.to_string (p+1) ; "affine"]
                | [p;q] -> String.concat ["A" ; Int.to_string (dim-1) ;"-"; Int.to_string (p+1) ; "-" ; Int.to_string (q+1) ; "affine"]
                |  _ -> invalid_arg "Only A affine should be single path")
            | [2;2;_(*n*)] -> String.concat ["D"; Int.to_string (dim-1); "affine"]
            | [2;3;3] -> "E6affine"
            | [2;3;4] -> "E7affine"
            | [2;3;5] -> "E8affine"
            | [2;2;2;2] -> "D4double"
            | [3;3;3] -> "E6double"
            | [2;4;4] -> "E7double"
            | [2;3;6] -> "E8double"
            | l -> "T"^ String.concat ~sep:"-" (List.map ~f:Int.to_string l)
           )
        | Some (_,_,_) -> invalid_arg "Every Tpqr quiver should have double edge"
      else "Not Dynkin or Tpqr"
    in
    match components c with
    | [] -> Int.to_string (dim c) ^ "_A0"
    | comps -> Int.to_string (dim c) ^ "_" ^String.concat ~sep:"x" (List.sort ~compare:String.compare (List.map ~f:comp_name comps))

  include Cluster
end
