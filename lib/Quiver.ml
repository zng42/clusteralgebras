open Core
open Algebra

type vertexKind =
  Frozen
| Unfrozen
| AnyKind

type nbrKind =
  OutNbr
| InNbr
| AnyNbr


module type S =
  sig
    type t
    val sexp_of_t : t -> Sexplib.Sexp.t
    val t_of_sexp : Sexplib.Sexp.t -> t

    module Weight : Ring.S

    val of_edgelist : graph : ((int*int*Weight.t) list) -> frozen:Int.Set.t -> t

    val freeze : t -> int list -> t
    val unfreeze : t -> int list -> t

    (* OUTPUT *)
    val frozen : t -> Int.Set.t
    val unfrozen : t -> Int.Set.t
    val verts : ?frozen:vertexKind -> t -> Int.Set.t
    (* c == of_edgelist ~graph:(edgelist c) ~frozen:(frozen c) *)

    val edgelist : t -> (int * int * Weight.t) list
    val matrix : ?vorder:(int list Option.t) -> t -> Weight.t list list
    (* Dimension of algebra (dim(c) = Set.length(unfrozen(c)) *)
    val dim : t -> int

    val edge_weight : t -> inp:int -> out:int -> Weight.t

    (* degree c v = (oDegree c v, iDegree c v) *)
    val degree : ?vkind:vertexKind -> ?nbrkind:nbrKind -> t -> int -> Weight.t

    (* T Isomorphism*)
    val is_isomorphic : ?unfrozen:bool -> ?directed:bool -> t -> t -> bool
    val isomorphism : ?unfrozen:bool -> ?directed:bool -> t -> t -> (int -> int) option

    (* Compute list of automorphism of c
     * default arguments are automorphism ~unfrozen:true ~directed:true c
     *)
    val automorphisms : ?unfrozen:bool -> ?directed:bool -> t -> (int -> int) list

    val mutate : t -> int -> t
    val mutate_multi : t -> int list -> t
  end
