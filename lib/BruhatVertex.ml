open Core
type t = int [@@deriving compare, sexp]
include Comparable.Make(Int)


let weight _ = 1 (* TODO: Determine proper weights of vertices in DoubleBruhat clusters *)
let canExchange _ ~inp:_ ~out:_ = true
let mutate v ~inp:_ ~out:_ = v
let id = Int.to_string
let to_string = id
let of_string = Int.of_string
let pp ppf v = Format.fprintf ppf "%s" (to_string v)
