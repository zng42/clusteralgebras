open Core
(* Color Scheme before fancy things *)
let classic i =
  match i with
  | -1 -> "salmon"
  | 0 -> "\"#000000\""
  | 1 -> "cyan"
  | 2 -> "purple"
  | 3 -> "red"
  | 4 -> "orange"
  | 5 -> "yellow"
  | 6 -> "green"
  | 7 -> "blue"
  | 8 -> "magenta"
  | i -> invalid_arg ("Need a color for " ^ Int.to_string i)

let rmodi x y =
  let res = x mod y in
  if res > 0
  then res
  else res + y

let rmod x y =
  let res = Float.mod_float x y in
  if Float.(res >= 0.)
  then res
  else res +. y

let by_group ~groups:l ~found x =
  match List.findi l ~f:(fun _ y -> found y x) with
  | Some (i,_) -> i+1
  | None -> 0

(* Take (h,s,v) with 0 <= h < 360, 0 <= s,v <= 100 to (r,g,b) in range 0 to 255 *)
let hsv_to_rgb (h,s,v) : int * int * int =
  let (h, s,v) = (Float.of_int h /. 360., Float.of_int s /. 100., Float.of_int v /. 100.) in
  let i = Float.round_down(h *. 6.) in
  let f = h *. 6. -. i in
  let (p,q,t) = (v *. (1. -. s), v *. (1. -. f *. s), v *. (1. -. (1. -. f) *. s))  in
  let (rp, gp, bp) =
    List.nth_exn [(v,t,p); (q,v,p); (p,v,t); (p, q, v); (t,p,v); (v,p,q)] (Float.to_int i) in
  let convert x = Float.to_int(Float.round (x *. 255.)) in
  (convert rp, convert gp, convert bp)

let  hsl_to_rgb (h , s, l) : int * int * int =
  if Float.(s = 0.)
  then let u = Float.to_int (l *. 255.)  in (u, u, u)
  else
    let t2 = if Float.(l < 0.5) then l *. (1. +. s) else l +. s -. (l *. s) in
    let t1 = 2. *. l -. t2 in
    let cout h =
      let a = rmod h 1.0 in
      match ( Float.between a ~low:0. ~high:(60. /. 360.)
            , Float.between a ~low:(60. /. 360.) ~high:(180. /. 360.)
            , Float.between a ~low:(180. /. 360.) ~high:(240. /. 360.)
            , Float.between a ~low:(240. /. 360.) ~high:1.) with
      | (true, _, _, _) -> t1 +. (t2 -. t1) *. (a *. 6.)
      | (_, true, _, _) -> t2
      | (_, _, true, _) -> t1 +. (t2 -. t1) *. (4. -. (a *. 6.))
      | (_, _, _, true) ->t1
      | _ -> invalid_arg ("angle " ^ Float.to_string a ^ "should be between 0 and 1")
    in
    (Float.to_int (cout (h +. (1. /. 3.)) *. 255.) , Float.to_int (cout h *. 255.) , Float.to_int (cout (h -. (1. /. 3.)) *. 255.))

(* Palette based on 9-class Set1 from  www.ColorBrewer.org by Cynthia A. Brewer, Geography, Pennsylvania State University *)
let set1 i =
  let l =
    [ (0, 0, 0)
    ; (359, 89, 90)
    ; (207, 70, 72)
    ; (118, 57, 69)
    ; (292, 52, 64)
    ; (30, 100, 100)
    ; (60, 80, 100)
    ; (22, 76, 65)
    ; (328, 48, 97)
    ; (0, 0, 60)] in
  match List.nth l i with
  | Some c -> c
  | None -> invalid_arg ("need color for " ^ Int.to_string i)

(* Palette based on 9-class Paired Palette from  www.ColorBrewer.org by Cynthia A. Brewer, Geography, Pennsylvania State University *)
let paired_hs i =
  List.nth_exn
    [ (  0,   0)
    ; (201,  26)
    ; (  1,  39)
    ; ( 34,  56)
    ; ( 52,  90)
    ; (116,  72)
    ; (205,  83)
    ; (359,  89)
    ; ( 30, 100)
    ; ( 92,  38)]
    (i mod 10)

let categories i =
  let i = i mod 100 in
  let color = i mod 10 in
  let dist = i / 10 in
  let (h,s) = paired_hs color in
  (* Need shuffle to be relatively prime to range to get all buckets *)
  let (shuffle, range) = (3,70) in
  let vshift = rmodi (dist * shuffle * range/10) range in
    match color with
    | 0 -> (h,s, (70 -  vshift))
    | _ -> (h,s, (100 - range) + vshift)

let grouped i =
  if i >= 100
  then invalid_arg "Only support 10 subdivisions per group"
  else
    let size =  i mod 10 in
    (* Move grayscale to last category grouping *)
    let color = ((i /10)+1) mod 10  in
    let (h,s) = paired_hs color in
    (h,s, 30 + size * 7)

type palette = Classic | Category | Grouped

let choose (s : palette) i =
  let f (r,g,b) = "\"#" ^ String.concat  (List.map ~f:(Printf.sprintf "%02X") [r;g;b]) ^ "\""in
  match s with
  | Classic -> classic i
  | Category -> f(hsv_to_rgb (categories i))
  | Grouped -> f(hsv_to_rgb (grouped i))
