open Core
module FMatrixPoly = Algebra.Rings.FMatrixPoly

module type S =
  sig
    (* Exotic (d,n,i) is then n^th  exotic coordinate of degree d found *)
    type vertex =  Plucker of PluckerVert.t | Exotic of int * int

    include Vertex.PS with type t = vertex

    val degree :  t -> int
    val p : int list -> t
  end

module Make (Arg: sig val n : int val k : int val epsilon : float end) (* : S *) =
  struct

    type vertex =  Plucker of Plucker.Vert.t | Exotic of int * int [@@deriving compare, sexp]
    type t = vertex [@@deriving compare, sexp]

    module Comp = Comparable.Make(struct type t = vertex [@@deriving compare, sexp] end)


    let of_plucker _ p = Plucker p

    let id (v:t ) =
      match v with
      | Plucker p -> "P" ^ (Plucker.Vert.to_string p)
      | Exotic (d,n) -> "E" ^ (Int.to_string d) ^ "#" ^ (Int.to_string n)
    let to_string = id
    let pp ppf v = Format.fprintf ppf "%s" (to_string v)
    let of_string s =
      match String.chop_prefix ~prefix:"P" s with
      | Some plucker -> Plucker(Plucker.Vert.of_string plucker)
      | None ->
         (match (Option.map (String.chop_prefix ~prefix:"E" s) ~f:(String.split ~on:'#')) with
          | Some [d;n] -> Exotic (Int.of_string d, Int.of_string n)
          | Some _ -> invalid_arg ("Expected E<d>#<n> instead of" ^ s)
          | None -> invalid_arg ("Expect P<plucker> or E<d>#<n> instead of " ^ s))

    let all_comb n k =
      let rec loop l k =
        match (l,k) with
        | (_, 0) -> [[]]
        | ([], _) -> []
        | (x :: xs, _) -> List.append (loop xs k) (List.map (loop xs (k-1)) ~f:(fun c -> x::c))
      in loop (List.range ~stop:`inclusive 1 n) k

    let weight (v : t) =
      match v with
      | Plucker _ -> 1
      | Exotic(d,_) -> d

    let p l = Plucker (Int.Set.of_list l)


    (* Assume that plkr coords are 1 indexed while matrices are 0 indexed *)
    let plkr (l : int list) : FMatrixPoly.t =
      let rem l i = List.filter l ~f:(fun x -> not(Int.equal x i)) in
      let rec loop rows cols =
        match cols with
        | [] -> FMatrixPoly.const 1.0
        | j :: js -> List.fold ~init:FMatrixPoly.zero ~f:FMatrixPoly.add
                               (List.mapi rows
                                          ~f:(fun index -> fun i -> FMatrixPoly.mult
                                                                   (FMatrixPoly.of_mono ((-. 1.0) ** (Float.of_int  index)) [((i,j),1)])
                                                                   (loop (rem rows i) js)))
      in loop (List.range 0 (List.length l)) (List.map l ~f:(fun i -> i -1))


    let mat = Array.map ~f:(Array.map ~f:Random.float) (Array.make_matrix ~dimx:Arg.n ~dimy:Arg.k 1.0)
    let plucker_values =
      List.map (all_comb Arg.n Arg.k) ~f:(fun l -> (Plucker (Int.Set.of_list l), FMatrixPoly.eval (plkr l) (fun (row,col) -> mat.(col).(row))))


    module FloatMap = Core.Map.Make(struct type t = float [@@deriving sexp]
                                               let compare f1 f2 = if Float.(abs( abs (f1 /. f2) -. 1.0) < Arg.epsilon)
                                                                   then 0
                                                                   else Float.compare f1 f2 end)

    let values : float Comp.Map.t ref = ref (Comp.Map.of_alist_exn plucker_values)
    let found : vertex FloatMap.t ref = ref (FloatMap.of_alist_exn (List.map ~f:(fun (v,f) -> (f,v)) plucker_values))

    let count : int Int.Table.t = Int.Table.create ()


    let value (v : t) =
      match Map.find !values v with
      | Some f -> f
      | _ -> invalid_arg (to_string v ^ "hasn't been found in a cluster so far")

    let canExchange (_ :t) ~inp:_ ~out:_ = true

    let rec pow f d =
      match d with
      | 0 -> 1.0
      | _ -> f *. (pow f (d-1))

    let mutate (v: t) ~(inp) ~(out) =
      let rhs =    (Map.fold inp ~init:1.0 ~f:(fun ~key:w -> fun ~data:deg -> fun res -> res *. pow (value w) (Int.abs deg)))
                +. (Map.fold out ~init:1.0 ~f:(fun ~key:w -> fun ~data:deg -> fun res -> res *. pow (value w) (Int.abs deg))) in
      (*       let _ = if Int.rem rhs (value v) = 0 then () else invalid_arg (to_string v ^ "doesn't divide it's neighbors)") in *)
      let inew = rhs /. (value v) in
      match FloatMap.find !found inew with
      | Some w -> w
      | None ->
         let d = (Map.fold ~init:0 ~f:(fun ~key:w -> fun ~data:deg -> fun res -> res + (deg * weight w)) inp) - (weight v) in
         let () = Int.Table.change count d ~f:(function None -> Some 1 | Some c -> Some (c + 1)) in
         let w = Exotic (d, Int.Table.find_exn count d) in
         let () = values := Comp.Map.add_exn !values ~key:w ~data:inew in
         let () = found := FloatMap.add_exn !found ~key:inew ~data:w in
         w

    include Comp
end
