(*
open Core

(* module PCluster = MapCluster.Make(Plucker) *)

module Vertex = PCluster.Vertex

    let comparedist (i, _) (j, _) = Int.compare i j

    module PSearch = ClusterSearch.Make(PCluster)
    include PSearch
    module XPlucker = XVert
    let collapse_path (start : PCluster.t) (path : Plucker.t list) =
      let rec collapse_path' (start : PCluster.t)
                (path : Plucker.t list)
                ((s,acc) : Plucker.Set.t * Plucker.Set.t list) : Plucker.Set.t list =
        match (path, acc) with
          ([], _) -> acc
        | (v :: vs, []) ->
           let (cnew,vnew) = PCluster.mutate_name start v in
           collapse_path' cnew vs (Plucker.Set.singleton vnew,  [Plucker.Set.singleton v])
        | (v :: vs, path_s :: rest) ->
           let nbrs = Set.union (PCluster.iNbrSet start v) (PCluster.oNbrSet start v) in
           (*let () = print_endline (Sexp.to_string (Plucker.Set.sexp_of_t nbrs)) in*)
           let (cnew, vnew) = PCluster.mutate_name start v in
           if Set.is_empty (Set.inter nbrs s)
           then collapse_path' cnew vs (Plucker.Set.add s vnew , (Plucker.Set.add path_s v) :: rest)
           else collapse_path' cnew vs (Plucker.Set.singleton vnew, (Plucker.Set.singleton v) :: path_s :: rest)
      in List.rev(collapse_path' start path (Plucker.Set.empty, []))



    type loc = Begin | Middle | End
    let loc_of_index ~(length : int) ~(index : int) =
      if index = 1 then Begin
      else if index = length then End
      else Middle

    let frozen n k = Vertex.Set.of_list
                       (List.map (List.range 1 (n+1))
                          ~f:(fun i -> Int.Set.of_list(List.map (List.range i (i+k)) ~f:(fun x -> match x mod n with 0 -> n | i -> i))))


    let gr (k:int) (n:int) : PCluster.t =
      let l = n - k in
      let range = List.range ~stop:`inclusive in
      let f (i, j) : Int.Set.t =
        if i <= l - j + 1
        then Int.Set.of_list (List.append (range (i + 1) k) (range (k + j) (k + j + i - 1)))
        else Int.Set.of_list (List.append (List.append (range 1 (i - (l - j + 1)) ) (range (i + 1) k)) (range (k + j) (k + l))) in
      let extra = Int.Set.of_list (range 1 k) in
      let grid = List.concat_map ~f:(fun i -> List.map ~f:(fun j -> (i,j)) (List.rev (range 1 l))) (range 1 k) in
      let edge (i,j)   =
        match l with
          1 -> (*In this case there is only one column *)
           (match (loc_of_index ~index:i ~length:k) with
            | Begin -> ( [extra] ,   [f(i+1,j)])
            | Middle -> ( [f(i-1,j)] ,  [f(i+1,j)])
            | End -> ( [f(i-1,j)] ,   [extra]))
        | _  ->
           (match (loc_of_index ~index:i ~length:k, loc_of_index ~index:(l+1 - j) ~length:l) with
            | (Begin, Begin) -> ( [f(i+1, j-1); extra]
                                ,  [f(i+1,j); f(i, j-1)] )
            | (Begin, Middle) -> (  [f(i+1, j-1); f(i, j+1)]
                                 ,  [f(i+1,j); f(i,j-1)])
            | (Begin, End) -> ( [f(i,j+1)] ,  [f(i+1,j)])
            | (Middle,Begin) -> (  [f(i-1,j); f(i+1,j-1)]
                                ,  [f(i+1,j); f(i, j-1)])
            | (Middle, Middle) -> (  [f(i-1,j); f(i, j+1); f(i+1,j-1)]
                                  ,  [f(i+1,j) ; f(i, j-1) ; f(i-1, j+1)])
            | (Middle, End) -> (  [f(i-1,j); f(i, j+1)]
                               ,  [f(i+1, j) ; f(i-1, j+1)])
            | (End, Begin) -> (  [f(i-1,j)] ,  [extra])
            | (End, Middle) | (End, End) -> (  [f(i-1,j)]
                                            ,  [f(i-1,j+1)]))
      in
      PCluster.of_nbrlist
        ~graph:( (extra, ([f (k,l)] , [f(1,l)])) :: (List.map ~f:(fun (i,j) -> (f(i,j) , edge(i,j))) grid))
        ~frozen:(frozen n k)
(*
    let dual n c =
      let full = Int.Set.of_list (List.range 1 (n+1)) in
      let compliment s = Set.diff full s in
      let rotate i = match (i + 1) mod n with 0 -> n | j -> j in
      let unrotate i = match (i - 1) mod n with 0 -> n | j -> j in
      let frz = frozen n (Set.length (Set.min_elt_exn (PCluster.frozen c))) in
      let newnbr v nbr =
        if Set.mem frz (compliment v)
        then
          let next = Int.Set.map ~f:rotate v in
          let prev = Int.Set.map ~f:unrotate v in
          (match (Set.mem (PCluster.oNbrSet c v) next, Set.mem (PCluster.iNbrSet c v) prev) with
           |(true, true)  -> {PCluster.oNbrs = Plucker.Set.map ~f:compliment (Set.remove iNbrs prev)
                             ;PCluster.iNbrs = Plucker.Set.map ~f:compliment (Set.remove oNbrs next)}
           |(true, false) -> {PCluster.oNbrs = Plucker.Set.map ~f:compliment iNbrs
                             ;PCluster.iNbrs = Plucker.Set.map ~f:compliment (Set.add(Set.remove oNbrs next) prev)}
           |(false, true) -> {PCluster.oNbrs = Plucker.Set.map ~f:compliment (Set.add(Set.remove iNbrs prev) next)
                             ;PCluster.iNbrs = Plucker.Set.map ~f:compliment oNbrs}
           |(false, false)-> {PCluster.oNbrs = Plucker.Set.map ~f:compliment (Set.add iNbrs next)
                             ;PCluster.iNbrs = Plucker.Set.map ~f:compliment (Set.add oNbrs prev)})
        else {PCluster.oNbrs = Plucker.Set.map iNbrs compliment; PCluster.iNbrs = Plucker.Set.map oNbrs compliment}
      in
      PCluster.of_quiver
        ~quiver:(Plucker.Map.of_alist_exn (List.map (Map.to_alist (PCluster.quiver c))
                                     (fun (v, nbrs) -> (compliment v, newnbr v nbrs))))
        ~frozen:(Plucker.Set.map (PCluster.frozen c) compliment)
      *)
    let boundaries_cluster k n graph =
      let a i j = if j < i then j else j+1 in
      let grkn_1 = gr k (n-1) in
      let grkn_1s = List.map (List.range 1 (n+1)) ~f:(fun i -> Map.keys (graph_all (PCluster.map grkn_1 ~f:(Int.Set.map ~f:(a i))))) in
      let found grkn i grkn_1s = if List.exists ~f:(fun grkn_1 -> PCluster.is_subset grkn_1 ~of_:grkn) grkn_1s then Some (i+1) else None in
      Map.mapi graph ~f:(fun ~key:grkn -> fun ~data -> List.filter_mapi grkn_1s  ~f:(found grkn))

    let graphviz_all_plucker_colors k n (color : int -> string): string =
      let graph = graph_all (gr k n) in
      let bndry = boundaries_cluster k n graph in
      let tab = "    " in
      let label_of_cluster (cluster : PCluster.t) =
        "\""
        ^ String.concat ~sep:"\n" (List.map ~f:Vertex.to_string (Set.to_list (PCluster.unfrozen cluster)))
        ^ "\"" in
      let edge (vin : PCluster.t) =
        let vinlabel = label_of_cluster vin in
        fun (vout, e ) -> String.concat ~sep:"" [ tab
                                                ; vinlabel
                                                ; " -> "
                                                ; label_of_cluster vout
                                                ; " ["
                                                ; "len = 3.0"
                                                ; "]"
                                                ; ";" ] in
      let vertlabel (embeds : int list) : string =
        " ["
        ^ (match embeds with
           | [] -> String.concat ~sep:"," ["color = black"; "penwidth = 1"]
           | [i] -> String.concat ~sep:"," ["color = " ^ (color i); "penwidth = 5"]
           | [i;j] -> String.concat ~sep:"," ["color = " ^ (color i); "style = filled"; "fillcolor = "^(color j); "penwidth = 5"]
           | _ -> String.concat ~sep:"," ["color = grey"; "penwidth = 3"])
        ^ "] " in
      let verts =
        Map.fold bndry
          ~init:[]
          ~f:( fun ~key ->
               fun ~data:embeds ->
               fun l -> String.concat ~sep:"" [tab; label_of_cluster key ; vertlabel embeds; ";"] :: l) in
      let edges =
        Map.fold graph
          ~init:[]
          ~f:(fun ~key -> fun ~data:es -> fun l -> (String.concat ~sep:"\n" (List.map ~f:(edge key) es)) :: l) in
      String.concat ~sep:"\n"
        [ "digraph G {"
        ; String.concat ~sep:"\n" verts
        ; String.concat ~sep:"\n" edges
        ; "}"]


      let color (i : int) =
        match i with
        | 0 -> "black"
        | 1 -> "cyan"
        | 2 -> "purple"
        | 3 -> "red"
        | 4 -> "orange"
        | 5 -> "yellow"
        | 6 -> "green"
        | 7 -> "blue"
        | 8 -> "magenta"
        | 9 -> "grey"
        | i -> invalid_arg ("Need a color for " ^ Int.to_string i)

(*
    let xpair cluster v =
      let {PCluster.oNbrs = oNbrs; PCluster.iNbrs = iNbrs} = PCluster.nbr cluster v
      in
      ( XPlucker.of_set ~top:(oNbrs) ~bot:(iNbrs)
      , XPlucker.of_list ~top:[v; Plucker.mutate v ~inp:(iNbrs) ~out:(oNbrs)] ~bot:(Set.to_list iNbrs))


    let xpairs cluster =
      Map.data (Map.filter_mapi (PCluster.quiver cluster)
                                ~f:(fun ~key -> fun ~data -> if Set.mem (PCluster.frozen cluster) key
                                                             then None
                                                             else Some(xpair cluster key)))
    module ShapePairSet = Set.Make(struct type t= XPlucker.t * XPlucker.t with sexp,compare end)

    let all_xpairs (graph) =
      Map.fold graph ~init:ShapePairSet.empty ~f:(fun ~key -> fun ~data -> fun acc -> Set.union (ShapePairSet.of_list (xpairs key)) acc)
 *)
(*

    let xmap (x : XPlucker.t) =
      Tensor.add
        (List.fold ~init:Tensor.zero ~f:Tensor.add (List.map (XPlucker.top_list x)
                                                             (fun a -> Tensor.of_mono (XPoly.of_mono 1 [x,1]) [a,1])))
        (List.fold ~init:Tensor.zero ~f:Tensor.add (List.map (XPlucker.bot_list x)
                                                             (fun a -> Tensor.of_mono (XPoly.of_mono (-1) [x,1]) [a,1])))
 *)
    module F = MapPolynomial.Make (struct include Plucker let to_string p = "a" ^ Plucker.to_string p
                                   let of_string s = invalid_arg "NYI for Plucker" end) (IntRing)
    module WF = MapForms.Make(F)
    let w c =
      List.fold ~init:WF.zero ~f:WF.add
                (List.concat_map (Set.to_list (PCluster.verts c))
                                 ~f:(fun a -> List.map (Set.to_list (PCluster.oNbrSet c a))
                                                    ~f:(fun b -> WF.wedge (WF.primitive 1 [[(a,1)]]) (WF.primitive 1 [[(b,1)]]))))

    let sum_form i l = List.fold ~init:WF.zero ~f:WF.add (List.map ~f:(fun p -> WF.primitive i [[(p,1)]]) l);;
    let x_to_L1 x = WF.add (sum_form 1 (XPlucker.top_list x)) (sum_form (-1) (XPlucker.bot_list x));;

    let l44 c26 = WF.wedge (w c26) (w c26)

    let l34 (c, v) =
      let x = xcoord c v in
      ( WF.add
        (sum_form 1 (XPlucker.top_list x))
        (sum_form (-1) (XPlucker.bot_list x))
      , WF.add (w c) (w (PCluster.mutate c v)))

    let delOl34 (c, v) =
      let x = xcoord c v in
      let v2 = Plucker.mutate v ~inp:(PCluster.iNbr c v) ~out:(PCluster.oNbr c v) in
      WF.wedge
        (WF.wedge (WF.add (sum_form 1 (XPlucker.top_list x)) (sum_form (-1) (XPlucker.bot_list x)))
                  (WF.add (sum_form 1 [v; v2]) (sum_form (-1) (XPlucker.bot_list x))))
        (WF.add (w c) (w (PCluster.mutate c v)))
    let l44Od (c,v) = WF.add (l44 c) (WF.scale (-1) (l44 (PCluster.mutate c v)))
 *)
