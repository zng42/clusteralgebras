(* open Core *)
module Functions = Algebra.MapLaurent.Make(Algebra.Indeces.Letter)(Algebra.Rings.Int)

module Make(Cluster: Cluster.S) =
  struct
    module Function = Algebra.MapLaurent.Make(Cluster.Vertex) (Algebra.Rings.Int)
(*
    (* Replaces *)
    let pullback f c v =
      let (d,w) = Cluster.mutate_name c v in
      Function.subs f (fun a -> if Cluster.Vertex.equal a v then w else a)

    (* pullback f c path gives the  *)
    let pullback f c path =
      let (target, new_path) = Cluster.mutate_multi_name c path in
      match Cluster.isomorphism ~frozen:false ~directed:true c target with
      | None -> invalid_arg "Path doesn't lead to an isomorphic cluster"
      | Some sigma -> invalid_arg "NYI"


    let mutate_poly (c,embed) (v: Cluster.Vertex.t) p =
        Functions.subs p ~f:(fun w -> if Int.equal (embed v) w
                                      then Functions.mult (Functions.of_mono 1 [(w,-1)])
                                             (Functions.add (Functions.of_mono 1 (List.map (Cluster.oNbrList c v) ~f:(fun a -> (embed a,1))))
                                                (Functions.of_mono 1 (List.map (Cluster.iNbrList c v) ~f:(fun a -> (embed a,1)))))
                                      else Functions.of_mono 1 [(w,1)])
    let mutate_name (c,embed, ps) v =
      let (cnew, vnew) = Cluster.mutate_name c v in
      ((cnew, embed (* TODO: Wrong if not FixedCluster*) ,List.map ~f:(mutate_poly (c,embed) v) ps), (v, vnew))

    let mutate (c,embed,p) v = fst(mutate_name (c,embed,p) v)

    module Node =
      struct
        module T = struct type t = Cluster.t * embedding * (Functions.t list)
                          let sexp_of_t(c,embed,p) = Cluster.sexp_of_t c
                          let t_of_sexp s = (Cluster.t_of_sexp s, (fun _ -> invalid_arg "TODO"), [Functions.one])
                          let compare (_,_,p1) (_,_,p2)  = List.compare Functions.compare p1 p2 end
        include T
        include Comparable.Make(T)
        type edge = Cluster.edge
        let edge_of_sexp = Cluster.edge_of_sexp
        let sexp_of_edge = Cluster.sexp_of_edge
        let n (c,embed,p) = List.map (Core.Set.to_list (Cluster.unfrozen c)) (mutate_name (c,embed,p))
        let label (c,embed,p) = String.concat ~sep:" , " (List.map ~f:Functions.to_string p)
        let id (c,embed,p) = String.concat ~sep:";" (List.map ~f:Functions.to_string p)
      end
    include SearchBase.Make(Node)
 *)
  end
