open Core

type vertex = Int.Set.t [@@deriving compare, sexp]

module T = struct
  type t = vertex
  let compare = compare_vertex
  let sexp_of_t = sexp_of_vertex
  let t_of_sexp = vertex_of_sexp
  let hash s = Set.fold ~init:0 ~f:(lxor) (Int.Set.map s ~f:Int.hash)
end
include T

module Comp = Comparable.Make(T)

let of_list l = Int.Set.of_list l

let int_to_digit i =
  if Int.between ~low:1 ~high:9 i then Char.of_string(Int.to_string i)
  else if Int.between ~low:10 ~high:35 i then Char.of_int_exn (Char.to_int 'a' + (i-10))
  else invalid_arg ("Can't process int larger than 35 "^ Int.to_string i)

let digit_to_int c =
  if Char.is_digit c then Int.of_string (Char.to_string c)
  else if Char.is_lowercase c then Char.to_int c - Char.to_int 'a' + 10
  else invalid_arg ("Only mapped 1-9, a-z to int" ^ Char.to_string c)


let id (v : t) = String.of_char_list (List.map ~f:int_to_digit (Int.Set.to_list v))
let to_string v = "p" ^ String.of_char_list (List.map ~f:int_to_digit (Int.Set.to_list v))
let pp ppf v = Format.fprintf ppf "%s" (to_string v)

let of_string s =
  match String.to_list s with
  | 'p'::nums -> Int.Set.of_list (List.map nums ~f:digit_to_int)
  | _ -> invalid_arg ("Plucker coords begin with p " ^ s)

let weight _ = 1


(* The only mutation that preserves plucker relations is with two inputs and two outputs*)
let canExchange (_) ~(inp : int Comp.Map.t) ~(out : int Comp.Map.t) =
  (2 = Comp.Map.length inp) && (2 = Comp.Map.length out)
  && Comp.Map.for_all inp ~f:(fun d -> Int.equal 1 (Int.abs d))
  && Comp.Map.for_all out ~f:(fun d -> Int.equal 1 (Int.abs d))

let mutate (v : t) ~(inp: int Comp.Map.t) ~(out: int Comp.Map.t) =
  let j : Int.Set.t = List.fold (Map.keys out) ~f:Int.Set.inter ~init:v in
  let newelts = Int.Set.of_list (List.map (Map.keys out)
                                          ~f:(fun s -> match Int.Set.choose (Int.Set.diff s v) with
                                                       | None -> let ()  = print_string (
                                                                              " Mutate: "
                                                                              ^ (to_string v) ^"\n"
                                                                              ^ "Out: "
                                                                              ^ String.concat ~sep:" " (List.map (Map.keys out) ~f:to_string) ^ "\n"
                                                                              ^ "In: "
                                                                              ^ String.concat ~sep:" " (List.map (Map.keys inp) ~f:to_string) ^ "\n"
                                                                              ^ "\n")
                                                                 in raise Division_by_zero
                                                       | Some i -> i) ) in
  Int.Set.union newelts j


let a ~i p = Int.Set.map p ~f:(fun j -> if j >= i then j+1 else j)

let rotate n p = Int.Set.map p ~f:(fun j -> match (j+1) mod n with 0 -> n | jnew -> jnew)

module IntPoly = Algebra.Rings.IntPoly
let rec det mat =
  match mat with
  | [] -> IntPoly.one
  | row::rest ->
     List.foldi row ~init:IntPoly.zero ~f:(fun i acc x -> IntPoly.add acc
                                                            (IntPoly.mult (IntPoly.scale (Int.pow (-1) i) x) (
                                                                 det (List.map rest ~f:(List.filteri ~f:(fun j _ -> not(i = j)))))))

let apply matrix v = det(List.map matrix ~f:(List.filteri ~f:(fun i _ -> Set.mem v (i+1))))
let eval_mat v mat = Algebra.Rings.LacamlMat.det_minor ~l:(Core.Set.to_list v) mat


include Comp
