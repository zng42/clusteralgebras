open Algebra
open Core
module ParamPoly = Flint.BigIntMPoly.Make(struct
                        let vars = ["a";"b";"c";"d";"x";"y";"v";"w";"a'";"b'";"c'";"d'";"x'";"y'";"z'";"v'";"w'"]
                        let ord = Flint.BigIntMPoly.Ord.LEX end)

module R = Rationals.Make(ParamPoly)
module Vert = RingVertex.Make(R)
module Clust = IMapCluster.Make(Vert)
module CSearch = SearchBase.Make(Clust)

let is_fat c v = Set.length(Clust.index_of c v) > 1
let is_nbr c v w = not(Clust.edge_weight c ~inp:v ~out:w = 0)

let (w11a,w11b,w21,w12a,w12b,w22,w13a,w13b,w23,b1a,b1b,b2,c1a,c1b,c2,d1a,d1b,d2,e1a,e1b,e2) =
  match List.init 21 ~f:Fn.id with
  | [w11a;w11b;w21;w12a;w12b;w22;w13a;w13b;w23;b1a;b1b;b2;c1a;c1b;c2;d1a;d1b;d2;e1a;e1b;e2] ->
     (w11a,w11b,w21,w12a,w12b,w22,w13a,w13b,w23,b1a,b1b,b2,c1a,c1b,c2,d1a,d1b,d2,e1a,e1b,e2)
  | _ -> invalid_arg "Can't happen"

let vname =
  let vmap = Int.Map.of_alist_exn [w11a,"W11"; w11b,"W11"
                                  ;w12a,"W12"; w12b,"W12"
                                  ;w13a,"W13"; w13b,"W13"
                                  ; w21,"w21"
                                  ; w22,"w22"
                                  ; w23,"w23"
                                  ; b1a,"B1";b1b,"B1"
                                  ; b2,"b2"
                                  ; c1a,"C1";c1b,"C1"
                                  ; c2,"c2"
                                  ; d1a,"D1";d1b,"D1"
                                  ; d2,"d2"
                                  ; e1a,"E1";e1b,"E1"
                                  ; e2,"e2" ]
  in (fun vis -> vis
                 |> List.map ~f:(fun i -> match Map.find vmap i with Some name -> name | None -> invalid_arg ("Missing index "^Int.to_string i))
                 |> List.dedup_and_sort ~compare:String.compare |> String.concat ~sep:"_");;

let labelSquare =
  let l = [[w11a;w11b],"(b*v+b*w)/(a*c*d)"
          ;[w21],"(b^2*y*v^2)/(c^2*d^2)"
          ;[w12a;w12b],"1/(c*d)"
          ;[w22],"1/c^2"
          ;[b1a;b1b],"a*b*y*v*w/(c*d)"
          ;[b2],"a^2*x*y*v^2/c^2"
          ;[c1a;c1b],"1/(a*b)"
          ;[c2],"1/a^2"
          ;[w13a;w13b],"(-1*b'*v'-1*b'*w')/(a'*c*d)"
          ;[w23],"(-v'^2*x'-2*v'*w'*x'-w'^2*x'-w'^2*y')/c^2"
          ;[d1a;d1b],"1/(a'*b')"
          ;[d2],"1/a'^2"
          ;[e1a;e1b],"-1*a'*b'*y'*v'*w'/(c*d)"
          ;[e2],"a'^2*x'*y'*v'^2/c^2"]
  in l |> List.concat_map ~f:(fun (is,v) -> is |> List.map ~f:(fun i -> (i,Vert.of_string v)))

let qB2square = Clust.of_edgelist
                  ~graph:[ w11a,w21; w11b,w21 (* Interior Right Triangle *)
                         ; w21,w22; w12a,w11a;w12b,w11b (* Edge A *)
                         ; b1a,w11a;b1b,w11b; w21,b1a;w21,b1b; b2,w21; w22,b2 (* Edge B *)
                         ; w11a,c1a;w11b,c1b; c1a,w12a;c1b,w12b; c2,w11a;c2,w11b; w21,c2 (* Edge C *)
                         ; w13a,w23;w13b,w23 (* Interior Left Triangle *)
                         ; w22,w13a;w22,w13b;  w13a,w12a;w13b,w12b; w23,w22 (* Edge A *)
                         ; w12a,d1a;w12b,d1b; d1a,w13a;d1b,w13b; d2,e2; w23,d2 (* Edge D *)
                         ; e2,w23; w23,e1a;w23,e1b; e1a,w13a;e1b,w13b] (* Edge E *)
                  ~frozen:(Int.Set.of_list [b1a;b1b;b2;c1a;c1b;c2;d1a;d1b;d2;e1a;e1b;e2])
                  ~label:labelSquare
let muP = [[w23];[w13a;w13b]; [w21]; [w11a;w11b]]
let muFlipCore = [[w13a;w13b] ; [w23]; [w12a;w12b]; [w22]; [w11a;w11b]; [w21]; [w13a;w13b]; [w23]; [w12a;w12b]; [w22]; [w13a;w13b]; [w23]]
let mu03 = [ [w11a;w11b]; [w12a;w12b]; [w13a;w13b]; [w11a;w11b]; [w12a;w12b]; [w11a;w11b]; [w13a;w13b]; [w12a;w12b]; [w11a;w11b]
           ; [w21]; [w22]; [w23]; [w21]; [w22]; [w21]; [w23]; [w22]; [w21]]
let muFlipGrouped = muP@ muFlipCore @ mu03
let muFlip = List.concat muFlipGrouped
