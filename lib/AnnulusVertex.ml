open Core

let int_to_digit i =
  if Int.between ~low:0 ~high:9 i then Char.of_string(Int.to_string i)
  else if Int.between ~low:10 ~high:35 i then Char.of_int_exn (Char.to_int 'a' + (i-10))
  else invalid_arg ("Can't process int larger than 35 "^ Int.to_string i)

let digit_to_int c =
  if Char.is_digit c then Int.of_string (Char.to_string c)
  else if Char.is_lowercase c then Char.to_int c - Char.to_int 'a' + 10
  else invalid_arg ("Only mapped 1-9, a-z to int" ^ Char.to_string c)



module T = struct
  (* Inner(i,j) is arc traveling counterclockwise and Outer(i,j) is traveling clockwise
   * This was the "left" side of the arc is inside the annulus on either boundary and logic for inner/outer should be symmetric*)
  type t =
    Inner of int * int (* Inner(i,j) is arc traveling counter clockwise from i to j, So Inner(1,3) and Inner(3,1) are distinct *)
  | Outer of int * int (* Outer(i,j) is arc tracveling clockwise  *)
  | Cross of int * int * int (* inner,outer, winding*) [@@deriving compare,sexp]


  let to_string v =
    match v with
    | Inner (i,j) -> "i" ^ String.of_char_list (List.map ~f:int_to_digit [i;j])
    | Outer (i,j) -> "o" ^ String.of_char_list (List.map ~f:int_to_digit [i;j])
    | Cross (i,j,wind) -> "c" ^ String.of_char_list (List.map ~f:int_to_digit [i;j]) ^ "_" ^ Int.to_string(wind)

  let pp ppf v = Format.fprintf ppf "%s" (to_string v)
  let id (v : t) = to_string v
  let of_string s =
    match String.to_list s with
    | ['i';di;dj] -> let (i,j) = (digit_to_int di, digit_to_int dj) in
                     if i < j then Inner(i,j) else Inner(j,j)
    | ['o';di;dj] -> let (i,j) = (digit_to_int di, digit_to_int dj) in
                     if i < j then Outer(i,j) else Outer(j,j)
    | 'c'::di::dO::'_'::dwind -> Cross(digit_to_int di, digit_to_int dO, Int.of_string (String.of_char_list dwind))
    | _ -> invalid_arg ("String doesn't describe an arc in an annulus " ^ s)

  let hash v = String.hash (to_string v)
end

include T
module Comp = Comparable.Make(T)

let weight v =
  match v with
  | Inner(_,_) -> 1
  | Outer(_,_) -> 1
  | Cross(_,_,wind) -> wind

let canExchange v ~inp ~out =
  match v with
  | Inner (_,_) -> (2= Comp.Map.length inp) && (2 = Comp.Map.length out)
  | Outer(_,_) ->  (2= Comp.Map.length inp) && (2 = Comp.Map.length out)
  | Cross(_,_,_) -> true

(* Returns true if test is strictly between left and right on circle *)
let cycBetween ~left ~right ~test =
  if left <= right
  then left <= test && test < right
  else not(right < test && test <= left)


let fCycCross v w =
  match (v,w) with
  | (Inner(_,_), Outer(_,_)) -> false
  | (Outer(_,_), Inner(_,_)) -> false
  | (Inner(l,r), Cross(i,_,_)) -> cycBetween ~left:l ~right:r ~test:i
  | (Cross(i,_,_), Inner(l,r)) -> cycBetween ~left:l ~right:r ~test:i
  | (Outer(l,r), Cross(_,o,_)) -> cycBetween ~left:l ~right:r ~test:o
  | (Cross(_,o,_), Outer(l,r)) -> cycBetween ~left:l ~right:r ~test:o
  | (Inner(l1,r1), Inner(l2,r2)) -> ((cycBetween ~left:l1 ~right:r1 ~test:l2 && cycBetween ~left:r1 ~right:l1 ~test:r2)
                                     || (cycBetween ~left:l2 ~right:r2 ~test:l1 && cycBetween ~left:r2 ~right:l2 ~test:r1) )
  | (Outer(l1,r1), Outer(l2,r2)) -> ((cycBetween ~left:l1 ~right:r1 ~test:l2 && cycBetween ~left:r1 ~right:l1 ~test:r2)
                                     || (cycBetween ~left:l2 ~right:r2 ~test:l1 && cycBetween ~left:r2 ~right:l2 ~test:r1) )
  | (Cross(i1,o1,w1), Cross(i2,o2,w2)) -> ((Int.abs(w1-w2) > 1)
                                           || (Int.abs(w1-w2)=1 && not(i1 = i2) && not(o1=o2)))


let mutate v ~inp:_ ~out =
  let rec decide v l =
    match v with
    | Cross(i,o,w) ->
       (match l with
        | [(Cross(_,_,_),2)] -> Cross(i,o,w+2)
        | [(Inner(i1,i2),1);(Outer(o1,o2),1)] ->
           (match (i=i1, i=i2, o=o1,o=o2) with
            | (true ,false, true ,false) -> Cross(i2,o2,w-1)
            | (true ,false, true ,true ) -> Cross(i2,o2,w-2)
            | (true ,true , true ,false) -> Cross(i2,o2,w-2)
            | (true ,true , true ,true ) -> Cross(i,o,w-2)

            | (false,true , true ,false) -> invalid_arg ("Invalid rectangle with: " ^ String.concat ~sep:"," (List.map ~f:Int.to_string [i1;i2;o1;o2]))
            | (true, false, false,true ) -> invalid_arg ("Invalid rectangle with: " ^ String.concat ~sep:"," (List.map ~f:Int.to_string [i1;i2;o1;o2]))
            | (false,true , true ,true ) -> invalid_arg ("Invalid rectangle with: " ^ String.concat ~sep:"," (List.map ~f:Int.to_string [i1;i2;o1;o2]))
            | (true ,true , false,true ) -> invalid_arg ("Invalid rectangle with: " ^ String.concat ~sep:"," (List.map ~f:Int.to_string [i1;i2;o1;o2]))
            | (false,true , false,true ) -> invalid_arg ("Invalid rectangle with: " ^ String.concat ~sep:"," (List.map ~f:Int.to_string [i1;i2;o1;o2]))

            | (_,_,false,false) -> invalid_arg "Diagonal must share end with out edge on outer circle"
            | (false,false,_,_) -> invalid_arg "Diagonal must share end with out edge on inner circle")
        | [(Outer(o1,o2),1);(Inner(i1,i2),1)] -> decide v [(Inner(i1,i2),1);(Outer(o1,o2),1)]
        | [(Cross(i1,o1,_),1);(Cross(i2,o2,_),1)] ->
           (match (i=i1, i=i2, o=o1,o=o2) with
            | (false,true , true ,false) -> Cross(i1,o2,w+1)
            | (true ,false, false,true ) -> Cross(i2,o1,w+1)

            | (true ,true , true ,false) -> Cross(i1,o2,w+1)
            | (true ,false, true ,true ) -> Cross(i2,o1,w+1)

            | (false,true , true ,true ) -> Cross(i1,o2,w+1)
            | (true ,true , false,true ) -> Cross(i2,o1,w+1)

            | (false,true , false,true ) -> invalid_arg ("Invalid rectangle with: " ^ String.concat ~sep:"," (List.map ~f:Int.to_string [i1;i2;o1;o2]))
            | (true ,false, true ,false) -> invalid_arg ("Invalid rectangle with: " ^ String.concat ~sep:"," (List.map ~f:Int.to_string [i1;i2;o1;o2]))
            | (true ,true , true ,true ) -> invalid_arg ("Invalid rectangle with: " ^ String.concat ~sep:"," (List.map ~f:Int.to_string [i1;i2;o1;o2]))
            | (_,_,false,false) -> invalid_arg "Diagonal must share end with out edge on outer circle"
            | (false,false,_,_) -> invalid_arg "Diagonal must share end with out edge on inner circle")
        | _ -> invalid_arg "Not how edges on annulus should be arranged"
       )
    | Inner (_,_) -> invalid_arg "NYI"
    | Outer (_,_) -> invalid_arg "NYI"
  in decide v (Map.to_alist out)

(*  | Inner (i,j) ->
     (match Map.keys out with
      | [Inner(v1,v2); Inner (v3,v4)] ->
         if i = v1
         then Inner (v2,v4)
         then Inner (v4,v2)
         let newVs = Set.remove (Set.remove (Int.Set.of_list [v1;v2;v3;v4]) i) j in
         Inner (Set.min_elt_exn newVs, Set.max_elt_exn newVs)
     )
 *)


include Comp
