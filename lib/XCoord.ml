open Core

module type S =
sig
  include Vertex.S
  module ACoord : Vertex.S

  module InvEq : Comparable.S with type t = t

  val of_list : top:ACoord.t list -> bot:ACoord.t list -> t
  val of_nbr : int ACoord.Map.t -> t
  val of_map : top: int ACoord.Map.t -> bot: int ACoord.Map.t -> t
  val of_pow_list : (ACoord.t*int) list -> t

  val length: t -> (int * int)
  val weight : t -> int
  val top : t -> int ACoord.Map.t
  val bot : t -> int ACoord.Map.t

  val top_list : t -> ACoord.t list
  val bot_list : t -> ACoord.t list
  val pow_list : t -> (ACoord.t * int) list
  val exponent_of : t -> ACoord.t -> int
  val mem : t -> ACoord.t -> bool

  val map : t -> f:(ACoord.t -> ACoord.t) -> t
  val exists : t -> f:(int -> ACoord.t -> bool) -> bool
  val for_all : t -> f:(int -> ACoord.t -> bool) -> bool
  val eval : t -> f:(ACoord.t -> 'a) -> one:'a -> mul:('a -> 'a -> 'a) -> div:('a -> 'a -> 'a) -> 'a
  val eval_fun : t -> f:(ACoord.t -> 'a -> float) -> ('a -> float)

  val inv : t -> t
  val mult : t -> t -> t
  val pow : t -> int -> t
  val one : t
end

open Core

module Make(A : Vertex.S) : S with module ACoord = A =
struct
  module ACoord = A

  type xcoord = {top: int ACoord.Map.t ; bot: int ACoord.Map.t} [@@deriving sexp]


  module ACoordMapSet = Set.Make(struct type t = int ACoord.Map.t [@@deriving sexp, compare] end)
  let compare_inveq x1 x2 =
    ACoordMapSet.compare
      (ACoordMapSet.of_list [x1.top; x1.bot])
      (ACoordMapSet.of_list [x2.top ; x2.bot])

  let compare_invNeq x1 x2 =
    match ACoord.Map.compare_direct Int.compare x1.top x2.top with
    | 0 -> ACoord.Map.compare_direct Int.compare x1.bot x2.bot
    | c -> c

    module T =
      struct
        type t = xcoord
        let sexp_of_t = sexp_of_xcoord
        let t_of_sexp = xcoord_of_sexp
        let compare = compare_invNeq
      end
    include T

    module CXCoord = Comparable.Make(T)
    module InvEq =
      struct
        type t= xcoord
        include Comparable.Make(struct type t= xcoord let sexp_of_t = sexp_of_xcoord let t_of_sexp = xcoord_of_sexp let compare = compare_inveq end)
      end

    (* Create *)
    let simplify x =
      let (top, bot) =
        ACoord.Map.fold2 x.top x.bot ~init:(ACoord.Map.empty, ACoord.Map.empty)
          ~f:(fun ~key ~data (top,bot) ->
            match data with
            | `Left v -> if v > 0
                         then (Map.add_exn top ~key:key ~data:v, bot)
                         else (top, Map.add_exn bot ~key:key ~data:(-v))
            | `Right v ->
               if v > 0
               then (top, Map.add_exn bot ~key:key ~data:v)
               else (Map.add_exn top ~key:key ~data:(-v), bot)
            | `Both(vtop, vbot) ->
               (match Ordering.of_int(Int.compare vtop vbot) with
                | Less -> (top, Map.add_exn bot ~key:key ~data:(vbot-vtop))
                | Greater -> (Map.add_exn top ~key:key ~data:(vtop-vbot), bot)
                | Equal -> (top,bot)))
      in {top= top; bot = bot}

    let of_list ~top ~bot = simplify { top = ACoord.Map.of_alist_fold  ~init:0 ~f:(+) (List.map top ~f:(fun a -> (a,1)))
                            ; bot = ACoord.Map.of_alist_fold  ~init:0 ~f:(+) (List.map bot ~f:(fun a -> (a,1))) }
    let of_map ~top ~bot = simplify {top = top ; bot = bot}
    let of_nbr (nbr : int ACoord.Map.t) =
      { top = ACoord.Map.filter_map nbr ~f:(fun d -> if d > 0 then Some d else None)
      ; bot = ACoord.Map.filter_map nbr ~f:(fun d -> if d < 0 then Some (-d) else None)}
    let of_pow_list pows = of_nbr (ACoord.Map.of_alist_reduce ~f:(+) pows)

    (* View *)
    let top x = x.top
    let bot x = x.bot
    let top_list x = List.concat_map (ACoord.Map.to_alist x.top) ~f:(fun (a,d) -> List.map (List.range 0 d) ~f:(fun _ -> a))
    let bot_list x = List.concat_map (ACoord.Map.to_alist x.bot) ~f:(fun (a,d) -> List.map (List.range 0 d) ~f:(fun _ -> a))
    let pow_list x = (ACoord.Map.to_alist x.top) @ (List.map (ACoord.Map.to_alist x.bot) ~f:(fun (x,p) -> (x,-p)))
    let exponent_of x a =
      match (Map.find x.top a, Map.find x.bot a) with
      | (None,None) -> 0
      | (Some e, None) -> e
      | (None, Some e) -> -e
      | (Some e, Some f) -> e-f
    let mem x a = not(exponent_of x a = 0)

    let length x = ( ACoord.Map.fold x.top ~init:0 ~f:(fun ~key:_ -> fun ~data -> fun acc -> data + acc)
                   , ACoord.Map.fold x.bot ~init:0 ~f:(fun ~key:_ -> fun ~data -> fun acc -> data + acc))
    let weight x = ACoord.Map.fold x.top ~init:0 ~f:(fun ~key:x -> fun ~data -> fun acc ->  data * ACoord.weight x + acc)

    (* Vertex Methods *)
    let canExchange _(*x*) ~inp:_ ~out:_ = true
    let mutate _ ~inp:_ ~out:_ = invalid_arg "Need to mutate at not just x"

    let to_string_base a_to_string x =
      let apow_to_string = (fun (a, d) -> a_to_string a ^ "^" ^ Int.to_string d) in
      String.concat ~sep:""
        [ "("
        ; String.concat ~sep:" * " (List.map ~f:apow_to_string (ACoord.Map.to_alist x.top))
        ; ")"
        ; " / "
        ; "("
        ; String.concat ~sep:" * " (List.map ~f:apow_to_string (ACoord.Map.to_alist x.bot))
        ; ")"]

    let id  = to_string_base (fun a -> "a" ^ ACoord.id a)
    let to_string = to_string_base ACoord.to_string
    let pp ppf v = Format.fprintf ppf "%s" (to_string v)

    let remove_parens s =
      match Option.bind ~f:(String.chop_prefix ~prefix:"(") (String.chop_suffix ~suffix:")" (String.strip s)) with
      | Some out -> out
      | None -> s
    let parse_power s =
      match String.strip s with
      | "" -> None
      | "1" -> None
      | s -> Some
         (match String.rindex s '^' with
          | Some i -> ( ACoord.of_string (String.strip (String.prefix s i))
                      , Int.of_string (String.strip ~drop:(fun c -> Char.(c=')') || Char.(c='(') || Char.is_whitespace c) (String.drop_prefix s (i+1))))
          | None -> (ACoord.of_string (String.strip s),1)
         )

    let of_string s =
      match String.split ~on:'/' s with
      | [top;bot] ->
         let top_list = List.filter_map ~f:parse_power (String.split ~on:'*' (remove_parens top)) in
         let bot_list = List.filter_map ~f:parse_power (String.split ~on:'*' (remove_parens bot)) in
         of_map ~top:(ACoord.Map.of_alist_fold ~init:0 ~f:(+) top_list) ~bot:(ACoord.Map.of_alist_fold ~init:0 ~f:(+) bot_list)
      | [top] -> of_map ~top:(ACoord.Map.of_alist_fold ~init:0 ~f:(+) (List.filter_map ~f:parse_power (String.split ~on:'*' (remove_parens top))))
                        ~bot:(ACoord.Map.empty)
      | _ -> invalid_arg ("XCoord should have a single / " ^ s)

    let map (x : t) ~(f : ACoord.t -> ACoord.t) =
      let map_keys m f = ACoord.Map.of_alist_reduce ~f:(+) (List.map (ACoord.Map.to_alist m) ~f:(fun (v,d) -> (f v, d))) in
      simplify {top = map_keys x.top f; bot = map_keys x.bot f}
    let exists (x : t) ~(f : int -> ACoord.t -> bool) =
      ACoord.Map.existsi x.top ~f:(fun ~key ~data -> f data key) || ACoord.Map.existsi x.bot ~f:(fun ~key ~data -> f data key)
    let for_all (x : t) ~(f : int -> ACoord.t -> bool) =
      ACoord.Map.for_alli x.top ~f:(fun ~key ~data -> f data key) && ACoord.Map.for_alli x.bot ~f:(fun ~key ~data -> f data key)

    let eval (x: t) ~f ~one ~mul ~div =
      div (ACoord.Map.fold x.top ~init:one ~f:(fun ~key ~data acc -> let value = f key in Fn.apply_n_times ~n:data (mul value) acc))
          (ACoord.Map.fold x.bot ~init:one ~f:(fun ~key ~data acc -> let value = f key in Fn.apply_n_times ~n:data (mul value) acc))


    let eval_fun (x: t) ~(f : ACoord.t -> 'a -> float) =
      (fun u -> ACoord.Map.fold x.top ~init:1.0 ~f:(fun  ~key ~data acc -> let value = f key u in Fn.apply_n_times ~n:data (fun x -> value *. x) acc )
      /. ACoord.Map.fold x.bot ~init:1.0 ~f:(fun  ~key ~data acc-> let value = f key u in Fn.apply_n_times ~n:data (fun x -> value *. x) acc))

    let inv x = {top = x.bot; bot = x.top}
    let mult x y =
      simplify {top = Map.merge x.top y.top ~f:(fun ~key:_ -> function `Both (dx, dy) -> Some(dx+dy) | `Left dx -> Some dx | `Right dy -> Some dy)
               ;bot = Map.merge x.bot y.bot ~f:(fun ~key:_ -> function `Both (dx, dy) -> Some(dx+dy) | `Left dx -> Some dx | `Right dy -> Some dy)}

    let one = {top = ACoord.Map.empty; bot = ACoord.Map.empty}
    let pow x n = Fn.apply_n_times ~n:(Int.abs n) (mult (if n > 0 then x else inv x)) one
    include CXCoord
  end
