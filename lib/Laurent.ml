open Core
module Vert = LaurentVertex

module Clust =
  struct
    include MapCluster.Make(LaurentVertex)
    let of_fixed c =
      let v_of_fixed i = LaurentVertex.of_mono 1 [(i,1)] in
      of_list ~graph:(List.map (Core.Map.to_alist (FixedCluster.quiver c))
                               ~f:(fun (v,nbr) -> (v_of_fixed v,
                                                   LaurentVertex.Map.of_alist_exn
                                                     (List.map (Core.Map.to_alist nbr) ~f:(fun (v,d) -> (v_of_fixed v, d))))))
              ~frozen:(LaurentVertex.Set.map (FixedCluster.frozen c) ~f:(v_of_fixed))
  end

include CAlg.Extend(struct module Vert = Vert module Clust = Clust end)
