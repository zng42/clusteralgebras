open Core
module IntLaurent = Algebra.Rings.IntLaurent

include IntLaurent

let weight = IntLaurent.degree
let canExchange _ ~inp:_ ~out:_ = true (* Map.is_empty(inp) || Map.is_empty(out)*)

let mutate v ~inp ~out =
  let process_nbr m =
    Core.Map.fold m ~init:IntLaurent.one ~f:(fun ~key ~data acc -> Fn.apply_n_times ~n:(Int.abs data) (IntLaurent.mult key) acc) in
  let rhs = IntLaurent.add (process_nbr inp) (process_nbr out) in
  match IntLaurent.inv v with
  | Some inv_v -> IntLaurent.mult rhs inv_v
  | None ->
     let (q,r) = IntLaurent.div_rem rhs v in
(*
     let _ = print_endline ("RHS  : " ^ IntLaurent.to_string rhs) in
     let _ = print_endline ("V    : " ^ IntLaurent.to_string v) in
     let _ = print_endline ("(q,r): " ^ IntLaurent.to_string q ^ "," ^ IntLaurent.to_string r) in
 *)
     if IntLaurent.equal r IntLaurent.zero
     then q
     else invalid_arg "Laurent phenominon failed"


let dvector n v =
  let denom = denominator v in
  if equal one denom
  then List.init n ~f:(fun i -> -1 * (idegree v ~i:i))
  else List.init n ~f:(fun i -> idegree denom ~i:i)


let id = IntLaurent.to_string
let pp ppf v = Format.fprintf ppf "%s" (IntLaurent.to_string v)

(* TOOD: Determine way to specify how many variables are used *)
let to_string v =
  String.concat ~sep:"," (List.map (dvector 3 v) ~f:Int.to_string)
