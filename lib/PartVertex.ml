open Core


let int_to_digit i =
  if Int.between ~low:1 ~high:9 i then Char.of_string(Int.to_string i)
  else if Int.between ~low:10 ~high:35 i then Char.of_int_exn (Char.to_int 'a' + (i-10))
  else invalid_arg ("Can't process int larger than 35 "^ Int.to_string i)

(* let digit_to_int c =
  if Char.is_digit c then Int.of_string (Char.to_string c)
  else if Char.is_lowercase c then Char.to_int c - Char.to_int 'a' + 10
  else invalid_arg ("Only mapped 1-9, a-z to int" ^ Char.to_string c)
 *)
type t = int list list [@@deriving sexp]

let repeat n x = List.map (List.range 0 n) ~f:(fun _ -> x)
let rec diags (l : int list list) : int list list =
  let rec zip_small l1 l2 =
    match (l1, l2) with
    | ([], l2) -> l2
    | (x::xs , l :: ls) -> (x :: l) :: (zip_small xs ls)
    | (_::_, []) -> invalid_arg "List 1 must be shorter then list 2 to zip"
  in
  match l with
  | [] -> []
  | [r] -> List.map r ~f:(fun x -> [x])
  | (x :: xs) :: rs -> [x] :: (zip_small xs (diags rs))
  | [] :: _ -> invalid_arg "Tableaux shouldn't have empty rows"


let compare p1 p2 =
  List.compare (List.compare Int.compare) p1 p2

module Comp = Comparable.Make(struct type t = int list list [@@deriving sexp] let compare = compare end)

let canExchange _ ~inp:_ ~out:_ = true


let partition_map2 l1 l2 ~f = List.map2_exn l1 l2 ~f:(List.map2_exn ~f:f)
let rev_cmp i j = Int.compare j i
let is_valid p = List.for_all p ~f:(List.is_sorted ~compare:rev_cmp)
                 && List.for_all (List.transpose_exn p) ~f:(List.is_sorted ~compare:rev_cmp)
let weight p = match List.hd_exn (List.hd_exn p) with 0 -> 1 | w -> w


let show_table p = print_endline(String.concat ~sep:"\n" (List.map p ~f:(fun r -> String.concat ~sep:" " (List.map r ~f:Int.to_string))) ^ "\n")
let show_tables (s : Comp.Set.t) =
  let ps_chunks = List.chunks_of ~length:10 (Set.to_list s) in
  let stringedrows : string list list list =
    List.map ps_chunks ~f:(fun ps -> List.map ps ~f:(fun p -> List.map p ~f:(fun r -> String.concat ~sep:" " (List.map r ~f:(fun i -> sprintf "%3d" i))))) in
  List.iter stringedrows ~f:(fun srows -> (List.iter (List.transpose_exn srows) ~f:(fun row -> print_endline (String.concat ~sep:"   " row))
                                       ; Out_channel.newline stdout))
let show_tables_list (l : t list ) =
  let ps_chunks = List.chunks_of ~length:10 l in
  let stringedrows : string list list list =
    List.map ps_chunks ~f:(fun ps -> List.map ps ~f:(fun p -> List.map p ~f:(fun r -> String.concat ~sep:" " (List.map r ~f:(fun i -> sprintf "%3d" i))))) in
  List.iter stringedrows ~f:(fun srows -> (List.iter (List.transpose_exn srows) ~f:(fun row -> print_endline (String.concat ~sep:"   " row))
                                       ; Out_channel.newline stdout))
let to_vars k n v =
  let w = weight v in
  let at l i = if i >= (n-k) then 0
               else if i < 0 then w
               else List.nth_exn l i
  in List.mapi v ~f:(fun i r -> List.init n ~f:(fun j -> at r ((n-k)+i-(j+1)) - at r ((n-k)+i-j)))
let vars_to_string m =
  m |> List.map ~f:(fun v -> v |> List.mapi ~f:(fun i v -> List.init v ~f:(fun _ -> int_to_digit (i+1)) |> String.of_char_list)
                             |> String.concat |> String.to_list)
  |> List.transpose_exn
  |> List.map ~f:String.of_char_list
  |> String.concat ~sep:"w"

let id p =
  String.concat ~sep:"-" (List.map p ~f:(fun r -> String.concat ~sep:"," (List.map r ~f:Int.to_string)))

let to_string p =
  let k = List.length p in
  let n = k + List.length (List.hd_exn p) in
  p |> to_vars k n |> vars_to_string |> fun s -> "a"^s (* This is nolonger parsable back to table but probably fine *)
  (* String.concat ~sep:"" (List.map ~f:Int.to_string (List.map p ~f:(List.fold ~init:0 ~f:(fun x -> fun y -> x + y)))) *)
(*TODO: Figure out how to display tables with this *)
let pp ppf v = Format.fprintf ppf "%s" (to_string v)

let of_string s = List.map (String.split ~on:'-' s) ~f:(fun row -> List.map (String.split ~on:',' row) ~f:Int.of_string)

let valid v =
  List.for_all ~f:(List.for_all ~f:(fun i -> i >= 0)) v
  && List.for_all ~f:(List.is_sorted ~compare:(fun i j -> Int.compare j i)) v

let scale s v = List.map ~f:(List.map ~f:(fun i -> s * i)) v
let mult v1 v2 = partition_map2 v1 v2 ~f:(fun x y -> x + y)
let inv v1 = Some (List.map ~f:(List.map ~f:(fun i -> -i)) v1)
let inv_exn v = match inv v with
  | Some iv -> iv
  | None -> invalid_arg (to_string v ^ " is not invertible")
let div v1 v2 = partition_map2 v1 v2 ~f:(fun x y -> x - y)
let add v1 v2 = Comp.max v1 v2
let pow v i = scale i v (* powers consist of adding n times *)
let one n m = repeat n (repeat m 0)

let tmod v ~by:w =
  let rec loop cur =
    let next = div cur w in
    if valid next  then loop next else cur
  in loop v
let div_mod v ~by:w =
    let rec loop cur i =
    let next = div cur w in
    if valid next  then loop next (i+1) else (cur,i)
  in loop v 0


let mutate p ~inp ~out =
  let base = List.map (List.range 0 (List.length p)) ~f:(fun _ -> List.map (List.range 0 (List.length (List.hd_exn p))) ~f:(fun _ -> 0)) in
  let ingrid = Comp.Map.fold ~init:base ~f:(fun ~key:v -> fun ~data:deg -> fun acc -> mult (scale (Int.abs deg) v) acc) inp in
  let outgrid = Comp.Map.fold ~init:base ~f:(fun ~key:v -> fun ~data:deg -> fun acc -> mult (scale (Int.abs deg) v) acc) out in
  add (div ingrid p) (div outgrid p)

let a ~i p =
  let len = List.length (List.hd_exn p) in
  let rec dup i l =
    if i < 0 then (weight p) :: l
    else
      match (l, i) with
      | ([], _) -> [0]
      | (x::xs, 0) -> x :: x :: xs
      | (x::xs, _) -> x :: (dup (i-1) xs) in
  List.mapi p ~f:(fun index -> fun row -> dup (len - (i-1) +index) row)

let dual p =
  let weight = weight p in
  List.transpose_exn(List.rev_map ~f:(List.rev_map ~f:(fun i -> weight - i)) p)

let of_plucker n s =
  let k = Int.Set.length s in
  let base = List.map (List.range 0 k) ~f:(fun _ -> List.map (List.range 0 (n-k) ) ~f:(fun _ -> 0)) in
  let partition_length = List.mapi (Int.Set.to_list s) ~f:(fun j -> fun i -> (n - k) - i + (j+1)) in
  List.map2_exn partition_length base ~f:(fun len -> fun l -> List.mapi l  ~f:(fun j -> fun _ -> if j < len then 1 else 0))

let to_plucker k n p = Int.Set.of_list (List.mapi p ~f:(fun i -> fun r -> n - k + i + 1 - (List.fold r ~init:0 ~f:(+)) ))

include Comp
