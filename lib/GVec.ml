open Core
open Algebra

module Vert = RingVertex.Make(FreeSemiRing)
module Clust = IMapCluster.Make(Vert)
module CSearch = SearchBase.Make(Clust)


let clust_of_edgelist edgelist =
  let matrix = Fixed.Clust.of_edgelist ~frozen:Int.Set.empty ~graph:edgelist |> Fixed.Clust.matrix |> List.transpose_exn in
  let vars = edgelist |> List.concat_map ~f:(fun (u,v) -> [u;v]) |> List.dedup_and_sort ~compare:Int.compare in 
  Clust.of_edgelist ~frozen:(vars |> List.map ~f:(fun v -> -v) |> Int.Set.of_list)
    ~graph:(edgelist @ (List.map vars ~f:(fun v -> (v,-v))))
    ~label:(List.map vars ~f:(fun v -> (v,List.map vars ~f:(fun w -> if Int.equal v w then 1 else 0)))
            @ (List.map vars ~f:(fun v -> (-v,List.nth_exn matrix (v-1)))))
