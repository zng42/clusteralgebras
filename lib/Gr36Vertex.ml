open Core
include PartVertex

let exoticX = [[2;1;1];[1;1;1];[1;1;0]]
let exoticY = [[2;2;1];[2;1;0];[1;0;0]]

let to_string v =
  match PartVertex.weight v with
  | 1 -> Plucker.Vert.to_string (to_plucker 3 6 v)
  | 2 -> (match v with
          | [[2;1;1];[1;1;1];[1;1;0]] -> "e2x"
          | [[2;2;1];[2;1;0];[1;0;0]] -> "e2y"
          | _ -> invalid_arg ("vertex isn't in gr36" ^ PartVertex.to_string v))
  | _ -> invalid_arg ("vertex isn't in gr36" ^ PartVertex.to_string v)
let pp ppf v = Format.fprintf ppf "%s" (to_string v)

let of_string s =
  match String.to_list s with
  | ['p';i;j;k] -> PartVertex.of_plucker 6 (Int.Set.of_list (List.map [i;j;k] ~f:(fun c -> Int.of_string(Char.to_string c))))
  | ['e';'2';'x'] -> exoticX
  | ['e';'2';'y'] -> exoticY
  | _ -> invalid_arg (s ^ " has unknown prefix")

let rotate v =
    match PartVertex.weight v with
  | 1 -> PartVertex.of_plucker 6 (Plucker.Vert.rotate 6 (PartVertex.to_plucker 3 6 v))
  | 2 -> if PartVertex.equal v exoticX
         then exoticY
         else if PartVertex.equal v exoticY
         then exoticX
         else invalid_arg ("vertex isn't in gr36: " ^ PartVertex.to_string v)
  | _ -> invalid_arg ("No vertices above weight 2 vertices in gr36 " ^ PartVertex.to_string v)


let to_string_short = to_string

let eval_mat v mat =
  let open Algebra.Rings.LacamlMat in
  match PartVertex.weight v with
  | 1 -> det_minor ~l:(Core.Set.to_list (to_plucker 3 6 v)) mat
  | 2 -> (match v with
          | [[2;1;1];[1;1;1];[1;1;0]] -> (det_minor ~l:[1;3;4] mat) *. (det_minor ~l:[2;5;6] mat) -. (det_minor ~l:[1;5;6] mat) *. (det_minor ~l:[2;3;4] mat)
          | [[2;2;1];[2;1;0];[1;0;0]] -> (det_minor ~l:[1;3;6] mat) *. (det_minor ~l:[2;4;5] mat) -. (det_minor ~l:[1;2;6] mat) *. (det_minor ~l:[3;4;5] mat)
          | _ -> invalid_arg ("vertex isn't in gr36" ^ PartVertex.to_string v))
  | _ -> invalid_arg ("vertex isn't in gr36" ^ PartVertex.to_string v)
