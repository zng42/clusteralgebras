open Core
module type S =
  sig
    type t
    val sexp_of_t : t -> Sexp.t
    val t_of_sexp : Sexp.t -> t
    include Comparable.S with type t := t


    val weight : t -> int

    val canExchange : t -> inp:(int Map.t)  -> out:(int Map.t) -> bool
    (* REQUIRES: canExchange v inp out == true*)
    val mutate : t -> inp:(int Map.t) -> out:(int Map.t) -> t
    val id : t -> string
    val to_string : t -> string
    val pp : Format.formatter -> t -> unit

    val of_string : string -> t

end

module type PS =
  sig
    include S
    val of_plucker : int -> Core.Int.Set.t -> t
  end
