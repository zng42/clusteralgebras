open Core

module Make ( V : Vertex.S) =
  struct
    module T =
      struct
        type t = int * V.t [@@deriving sexp]
        let compare (_,v) (_,w) = V.compare v w
      end
    include T
    include Comparable.Make(T)
    module ICompare = Comparable.Make(struct
                          include T
                          let compare (i,v) (j,w) =
                            match Ordering.of_int (Int.compare i j) with
                            | Less -> -1
                            | Greater -> 1
                            | Equal -> V.compare v w
                        end)

    let v_map m = V.Map.of_alist_exn (List.map ~f:(fun ((_,v), d) -> (v,d)) (Map.to_alist m))

    let of_iv (i,v) = (i,v)
    let to_v (_,v) = v
    let to_i (i,_) = i

    let canExchange (_,v) ~inp ~out = V.canExchange v ~inp:(v_map inp) ~out:(v_map out)
    let weight (_,v) = V.weight v

    let mutate (i,v) ~inp ~out = (i, V.mutate v ~inp:(v_map inp) ~out:(v_map out))
    let id (_, v) = V.id v
    let to_string (i,v) = (Int.to_string i) ^ ":" ^ (V.to_string v)
    let pp ppf v = Format.fprintf ppf "%s" (to_string v)
    let of_string s =
      match String.split ~on:':' s with
      | [i;v] -> (Int.of_string i, V.of_string v)
      | _ -> invalid_arg ("Indexed vertex should have one : seperating index and vertex " ^ s)
  end
