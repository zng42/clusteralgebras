open Core

module type S =
  sig
    include Cluster.S
    module BaseCluster : Cluster.S

    val index : ?vlist:BaseCluster.Vertex.t list -> BaseCluster.t -> t
    val re_index : t -> f:(int -> int) -> t
    val un_index : t -> BaseCluster.t

    val vertex_to_i : Vertex.t -> int
    val vertex_to_v : Vertex.t -> BaseCluster.Vertex.t

    val at : ?frozen:bool -> t -> int -> BaseCluster.Vertex.t option
    val at_exn : ?frozen:bool -> t -> int -> BaseCluster.Vertex.t

    val at_ivertex : ?frozen:bool -> t -> int -> Vertex.t option
    val at_ivertex_exn : ?frozen:bool -> t -> int -> Vertex.t

    val index_of : t -> BaseCluster.Vertex.t -> int option
    val mutateI : t -> int -> t
    val mutateI_multi : t -> int list -> t

    val id_ordered : t -> string
    val compare_ordered : t -> t -> int
  end

module Make (Cluster : Cluster.S) : (S with module BaseCluster = Cluster) =
  struct
    module BaseCluster = Cluster
    module BaseVertex = BaseCluster.Vertex
    module IVertex = IndexedVertex.Make(Cluster.Vertex)
    include MapCluster.Make(IVertex)

    let vertex_to_i  = IVertex.to_i
    let vertex_to_v  = IVertex.to_v


    let index ?vlist:(vlist = []) (c : Cluster.t) =
      let start_set = Cluster.Vertex.Set.of_list vlist in
      let full_list = vlist
                      @ (Core.Set.to_list (Core.Set.diff (Cluster.unfrozen c) start_set))
                      @ (Core.Set.to_list (Core.Set.diff (Cluster.frozen c) start_set)) in
      let imap = Cluster.Vertex.Map.of_alist_exn (List.mapi full_list ~f:(fun i v -> (v,i+1))) in
      of_edgelist
        ~graph:(List.map (Cluster.edgelist c) ~f:(fun (v,w) -> ((Core.Map.find_exn imap v, v), (Core.Map.find_exn imap w, w))))
        ~frozen:(Vertex.Set.map (Cluster.frozen c) ~f:(fun v -> (Core.Map.find_exn imap v, v)))

    let re_index c ~f =  map ~f:(fun (i,v) -> (f i, v)) c

    let un_index c =
      Cluster.of_edgelist ~graph:(List.map (edgelist c) ~f:(fun ((_,v),(_,w)) -> (v,w)))
        ~frozen:(Cluster.Vertex.Set.map (frozen c) ~f:snd)

    let at_ivertex ?frozen:(frozen = true) c i = (Core.Set.find (verts ~frozen:frozen c) ~f:(fun (j,_) -> Int.equal i j))
    let at_ivertex_exn ?frozen:(frozen = true) c i = Option.value_exn (at_ivertex ~frozen:frozen c i)

    let at ?frozen:(frozen = true) c i = Option.map ~f:vertex_to_v (at_ivertex ~frozen:frozen c i)
    let at_exn ?frozen:(frozen = true) c i = Option.value_exn (at ~frozen:frozen c i)

    let index_of c v =
      Option.map ~f:fst (Core.Set.find (verts c) ~f:(fun (_,w) -> Cluster.Vertex.equal v w))

    let mutateI c i =
      match at ~frozen:false c i with
      | Some v -> mutate c (IVertex.of_iv (i,v))
      | None -> c

    let mutateI_multi c l = List.fold ~init:c ~f:mutateI l

    let id_ordered cluster = String.concat ~sep:"" (List.map (Core.Set.to_list (unfrozen cluster)) ~f:(fun (i,v) -> Int.to_string i ^ ":" ^ BaseVertex.id v))
    let compare_ordered c1 c2 =
      match Ordering.of_int (compare c1 c2) with
      | Less -> -1
      | Greater -> 1
      | Equal -> IVertex.ICompare.Set.compare (IVertex.ICompare.Set.map ~f:Fn.id (verts c1)) (IVertex.ICompare.Set.map ~f:Fn.id (verts c2))
  end
