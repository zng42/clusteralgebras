open Core

include Int

let canExchange _ ~inp:_ ~out:_ = true
let mutate v ~inp:_ ~out:_ = v
let id = Int.to_string
let weight _ = 1
