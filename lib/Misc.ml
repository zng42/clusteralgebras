open Core

module Perm = Algebra.Permutation.MapPermutation

let cvec_sign l =
  if List.for_all l ~f:(fun i -> i >= 0)
  then 1
  else
    if List.for_all l ~f:(fun i -> i <= 0)
    then -1
    else 0

let (&&&) f g = (fun x -> f x && g x)
let (|||) f g = (fun x -> f x || g x)
