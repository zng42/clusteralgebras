open Core
include CAlgGenPlucker.Make(PartVertex)

let vert_to_plucker k n p =
  Int.Set.of_list (List.mapi p ~f:(fun i -> fun r -> n - k + i + 1 - (List.fold r ~init:0 ~f:(+)) ))
