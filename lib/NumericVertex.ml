open Core

module Make (Arg: sig val epsilon : float end) (* : S *) =
  struct
    type vertex = Value of int [@@deriving compare, sexp]
    type t = vertex [@@deriving compare, sexp]
    module Comp = Comparable.Make(struct type t = vertex [@@deriving compare, sexp] end)

    let id (Value i)  = "V" ^ Int.to_string i
    let to_string (Value i)  = Int.to_string i
    let of_string s = Value (Int.of_string s)
    let pp ppf v = Format.fprintf ppf "%s" (to_string v)

    module FloatMap = Core.Map.Make(struct type t = float [@@deriving sexp]
                                               let compare f1 f2 = if Float.(abs( abs (f1 /. f2) -. 1.0) < Arg.epsilon)
                                                                   then 0
                                                                   else Float.compare f1 f2 end)
    let values : float Comp.Map.t ref = ref(Comp.Map.empty)
    let found : vertex FloatMap.t ref = ref(FloatMap.empty)
    let count : int ref = ref 0
    let canExchange _ ~inp:_ ~out:_ = true

    let newCoord () =
      let value = Random.float 1.0 in
      let () = count := (!count + 1) in
      let w = Value (!count) in
      let () = values := Comp.Map.add_exn !values ~key:w ~data:value in
      let () = found := FloatMap.add_exn !found ~key:value ~data:w in
      w

    let value (v : t) =
      match Map.find !values v with
      | Some f -> f
      | _ -> invalid_arg (to_string v ^ "hasn't been found in a cluster so far")
    let weight _ = 1

    let rec pow f d =
      match d with
      | 0 -> 1.0
      | _ -> f *. (pow f (d-1))

    let mutate (v) ~(inp) ~(out) =
      let rhs =    (Map.fold inp ~init:1.0 ~f:(fun ~key:w -> fun ~data:deg -> fun res -> res *. pow (value w) (Int.abs deg)))
                +. (Map.fold out ~init:1.0 ~f:(fun ~key:w -> fun ~data:deg -> fun res -> res *. pow (value w) (Int.abs deg))) in
      let inew = rhs /. (value v) in
      match FloatMap.find !found inew with
      | Some w -> w
      | None ->
         let () = count := (!count + 1) in
         let w = Value (!count) in
         let () = values := Map.add_exn !values ~key:w ~data:inew in
         let () = found := FloatMap.add_exn !found ~key:inew ~data:w in
         w

    include Comp
end
