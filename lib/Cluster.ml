open Core

type vertexKind =
  Frozen
| Unfrozen
| AnyKind

type nbrKind =
  OutNbr
| InNbr
| AnyNbr


module type S =
  sig
    module Vertex : Vertex.S

    type nbrs = int Vertex.Map.t

    type t
    type cluster = t
    val sexp_of_t : t -> Sexplib.Sexp.t
    val t_of_sexp : Sexplib.Sexp.t -> t

    include Comparable with type t := t

    (* CREATE *)
    val of_edgelist : graph:((Vertex.t * Vertex.t) list) -> frozen:Vertex.Set.t -> cluster
    (* Each element of the list is of form (v, (iNbrs, oNbrs)). Repeated elements create multi-edges*)
    val of_nbrlist : graph: (Vertex.t * (Vertex.t list * Vertex.t list)) list -> frozen: Vertex.Set.t -> cluster
    val of_list : graph: (Vertex.t * int Vertex.Map.t) list -> frozen: Vertex.Set.t -> cluster
    val of_quiver : quiver : nbrs Vertex.Map.t -> frozen : Vertex.Set.t -> cluster

    val freeze : t -> Vertex.t list -> t
    val unfreeze : t -> Vertex.t list -> t

    (* OUTPUT *)
    val frozen : cluster -> Vertex.Set.t
    val unfrozen : cluster -> Vertex.Set.t
    val verts : ?frozen:bool -> cluster -> Vertex.Set.t
    (* c == of_quiver ~quiver:(quiver c) ~frozen:(frozen c) *)
    val quiver : cluster -> nbrs Vertex.Map.t
    (* c == of_edgelist ~graph:(edgelist c) ~frozen:(frozen c) *)
    val edgelist : cluster -> (Vertex.t * Vertex.t) list
    val matrix : ?vorder:(Vertex.t list Option.t) -> cluster -> int list list
    (* Dimension of algebra (dim(c) = Set.length(unfrozen(c)) *)
    val dim : cluster -> int


    (* Identify unfrozen components of the underlying quiver (treated as an undirected graph).
     * Each cluster in the output is connected with all vertices in other components frozen
     *)
    val components : cluster -> cluster list
    (* Undefined behavior if specified vertex is already frozen *)
    val component_of : cluster -> Vertex.t -> cluster
    (* Returns true if given underlying quiver is connected (as an undirected graph)*)
    val fConnected : cluster -> bool

    (* Warning: Calling this function could break invariants needed to get the correct names of vertices after mutation
     *   The main use of this function is produce simpler pictures of the mutable portion of the cluster *)
    val removeFrozen : cluster -> cluster

    (* returns a canonical list of cVectors associated to a cluster
     * The j^th entry of the cVector at unfrozen vertex u corresponds to the edge weight from the "j^th" frozen vertex to u
     * Since the number of the unfrozen vertices can change with mutation, the list of cVectors is sorted by lexograph order cVectors.
     *)
    val cVector : ?frozenlist : Vertex.t list option -> cluster -> Vertex.t -> int list
    val cVectors : ?frozenlist: Vertex.t list option -> cluster -> int list list
    val is_vec_green : int list -> bool
    val is_green : ?frozenlist: Vertex.t list option -> t -> bool
    val is_vec_red : int list -> bool
    val is_red : ?frozenlist:Vertex.t list option -> t -> bool

    val mem : cluster -> Vertex.t -> bool
    val memUnfrozen : cluster -> Vertex.t -> bool
    val memFrozen : cluster -> Vertex.t -> bool

    val exists : ?frozen:bool -> cluster -> f:(Vertex.t -> bool) -> bool
    val for_all : ?frozen:bool -> cluster -> f:(Vertex.t -> bool) -> bool
    val count : ?frozen:bool -> cluster -> f:(Vertex.t -> bool) -> int
    val find : ?frozen:bool -> cluster -> f:(Vertex.t -> bool) -> Vertex.t option
    val find_exn : ?frozen:bool -> cluster -> f:(Vertex.t -> bool) -> Vertex.t
    val filter : cluster -> f:(Vertex.t -> bool) -> cluster

    val exists_edge : ?frozen:bool -> cluster -> f:(Vertex.t * Vertex.t * int -> bool) -> bool
    val for_all_edge : ?frozen:bool -> cluster -> f:(Vertex.t * Vertex.t * int -> bool) -> bool
    val count_edge : ?frozen:bool -> cluster -> f:(Vertex.t * Vertex.t * int -> bool) -> int
    val find_edge : ?frozen:bool -> cluster -> f:(Vertex.t * Vertex.t * int -> bool) -> (Vertex.t*Vertex.t*int) option
    val find_edge_exn : ?frozen:bool -> cluster -> f:(Vertex.t * Vertex.t * int -> bool) -> (Vertex.t*Vertex.t*int)
    val filter_edge : cluster -> f:(Vertex.t*Vertex.t*int -> bool) -> cluster

    val sources : cluster -> Vertex.Set.t
    val sinks : cluster -> Vertex.Set.t
    val sources_sinks_neithers : cluster -> Vertex.Set.t * Vertex.Set.t * Vertex.Set.t

    val nbr : ?frozen:bool -> cluster -> Vertex.t -> nbrs

    val oNbr : ?frozen:bool -> cluster -> Vertex.t -> int Vertex.Map.t
    val iNbr : ?frozen:bool -> cluster -> Vertex.t -> int Vertex.Map.t

    val nbrSet : ?frozen:bool -> cluster -> Vertex.t -> Vertex.Set.t
    val oNbrSet : ?frozen:bool -> cluster -> Vertex.t -> Vertex.Set.t
    val iNbrSet : ?frozen:bool -> cluster -> Vertex.t -> Vertex.Set.t

    val nbrList : ?frozen:bool -> cluster -> Vertex.t -> Vertex.t list
    val oNbrList : ?frozen:bool -> cluster -> Vertex.t -> Vertex.t list
    val iNbrList : ?frozen:bool -> cluster -> Vertex.t -> Vertex.t list

    val edge_weight : cluster -> inp:Vertex.t -> out:Vertex.t -> int

    (* degree c v = (oDegree c v, iDegree c v) *)
    val degree : ?frozen:bool -> cluster -> Vertex.t -> int * int
    (* Total number of in/out edges from c *)
    val valence : ?frozen:bool -> cluster -> Vertex.t -> int
    val iDegree : ?frozen:bool -> cluster -> Vertex.t -> int
    val oDegree : ?frozen:bool -> cluster -> Vertex.t -> int

    (* iDegreeWeighted c v is sum of edgeweight * weight of neighbor for edges into v *)
    val degreeWeighted : ?frozen:bool -> cluster -> Vertex.t -> int * int
    val iDegreeWeighted : ?frozen:bool -> cluster -> Vertex.t -> int
    val oDegreeWeighted : ?frozen:bool -> cluster -> Vertex.t -> int

    (* Returns vectors in the null space of the adjacency matrixed produced with the given vorder
     * The corresponding product of X coordinates is a casimir element for the Poisson structure *)
    val casimir_vectors : ?vorder:(Vertex.t list Option.t) -> cluster -> int list list

    (* returns the F2 matrix corresponding to transvection at v, the matrix transvection c v corresponds to:
     * tau_v x = x- Omega(v,x)v mod 2 where Omega is adjacency matrix for c
     *)
    val transvection : ?vorder:Vertex.t list Option.t -> cluster -> Vertex.t -> int list list

    (* tail c v returns the vertices in a tail starting at v
     * v = u_0 - u_1 - u_2 -... u_k where u1...u_k-1 have valence 2 and v has valence 1 in the mutable quiver
     * if valence(v) > 1, tail c v = []*)
    val tail : cluster -> Vertex.t -> Vertex.t list
    (* tail_lengths c = [n1,...nk] where n1 to nk are the lenghts of tails of c in increasing order
     * Note that a single path of length n returns [n;n] as each side is the `start' of a tail *)
    val tail_lengths : cluster -> int list

    val rank_pi1 : cluster -> int
    (* Counts induced oriented cycles in quiver *)
    val count_oriented_cycles : cluster -> int
    val two_color : cluster -> (Vertex.t list *  Vertex.t list) option

    val follow_two_color_path : t -> int -> t list
    val follow_sources_path : t -> int -> (int * t list)

    val is_SourceSink : cluster -> bool
    (* Returns true if c has no directed cycles (in mutable part) *)
    val is_acylic : cluster -> bool
    (* Returns true if each component of the quiver is one of:
     *  1. acylic quiver with each node a sources/sink, # sources >= #sinks
     *  2. unoriented cylce with exactly one source and sink
     * These quivers can be used to easily identify the type of a finite/affine algebra*)
    val fAffineDynkin : cluster -> bool
    val is_Tpqr : cluster -> bool
    (* REQUIRES: fAffineDynkin c = true or is_Tpqr c = true
     * returns string identifying the kind of cluster*)
    val dynkin_type : cluster -> string
    (* Returns Some r if  c is a fork with point of return r and None otherwise
     * A fork is a quiver with the following properties:
     * 1. Every edge is weight >=2
     * 2. for each i -> r -> j have edge_weigth j i > edge_weight i r
     *                          and edge_weight j i > edge_weight r j
     * 3. the full subquiver of nodes into r is acylic
     * 4. the full subquiver of nodes out of r is acyclic*)
    val is_fork : cluster -> Vertex.t option

    (* Returns Some (k,k') if c is a key with vertices k and k' such that
     * Q-k and Q-k' are both abundant acylic
     * For each i in Q-{k,k'} either k->i and k'->i
     *                            or i->k and i->k'
     *)
    val is_key : cluster -> (Vertex.t*Vertex.t) option

    (* is_prefork c == Some(r,(k,k')) if c is a prefork such that
     * 1. Q-k and Q-k' are forks with common point of return r
     * 2. For each i in Q-{k,k'} either k->i and k'->i
     *                               or i->k and i->k'
     *)
    val is_prefork : cluster -> (Vertex.t*(Vertex.t*Vertex.t)) option

    (* fEdgeAbove c w returns true is there is an edge above weight w in c *)
    val fEdgeAbove :  ?frozen:bool -> cluster -> w:int -> bool

    (* A quiver is abundant if every edge has weight at least 2*)
    val is_abundant : cluster -> bool
    val fCVectorAbove : ?frozenlist:Vertex.t list option -> cluster -> w:int -> bool


    val write_cluster : ?image_size:((int*int*int) option) -> ?node_color:(Vertex.t->int) -> ?vlabel:(Vertex.t-> string) -> string -> cluster -> unit
    val write_clav : string -> cluster -> unit
    val write_path : string -> cluster -> Vertex.t list -> unit
    val write_path_condense : string -> cluster -> Vertex.t list -> unit

    (* CLUSTER Isomorphism*)
    val is_subset : ?unfrozen:bool -> cluster -> of_:cluster -> bool

    val is_subgraph: ?unfrozen:bool -> ?directed:bool -> cluster -> of_:cluster -> bool
    val all_embeddings : ?unfrozen:bool -> ?directed:bool -> cluster -> of_:cluster -> (Vertex.t -> Vertex.t) list

    val is_isomorphic : ?unfrozen:bool -> ?directed:bool -> cluster -> cluster -> bool
    val isomorphism : ?unfrozen:bool -> ?directed:bool -> cluster -> cluster -> (Vertex.t -> Vertex.t) option

    (* Compute list of automorphism of c
     * default arguments are automorphism ~unfrozen:true ~directed:true c
     *)
    val automorphisms : ?unfrozen:bool -> ?directed:bool -> cluster -> (Vertex.t ->  Vertex.t) list

    val name : cluster -> string

    (* Search Graph Operations*)
    type edge = Vertex.t
    val edge_of_sexp : Sexplib.Sexp.t -> edge
    val sexp_of_edge : edge -> Sexplib.Sexp.t
    val equal_edge : edge -> edge -> bool
    val n : cluster -> (cluster * edge) list
    val follow : cluster -> edge list -> cluster * (edge list)
    val label : cluster -> string
    val id : cluster -> string

    (* Returns a cluster with every edge pointing in the oppositie direction *)
    val rev : cluster -> cluster
    (* Requires: f to be injective on the vertecies in cluster*)
    val map : cluster -> f:(Vertex.t -> Vertex.t) -> cluster

    (* mutate_multi c v == (c', w) where c' is the result of mutating in the direction of v, and w replaces v
     * since mutation is an involution: (uncurry mutate) (mutate_name c v) == c *)
    val mutate : cluster -> Vertex.t -> cluster
    val mutate_name : cluster -> Vertex.t -> (cluster * Vertex.t)

    (* mutate_multi_name (c,path) = (c',path_back) where path_back is the list of mutations to perform to return to c from c'
     * (uncurry mutate_multi) (mutate_multi_name c path) == c *)
    val mutate_multi : cluster -> Vertex.t list -> cluster
    val mutate_multi_name : cluster -> Vertex.t list -> (cluster * Vertex.t list)
  end
