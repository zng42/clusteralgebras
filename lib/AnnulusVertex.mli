(* #mod_use "Vertex.ml" *)
include Vertex.S
val hash : t -> int
val fCycCross : t -> t -> bool
