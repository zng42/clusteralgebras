include Vertex.PS with type t = int list list

val is_valid : t -> bool
val diags : t -> int list list

val show_table : t -> unit
val show_tables : Set.t -> unit
val show_tables_list : t list -> unit

val a : i: int -> t -> int list list
val dual : t -> int list list
val weight : t -> int
val to_plucker : int -> int -> t -> Plucker.Vert.t
val to_vars : int -> int -> t -> int list list


(* Tropical Ring Structure *)
val one : int -> int -> t
val add : t -> t -> t
val mult : t -> t -> t
val inv : t -> t Option.t
val inv_exn : t -> t
val div : t -> t -> t
val pow : t -> int -> t

val tmod : t -> by:t -> t
val div_mod : t -> by:t -> t * int
