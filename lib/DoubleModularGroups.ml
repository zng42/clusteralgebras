open Core
module Perm = Algebra.Permutation.MapPermutation

module ModE7DoubleBroken =
  struct
    (*** Work With Cluster Modular Group for gr48/ E7double***)
    let vorder = [ [[1;1;1;1];[1;1;1;0];[1;1;1;0];[0;0;0;0]] ; [[2;2;1;1];[2;2;0;0];[2;1;0;0];[1;0;0;0]] ; [[2;2;1;1];[2;2;1;0];[2;1;0;0];[1;0;0;0]]
                   ; [[2;2;1;1];[2;2;1;1];[2;1;0;0];[1;0;0;0]] ; [[1;1;0;0];[1;1;0;0];[1;1;0;0];[1;0;0;0]] ; [[2;2;2;1];[2;2;1;0];[2;1;1;0];[1;0;0;0]]
                   ; [[1;1;1;1];[1;1;0;0];[1;0;0;0];[1;0;0;0]] ; [[1;0;0;0];[0;0;0;0];[0;0;0;0];[0;0;0;0]] ;[[1;1;1;1];[1;1;0;0];[1;1;0;0];[0;0;0;0]]]
    let qStart = Partition.CSearch.stoch_find_cond 5000 ~cond:(fun v -> Partition.Vert.weight v <= 2) (Partition.Vert.Set.of_list vorder) (Partition.gr 4 8)
    let iqStart = Partition.IClust.index ~vlist:vorder qStart
    let gp = ([3;1;2;4;5], Perm.of_cycles [[1;3;2]])
    let gq = ([6;1;2;7;8], Perm.of_cycles [[1;6;2]])
    let gr = ([9;1;2], Perm.of_cycles [[1;9;2]])
    let gDelta = ModularGroup.pow ([2;4;7;5;8;3;6;9],Perm.one) 3
    let gX = ([], Perm.of_cycles [[3;6];[4;7];[5;8]])
    let gY = ([3; 1; 2; 4; 5; 2; 3; 1; 4; 5; 9; 3; 2], Perm.of_cycles [[1;3];[2;9]])
    let gZ = ([3; 1; 2; 4; 5; 8; 7; 6; 1; 3], Perm.of_cycles [[2;6];[4;7];[5;8]])
    let gRhot = ([6; 8; 3; 4; 9; 1; 2; 7; 2; 3; 6; 8; 9; 5], Perm.of_cycles [[1;2;7];[3;8;9];[4;6;5]]) (* <2,7,8,6,4,5,1,9,3>*)
    let gTheta = ([6; 8; 3; 4; 9; 1; 2; 9; 8; 3; 2; 7; 9; 5; 8; 3; 6], Perm.of_cycles [[1;7];[4;5];[9;8]]) (* <7,2,3,5,4,6,1,9,8>*)
    let gIota = ([5; 4; 5; 8; 7; 8], Perm.of_cycles [[1;2]])
    let gAlpha = ([1], Perm.of_cycles [[1;2]])
    let gBeta = (Fn.apply_n_times ~n:12 (List.append [9;3;4;5;6;7;8;2]) [] @ [2], Perm.of_cycles [[1;2]])

    let mobiusKantor = Partition.GroupSearch.graph (iqStart, [gX;gY;gZ])
    let name_group =
      let groupMap = ModularGroup.Map.of_alist_exn [(gp,"gp");(gq,"gq");(gr,"gr");
                                                    (gX,"gX"); (gY,"gY"); (gZ,"gZ");
                                                    (gRhot, "gRhot"); (gTheta, "gTheta"); (gIota, "gIota");
                                                    (gAlpha, "gAlpha"); (gBeta, "gBeta")] in
      (fun g -> match Map.find groupMap g with Some s -> s | None -> "Unknown " ^ ModularGroup.to_string g)

  end

 module ModE7Double =
   struct
     open Partition
     let vorder = [[[1; 1; 1; 1]; [1; 1; 1; 0]; [1; 1; 1; 0]; [0; 0; 0; 0]];
                   [[2; 2; 1; 1]; [2; 2; 0; 0]; [2; 1; 0; 0]; [1; 0; 0; 0]];
                   [[1; 1; 1; 1]; [1; 1; 0; 0]; [1; 1; 0; 0]; [0; 0; 0; 0]];
                   [[2; 2; 1; 1]; [2; 2; 1; 0]; [2; 1; 0; 0]; [1; 0; 0; 0]];
                   [[1; 1; 1; 1]; [1; 1; 1; 0]; [1; 0; 0; 0]; [0; 0; 0; 0]];
                   [[1; 1; 1; 1]; [1; 1; 1; 1]; [1; 0; 0; 0]; [0; 0; 0; 0]];
                   [[2; 2; 2; 1]; [2; 2; 1; 0]; [2; 1; 1; 0]; [1; 0; 0; 0]];
                   [[2; 2; 2; 1]; [2; 1; 1; 0]; [2; 1; 1; 0]; [1; 0; 0; 0]];
                   [[1; 1; 1; 1]; [1; 0; 0; 0]; [1; 0; 0; 0]; [1; 0; 0; 0]]]
     let qStart = CSearch.stoch_find_cond 5000 ~cond:(fun v -> Vert.weight v <= 2) (Vert.Set.of_list vorder) (gr 4 8)
     let iqStart = Partition.IClust.index ~vlist:vorder qStart
     let tau1 = ([5;6; 4;1;2], Perm.of_cycles [[2;1;4]])
     let tau2 = ([8;9; 7;1;2], Perm.of_cycles [[2;1;7]])
     let tau3 = ([3;1;2], Perm.of_cycles [[2;1;3]])
     let gamma = ([1],Perm.of_cycles [[1;2]])
     let delta = ModularGroup.pow ([2;5;8;6;9;4;7;3],Perm.one) 3
     let sigma = ([], Perm.of_cycles [[4;7];[5;8];[6;9]])
     let rot = [gamma; delta; tau1; tau2; delta; delta] |> List.reduce_exn ~f:ModularGroup.mult
     let name_groupE7 =
       let groupMap = ModularGroup.Map.of_alist_exn
                        [(tau1,"tau1");(tau2,"tau2");(tau3,"tau3");
                         (delta,"delta"); (gamma,"gamma"); (sigma, "sigma");
                         (ModularGroup.inv tau1,"tau1Inv");(ModularGroup.inv tau2,"tau2Inv");(ModularGroup.inv tau3,"tau3Inv");
                         (ModularGroup.inv delta,"deltaInv"); (ModularGroup.inv gamma,"gammaInv")
                        ] in
       (fun g -> match Map.find groupMap g with Some s -> s | None -> "Unknown " ^ ModularGroup.to_string g)
   end


module ModE7DoubleFixed =
  struct
    let (qT442,_)  = Fixed.Clust.of_edgelist ~frozen:Int.Set.empty ~graph:[(1,2);(1,2);(2,3);(3,1);(2,4);(4,1);(5,4);(5,6);(2,7);(7,1);(8,7);(8,9)]
                     |> Fixed.Clust.frame
    let tau1E7 = ([5;6; 4;1;2], Perm.of_cycles [[2;1;4]])
    let tau2E7 = ([8;9; 7;1;2], Perm.of_cycles [[2;1;7]])
    let tau3E7 = ([3;1;2], Perm.of_cycles [[2;1;3]])
    let gammaE7 = ([1],Perm.of_cycles [[1;2]])
    let deltaE7 = ModularGroup.pow ([2;5;8;6;9;4;7;3],Perm.one) 3
    let sigmaE7 = ([], Perm.of_cycles [[4;7];[5;8];[6;9]])
    let name_groupE7 =
      let groupMap = ModularGroup.Map.of_alist_exn
                       [(tau1E7,"tau1");(tau2E7,"tau2");(tau3E7,"tau3");
                        (deltaE7,"delta"); (gammaE7,"gamma"); (sigmaE7, "sigma");
                        (ModularGroup.inv tau1E7,"tau1Inv");(ModularGroup.inv tau2E7,"tau2Inv");(ModularGroup.inv tau3E7,"tau3Inv");
                        (ModularGroup.inv deltaE7,"deltaInv"); (ModularGroup.inv gammaE7,"gammaInv")
                       ] in
      (fun g -> match Map.find groupMap g with Some s -> s | None -> "Unknown " ^ ModularGroup.to_string g)

    let pathToC3 = ([9;6;8;7;5;4;2], Perm.one)
    let tau1C3 = ModularGroup.mult ([5;6; 1;2; 4;7;], Perm.of_cycles [[5;7;1];[6;4;2]]) ([], Perm.of_cycles [[1;2]])
    let tau2C3 = ModularGroup.mult ([8;9; 1;2; 4;7;], Perm.of_cycles [[9;7;1];[8;4;2]]) ([], Perm.of_cycles [[4;7]])
    let tau3C3 = ModularGroup.mult ([3; 1;2; 4;7; 3; 1;2], Perm.one) ([],Perm.of_cycles [[4;7]])
    let deltaC3 = ModularGroup.mult (ModularGroup.pow ([4;7; 3; 5;6; 8;9], Perm.one) 1) ([],Perm.of_cycles [[1;2]])
    let sigmaC3 = ModularGroup.mult ([],Perm.of_cycles [[5;8];[6;9]]) ([],Perm.of_cycles [[4;7]])
    let gammaC3 = ModularGroup.mult ([1;2],Perm.of_cycles [[1;7];[2;4]]) ([], Perm.of_cycles [])
    let name_groupC3 =
      let groupMap = ModularGroup.Map.of_alist_exn [(tau1C3,"tau1");(tau2C3,"tau2");(tau3C3,"tau3")
                                                   ;(deltaC3,"delta"); (gammaC3,"gamma"); (sigmaC3, "sigma")
                                                   ;(ModularGroup.inv tau1C3,"tau1Inv");(ModularGroup.inv tau2C3,"tau2Inv");(ModularGroup.inv tau3C3,"tau3Inv")
                                                   ;(ModularGroup.inv deltaC3,"deltaInv"); (ModularGroup.inv gammaC3,"gammaInv")
                                                   ] in
      (fun g -> match Map.find groupMap g with Some s -> s | None -> "Unknown " ^ ModularGroup.to_string g)

  end

module ModE8Double =
  struct
    (* Presented from T_{6,3,2} *)
    (* 1   2   3   4   5   6   7   8   9   10
     * N_i N_1 3_2 2_2 2_3 1_2 1_3 1_4 1_5 1_6*)
    let nameT632 =
      let dict = Int.Map.of_alist_exn [(1,"N_i"); (2,"N_1"); (3,"3_2");(4,"2_2");(5,"2_3");(6,"1_2");(7,"1_3");(8,"1_4");(9,"1_5");(10,"1_6")] in
      Map.find_exn dict

    let (qT632,_)  = Fixed.Clust.of_edgelist ~frozen:Int.Set.empty ~graph:[(1,2);(1,2);(2,3);(3,1);(2,4);(4,1);(5,4);(2,6);(6,1);(7,6);(7,8);(9,8);(9,10)]
                     |> Fixed.Clust.frame
    let tau1E8 = ([9;7; 10;8; 6;1;2] , Perm.of_cycles [[6;2;1]])
    let tau2E8 = ([5;  4;1;2] , Perm.of_cycles [[4;2;1]])
    let tau3E8 = ([3;1;2] , Perm.of_cycles [[3;2;1]])
    let deltaE8 = ModularGroup.pow ([9;7;2;5; 3;4;6;8;10],Perm.of_cycles [[]]) 5
    (*    let delta = ([10;7;9; 3; 8;5;2],Perm.of_cycles [[1;6;4]]) *)
    let gammaE8 = ([1],Perm.of_cycles [[1;2]])
    let groupE8 =  List.concat_map ~f:(fun g -> [g;ModularGroup.inv g]) [tau1E8;tau2E8;tau3E8;deltaE8;gammaE8]
    let groupPSL2E8 = List.concat_map ~f:(fun g -> [g;ModularGroup.inv g]) [tau1E8;deltaE8;gammaE8]

    let name_groupE8 =
      let groupMap = ModularGroup.Map.of_alist_exn
                       [(tau1E8,"tau1");(tau2E8,"tau2");(tau3E8,"tau3");
                        (deltaE8,"delta"); (gammaE8,"gamma");
                        (ModularGroup.inv tau1E8,"tau1Inv");(ModularGroup.inv tau2E8,"tau2Inv");(ModularGroup.inv tau3E8,"tau3Inv");
                        (ModularGroup.inv deltaE8,"deltaInv"); (ModularGroup.inv gammaE8,"gammaInv")] in
      (fun g -> match Map.find groupMap g with Some s -> s | None -> "Unknown " ^ ModularGroup.to_string g)

    (* Presented from triangle quiver that folds to G2double (with 3 fat verteces *)
    let pathToG2 = ([4; 8; 9; 7; 6; 8; 5; 2; 1], Perm.one)
    let qG2 = Fixed.ModG.apply qT632 pathToG2
    let deltaG2 = ([1;4;6; 3; 7;9;10],Perm.of_cycles [[5;8;2]])
    let tauG2 = ([7;9;10; 2;5;8; 1;4;6], Perm.mult (Perm.of_cycles [[2;5;8]]) (Perm.of_cycles [[10;6;8];[7;4;2];[9;1;5]]))
    (*let gammaG2 = ([2;5;8], Perm.of_cycles [[2;6];[1;8];[4;5]])*)
    let name_groupG2 =
      let groupMap = ModularGroup.Map.of_alist_exn [(tauG2,"tau"); (deltaG2,"delta"); (*(gammaG2,"gamma");*)
                                                    (ModularGroup.inv tauG2,"tauInv");
                                                    (ModularGroup.inv deltaG2,"deltaInv"); (*ModularGroup.inv ([1],Perm.of_cycles [[1;2]]),"gammaInv"*)] in
      (fun g -> match Map.find groupMap g with Some s -> s | None -> "Unknown " ^ ModularGroup.to_string g)
        (* tauG2 ~ tau1, deltaG2 ~ tau1 delta^(-2) tau1 delta *)

    (* F4^(2,2) has weight 2 nodes on double edge. So group names based on dual cluster F_4^{1,1} *)
    (* Using the path below from qT632 the groups are {9,10}->{8,7}, {1,2}=> {3,6}, {4] {5} *)
    let pathToF4 = ([10; 3; 9; 8; 10; 7; 6; 2], Perm.one);;
    let qF4 = Fixed.ModG.apply qT632 pathToF4
    let tau1F4 = ModularGroup.mult ([10;9; 8;7; 1;2; 3;6], (Perm.of_cycles [[7;3;2]; [8; 6;1]]) ) ([],Perm.of_cycles [[6;3]]);;
    let tau2F4 = ModularGroup.mult ([5; 4; 1;2; 3;6; 4; 3;6], Perm.one) ([], Perm.of_cycles [[1;2]]);;
    let deltaF4 = ModularGroup.mult (ModularGroup.pow ([10;9; 6;3; 5; 7;8; 4], Perm.one) 2) ([],Perm.of_cycles [[1;2]])
    let gammaF4 = ([1;2], Perm.of_cycles [[6;1];[2;3]])
    let name_groupF4 =
      let groupMap = ModularGroup.Map.of_alist_exn [(tau1F4,"tau1");(tau2F4,"tau2");
                                                    (deltaF4,"delta"); (gammaF4,"gamma");
                                                    (ModularGroup.inv tau1F4,"tau1Inv");(ModularGroup.inv tau2F4,"tau2Inv");
                                                    (ModularGroup.inv deltaF4,"deltaInv"); (ModularGroup.inv gammaF4,"gammaInv")] in
      (fun g -> match Map.find groupMap g with Some s -> s | None -> "Unknown " ^ ModularGroup.to_string g)

  end
