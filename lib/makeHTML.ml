open Core

module Make(CSearch : ClusterSearch.S) : sig
  val create_html: string -> unit
  val create_all : ?maxcodim:int -> ?xorbit:(CSearch.XVert.t ->CSearch.XVert.InvEq.Set.t) option ->  ?fLabels:bool  -> string -> CSearch.graph ->  unit
  val write_iso_rep : ?fLabels:bool -> string -> CSearch.graph -> int ->  int -> (CSearch.Cluster.Set.t) -> unit
  val write_xorbit : ?fLabels:bool -> string -> CSearch.graph -> CSearch.XVert.InvEq.Set.t -> unit
end =
  struct
    module Cluster = CSearch.Cluster
    module Vert = Cluster.Vertex
    module XVert = CSearch.XVert

    let tab = "   "
    let imgdpi = 400
    
    let ctype_display s =
      let display s =
      match String.chop_suffix s ~suffix:"affine" with
      | Some(s) -> (match String.prefix s 1 with
                    | "A" -> (match String.split ~on:'-' s with
                              | [_;p;q] -> "A(" ^ p ^ "," ^ q ^ ") Affine"
                              | _ -> invalid_arg (s ^ " isn't name of type A affine subalg"))
                    | "B" -> s ^ " Affine"
                    | "C" -> s ^ " Affine"
                    | "D" -> s ^ " Affine"
                    | "E" -> s ^ " Affine"
                    | "F" -> s ^ " Affine"
                    | "G" -> s ^ " Affine"
                    | _ -> invalid_arg (s ^ " isn't name of affine subalg"))
      | None -> s in
      String.concat ~sep:" x " (List.map ~f:display (String.split ~on:'x' s))

    let subalg_to_label s =
      match String.split ~on:'_' s with
      | _::ctype::frozen -> (ctype_display ctype) ^ " " ^ String.concat ~sep:" " frozen
      | _ -> s

    let good_xname base = String.concat ~sep:"/" (List.map ~f:(fun s -> "(" ^ s ^ ")") (String.split ~on:'_' base))

    let subalg_to_type s =
      match String.split ~on:'_' s with
      | _ :: ctype::_ -> (ctype_display ctype)
      | _ -> invalid_arg (s ^ " is missing a subalg type")

    let iso_number s =
      match String.split ~on:'_' s with
      | [_ ; "isoClass" ; i ; j] -> (Int.of_string i, Int.of_string j)
      | _ -> invalid_arg (s ^ " isn't the name of file for isomorphism class")

    let focused_name base = "graph_focused_" ^ base ^ ".png"
    let embeded_name base = "graph_embeded_" ^ base ^ ".png"
    let incompatfile = "incompat.csv"


    let cluster_to_fixed c =
      let verts_map = Vert.Map.of_alist_exn (List.mapi ~f:(fun i v -> (v,i)) (Set.to_list (Cluster.verts c))) in
      Fixed.Clust.of_edgelist ~frozen:(Fixed.Vert.Set.map ~f:(Map.find_exn verts_map) (Cluster.frozen c))
        ~graph:(c |> Cluster.edgelist |> List.map ~f:(fun (u,v) -> (Map.find_exn verts_map u, Map.find_exn verts_map v)))

    let st_focusButton embedSrc focusSrc buttonText = String.concat
      [ "<div class=\"radio\">"
      ; "<input type=\"radio\" name=\"focus\" onclick=\"focusSubalgebra('"^ embedSrc ^ "','" ^ focusSrc ^ "')\"" ^ ">"
      ; buttonText
      ; "</input>"
      ; "</div>"]

    
    let st_vertexCheckbox v incompat = String.concat
      [ "<input type=\"checkbox\" name=\"vertex\" onChange=\"updateFocus(this.value)\" "
      ; "value=\"" ^ v ^ "_" ^ incompat ^ "\""
      ; "id = \""^ v ^ "\">"
      ; "<label for = \"" ^ v ^ "\">" ^ v ^ "</label>"
      ; "</input>"
      ]

    (* let st_focusButton embedSrc focusSrc buttonText =
      "<button type=\"button\" onclick=\"focusSubalgebra('"^ embedSrc ^ "','" ^ focusSrc ^ "')\"" ^ ">" ^ buttonText ^ "</button>"*)

    let create_html dir =
      Out_channel.with_file (Filename.concat dir  (Filename.basename dir ^ ".html")) ~f:(fun file ->
          let w_line tabdepth s = Out_channel.output_string file (Fn.apply_n_times ~n:tabdepth ((^) tab) (s ^ "\n")) in
          let (_(*prevdir*),name) = Filename.split dir in
          let subfiles = Caml.Sys.readdir dir |> Array.to_list in
          let incompat =
            if not(Caml.Sys.file_exists (Filename.concat dir incompatfile))
            then []
            else In_channel.read_lines (Filename.concat dir incompatfile) |> List.map ~f:(String.split ~on:'_')
                 |> List.map ~f:(function [v;incomp] -> (v,incomp) | _-> invalid_arg "incompatible vars misformated") in
          let subalgKinds = [] (* subfiles
                            |> List.filter_map ~f:(String.chop_prefix ~prefix:"graph_focused_")
                            |> List.filter_map ~f:(fun s -> match (Filename.split_extension s) with (name,Some("png")) -> Some name | _ -> None)
                            |> String.Set.of_list
                            |> Set.group_by ~equiv:(fun s t -> String.equal (subalg_to_type s) (subalg_to_type t))
                                *)
                             in
          let iso_reps = subfiles
                         |> List.filter ~f:(String.is_prefix ~prefix:"graph_isoClass")
                         |> List.filter_map ~f:(fun s -> match (Filename.split_extension s) with (name,Some("png")) -> Some name | _ -> None)
                         |> List.sort ~compare:(fun s t -> Tuple2.compare ~cmp1:Int.compare ~cmp2:Int.compare (iso_number s) (iso_number t))
                         |> List.group ~break:(fun s t -> not((fst (iso_number s)) = (fst (iso_number t))) ) in
          let xorbits = subfiles
                        |> List.filter ~f:(String.is_prefix ~prefix:"graph_xorbit")
                        |> List.filter_map ~f:(fun s -> match (Filename.split_extension s) with (name,Some("png")) -> Some name | _ -> None)
                        |> List.sort ~compare:String.compare in
          ( w_line 0 "<html>"
          ; w_line 0 ("<head>\n <title> Cluster Complex for " ^ name ^ "</title>\n <link rel=\"stylesheet\" href=\"../style.css\"> </head>")
          ; w_line 0 ("<script src=\"../clusterNavigate.js\"> </script>")
          ; w_line 1 ("<div style=\"height:15%\">")
          ; w_line 2 ("<h1>" ^ name ^ "</h1>")
          ; w_line 2 ("<a href=\"../index.html\">back </a>")
          ; w_line 1 (" </div>")
          ; w_line 0 ("<div style=\"height:55%;overflow-y:hidden;overflow-x:auto;width=100%;white-space:nowrap\">")
          ; w_line 1 ("<img id=\"embeded\" src=\"graph_embeded_.png\" alt=\"Complex embeded in larger subalgebra\" style= \"height:100%\" />")
          ; w_line 1 ("<img id=\"focused\" src=\"graph_focused_.png\" alt=\"Cluster Complex for " ^ name ^ "\" style=\"height:100%\" >")
          ; w_line 0 "</div>"
          ; w_line 0 ""
          ; w_line 0 "<div style=\"height:30%;overflow:auto\">"
          ;if List.length incompat > 0
           then
             (w_line 1 ("<h2> Coordinates </h2>")
             ;w_line 1 ("<button type=\"button\" onclick=\"resetFocus()\">Reset</button>")
             ;w_line 1 ("<div id=\"coordCheckbox\">")
             ;List.iter incompat ~f:(fun (v,incom) -> w_line 2 (st_vertexCheckbox v incom))
             ;w_line 1 ("</div>")
             )
           else ()
          ;if List.length subalgKinds > 0
           then
             ( w_line 1 ("<h2> Subalgebras </h2>")
             ; List.iter subalgKinds ~f:(fun (subalgs) ->
                   (w_line 1 ("<h3>" ^ subalg_to_type (Set.min_elt_exn subalgs) ^ "</h3>")
                   ; w_line 1 ("<div>")
                   ; Set.iter subalgs ~f:(fun name -> w_line 2 (st_focusButton (embeded_name name) (focused_name name) (subalg_to_label name)) )
                   ; w_line 1 ("</div>"))
             ))
           else ()
          ; if List.length iso_reps > 0
            then
              ( w_line 1 ("<h2> Quiver Isomorphism Classes </h2>")
              ; List.iteri iso_reps ~f:(fun i dirclass ->
                    ( w_line 1  "<div class=\"clear\">"
                    ; List.iteri dirclass ~f:(fun j graphName ->
                          w_line 2 (st_focusButton (graphName ^ ".png")
                                      ("quiver" ^ String.drop_prefix graphName 5 ^ ".png")
                                      ("Isomorphism Class " ^ Int.to_string (i+1) ^ " " ^ Int.to_string (j+1) )))
                    ; w_line 1  "</div>"))
              )
            else ()
          ; if List.length xorbits > 0
            then
              ( w_line 1 ("<h2> Orbits of X-Coordinates </h2>")
              ; w_line 1  "<div>"
              ; List.iter xorbits
                  ~f:(fun graphName -> w_line 2 (st_focusButton (graphName ^ ".png")
                                                (String.drop_prefix graphName 6 ^ ".png")
                                                (good_xname (String.drop_prefix graphName (6+7)))))
              ; w_line 1  "</div>")
            else ()
          ; w_line 0 "</div>"
          ; w_line 0 "</html>"))

    let write_iso_rep ?fLabels:(fLabels=true) folder fullgraph i j iso_class =
      let rep = cluster_to_fixed (Cluster.removeFrozen (Set.min_elt_exn iso_class)) in
      let autos = FixedCluster.automorphisms rep in
      let orbits = Set.group_by (Fixed.Clust.unfrozen rep) ~equiv:(fun v1 v2 -> List.exists autos ~f:(fun f -> Fixed.Vert.equal (f v1) v2)) in
      let nameQuiver = (Filename.concat folder ("quiver_isoClass_" ^ Int.to_string i ^ "_" ^ Int.to_string j)) in
      let () = Fixed.Clust.write_cluster ~image_size:(Some(5,5,imgdpi)) nameQuiver rep ~node_color:(Color.by_group ~groups:orbits ~found:Set.mem) in
      let nameGraph = (Filename.concat folder ("graph_isoClass_"^ Int.to_string i ^"_" ^ Int.to_string j)) in
      let _:int =  CSearch.write_picture
                 ~fLabels:fLabels
                 ~image_size:(Some(5,5,imgdpi))
                 ~node_color:(fun c -> if Set.mem iso_class c then 1 else 0)
                 nameGraph
                 fullgraph in
      ()

    let write_subalgebraPics ?fLabels:(fLabels=true) dir fullgraph (_(*name*),frozen,graph) =
      let good c = Set.for_all frozen ~f:(fun v -> Cluster.mem c v) in
      let frozen_names = frozen |> Set.to_list |> List.map ~f:Vert.to_string |> String.concat ~sep:"_" in
      let nameEmbed = (Filename.concat dir ("graph_embeded_" (*^ name ^ "_"*)^frozen_names)) in
      let _:int = CSearch.write_picture
                ~fLabels:fLabels
                ~node_color:(fun c -> if good c then 3 else 0)  ~edge_color:(fun (c,d,_)-> if good c && good d then 3 else 0)
                ~image_size:(Some(5,5,imgdpi))
                nameEmbed fullgraph in
      let nameFocus = (Filename.concat dir ("graph_focused_" (*^ name ^ "_"*) ^ frozen_names)) in
      let _:int = CSearch.write_picture ~image_size:(Some(5,5,imgdpi)) nameFocus graph in
      ()

    let xname_short x = String.concat ~sep:"*" (List.map ~f:Vert.to_string (XVert.top_list x)) ^ "_"
                        ^ String.concat ~sep:"*" (List.map ~f:Vert.to_string (XVert.bot_list x))

    let write_xorbit ?fLabels:(fLabels=true) dir graph (orbit:XVert.InvEq.Set.t) =
      let xname = xname_short (Set.min_elt_exn orbit) in
      let _:int = CSearch.write_picture
                ~fLabels:fLabels
                ~edge_weight:5
                ~image_size:(Some (5,5,imgdpi))
                ~edge_color:(fun (c,_,(v)) -> if XVert.InvEq.Set.mem orbit (CSearch.xcoord c v) then 5 else 0)
                (Filename.concat dir ("graph_xorbit_" ^ xname))
                graph in
      let _:int = Caml.Sys.command("cp \"" ^ Filename.concat dir "../empty.png" ^ "\" \""^ Filename.concat dir ("xorbit_"^xname) ^ "\"")
      in ()

    let fAffine s = String.is_substring ~substring:"affine" s

    let create_all ?maxcodim:(maxcodim=1) ?xorbit:(xorbit= None) ?fLabels:(fLabels=true) dir graph  =
      let _:int = CSearch.write_picture ~fLabels:fLabels ~image_size:(Some(5,5,imgdpi)) (Filename.concat dir "graph_embeded_") graph in
      let dim = Cluster.dim(List.hd_exn(Map.keys graph)) in
      let codim = List.init maxcodim ~f:(fun i ->
                      CSearch.subalgebras_named (i+1) graph
                          |> List.filter  ~f:(fun (name,_,graph) -> fAffine name
                                                                    || Map.for_all graph ~f:(fun nbr -> List.length(nbr) = dim-(i+1))) )
                  |> List.concat
      in
      let () = List.iter codim ~f:(write_subalgebraPics ~fLabels:fLabels dir graph) in
      let () = if maxcodim = dim
               then graph |> Map.keys
                    |> List.iter ~f:(fun c -> c |> Cluster.write_cluster
                                                     ~image_size:(Some(5,5,imgdpi))
                                                     (Filename.concat dir ("graph_focused_"^((*"0_A0_"^*)(c |> Cluster.unfrozen |> Set.to_list
                                                                                     |> List.map ~f:Vert.to_string
                                                                                     |> String.concat ~sep:"_"))))) in
      let () = CSearch.incompatMap graph |> Map.to_alist
               |> List.map ~f:(fun (v,incomp) -> (v |> Vert.to_string)
                                                 ^ "_" ^ (incomp |> Set.to_list |> List.map ~f:Vert.to_string |> String.concat ~sep:" "))
               |> Out_channel.write_lines (Filename.concat dir incompatfile) in
      let iso_undir = Set.group_by (Cluster.Set.of_list(Map.keys graph)) ~equiv:(Cluster.is_isomorphic ~directed:false)
                      |> List.sort ~compare:(fun s1 s2 -> Int.compare (Cluster.rank_pi1 (Set.choose_exn s1)) (Cluster.rank_pi1 (Set.choose_exn s2))) in
      let iso_classes = List.map iso_undir
                          ~f:(fun s -> s |> Set.group_by ~equiv:(Cluster.is_isomorphic ~directed:true)
                                      |> List.sort ~compare:(fun s1 s2 -> Int.compare (List.length(Cluster.automorphisms (Set.choose_exn s1)))
                                           (List.length(Cluster.automorphisms (Set.choose_exn s2))) ) ) in
      let () = List.iteri iso_classes ~f:(fun i l -> List.iteri l ~f:(write_iso_rep ~fLabels:fLabels dir graph i)) in
      let () = match xorbit with
        | Some orbit ->
           let xcoords = CSearch.all_xcoords graph |> XVert.InvEq.Set.map ~f:Fn.id in
           let xorbits = Set.group_by xcoords ~equiv:(fun x y -> Set.length(XVert.InvEq.Set.inter (orbit x) (orbit y)) > 0) in
           List.iter xorbits ~f:(write_xorbit ~fLabels:fLabels dir graph)
        | None -> () in
      let _:int = Caml.Sys.command("cp \"" ^ Filename.concat dir "../empty.png" ^ "\" \""^ Filename.concat dir ("graph_focused_.png\""))in
      let () = create_html dir in
      ()
end

module Plucker = Make(Plucker.CSearch)
module Gr36 = Make(Gr36.CSearch)
module Gr37 = Make(Gr37.CSearch)
module Gr38 = Make(Gr38.CSearch)
module Annulus = Make(Annulus.CSearch)
