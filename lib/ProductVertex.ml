open Core
module Make( V1: Vertex.S) (V2 : Vertex.S) : (Vertex.S with type t = V1.t * V2.t) =
  struct
    type t = V1.t * V2.t [@@deriving compare, sexp]


    let v1map m = V1.Map.of_alist_exn (List.map ~f:(fun ((x,_), d) -> (x,d)) (Map.to_alist m))
    let v2map m = V2.Map.of_alist_exn (List.map ~f:(fun ((_,y), d) -> (y,d)) (Map.to_alist m))

    let canExchange (v1,v2) ~inp ~out = V1.canExchange v1 ~inp:(v1map inp) ~out:(v1map out)
                                        && V2.canExchange v2 ~inp:(v2map inp) ~out:(v2map out)

    let weight (v1,_) = V1.weight v1

    let mutate (v1,v2) ~inp ~out = (V1.mutate v1 ~inp:(v1map inp) ~out:(v1map out)
                                   ,V2.mutate v2 ~inp:(v2map inp) ~out:(v2map out))
    let id (v1, v2) = String.concat ["("; (V1.id v1) ; "," ; (V2.id v2) ;")"]
    let to_string (v1,v2) = (V1.to_string v1) ^ "," ^ (V2.to_string v2)
    let pp ppf v = Format.fprintf ppf "%s" (to_string v)
    let of_string s =
      match String.split ~on:',' s with
      | [v1;v2] -> (V1.of_string v1, V2.of_string v2)
      | _ -> invalid_arg "inner vertecies with ',' not supported"

    module Comp = Comparable.Make(struct type t = V1.t * V2.t let compare = compare let sexp_of_t = sexp_of_t let t_of_sexp = t_of_sexp end)
    include Comp
  end

module MakeP (V1 : Vertex.PS) (V2 : Vertex.PS) : (Vertex.PS with type t = V1.t * V2.t) =
  struct
    include Make (V1) (V2)
    let of_plucker n s = (V1.of_plucker n s, V2.of_plucker n s)
  end
