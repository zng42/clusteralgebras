open Core

module type NodeS =
  sig
    type t
    val t_of_sexp : Sexplib.Sexp.t -> t
    val sexp_of_t : t -> Sexplib.Sexp.t
    include Comparable with type t := t

    type edge
    val edge_of_sexp : Sexplib.Sexp.t -> edge
    val sexp_of_edge : edge -> Sexplib.Sexp.t
    val equal_edge : edge -> edge -> bool

    val n : t -> (t * edge) list
    val follow : t -> edge list -> t * edge list
    val label : t -> string
    (* Unique (ideally small) string for each node of type t. Used to make minimal hashtable keys*)
    val id : t -> string
  end

module type S =
  sig
    module Node : NodeS

    type graph = (Node.t * Node.edge) list Node.Map.t
    val graph_of_sexp : Sexplib.Sexp.t -> graph
    val sexp_of_graph : graph -> Sexplib.Sexp.t

    (* Returns subgraph induced by the cluster set specified *)
    val induced_subgraph : graph -> Node.Set.t -> graph
    val induced : graph -> graph

    val shortest : ?cond:(int -> Node.t -> Node.edge -> bool)
                   -> ?heuristic:(Node.t -> int)
                   -> Node.t list
                   -> (Node.t -> bool)
                   ->  (Node.t * Node.edge list) option
    (* Returns Map of a shortest path from a start node to each node satisfying the condition to the start node within max_dist encounterning no more than
     *  max_size nodes.*)
    val paths_mstart : ?max_size:int
                       -> ?max_dist:int
                       -> ?cond:(Node.t -> Node.edge -> bool)
                       -> ?heuristic:(Node.t -> int)
                       -> Node.t list
                       -> (Node.edge list) Node.Map.t
    val paths : ?max_size:int
                -> ?max_dist:int
                -> ?cond:(Node.t -> Node.edge -> bool)
                -> ?heuristic:(Node.t -> int)
                -> Node.t
                -> (Node.edge list) Node.Map.t

    val search_base : Node.t list
                     -> heuristic:(Node.t -> int)
                     -> cond:(int -> Node.t -> Node.edge -> bool)
                     -> save:(Node.edge list -> Node.t -> 'a)
                     -> stop:((string , 'a) Hashtbl.t -> Node.t -> bool)
                     -> process:(Node.t option -> (string , 'a) Hashtbl.t -> 'b) -> 'b
    (* graph ~max_size ~max_dist ~cond starts  Returns the portion of the graph satisfying cond within max_dist of start nodes
     * WARNING: This will not terminate on infinite graphs unless proper finite condition/max_dist/max_size specified *)
    val graph_mstart : ?max_size:int -> ?max_dist:int -> ?cond:(Node.t -> Node.edge -> bool) -> Node.t list -> graph
    val graph : ?max_size:int -> ?max_dist:int -> ?cond:(Node.t -> Node.edge -> bool) -> Node.t -> graph

    (* Search from the start node until all neighbors don't change the collected value which is recorded whenever a node is saved *)
    val graph_collect : Node.t -> init:'a -> mem:('a -> Node.t -> bool) -> add:('a -> Node.t -> 'a) -> ('a * graph)

    val rand_search : ?cond:(Node.t * Node.edge -> bool) -> int -> (Node.t -> bool) -> Node.t -> Node.t option
    val rand_search_all : ?cond:(Node.t * Node.edge -> bool) -> int -> (Node.t -> bool) -> Node.t -> Node.Set.t
    val rand_search_max : ?cond:(Node.t * Node.edge -> bool) -> int -> (Node.t -> int) -> Node.t -> (int * Node.t)

    val pi1 : ?max_size:int -> ?cond:(Node.t*Node.edge -> bool) -> Node.t -> Node.t list list
    
    val write_graphviz :
      ?fLabels:bool ->
      ?edge_weight:int ->
      ?palette:Color.palette ->
      ?node_color:(Node.t -> int) ->
      ?edge_color:(Node.t * Node.t * Node.edge  -> int ) ->
      string ->
      graph -> unit
                 (*
    val write_graphviz : ?fLabels:bool -> ?edge_weight:int -> string -> graph -> unit
    val write_graphviz_color_nodes : ?fLabels:bool -> ?edge_weight:int -> ?palette:Color.palette -> string -> node_color:(Node.t -> int) -> graph -> unit
    val write_graphviz_color_edges :
      ?fLabels:bool -> ?edge_weight:int -> ?palette:Color.palette -> string -> edge_color:(Node.t * Node.t * Node.edge -> int) -> graph -> unit
                  *)
    val write_picture :
      ?fLabels:bool ->
      ?image_size:(int*int*int) option ->
      ?edge_weight:int ->
      ?palette:Color.palette ->
      ?node_color:(Node.t -> int) ->
      ?edge_color:(Node.t * Node.t * Node.edge  -> int) ->
      string ->
      graph ->
      int
(*
    val write_picture : ?fLabels:bool -> ?image_size:(int*int*int) option -> ?edge_weight:int -> string -> graph -> int
    val write_picture_color_nodes :
      ?fLabels:bool ->
      ?image_size:(int*int*int) option ->
      ?edge_weight:int ->
      ?palette:Color.palette ->
      string ->
      node_color:(Node.t -> int) ->
      graph ->
      int
    val write_picture_color_edges :
      ?fLabels:bool ->
      ?image_size:(int*int*int) option ->
      ?edge_weight:int ->
      ?palette:Color.palette ->
      string ->
      edge_color:(Node.t * Node.t * Node.edge -> int) ->
      graph ->
      int
 *)
  end
