open Core

type parse_normaliz =
  | Begin
  | PassStars
  | ExtremeRays of int * (int list list)
  | Finished of int list list

let parse file = 
  let channel = In_channel.create  (file^".out") in
  let res = 
    In_channel.fold_lines channel
      ~init:Begin ~f:(fun time line -> match time with
                                       | Begin -> if String.for_all ~f:(Char.equal '*') line && String.length line > 1
                                                  then PassStars
                                                  else Begin
                                       | PassStars -> (match String.chop_suffix line ~suffix:" extreme rays:" with
                                                       | None ->  PassStars
                                                       | Some pref -> ExtremeRays(Int.of_string pref,[])) 
                                       |  ExtremeRays(t,vecs) -> if Int.(t > 0)
                                                                 then ExtremeRays(t-1, (line |> String.strip
                                                                                        |> String.split ~on:' '
                                                                                        |> List.filter ~f:(fun s -> String.length s > 0)
                                                                                        |> List.map ~f:Int.of_string)::vecs)
                                                                 else Finished(vecs)
                                       | Finished(vecs) -> Finished(vecs))
  in
  let () = In_channel.close channel in
  match res with
    | Finished cone -> cone
    | _ -> invalid_arg ("Failed to generate cone for normaliz output for " ^ file)

let run file =
  let _:int = Caml.Sys.command ("normaliz -s \"" ^ file ^ ".in\"") in
  parse file
  
let write file cone =
  let f = Out_channel.create (file ^ ".in") in
  let () = Out_channel.output_string f ("amb_space " ^ (cone |> List.hd_exn |> List.length |> Int.to_string) ^ "\n") in
  let () = Out_channel.output_string f ("inequalities " ^ (cone |> List.length |> Int.to_string) ^ "\n") in
  let () = List.iter cone ~f:(fun ineq -> Out_channel.output_string f (ineq |> List.map ~f:Int.to_string |> String.concat ~sep:" " |> (fun s -> s ^ "\n"))) in
  Out_channel.close f
