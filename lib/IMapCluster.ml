open Core
module Perm = Algebra.Permutation.MapPermutation
module Heap = Pairing_heap
module Make (V : Vertex.S)  =
  struct
  open Cluster
  module Vertex = V
  type nbrs = int Int.Map.t [@@deriving compare, sexp]

  type graph = nbrs Int.Map.t [@@deriving compare, sexp]
  type cluster =  { graph: graph
                  ; label:Vertex.t Int.Map.t
                  ; index: Int.Set.t Vertex.Map.t
                  ; frozen : Int.Set.t
                  ; unfrozen : Int.Set.t} [@@deriving sexp]

  module T = struct
    type t = cluster
    let sexp_of_t = sexp_of_cluster
    let t_of_sexp = cluster_of_sexp
    (* Clusters are identified by the multiset of a coordinates regardless of index *)
    let compare c1 c2  = Vertex.Map.compare_direct (fun l1 l2 -> Int.compare (Set.length l1) (Set.length l2)) c1.index c2.index
  end

  include T

  (* Output *)
  let at c i = Map.find c.label i
  let at_exn c i =
    match at c i with
    | Some v -> v
    | None -> invalid_arg ("Clusters doesn't have a vertex indexed "^ Int.to_string i)

  let index_of c v =
    match Map.find c.index v with
    | Some s -> s
    | None -> Int.Set.empty

  let index_of_exn c v =
    match Int.Set.to_list (index_of c v) with
    | [] -> invalid_arg ("Vertex " ^ Vertex.to_string v ^ " isn't in the cluster")
    | [i] -> i
    | _ -> invalid_arg ("Vertex " ^ Vertex.to_string v ^ " occurs multiple times")

  let frozen c = Vertex.Set.map ~f:(at_exn c) c.frozen
  let unfrozen c = Vertex.Set.map ~f:(at_exn c) c.unfrozen
  let unfrozen_list c =
    c.unfrozen |> Int.Set.to_list |> List.map ~f:(at_exn c)
  let verts ?vkind:(vkind = AnyKind) c =
    match vkind with
    | Frozen -> frozen c
    | Unfrozen -> unfrozen c
    | AnyKind -> Vertex.Set.union (frozen c) (unfrozen c)
 let vertsI ?vkind:(vkind = AnyKind) c =
    match vkind with
    | Frozen -> c.frozen
    | Unfrozen -> c.unfrozen
    | AnyKind -> Int.Set.union (c.frozen) (c.unfrozen)




  let  name c =
    "q"
    ^ (c.unfrozen
      |> Int.Set.to_list
      |> List.map ~f:(fun i -> at_exn c i)
      |> List.sort ~compare:Vertex.compare
      |> List.map ~f:Vertex.to_string
       |> String.concat ~sep:"_")

  let label c =
    c.unfrozen
      |> Int.Set.to_list
      |> List.map ~f:(fun i -> at_exn c i)
      |> List.sort ~compare:Vertex.compare
      |> List.map ~f:Vertex.to_string
      |> String.concat ~sep:"\n"

  let id c =
    let s =
    c.unfrozen
    |> Set.to_list
    |> List.map ~f:(fun i -> at_exn c i)
    |> List.sort ~compare:Vertex.compare
    |> List.map ~f:Vertex.to_string
    |> String.concat ~sep:"," in
    s

  module Cluster =Comparable.Make(T)

  (* INPUT *)
  let of_nbrlist ~(graph) ~label ~(frozen) : cluster =
    { graph =  Int.Map.of_alist_exn (List.map graph ~f:(fun (v, (inbr, onbr)) ->
                                                   (v, Int.Map.of_alist_fold ~init:0 ~f:(+)
                                                         (List.append (List.map onbr ~f:(fun w -> (w,1)))
                                                            (List.map inbr ~f:(fun w -> (w,-1)))))))
    ; frozen = frozen
    ; label = label |> Int.Map.of_alist_exn
    ; index = label |> List.map ~f:(fun (x,y) -> (y,x)) |> Vertex.Map.of_alist_multi |> Vertex.Map.map ~f:Int.Set.of_list
    ; unfrozen = Int.Set.of_list(List.filter_map label ~f:(fun (key,_) -> if not (Set.mem frozen key) then Some key else None))}

  let of_list ~graph ~frozen ~label : cluster =
    { graph = Int.Map.of_alist_exn graph
    ; frozen = frozen
    ; label = label |> Int.Map.of_alist_exn
    ; index = label |> List.map ~f:(fun (x,y) -> (y,x)) |> Vertex.Map.of_alist_multi |> Map.map ~f:Int.Set.of_list
    ; unfrozen = Int.Set.of_list(List.filter_map label ~f:(fun (key,_) -> if not (Set.mem frozen key) then Some key else None))}

  let of_quiver ~(quiver:graph)  ~(frozen : Int.Set.t) ~label : cluster =
    { graph = quiver
    ; frozen = frozen
    ; label = label |> Int.Map.of_alist_exn
    ; index =  label |> List.map ~f:(fun (x,y) -> (y,x)) |> Vertex.Map.of_alist_multi |> Map.map ~f:Int.Set.of_list
    ; unfrozen = Int.Set.of_list(List.filter_map label ~f:(fun (key,_) -> if not (Set.mem frozen key) then Some key else None))}

  let of_edgelist ~graph ~frozen ~label : cluster =
    let nbrlist = Int.Map.of_alist_fold (List.concat_map graph ~f:(fun (i,j) -> [(i,(j,1));(j,(i,-1))]))
                                           ~init:Int.Map.empty
                                           ~f:(fun nbr -> fun (j, dir) -> Map.change nbr j ~f:(function None ->  Some dir | Some old -> Some(dir + old)))
    in
    of_quiver ~quiver:nbrlist ~frozen:frozen ~label:label

  let freeze c l =
    { graph = c.graph
    ; frozen = List.fold ~init:c.frozen ~f:Int.Set.add l
    ; unfrozen = List.fold ~init:c.unfrozen ~f:Int.Set.remove l
    ; label= c.label
    ; index = c.index}

  let unfreeze c l =
    { graph = c.graph
    ; frozen = List.fold ~init:c.frozen ~f:Int.Set.remove l
    ; unfrozen = List.fold ~init:c.unfrozen ~f:Int.Set.add l
    ; label = c.label
    ; index = c.index}

  let edgelistI c =
    Int.Map.fold
      c.graph
      ~init:[]
      ~f:(fun ~key:v ~data:nbr acc -> Int.Map.fold
                                        nbr
                                        ~init:acc
                                        ~f:(fun ~key:w ~data:d acc -> if d > 0 then List.append (List.init (Int.abs d) ~f:(fun _ -> (v,w)) ) acc else acc))

  let re_index c ~f = of_edgelist ~graph:(c |> edgelistI |> List.map ~f:(fun (u,v) -> (f u, f v)))
                        ~frozen:(c.frozen |> Int.Set.map ~f:f)
                        ~label:(c.label |> Int.Map.to_alist |> List.map ~f:(fun (i,weight) -> (f i,weight)))

  let flip_edges c ~f = of_edgelist ~graph:(c |> edgelistI |> List.map ~f:(fun (u,v) -> (v, u)))
                        ~frozen:(c.frozen |> Int.Set.map ~f:f)
                        ~label:(c.label |> Int.Map.to_alist |> List.map ~f:(fun (i,weight) -> (f i,weight)))


  let edgelist c =
    c |> edgelistI |> List.map ~f:(fun (vi,vo) -> (Map.find_exn c.label vi, Map.find_exn c.label vo))

  let rev c =
    { graph  = Map.map (c.graph) ~f:(Map.map ~f:(fun dir -> -dir))
    ; frozen = c.frozen
    ; unfrozen = c.unfrozen
    ; label = c.label
    ; index = c.index}

  let removeFrozen c =
    of_list ~frozen:Int.Set.empty
      ~graph:(List.filter_map (Map.to_alist c.graph)
                ~f:(fun (key,data) -> if Set.mem c.unfrozen key
                                   then Some(key, Map.filteri data ~f:(fun ~key ~data:_ -> Set.mem c.unfrozen key))
                                      else None))
      ~label:(c.label |> Map.to_alist |> List.filter ~f:(fun (i,_)-> Set.mem c.unfrozen i))


  let mem ?vkind:(vkind=AnyKind) c v =
    match (vkind, Map.find c.index v) with
    | (_, None) -> false
    | (Frozen, Some is) -> Set.for_all ~f:(Set.mem c.frozen) is
    | (Unfrozen, Some is) -> Set.for_all ~f:(Set.mem c.unfrozen) is
    | (AnyKind, Some _) -> true
  let memI ?vkind:(vkind=AnyKind) c i =
    match vkind with
    | Frozen -> Set.mem c.frozen i
    | Unfrozen -> Set.mem c.unfrozen i
    | AnyKind -> Set.mem c.unfrozen i || Set.mem c.frozen i

  let exists ?vkind:(vkind = AnyKind) (c) ~f =  Map.existsi c.index ~f:(fun ~key:v ~data:_ -> (mem ~vkind:vkind c v) && f v)
  let for_all ?vkind:(vkind = AnyKind) (c) ~f = Map.for_alli c.index ~f:(fun ~key:v ~data:_ -> not(mem ~vkind:vkind c v) || f v)
  let count ?vkind:(vkind = AnyKind) (c:cluster) ~f = Map.fold c.index ~init:0 ~f:(fun ~key:v ~data:is acc -> Set.count is ~f:(fun i -> (memI ~vkind:vkind c i) && f v) + acc)
  let find ?vkind:(vkind = AnyKind) (c:cluster) ~f = Set.find (verts ~vkind:vkind c) ~f:f
  let find_exn ?vkind:(vkind = AnyKind) (c:cluster) ~f = Set.find_exn (verts ~vkind:vkind c) ~f:f
  let filter c ~f =
    {frozen = c.frozen |> Int.Set.filter ~f:(fun i -> f(at_exn c i))
    ;unfrozen= c.unfrozen |> Int.Set.filter ~f:(fun i -> f(at_exn c i))
    ;label = c.label |> Int.Map.filter ~f:f
    ;index = c.index |> Vertex.Map.filter_keys ~f:f
    ;graph = Int.Map.filter_mapi c.graph ~f:(fun ~key:vi ~data:nbr -> if f(at_exn c vi)
                                                                      then Some(Int.Map.filteri nbr ~f:(fun ~key:wi ~data:_ -> f (at_exn c wi)))
                                                                      else None)}


  let exists_edge ?vkind:(vkind= AnyKind) (c:cluster) ~f =
    Map.existsi c.graph ~f:(fun ~key:v ~data -> (memI ~vkind:vkind c v)
                                             && Map.existsi data ~f:(fun ~key:w ~data:weight -> (memI ~vkind:vkind c w) && f(at_exn c v,at_exn c w,weight)))

  let for_all_edge ?vkind:(vkind = AnyKind) (c:cluster) ~f =
    Map.for_alli c.graph ~f:(fun ~key:v ~data -> (not (memI ~vkind:vkind c v))
                                              || Map.for_alli data ~f:(fun ~key:w ~data:weight -> (not (memI ~vkind:vkind c w)) || f(at_exn c v,at_exn c w,weight)))

  let count_edge ?vkind:(vkind=AnyKind) (c:cluster) ~f =
    Map.fold ~init:0 c.graph ~f:(fun ~key:v ~data acc -> if memI ~vkind:vkind c v
                                                         then acc+Map.counti data ~f:(fun ~key:w ~data:weight -> (memI ~vkind:vkind c w) && f(at_exn c v,at_exn c w,weight))
                                                         else acc)
  let find_edge ?vkind:(vkind=AnyKind) (c:cluster) ~f =
    Map.fold ~init:None c.graph ~f:(fun ~key:v ~data acc ->
          match acc with
          | Some ans -> Some ans
          | None ->
             if memI ~vkind:vkind c v
             then List.find_map (Map.to_alist data) ~f:(fun (w,weight) -> if (memI ~vkind:vkind c w) && f(at_exn c v,at_exn c w,weight)
                                                                          then Some (at_exn c v,at_exn c w,weight)
                                                                          else None)
             else None)

  let find_edgeI ?vkind:(vkind=AnyKind) (c:cluster) ~f =
    Map.fold ~init:None c.graph ~f:(fun ~key:v ~data acc ->
          match acc with
          | Some ans -> Some ans
          | None ->
             if memI ~vkind:vkind c v
             then List.find_map (Map.to_alist data) ~f:(fun (w,weight) -> if (memI ~vkind:vkind c w) && f((v,at_exn c v),(w,at_exn c w),weight)
                                                                          then Some (v,w,weight)
                                                                          else None)
             else None)

  let find_edge_exn ?vkind:(vkind=AnyKind) (c:cluster) ~f =
    match find_edge ~vkind:vkind c ~f:f with
    | Some ans -> ans
    | None -> invalid_arg "There is no edge in the cluster satisfying f"

  (* Note: By checking f (v,w, d) and f(v,w,d) we ensure that both copies of each edge are kept/removed together.*)
  let filter_edge c ~f =
    {frozen = c.frozen
    ;unfrozen= c.unfrozen
    ;label = c.label
    ;index = c.index
    ;graph = Int.Map.mapi c.graph ~f:(fun ~key:vi ~data:nbr -> Int.Map.filteri nbr ~f:(fun ~key:wi ~data:d -> let v = at_exn c vi in
                                                                                                              let w = at_exn c wi in
                                                                                                              f(v,w,d)|| f(w,v,-d)))}
  let filter_edgeI c ~f =
    {frozen = c.frozen
    ;unfrozen= c.unfrozen
    ;label = c.label
    ;index = c.index
    ;graph = Int.Map.mapi c.graph ~f:(fun ~key:vi ~data:nbr -> Int.Map.filteri nbr ~f:(fun ~key:wi ~data:d -> let v = at_exn c vi in
                                                                                                              let w = at_exn c wi in
                                                                                                              f((vi,v),(wi,w),d)|| f((wi,w),(vi,v),-d)))}


  let nbrI ?vkind:(vkind=AnyKind) ?nbrkind:(nbrkind=AnyNbr) c (vi) =
    match Map.find c.graph vi with
    | None -> Int.Map.empty
    | Some nbrs -> Map.filteri nbrs ~f:(fun ~key ~data:d -> memI ~vkind:vkind c key && (match nbrkind with | OutNbr -> d>0 | InNbr -> d < 0 | AnyNbr-> true))

  let nbrIv ?vkind:(vkind=AnyKind) ?nbrkind:(nbrkind=AnyNbr) c vi =
    nbrI ~vkind:vkind ~nbrkind:nbrkind c vi
    |> Map.to_alist
    |> List.map ~f:(fun (key, data) -> (at_exn c key, data))
    |> Vertex.Map.of_alist_reduce ~f:(+)

  let nbr ?vkind:(vkind=AnyKind) ?nbrkind:(nbrkind=AnyNbr) c v = nbrIv ~vkind:vkind ~nbrkind:nbrkind c (index_of_exn c v)

  let nbrSetI ?vkind:(vkind=AnyKind) ?nbrkind:(nbrkind=AnyNbr) c vi = (nbrI ~vkind:vkind ~nbrkind:nbrkind c vi) |> Map.keys |> Int.Set.of_list
  let nbrSet  ?vkind:(vkind=AnyKind) ?nbrkind:(nbrkind=AnyNbr) c v =
    index_of c v |> Set.to_list |> List.map ~f:(fun vi -> nbrSetI ~vkind:vkind ~nbrkind:nbrkind c vi) |> Int.Set.union_list |> Vertex.Set.map ~f:(at_exn c)

  let nbrListI ?vkind:(vkind=AnyKind) ?nbrkind:(nbrkind=AnyNbr) c vi =
    List.concat_map (Map.to_alist (nbrI ~vkind:vkind ~nbrkind:nbrkind c vi)) ~f:(fun (w,d) -> List.init (Int.abs d) ~f:(fun _ -> w))
  let nbrList ?vkind:(vkind=AnyKind) ?nbrkind:(nbrkind=AnyNbr) c v =
    index_of c v |> Set.to_list |> List.concat_map ~f:(fun vi -> nbrListI ~vkind:vkind ~nbrkind:nbrkind c vi) |> List.map ~f:(at_exn c)

  let dim c = Set.length(c.unfrozen)

  let edge_weightI c ~inp ~out =
    match Map.find c.graph inp with
    | None -> 0
    | Some nbr ->
       (match Map.find nbr out with
        | None -> 0
        | Some d -> d)
  let edge_weight c ~inp ~out =
    match (Set.to_list (index_of c inp) , Set.to_list (index_of c out)) with
    | ([],_) -> 0
    | (_,[]) -> 0
    | (vis,vos) ->
       List.fold vis ~init:0 ~f:(fun acc vi -> List.fold vos ~init:acc ~f:(fun acc vo -> acc + edge_weightI c ~inp:vi ~out:vo)) / (List.length vis)
  (* Note for fat vertices this isn't symmetric in inp/out. for weight 1 -> 2, edgeweight ~inp:1 ~out:2 = 2 but edgeweight ~inp:2 ~out:1 = -1 *)

  let fEdgeAbove ?vkind:(vkind=AnyKind) c ~w = exists_edge ~vkind:vkind c ~f:(fun (_,_,i) -> Int.abs i > w)

  let degree ?vkind:(vkind=AnyKind) ?nbrkind:(nbrkind=AnyNbr) (c) (v: Vertex.t) =
    Map.fold (nbr ~vkind:vkind ~nbrkind:nbrkind c v) ~init:0 ~f:(fun ~key:_ ~data acc -> acc + Int.abs data)

  let degreeWeighted ?vkind:(vkind=AnyKind) (c) (v: Vertex.t) =
    Map.fold (nbr ~vkind:vkind ~nbrkind:AnyNbr c v) ~init:0 ~f:(fun ~key ~data acc -> acc + (Vertex.weight key * data))
  let cVector ?frozenlist:(frozenlist = None) c u =
    let frozenlist = match frozenlist with Some l -> l | None -> List.sort ~compare:Vertex.compare (Set.to_list (frozen c)) in
    List.map frozenlist ~f:(fun v -> edge_weight c ~inp:v ~out:u)

  let cVectors ?frozenlist:(frozenlist = None) c =
    let frozenlist = match frozenlist with Some l -> l | None -> List.sort ~compare:Vertex.compare (Set.to_list (frozen c)) in
    let unfrozenlist = Set.to_list (unfrozen c) in
    let vectors = List.map unfrozenlist ~f:(cVector ~frozenlist:(Some frozenlist) c) in
    List.sort ~compare:(List.compare Int.compare) vectors

  let fCVectorAbove ?frozenlist:(frozenlist = None) c  ~w =
    List.exists (cVectors ~frozenlist:frozenlist c) ~f:(List.exists ~f:(fun i -> (Int.abs i) > w))
  (* A c-vector is green if all entries are nonpositive and red if all entries are nonnegative*)
  let is_vec_green = List.for_all ~f:(fun i -> Int.(i <= 0))
  let is_green ?frozenlist:(frozenlist=None) c = List.for_all ~f:is_vec_green (cVectors ~frozenlist:frozenlist c)
  let is_vec_red = List.for_all ~f:(fun i -> Int.(i>=0))
  let is_red ?frozenlist:(frozenlist=None) c = List.for_all ~f:is_vec_red (cVectors ~frozenlist:frozenlist c)

  let matrix ?(vorder=None) c =
    let verts = match vorder with None -> List.append (Set.to_list c.unfrozen) (Set.to_list c.frozen) | Some order -> order in
    List.map verts ~f:(fun v -> List.map verts ~f:(fun w -> edge_weightI c ~inp:v ~out:w))
  let transvection ?(vorder=None) c v =
    let vs = match vorder with None -> c.index |> Map.keys | Some order -> order in
    List.map vs ~f:(fun vi -> if Vertex.equal v vi
                              then List.map vs ~f:(fun vj -> if Vertex.equal vi vj then 1 else Algebra.Load.cmod (edge_weight c ~inp:vj ~out:vi) 2)
                              else List.map vs ~f:(fun vj -> if Vertex.equal vi vj then 1 else 0) )

  let two_colorI c =
    let comparedist (i, _) (j, _) = Int.compare i j in
    let q = (Heap.of_list [(0, Set.min_elt_exn (c.unfrozen))] ~cmp:comparedist) in
    let cadd (dist, w) = if Set.mem c.frozen w then () else Heap.add q (dist, w) in
    let rec loop (d) =
      match Heap.pop q with
      | None -> Some d
      | Some (dist, v) ->
         match Map.find d v with
         | Some i ->
            if i = (dist mod 2)
            then loop d
            else None
         | None ->
            if List.exists (Map.keys (nbrI c v)) ~f:(fun w -> match Map.find d w with None -> false | Some i -> i = (dist mod 2))
            then None
            else
              let () = Map.iter_keys (nbrI c v) ~f:(fun key -> cadd (dist+1, key)) in
              loop (Map.add_exn d ~key:v ~data:(dist mod 2))
    in
    match loop (Int.Map.empty) with
    | None -> None
    | Some dict ->
       Some (Map.keys (Map.filter dict ~f:(fun data -> data = 0)), Map.keys (Map.filter dict ~f:(fun data -> 1 = data)))


  let sources c = Set.filter (unfrozen c) ~f:(fun v -> Vertex.Set.is_subset (nbrSet ~nbrkind:InNbr c v) ~of_:(frozen c))
  let sinks c = Set.filter (unfrozen c) ~f:(fun v -> Vertex.Set.is_subset (nbrSet ~nbrkind:OutNbr c v) ~of_:(frozen c))
  let sources_sinks_neithers c =
    let (sources, rest) = Vertex.Set.partition_tf ~f:(fun v -> Vertex.Set.is_subset (nbrSet ~nbrkind:InNbr c v) ~of_:(frozen c)) (unfrozen c) in
    let (sinks, neithers) = Vertex.Set.partition_tf ~f:(fun v -> Vertex.Set.is_subset (nbrSet ~nbrkind:OutNbr c v) ~of_:(frozen c)) rest in
    (sources, sinks, neithers)

  let is_SourceSink c =
    let unfrozen = unfrozen c
    in Vertex.Set.for_all unfrozen ~f:(fun v -> let ins = nbrSet ~nbrkind:InNbr c v in
                                                let out = nbrSet ~nbrkind:OutNbr c v in
                                                (Vertex.Set.is_empty (Set.inter ins unfrozen))
                                                || (Vertex.Set.is_empty (Set.inter out unfrozen)))


  let directed_degree ?vkind:(vkind=AnyKind) c v = (degree ~vkind:vkind ~nbrkind:OutNbr c v, -1*degree ~vkind:vkind ~nbrkind:InNbr c v)

  let quiver c = c.graph

  let graphviz_of_cluster_color
        vlabel
        noOverlap
        c
        (scheme : Vertex.t -> int)
        (colors : int -> string): string =
    let tab = "    " in
    let edge_color = "black" in
    let string_of_edge ~inp ~out mult =
      String.concat ~sep:""
                    [ tab
                    ; "\""
                    ; Int.to_string inp
                    ; "\""
                    ; " -> "
                    ; "\""
                    ; Int.to_string out
                    ; "\""
                    ; " ["
                    ; "len = 2.0, "
                    ; if mult <= 4 then " color = \"" ^ edge_color ^ (String.concat (List.map (List.range 1 mult) ~f:(fun _ -> ":invis:" ^ edge_color))) ^ "\""
                      else " color = \"" ^ edge_color ^ "\", label = \" " ^ Int.to_string mult ^ "\""
                    ; "]"
                    ] in
    let strings_of_nbrs ~key:v ~data:nbrs  =
      List.map (Map.to_alist (Map.filter nbrs ~f:(fun w -> w > 0))) ~f:(fun (w, mult) -> string_of_edge ~inp:v ~out:w mult) in
    let edges =
      Map.fold c.graph
          ~init:[]
          ~f:(fun ~key -> fun ~data -> fun l -> List.append (strings_of_nbrs ~key:key ~data:data)  l)
    in
    String.concat ~sep:"\n"
                  [ "digraph G {"
                  ; if noOverlap then "overlap=false" else ""
                  ; String.concat ~sep:"\n" edges
                  ; ""
                  ; String.concat ~sep:"\n" (List.map ~f:(fun v -> String.concat ~sep:""
                                                                                 [ tab
                                                                                 ; "\"" ^ Int.to_string v ^ "\""
                                                                                 ; " [shape=box, color = \"#d1f2f2\", "
                                                                                 ; "label=\"" ^ (vlabel v (at_exn c v))  ^"\","
                                                                                 ; "penwidth=\"5\""
                                                                                 ; "]"])
                                                      (Set.to_list c.frozen))
                  ; String.concat ~sep:"\n" (List.map ~f:(fun v -> String.concat ~sep:"" [ tab
                                                                                         ; "\"" ^ Int.to_string v ^ "\""
                                                                                         ; " [color = "
                                                                                         ; colors (scheme (at_exn c v))
                                                                                         ; ",penwidth=\"5\""
                                                                                         ; ", label = \"" ^ (vlabel v (at_exn c v)) ^ "\""
                                                                                         ; "]" ])
                                                      (Set.to_list (c.unfrozen)))
                  ; "}"]


  let default_colors i =
    match i with
    | -1 -> "salmon"
    | 0 -> "\"#000000\""
    | 1 -> "cyan"
    | 2 -> "purple"
    | 3 -> "red"
    | 4 -> "orange"
    | 5 -> "yellow"
    | 6 -> "green"
    | 7 -> "blue"
    | 8 -> "magenta"
    | i -> invalid_arg ("Need a color for " ^ Int.to_string i)

  let vlabel _ v = Vertex.to_string(v)
  let vlabel_iv ?f:(f=vlabel) i v = Int.to_string i ^ ":" ^ (f i v)

  let write_cluster ?image_size:(isize = None) ?node_color:(scheme =(fun _ -> 0)) ?vlabel:(vlabel=vlabel) ?noOverlap:(noOverlap=true) (filename : string) (cluster : cluster)  =
    let () =  Out_channel.write_all filename ~data:(graphviz_of_cluster_color vlabel noOverlap cluster scheme default_colors) in
    match isize with
    | None -> let _:int = Caml.Sys.command ("neato " ^ "\"" ^ filename ^ "\"" ^ " -Tpng -O") in ()
    | Some(xin,yin,dpi) ->
       let _:int=Caml.Sys.command ("neato \"" ^ filename ^ "\" -Gsize="^Int.to_string xin^","^Int.to_string yin ^ "\\! -Gdpi=" ^ Int.to_string dpi
                                   ^ " -Tpng -O") in
       let _:int=Caml.Sys.command ("convert \"" ^ filename^".png\" -gravity center -background white -extent "
                                   ^ Int.to_string (xin*dpi)^"x"^Int.to_string(yin*dpi) ^ " \"" ^ filename^".png\"") in
       ()

  let write_clav (filename : string) (cluster : cluster) =
    let () = Out_channel.write_all filename ~data:(graphviz_of_cluster_color vlabel false cluster (fun _ -> 0) default_colors) in
    let vid_to_v id = Int.of_string (String.strip id) in
    let process_line l =
      match l with
      | "graph" :: _(*scale*) :: _(*width*) :: _(*height*) :: []-> ""
      | "node" :: name :: x :: y :: _(*width*) :: _(*height*) :: label :: _(*style*) :: _(*shape*) :: _(*color*) :: fillcolor :: [] ->
         String.concat ~sep:" " [ "v[" ^ name ^ "]"
                                ; "f:1"
                                ; "x:" ^ Int.to_string(Float.to_int (Float.of_string x *. 50.))
                                ; "y:" ^ Int.to_string(Float.to_int (Float.of_string y *. 50.))
                                ; "c:" ^ "0x" ^ (String.drop_prefix fillcolor 1)
                                ; "l:" ^ Int.to_string(String.length label)
                                ; "n:" ^ label
                                ; "\n"]
      | "edge" :: tail :: head :: _ ->
         let weight = Int.abs(edge_weightI cluster ~inp:(vid_to_v head) ~out:(vid_to_v tail)) in
         String.concat ~sep:""
           [ "e[" ;  tail ; "][" ; head ; "]" ; " " ; Int.to_string(weight);  "/1"; "\n"
           ; "e[" ;  head ; "][" ; tail ; "]" ; " " ; Int.to_string(-weight); "/1"; "\n"]
      | ["stop"] -> ""
      | _ -> invalid_arg "Unknown line type in plain file " in
    let process_plain l =
      let (verts, edges) = List.split_while (List.map (List.drop l 1) ~f:(fun line -> process_line (String.split line ~on:' ')))
                                            ~f:(String.is_prefix ~prefix:"v") in
      (Int.to_string (Map.length cluster.graph) ^  " Vertices" ^ "\n")
      ^ String.concat ~sep:"" (List.append (List.sort ~compare:String.compare verts) edges)

    in match Caml.Sys.command ("neato " ^ "\"" ^ filename ^ "\"" ^ " -Tplain -O") with
       | 0 -> let () = Out_channel.write_all (filename ^ ".clav") ~data:(process_plain (In_channel.read_lines (filename ^ ".plain"))) in
              (match Caml.Sys.command ("rm \"" ^ filename ^ ".plain\"") with
               | 0 -> ()
               | _ -> raise (Sys_error "Failed to delete .plain file"))
       | _ -> raise (Sys_error "Failed to produce .plain file")


  (* Functions on clusters *)
  let subset_map m1 m2 =
    Map.fold m1 ~init:true ~f:(fun ~key -> fun ~data:d1 -> fun acc -> match Map.find m2 key with Some d2 -> (Int.abs(d1) < Int.abs(d2)) && acc | None -> false)
  let subset_nbrset (nbr1 :nbrs) (nbr2 : nbrs) =
    subset_map nbr1 nbr2

  let is_subset ?vkind:(vkind=Unfrozen) c1 ~of_:c2 =
    let check ~key:u ~data:weight acc =
      acc && (match weight with
             | `Both(w1,w2) -> w1 = w2
             | `Left(_) -> false
             | `Right (_) -> not(Vertex.Set.mem (verts ~vkind:vkind c1) u)) in
    Set.for_all (verts ~vkind:vkind c1) ~f:(fun v -> Map.fold2 (nbr ~vkind:vkind c1 v) (nbr ~vkind:vkind c2 v) ~init:true ~f:check)

  let dfsnbr c parent v =
    Map.to_alist(Map.filter_keys (nbr c v) ~f:(fun key -> not(Vertex.compare key parent = 0) && not(Set.mem (frozen c) key)))

  let rank_pi1 (c : cluster) : int =
    let dfs start =
      let rec loop l parent visited count =
        match l with
        | [] -> (count, visited)
        | (v, num_edge) :: vs ->
           if Set.mem visited v
           then loop vs parent visited (count + (abs num_edge))
           else
             let (count_rec, visited_new) = loop (dfsnbr c parent v) v (Set.add visited v) (count + (abs num_edge) - 1)
             in loop (List.filter vs ~f:(fun (w,_) -> not(Set.mem (Set.diff visited_new visited) w))) parent visited_new count_rec
      in loop (dfsnbr c start start) start (Vertex.Set.singleton start) 0 in
    fst(dfs (Set.min_elt_exn (unfrozen c)))

  let component_of c v =
    let istart = index_of c v in
    let comp =
      let rec loop visited queue  =
        match Int.Set.min_elt queue with
        | None -> visited
        | Some w  ->
           if Int.Set.mem visited w
           then loop visited (Int.Set.remove queue w)
           else loop (Int.Set.add visited w) (Int.Set.union queue (Int.Set.diff (nbrSetI ~vkind:Unfrozen c w) visited))
      in loop Int.Set.empty (istart)
    in { graph = c.graph
       ; unfrozen = comp
       ; frozen = Int.Set.diff (Int.Set.union c.frozen c.unfrozen) comp
       ; label = c.label
       ; index = c.index}

  let components c =
    let rec loop queue comps  =
      match Vertex.Set.min_elt queue with
      | None -> comps
      | Some v ->
         let comp = component_of c v
         in loop (Vertex.Set.diff queue (unfrozen comp)) (comp::comps)
    in loop (unfrozen c) []

  let fConnected c = List.length (components c) = 1

  (* Requires: f to be injective on integers appearing in cluster*)
  let map (c: cluster) ~(f : Vertex.t -> Vertex.t) : cluster =
    let mapkeys m ~f = Vertex.Map.of_alist_reduce ~f:(Set.union) (List.map (Map.to_alist m) ~f:(fun (k, d) -> (f k , d))) in
    { frozen = c.frozen
    ; unfrozen = c.unfrozen
    ; index = mapkeys c.index ~f:f
    ; label = Int.Map.map c.label ~f:f
    ; graph = c.graph
    }

  let non_zero i = if i = 0 then None else Some i

  let geometric_exchangeI c (vi : int) : Vertex.t option * cluster =
    let formula ~bxy ~bxv ~bvy = non_zero(bxy + ((Int.abs(bxv) * bvy + bxv * Int.abs(bvy)) / 2)) in
    match Map.find c.graph vi with
    | None -> (None,c)
    | Some vnbrs ->
       let v = at_exn c vi in
       let vnew = Vertex.mutate v ~inp:(nbrIv ~nbrkind:InNbr c vi) ~out:(nbrIv ~nbrkind:OutNbr c vi) in
       let mutate_edge ~key:x ~data:nbr =
         if Int.equal x vi
         then Map.map nbr ~f:(fun d -> -d)
         else
           match Map.find nbr vi with
           | None -> nbr
           | Some bxv ->
            (* Map.add_exn ~key:vnew ~data:(-bxv) (Map.remove nbr v)*)
              Map.add_exn (Map.merge (Map.remove nbr vi) vnbrs ~f:(fun ~key:_ -> function `Left bxy -> formula ~bxy:bxy ~bxv:bxv ~bvy:0
                                                                                       | `Right bvy -> formula ~bxy:0 ~bxv:bxv ~bvy:bvy
                                                                                       | `Both(bxy, bvy) -> formula ~bxy:bxy ~bxv:bxv ~bvy:bvy))
                ~key:vi
                ~data:(-bxv) in

       let seed_fix_edges = Map.mapi c.graph ~f:mutate_edge in
       let index_remove_old = Map.change c.index v ~f:(function Some s -> if Set.is_empty (Set.remove s vi) then None else Some(Set.remove s vi)
                                                               | None -> invalid_arg "Can't happen") in
       let index_add_new = Map.update index_remove_old vnew ~f:(function None -> Int.Set.singleton vi | Some s -> Set.add s vi) in
       (Some vnew , { graph = seed_fix_edges
                    ; frozen = c.frozen
                    ; unfrozen = c.unfrozen
                    ; label = Map.update c.label vi ~f:(function Some _ -> vnew | None -> invalid_arg ("label should contain an entry for: " ^ Int.to_string vi))
                    ; index = index_add_new
                    })


  let mutateI_name c vi =
    let (vnew,cnew) = geometric_exchangeI c vi
    in (cnew, vnew)
  let mutateI c v = mutateI_name c v |> fst
  let mutateI_multi c l= List.fold ~init:c ~f:mutateI l
  let mutateI_multi_name c path =
      List.fold path ~init:(c,[]) ~f:(fun (c,names) v -> match mutateI_name c v with | (_,None) -> (c,names) | (cnew,Some vnew)-> (cnew, vnew::names))


  (* mutate_name c v == (c', w) where c' is the result of mutating in the direction of v, and w replaces v
   * since mutation is an involution: (uncurry mutate) (mutate_name c v) == c
   * If v occurs mutliple times, mutate at all of the them *)
  let mutate_name c v =
    match Map.find c.index v with
    | Some s ->
       let (cnew, vnews) = mutateI_multi_name c (Set.to_list s) in
       if Set.length (Vertex.Set.of_list vnews) = 1
       then (cnew, List.hd_exn vnews) (* Every v mutated to same name *)
       else invalid_arg ("Not every vertex labeled: " ^ Vertex.to_string v ^ " mutates the same way")
    | None -> (c,v)
  let mutate c v = mutate_name c v |> fst

  let mutate_multi c l = List.fold ~init:c ~f:mutate l
  let mutate_multi_name c path =
    List.fold path ~init:(c,[]) ~f:(fun (c,names) v -> let (cnew, vnew) = mutate_name c v in (cnew, vnew::names))


  let canExchange (unfrozen : Int.Set.t) (v : Vertex.t) (seed) =
    let vis = v |> index_of seed |> Set.filter ~f:(Set.mem unfrozen) in
    Set.for_all vis ~f:(fun vi -> Vertex.canExchange (at_exn seed vi) ~inp:(nbrIv ~nbrkind:InNbr seed vi) ~out:(nbrIv ~nbrkind:OutNbr seed vi))

  type edge = Vertex.t * Vertex.t [@@deriving sexp, equal]
  let follow c es = (mutate_multi c (List.map ~f:fst es), List.rev_map ~f:(fun (u,v) -> (v,u)) es)
  let n (cluster) : (cluster * edge) list =
    cluster |> unfrozen
    |> Vertex.Set.filter ~f:(fun key-> canExchange cluster.unfrozen key cluster)
    |> Vertex.Set.fold  ~init:[] ~f:(fun l key -> let (nbr,w) = mutate_name cluster key in (nbr, (key, w)) :: l)


  type edgeI = int [@@deriving sexp, equal]
  let followI c es = (mutateI_multi c es, List.rev es)
  let nI (cluster) : (cluster * edgeI) list =
    cluster.unfrozen
    |> Int.Set.filter ~f:(fun key-> canExchange cluster.unfrozen (at_exn cluster key) cluster)
    |> Int.Set.fold  ~init:[] ~f:(fun l key -> let (nbr) = mutateI cluster key in (nbr, key) :: l)


  let follow_two_color_path start count =
    let two_color_nbrs c =
      match two_colorI c with
      | None -> invalid_arg "Cluster isn't two colorable"
      | Some (odd, even) -> ( mutateI_multi (mutateI_multi c odd) even
                            , mutateI_multi (mutateI_multi c even) odd) in
    let rec loop start count current path =
      match count with
      | 0 -> path
      | _ -> let (next1,next2) = two_color_nbrs current in
             match path with
             | [] -> loop start (count - 1) next1 [current]
             | (prev :: _ ) ->
                let next = if Cluster.equal next1 prev
                           then (print_endline "right"; next2)
                           else (print_endline "left" ; next1)
                in if Cluster.equal next start
                   then path
                   else loop start (count - 1) next (current :: path)
    in List.rev(loop start count start [])

  let follow_sources_path start max_count =
    let hashtbl=  String.Table.create () in
    let rec loop start count current path =
      match count <= max_count with
      | false -> (max_count + 1, path)
      | true -> let next = mutate_multi current (Vertex.Set.to_list (sources current)) in
             match Hashtbl.find hashtbl (id next) with
             | Some i -> (i, path)
             | None ->
                let () = Hashtbl.add_exn hashtbl ~key:(id current) ~data:count in
                loop start (count + 1) next (current :: path)
    in
    let (i, path) = loop start 0 start [] in
    (i, List.rev path)


  let write_path ?filterPic:(filterPic=(fun _-> true)) ?vname:(vname=Vertex.id) dirname start path =
    let (cend, vend) =
      List.foldi
        ~init:(start,None)
        ~f:(fun i -> fun (c, vprev) -> fun v ->
                                       let vnew = Vertex.mutate v ~inp:(nbr ~nbrkind:InNbr c v) ~out:(nbr ~nbrkind:OutNbr c v) in
                                       let cnew = mutate c v in
                                       let () = write_cluster
                                                 (String.concat [dirname ; Int.to_string i; "_" ; vname v])
                                                 (filter ~f:filterPic c)
                                                 ~node_color:(fun w -> if Vertex.equal v w
                                                           then 3
                                                           else match vprev with
                                                                | None -> 0
                                                                | Some vprev -> if Vertex.equal vprev w then 2 else 0)
                                       in (cnew, Some vnew))
        path
    in write_cluster
         (String.concat [dirname; Int.to_string (List.length path); "_End_"; name cend])
         cend
         ~node_color:(fun w -> match vend with None -> 0 | Some vend -> if Vertex.equal vend w then 2 else 0)

  let write_pathIs
        ?filterPic:(filterPic=(fun _-> true))
        ?vname:(vname=(fun vis -> vis |> List.map ~f:Int.to_string |> String.concat ~sep:"_"))
        dirname
        start
        path =
    let (cend, vend) =
      List.foldi
        ~init:(start,None)
        ~f:(fun i (c, vprev) vis ->
          let cnew = mutateI_multi c vis in
          let () = write_cluster
                     ~vlabel:(fun i _ -> vname [i])
                     (String.concat [dirname ; Int.to_string i; "_" ; vname vis])
                     (filter ~f:filterPic c)
                     ~node_color:(fun w -> if List.exists vis ~f:(fun vi -> Vertex.equal (at_exn c vi) w)
                                           then 3
                                           else match vprev with
                                                | None -> 0
                                                | Some vprev -> if List.exists vprev ~f:(fun vi -> Vertex.equal (at_exn c vi) w) then 2 else 0)
          in (cnew, Some vis))
        path
    in write_cluster
         (String.concat [dirname; Int.to_string (List.length path); "_End"])
         cend
         ~node_color:(fun w -> match vend with None -> 0 | Some vend -> if List.exists vend ~f:(fun vi -> Vertex.equal (at_exn cend vi) w) then 2 else 0)

  let write_pathI ?filterPic:(filterPic=(fun _-> true)) ?vname:(vname=Int.to_string) dirname start path =
    write_pathIs ~filterPic:filterPic ~vname:(fun vis -> vis|> List.map ~f:vname |> String.concat)
      dirname start (List.map ~f:(fun x -> [x]) path)

  let write_path_condense dirname start path =
    let rec loop l i current =
      match l with
      | [] -> ()
      | _ -> let norepeat = List.take_while l ~f:(Set.mem (unfrozen current)) in
             let () = write_cluster
                       (dirname
                        ^ Int.to_string i
                        ^ "-"
                        ^ Int.to_string (i + List.length norepeat)
                        ^ "_"
                        ^ String.concat ~sep:"_" (List.map ~f:Vertex.to_string norepeat))
                       current
                       ~node_color:(fun v -> match List.findi norepeat ~f:(fun _(*i*) -> Vertex.equal v) with
                                 | None -> 0
                                 | Some (i,_) -> i+1)
             in loop (List.drop l (List.length norepeat)) (i + List.length norepeat) (mutate_multi current norepeat)
    in loop path 0 start

  (* Helper functions for Isomorphism Checking *)
  let group_by_degree directed (m : nbrs) =
    Int.Map.of_alist_fold
      ~init:Int.Set.empty
      ~f:Int.Set.add
      (Map.fold m ~init:[] ~f:(fun ~(key) ~(data:int) acc -> ((if directed then data else Int.abs data), key)::acc))
  let compatible (frozen1,m1) (frozen2, m2) =
    ((frozen1 && frozen2) || (not frozen1 && not frozen2)) &&
    Map.fold2 m1 m2 ~init:true
      ~f:(fun ~key:_ ~data acc -> match data with `Both (d1,d2) -> Set.length d1 = Set.length d2 && acc | `Right _ -> false | `Left _ -> false)
  let least_choices ~key ~data acc =
    match acc with
    | None -> Some(key, data)
    | Some (v,l) -> if Set.length data < Set.length l
                    then Some (key, data)
                    else Some (v,l)

  let isomorphismI ?vkind:(vkind=Unfrozen) ?directed:(directed = true) c1 c2 =
    let c2_verts = vertsI ~vkind:vkind c2 in
    let c2_by_degree = Set.to_map c2_verts ~f:(fun v -> group_by_degree directed (nbrI ~vkind:vkind c2 v)) in
    let c1_verts = vertsI ~vkind:vkind c1 in
    let iso_possible =
      Set.to_map c1_verts ~f:(fun v -> Set.filter c2_verts
                                ~f:(fun w -> compatible (Set.mem c1.frozen v, group_by_degree directed (nbrI ~vkind:vkind c1 v)) (Set.mem c2.frozen w, Map.find_exn c2_by_degree w))) in
    let prune possible v w =
      let wnbr = Map.find_exn c2_by_degree w in
      Map.mapi (Map.remove possible v) ~f:(fun ~key:v' ~data:set -> match edge_weightI c1 ~inp:v ~out:v' with
                                                                  | 0 -> Set.remove set w
                                                                  | weight -> Set.inter (Set.remove set w)
                                                                           (Map.find_exn wnbr (if directed then weight else Int.abs weight))) in
    let rec loop possible =
      match Map.fold possible ~init:None ~f:least_choices with
      | None -> Some []
      | Some(v,s) -> choose possible v (Set.to_list s)
    and choose possible v l =
      match l with
      | [] -> None
      | w::ws ->
         (match loop (prune possible v w) with
          | None ->  choose possible v ws
          | Some partial -> Some( (v,w) :: partial) )
    in Option.map (loop iso_possible) ~f:(fun l -> let m = Int.Map.of_alist_exn l in (fun v -> match Map.find m v with | Some w -> w | None -> v))

  let isomorphism ?vkind:(vkind=Unfrozen) ?directed:(directed = true) c1 c2 =
    Option.map (isomorphismI ~vkind:vkind ~directed:directed c1 c2) ~f:(fun f -> (fun v -> at_exn c2 (f (Set.min_elt_exn (index_of c1 v)))))

  let compatible_subgraph m1 m2 =
    Map.fold2 m1 m2 ~init:true
      ~f:(fun ~key:_ ~data acc -> match data with `Both (d1,d2) -> Set.length d1 <= Set.length d2 && acc | `Right _ -> true | `Left _ -> false)

  let all_embeddings ?vkind:(vkind = Unfrozen) ?directed:(directed = true) c1 ~of_:c2 =
    let c2_verts = vertsI ~vkind:vkind c2 in
    let c2_by_degree = Set.to_map c2_verts ~f:(fun v -> group_by_degree directed (nbrI ~vkind:vkind c2 v)) in
    let c1_verts = vertsI ~vkind:vkind c1 in
    let iso_possible =
      Set.to_map c1_verts ~f:(fun v -> Set.filter c2_verts
                                      ~f:(fun w -> compatible_subgraph (group_by_degree directed (nbrI ~vkind:vkind c1 v)) (Map.find_exn c2_by_degree w))) in
    let prune possible v w =
      let wnbr = Map.find_exn c2_by_degree w in
      Map.mapi (Map.remove possible v) ~f:(fun ~key:v' ~data:set -> match edge_weightI c1 ~inp:v ~out:v' with
                                                                  | 0 -> Set.remove set w
                                                                  | weight -> Set.inter (Set.remove set w)
                                                                           (Map.find_exn wnbr (if directed then weight else Int.abs weight))) in
    let rec loop possible =
      match Map.fold possible ~init:None ~f:least_choices with
      | None -> [[]]
      | Some (v,s) -> List.concat_map (Set.to_list s) ~f:(fun w -> List.map ~f:(fun partial -> (v,w) :: partial) (loop (prune possible v w))) in
    let subgraphs =
      List.map (loop iso_possible) ~f:(fun l -> let m = Int.Map.of_alist_exn l in (fun v -> match Map.find m (Set.min_elt_exn (index_of c1 v)) with
                                                                                               | Some w -> at_exn c2 w
                                                                                               | None -> v)) in
    List.filter subgraphs ~f:(fun f -> is_subset (map ~f:f c1) ~of_:c2)

  let is_subgraph  ?vkind:(vkind=Unfrozen) ?directed:(directed = true) c1 ~of_:c2 =
    not(List.is_empty (all_embeddings ~vkind:vkind ~directed:directed c1 ~of_:c2))

  let is_isomorphic ?vkind:(vkind=Unfrozen) ?directed:(directed = true) c1 c2 =
    match isomorphismI ~vkind:vkind ~directed:directed c1 c2 with
    | Some _ -> true
    | None -> false

  let automorphismsI ?vkind:(vkind = Unfrozen) ?directed:(directed = true) c =
    let c_verts = vertsI ~vkind:vkind c in
    let c_by_degree = Set.to_map c_verts ~f:(fun v -> group_by_degree directed (nbrI ~vkind:vkind c v)) in
    let iso_possible =
      Map.mapi c_by_degree ~f:(fun ~key:v ~data:vgroups -> Set.filter c_verts ~f:(fun w -> compatible (Set.mem c.frozen v, vgroups) (Set.mem c.frozen w,Map.find_exn c_by_degree w))) in
    let prune possible v w =
      let wnbr = Map.find_exn c_by_degree w in
      Map.mapi (Map.remove possible v) ~f:(fun ~key:v' ~data:set -> match edge_weightI c ~inp:v ~out:v' with
                                                                    | 0 -> Set.remove set w
                                                                    | weight -> Set.inter (Set.remove set w)
                                                                                  (Map.find_exn wnbr (if directed then weight else Int.abs weight))) in
    let rec loop possible =
      match Map.fold possible ~init:None ~f:least_choices with
      | None -> [[]]
      | Some (v,s) -> List.concat_map (Set.to_list s) ~f:(fun w -> List.map ~f:(fun partial -> (v,w) :: partial) (loop (prune possible v w))) in
    List.map (loop iso_possible) ~f:(fun l -> let m = Int.Map.of_alist_exn l in (fun v -> match Map.find m v with | Some w -> w | None -> v))

  let automorphisms  ?vkind:(vkind = Unfrozen) ?directed:(directed = true) c =
    List.map (automorphismsI ~vkind:vkind ~directed:directed c) ~f:(fun f -> (fun v -> if mem c v
                                                                                     then at_exn c (f (Set.min_elt_exn (index_of c v)))
                                                                                     else v))

  let tail c v =
    let rec loop prev w =
      let nbrs = nbrList ~vkind:Unfrozen c w in
      if List.length nbrs = 2 then
        match List.find nbrs ~f:(fun u -> not(Vertex.equal prev u) && not(Vertex.equal v u)) with
        | Some u -> prev:: loop w u
        | None -> invalid_arg "Tail shouldn't loop"
      else  [prev;w]
    in match nbrList ~vkind:Unfrozen c v with
       | [w] -> loop v w
       | _ -> []

  let tail_lengths c =
    List.map (Set.to_list (unfrozen c)) ~f:(fun v -> List.length (tail c v))
    |> List.filter ~f:(fun len -> len > 0)
    |> List.sort ~compare:Int.compare

  let fAffineDynkin c =
    let test c =
      (rank_pi1 c = 0 && is_SourceSink c && Vertex.Set.length (sources c ) >= (Vertex.Set.length (sinks c)))
      || (rank_pi1 c = 1 && for_all ~vkind:Unfrozen c ~f:(fun v -> degree ~vkind:Unfrozen c v = 2)
          && Vertex.Set.length(sources c) = 1 && Vertex.Set.length(sinks c) = 1)
    in List.for_all (components c) ~f:test

  let is_Tpqr c =
    match find_edgeI ~vkind:Unfrozen c ~f:(fun (_,_,w) -> w = 2) with
    | Some(u,v,_) -> is_SourceSink (freeze c [u])
                     && count_edge ~vkind:Unfrozen c ~f:(fun (_,_,w)-> w > 1) = 1
                     && Set.equal (nbrSetI ~vkind:Unfrozen ~nbrkind:OutNbr c v) (nbrSetI ~vkind:Unfrozen ~nbrkind:InNbr c u)
    | None -> false

  let dynkin_type c =
    let comp_name c =
      let dim = dim c in
      if fAffineDynkin c then
        (match tail_lengths c with
         | [_;_] ->  "A"^(Int.to_string dim)
         | [2;2;2] -> "D4"
         | [2;2;_] -> "D"^(Int.to_string dim)
         | [2;3;3] -> "E6"
         | [2;3;4] -> "E7"
         | [2;3;5] -> "E8"
         | [3;3;3] -> "E6affine"
         | [2;4;4] -> "E7affine"
         | [2;3;6] -> "E8affine"
         | [2;2;2;2] -> "D" ^ (Int.to_string(dim - 1)) ^ "affine"
         | [] ->
            let rec out_len v =
              match nbrList ~nbrkind:OutNbr c v with
              | [u] -> 1 + out_len u
              | _ -> 1
            in
            if dim = 1 then "A1" else
            (match Set.to_list (sources c) with
             | [so] -> "A" ^ Int.to_string (dim - 1) ^ "-"
                       ^ String.concat ~sep:"-" (List.map (nbrList ~vkind:Unfrozen ~nbrkind:OutNbr c so) ~f:(fun v -> Int.to_string(out_len v)))
                       ^ "affine"
             | _ -> invalid_arg "Defining quiver for Apq affine has a single source"
            )
         | _ -> "Unknown"
        )
      else if is_Tpqr c then
        match find_edgeI c ~f:(fun (_,_,w) -> Int.equal w 2) with
        | None -> invalid_arg "Every Tqr quiver has a double edge"
        | Some (u,_,2) ->
           (match tail_lengths (freeze c [u]) with
            | [] -> "A1affine"
            | [_;_] ->
               (match tail_lengths c with
                | [p] -> String.concat ["A" ; Int.to_string (dim-1) ;"-"; Int.to_string (dim-p-1) ; "-" ; Int.to_string (p+1) ; "affine"]
                | [p;q] -> String.concat ["A" ; Int.to_string (dim-1) ;"-"; Int.to_string (p+1) ; "-" ; Int.to_string (q+1) ; "affine"]
                |  _ -> invalid_arg "Only A affine should be single path")
            | [2;2;_(*n*)] -> String.concat ["D"; Int.to_string (dim-1); "affine"]
            | [2;3;3] -> "E6affine"
            | [2;3;4] -> "E7affine"
            | [2;3;5] -> "E8affine"
            | [2;2;2;2] -> "D4double"
            | [3;3;3] -> "E6double"
            | [2;4;4] -> "E7double"
            | [2;3;6] -> "E8double"
            | l -> "T"^ String.concat ~sep:"-" (List.map ~f:Int.to_string l)
           )
        | Some (_,_,_) -> invalid_arg "Every Tpqr quiver should have double edge"
      else "Not Dynkin or Tpqr"
    in Int.to_string (dim c) ^ "_" ^String.concat ~sep:"x" (List.sort ~compare:String.compare (List.map ~f:comp_name (components c)))

  let id_ordered cluster =
    cluster.label |> Int.Map.to_alist
    |> List.sort ~compare:(fun (i,_) (j,_) -> Int.compare i j)
    |> List.map  ~f:(fun (i,v) -> Int.to_string i ^ ":" ^ Vertex.id v) |> String.concat ~sep:""
  let compare_ordered c1 c2 =
    match Ordering.of_int (compare c1 c2) with
      | Less -> -1
      | Greater -> 1
      | Equal -> String.compare (id_ordered c1) (id_ordered c2)


  let applyG c (path,f) = re_index ~f:(Perm.apply (Perm.inv f)) (mutateI_multi c path)
  let applyG_list c gs = List.fold ~init:c ~f:applyG gs


  include Cluster
end
