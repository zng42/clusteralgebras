open Core
module PCluster = Plucker.Clust

module Base(V:Vertex.PS) =
  struct
    module Vert = V
    module Clust =
      struct
        include MapCluster.Make(V)
        let of_pcluster n q =
          of_list ~graph:(List.map (Core.Map.to_alist (PCluster.quiver q))
                            ~f:(fun (v, nbr) -> ( V.of_plucker n v
                                                , V.Map.of_alist_exn
                                                    (List.map (Core.Map.to_alist nbr) ~f:(fun (v,d) -> (V.of_plucker n v, d)))))
            )
            ~frozen:(V.Set.map (PCluster.frozen q) ~f:(V.of_plucker n));;
      end
  end


module Make(V: Vertex.PS) =
  struct
    module Vert = V

    module Clust =
      struct
        include MapCluster.Make(V)
        let of_pcluster n q =
          of_list ~graph:(List.map (Core.Map.to_alist (PCluster.quiver q))
                            ~f:(fun (v, nbr) -> ( V.of_plucker n v
                                                , V.Map.of_alist_exn
                                                    (List.map (Core.Map.to_alist nbr) ~f:(fun (v,d) -> (V.of_plucker n v, d)))))
            )
            ~frozen:(V.Set.map (PCluster.frozen q) ~f:(V.of_plucker n));;
      end
    let gr k n = Clust.of_pcluster n (Plucker.gr k n)
    include CAlg.Extend(struct module Vert = Vert module Clust=Clust end)

    let mapreduce l ~(init:'b) ~map ~(f: 'b -> 'b -> 'b) = List.fold l ~init:init ~f:(fun acc -> fun x -> f acc (map x))

    let write_xpairs k n folder_name str xset =
      let xWeightCompare (x1,_) (x2,_) =
        match Int.compare (mapreduce (XVert.top_list x1) ~init:0 ~map:Vert.weight ~f:(+))
                (mapreduce (XVert.top_list x2) ~init:0 ~map:Vert.weight ~f:(+)) with
        | 0 -> XVert.compare x1 x2
        | i -> i in
      let acoords = Core.Set.fold xset ~init:(Clust.frozen (gr k n))
                      ~f:(fun acc (x1,x2)  -> [XVert.top_list x1; XVert.bot_list x1; XVert.top_list x2; XVert.bot_list x2]
                                             |> List.map ~f:Vert.Set.of_list
                                             |> Vert.Set.union_list
                                             |> Vert.Set.union acc) in
      let acoords_labels =
        Vert.Map.of_alist_exn (List.concat_map
                                   (Core.Set.group_by acoords ~equiv:(fun v1 v2 -> Vert.weight v1 = Vert.weight v2))
                                   ~f:(fun s -> let w = Vert.weight (Set.min_elt_exn s) in
                                                List.mapi (Set.to_list s) ~f:(fun i -> fun v -> (v, "E"^(Int.to_string w)^"n"^Int.to_string (i+1))))) in
      let a_to_string a =  match Map.find acoords_labels a with Some s -> s | None -> invalid_arg ("Unknown A coord" ^ Vert.to_string a) in
      let x_to_string x =
        String.concat [ "("
                      ; (String.concat ~sep:" * " (List.map (XVert.top_list x) ~f:a_to_string))
                      ; ")  / ("
                      ; (String.concat ~sep:" * " (List.map (XVert.bot_list x) ~f:a_to_string))
                      ; ")"] in
      let file = Out_channel.create ("../_Clusters/gr" ^ Int.to_string k ^ Int.to_string n ^ "/" ^ folder_name ^ "/xcoords_" ^ str ^ ".txt")  in
      let () = try List.iter (List.sort ~compare:xWeightCompare (Core.Set.to_list xset))
                     ~f:(fun (x1,x2) -> Out_channel.output_string file ((x_to_string x1) ^ "  ,  " ^ (x_to_string x2) ^"\n"))
               with _ -> (Out_channel.flush file; Out_channel.close file) in
      let () = Out_channel.flush file in
      Out_channel.close file
  end
