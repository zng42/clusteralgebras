open Core
open Algebra
module IntRing = Rings.Int

(* #require "lacaml" *)
module Make (X: XCoord.S) =
  struct
    module A = X.ACoord
    module AMod = FreeRMod.Make(struct include A let of_string _(*s*) = invalid_arg "NYI" end)(IntRing)
    module ATensor = MapTensor.Make(AMod)


    let symbol n (x,xp1) =
      let x_to_tensor x =
        ATensor.of_base (AMod.of_list (List.map (X.top_list x) ~f:(fun a -> (1,a)) @ List.map (X.bot_list x) ~f:(fun a -> (-1,a)))) in
      ATensor.tensor
        (ATensor.wedge (x_to_tensor xp1) (x_to_tensor x))
        (List.fold ~init:(ATensor.primitive 1 []) ~f:ATensor.tensor (List.init (n-2) ~f:(fun _ -> x_to_tensor x)))

    let matrix n xpairs =
      let symbols = List.map xpairs ~f:(symbol n) in
      let terms = ATensor.all_terms symbols in
      List.map terms ~f:(fun i -> List.map symbols ~f:(ATensor.at ~i:i))

  end
