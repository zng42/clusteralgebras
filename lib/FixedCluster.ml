open Core

module BaseCluster =
  struct
    module T = MapCluster.Make(FixedVertex)
    include T
    include Comparable.Make(struct
                include T
                let compare c1 c2 = List.compare (List.compare Int.compare) (cVectors c1) (cVectors c2)
              end)

    let label c = String.concat ~sep:"\n" (List.map ~f:(String.concat ~sep:" ") (List.map ~f:(List.map ~f:Int.to_string) (cVectors c)))
    let id c = String.concat ~sep:"" (List.map ~f:(String.concat ~sep:"") (List.map ~f:(List.map ~f:Int.to_string) (cVectors c)))

(*     type edge = int [@@deriving compare,sexp]
    let follow c es = (mutate_multi c es, List.rev es)
    let n c = c |> T.n |> List.map ~f:(fun (nbr, (v,_(*w*))) -> (nbr,v))
 *)
  end
module BaseCompare : Comparable.S  = BaseCluster

include BaseCluster

let cVec_ordered c =
  let unfrozenlist = List.sort ~compare:Int.compare (Core.Set.to_list (unfrozen c)) in
  let frozenlist = List.sort ~compare:Int.compare (Core.Set.to_list (frozen c)) in
  List.map unfrozenlist ~f:(cVector ~frozenlist:(Some frozenlist) c)


let id_ordered c =
  String.concat ~sep:"" (List.map ~f:(String.concat ~sep:"") (List.map ~f:(List.map ~f:Int.to_string) (cVec_ordered c)))

let compare_ordered c1 c2 = List.compare (List.compare Int.compare) (cVec_ordered c1) (cVec_ordered c2)

let index ?vlist:(vlist = []) (c) =
  let start_set = Int.Set.of_list vlist in
  let full_list = vlist
                  @ (Core.Set.to_list (Core.Set.diff (unfrozen c) start_set))
                  @ (Core.Set.to_list (Core.Set.diff (frozen c) start_set)) in
  let imap = Vertex.Map.of_alist_exn (List.mapi full_list ~f:(fun i v -> (v,i+1))) in
  map ~f:(Core.Map.find_exn imap) c

let re_index = map
let un_index c = c

let vertex_to_i i = i
let vertex_to_v i = i

let at ?frozen:(frozen = true) c i = if Core.Set.mem (verts ~frozen:frozen c) i then Some i else None
let at_exn ?frozen:(frozen = true) c i = Option.value_exn (at ~frozen:frozen c i)
let at_ivertex = at
let at_ivertex_exn = at_exn


let index_of c v = if Core.Set.mem (verts c) v then Some v else None
let mutateI = mutate
let mutateI_multi = mutate_multi

let of_matrix m =
  let (num_rows, num_cols) = (List.length m, List.length (List.hd_exn m)) in
  of_edgelist ~frozen:(Int.Set.of_list (List.range (num_rows+1) (Int.abs (num_rows - num_cols))))
    ~graph:(List.concat_mapi m ~f:(fun i -> List.concat_mapi ~f:(fun j -> fun n -> if Int.(n > 0) then List.init n ~f:(fun _ -> (i+1,j+1)) else [])))

let process_keller s =
  In_channel.with_file s ~f:(fun file ->
      let rec loop acc =
        match In_channel.input_line file with
        | Some "//Matrix" ->
           (match In_channel.input_line file with
            | Some s ->
               let size = Int.of_string (String.prefix s (String.index_exn s ' ')) in
               let m = List.fold (List.range 0 size) ~init:[]
                         ~f:(fun acc _ -> match In_channel.input_line file with
                                          | None -> invalid_arg "Keller Matrix doens't have enough rows"
                                          | Some row -> (List.filter_map (String.split ~on:' ' row) ~f:(fun cell -> if String.is_empty cell
                                                                                                                 then None
                                                                                                                 else Some(Int.of_string cell))::acc)) in
               loop ((List.rev m) :: acc)
            | None -> invalid_arg "Keller matrix isn't followed by size")
        | Some _ -> loop acc
        | None -> acc in
      List.rev_map ~f:of_matrix  (loop []))

let frame (c : t) : (t * int list list) =
  let frozen = List.map (Core.Set.to_list (unfrozen c)) ~f:(fun i ->  (-i, Int.Map.singleton i 1)) in
  let graph = List.filter_map (Core.Map.to_alist (quiver c))
                ~f:(fun (v, nbr) -> if Core.Set.mem (unfrozen c) v
                                    then Some(v, Vertex.Map.of_alist_exn
                                                   ((-v,-1)::List.filter (Core.Map.to_alist nbr) ~f:(fun (w, _) -> Core.Set.mem(unfrozen c) w)))
                                    else None) in
  let c =  of_list ~graph:(List.append graph frozen) ~frozen:(Int.Set.map (unfrozen c) ~f:(fun i -> -i)) in
      (c, cVectors c)


(* Special Quiver Shapes *)
let qTns ns =
  let edge start i =
    if Int.(i = 0)
    then (start, 1)
    else if Int.(i mod 2 = 0)then (i+start-1,i+start) else (i+start, i+start-1) in
  let (_,edges) = List.fold ns ~init:(2,[(1,2);(1,2)]) ~f:(fun (tot,acc) n -> (tot+n-1, (2,tot+1)::List.init (n-1) ~f:(edge (tot+1)) @ acc)) in
  of_edgelist ~graph:edges ~frozen:Int.Set.empty

(* let qAaffine m n = qTns [m;n] *)

let qSpider ns =
  let edge start i =
    if Int.(i = 0)
    then (start, 1)
    else if Int.(i mod 2 = 0)then (i+start-1,i+start) else (i+start, i+start-1) in
  let (_,edges) = List.fold ns ~init:(0,[]) ~f:(fun (tot,acc) n -> (tot+n-1, (2,tot+1)::List.init (n-1) ~f:(edge (tot+1)) @ acc)) in
  of_edgelist ~graph:edges ~frozen:Int.Set.empty

let qGrid h w =
  let f i j = i*w+j+1 in
  let grid = ((h-1) |> Int.max 0  |> List.init ~f:(fun i -> (w-1) |> Int.max 0 |> List.init ~f:(fun j ->
                                                                                      match Int.(i mod 2 = 0, j mod 2 = 0) with
                                                                                        (true,true) -> [(f i j, f (i+1) j); (f i (j+1), f i j)]
                                                                                       |(true,false) -> [(f (i+1) j, f i j); (f i j, f i  (j+1))]
                                                                                       |(false,true) -> [(f (i+1) j, f i j); (f i j, f i  (j+1))]
                                                                                       |(false,false) -> [(f i j, f (i+1) j); (f i (j+1), f i j)]
                                       )) |> List.concat |> List.concat)
             @ ((h-1) |> Int.max 0 |> List.init ~f:(fun i -> if Bool.(Int.(i mod 2 = 0) <> Int.((w-1) mod 2 = 0))
                                                             then  (f (i+1) (w-1), f i (w-1))
                                                             else (f i (w-1), f (i+1) (w-1)) ))
             @ ((w-1) |> Int.max 0 |> List.init ~f:(fun j -> if Bool.(Int.(j mod 2 = 0) <> Int.((h-1) mod 2 = 0))
                                                             then (f (h-1) j, f (h-1) (j+1))
                                                             else (f (h-1) (j+1), f (h-1) j))) in
  of_edgelist ~graph:grid ~frozen:Int.Set.empty

let qSLnTri n =
let f (i, j, k) = Int.(if i=0 || j=0 || k = 0 then -1 else 1) * Int.(if i = 0 then j else n + n*(n+1)/2 - (n-(i-1)+1)*(n-(i-1))/2+j) in
  let good (i,j,k) = let l = [i;j;k] in List.for_all ~f:(Int.between ~low:0 ~high:n) l && Int.(List.count l ~f:(Int.equal 0) <2) in
  let vs = List.init n ~f:(fun i -> List.init (n-i+1) ~f:(fun j -> let k = n-i-j in (i,j,k))) |> List.concat
           |> List.filter ~f:(fun (i,j,k) -> Int.(List.count [i;j;k] ~f:(Int.equal 0) <2)) in
  let edges = vs |> List.concat_map ~f:(fun (i,j,k) -> [i-1,j,k+1; i,j+1,k-1; i+1,j-1,k]
                                                       |> List.filter ~f:good |> List.map ~f:(fun out -> (f(i,j,k),f out)) )in
  let frozen = vs |> List.filter ~f:(fun (i,j,k) -> List.exists [i;j;k] ~f:(Int.equal 0)) |> List.map ~f:f |> Int.Set.of_list in
  of_edgelist ~graph:edges ~frozen:frozen

let qAaffineFramed m n =
  let edge (start : int) (i: int) =
    if Int.(i = start)
    then [(1, i); (-i, i)]
    else
      if Int.((i-start) mod 2 = 0) then [(i-1,i); (-i,i)] else [(i, i-1); (i, -i)] in
  let left = List.concat_map (List.range ~stop:`inclusive 2 m) ~f:(edge 2) in
  let right = List.concat_map (List.range ~stop:`inclusive (m+1) (m + n -1)) ~f:(edge (m+1)) in
  let double = (-1, n+m) :: (1, -1) :: (n+m,1) :: (n+m,1) :: (List.append (if Int.(m > 1) then [(2, n+m)] else []) (if Int.(n > 1)then [(m+1,n+m)] else [])) in
  of_edgelist ~graph:(List.append double (List.append left right)) ~frozen: (Int.Set.of_list (List.range (-(m+n-1)) 0))

let qDaffineFramed n =
  let edge start i =
    if Int.(i = start)
    then [(1, i); (-i, i)]
    else
      if Int.((i-start) mod 2 = 0) then [(i-1,i); (-i,i)] else [(i, i-1); (i, -i)] in
  let long = List.concat_map (List.range ~stop:`inclusive 4 n) ~f:(edge 4) in
  let right = [(-2,2) ; (2,0) ; (1, 2)] in
  let left = [(-3,3) ; (3,0) ; (1,3)] in
  let double = (-1, 0) :: (1, -1) :: (0,1) :: (0,1) :: (if Int.(n >= 4) then [(4,0)] else []) in
  of_edgelist ~graph:(List.append (List.append double long) (List.append left right)) ~frozen: (Int.Set.of_list (List.range (-n) 0))

let qEaffineFramed n =
  let edge start i =
    if Int.(i = start)
    then [(1, i); (-i, i)]
    else
      if Int.((i-start) mod 2 = 0) then [(i-1,i); (-i,i)] else [(i, i-1); (i, -i)] in
  let long = List.concat_map (List.range ~stop:`inclusive 5 n) ~f:(edge 5) in
  let right = [(-2,2) ; (2,0) ; (1, 2)] in
  let left = [(-3,3) ; (3,0) ; (1,3); (4,3) ; (4,-4)] in
  let double = (-1, 0) :: (1, -1) :: (0,1) :: (0,1) :: (if Int.(n >= 5) then [(5,0)] else []) in
  of_edgelist ~graph:(List.append (List.append double long) (List.append left right)) ~frozen: (Int.Set.of_list (List.range (-n) 0))

let qAaffine m n =
  let edge start i =
    if Int.(i = start)
    then [(1, i); (-i, i)]
    else
      if Int.((i-start) mod 2 = 0) then [(i-1,i); (-i,i)] else [(i, i-1); (-i, i)] in
  let left = List.concat_map (List.range ~stop:`inclusive 2 m) ~f:(edge 2) in
  let right = List.concat_map (List.range ~stop:`inclusive (m+1) (m + n -1)) ~f:(edge (m+1)) in
  let double =
    (-(m+n), m+n) :: (-1, 1) :: (m+n,1) :: (m+n,1) :: (List.append (if Int.(m > 1) then [(2, m+n)] else []) (if Int.(n > 1) then [(m+1,m+n)] else [])) in
  of_edgelist ~graph:(List.append double (List.append left right)) ~frozen: (Int.Set.of_list (List.range (-(m+n)) 0))
