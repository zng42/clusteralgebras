open Core
open Algebra
open Quiver

module Make(Weights:sig include Ring.S
                        val abs : t -> t
                        val (+) : t -> t -> t
                        val ( * ) : t -> t -> t
                        val (-) : t -> t -> t
                        val (/) : t -> t -> t end) =
  struct
    module Weights = Weights
    type nbrs = Weights.t Int.Map.t [@@deriving compare,sexp]
    type quiver = nbrs Int.Map.t [@@deriving compare,sexp]

    module T =
      struct
        type t = {quiver: quiver
                 ;frozen: Int.Set.t
                 ;unfrozen: Int.Set.t} [@@deriving sexp]

        let edge_weight c ~inp ~out =
          match Map.find c.quiver inp with
          | None -> Weights.zero
          | Some nbr ->
             (match Map.find nbr out with
              | None -> Weights.zero
              | Some d -> d)

        let cVector ?frozenlist:(frozenlist = None) c u =
          let frozenlist = match frozenlist with Some l -> l | None -> List.sort ~compare:Int.compare (Set.to_list (c.frozen)) in
          List.map frozenlist ~f:(fun v -> edge_weight c ~inp:v ~out:u)

        let cVectors ?frozenlist:(frozenlist = None) c =
          let frozenlist = match frozenlist with Some l -> l | None -> List.sort ~compare:Int.compare (Set.to_list (c.frozen)) in
          let unfrozenlist = Set.to_list (c.unfrozen) in
          let vectors = List.map unfrozenlist ~f:(cVector ~frozenlist:(Some frozenlist) c) in
          List.sort ~compare:(List.compare Weights.compare) vectors

        let compare c1 c2 = List.compare (List.compare Weights.compare) (cVectors c1) (cVectors c2)
      end
    include T

    let of_edgelist ~graph ~frozen =
      let nbrlist = Int.Map.of_alist_fold (List.concat_map graph ~f:(fun (i,j,w) -> [(i,(j,w));(j,(i,Weights.neg w))]))
                      ~init:Int.Map.empty
                      ~f:(fun nbr -> fun (j, dir) -> Map.change nbr j ~f:(function None ->  Some dir
                                                                                 | Some old -> Some(Weights.add dir old)))
      in
      {quiver = nbrlist
      ;frozen=frozen
      ;unfrozen = nbrlist |> Map.keys |> List.filter ~f:(fun i -> not(Set.mem frozen i)) |> Int.Set.of_list}


    let frozen q = q.frozen
    let unfrozen q = q.unfrozen
    let verts ?vkind:(frozen=AnyKind) q =
      match frozen with
      | Frozen -> q.frozen
      | Unfrozen -> q.unfrozen
      | AnyKind -> Int.Set.union q.frozen q.unfrozen

    let freeze q l =
      {quiver = q.quiver
      ;frozen= List.fold ~init:q.frozen ~f:Int.Set.add l
      ;unfrozen = List.fold ~init:q.unfrozen ~f:Int.Set.remove l
      }

    let unfreeze q l =
      {quiver = q.quiver
      ;frozen= List.fold ~init:q.frozen ~f:Int.Set.remove l
      ;unfrozen = List.fold ~init:q.unfrozen ~f:Int.Set.add l
      }

    let edgelist q =
      q.quiver |> Int.Map.mapi ~f:(fun ~key ~data:nbr -> nbr |> Int.Map.filter_keys ~f:(fun j -> j>key)
                                                         |> Int.Map.to_alist
                                                         |> List.map ~f:(fun (j,w) -> (key,j,w)))
      |> Map.data
      |> List.concat

    let dim c = Set.length(c.unfrozen)

    let mem ?vkind:(vkind=AnyKind) c i =
      match vkind with
      | Frozen -> Set.mem c.frozen i
      | Unfrozen -> Set.mem c.unfrozen i
      | AnyKind -> Set.mem c.unfrozen i || Set.mem c.frozen i

    let nbr ?vkind:(vkind=AnyKind) ?nbrkind:(nbrkind=AnyNbr) q (vi) =
      match Map.find q.quiver vi with
      | None -> Int.Map.empty
      | Some nbrs -> Map.filteri nbrs ~f:(fun ~key ~data:d -> mem ~vkind:vkind q key && (match nbrkind with
                                                                                         | OutNbr -> Weights.(d>zero)
                                                                                         | InNbr  -> Weights.(d<zero)
                                                                                         | AnyNbr-> true))

    let exists_edge ?vkind:(vkind= AnyKind) (c) ~f =
      Map.existsi c.quiver ~f:(fun ~key:v ~data -> (mem ~vkind:vkind c v)
                                             && Map.existsi data ~f:(fun ~key:w ~data:weight -> (mem ~vkind:vkind c w) && f(v,w,weight)))

    let for_all_edge ?vkind:(vkind = AnyKind) (c) ~f =
      Map.for_alli c.quiver ~f:(fun ~key:v ~data -> (not (mem ~vkind:vkind c v))
                                              || Map.for_alli data ~f:(fun ~key:w ~data:weight -> (not (mem ~vkind:vkind c w)) || f(v,w,weight)))

    let count_edge ?vkind:(vkind=AnyKind) (c) ~f =
      Map.fold ~init:0 c.quiver ~f:(fun ~key:v ~data acc -> if mem ~vkind:vkind c v
                                                            then acc+Map.counti data ~f:(fun ~key:w ~data:weight -> (mem ~vkind:vkind c w) && f(v,w,weight))
                                                            else acc)
    let find_edge ?vkind:(vkind=AnyKind) (c) ~f =
      Map.fold ~init:None c.quiver ~f:(fun ~key:v ~data acc ->
        match acc with
        | Some ans -> Some ans
        | None -> List.find_map (Map.to_alist data) ~f:(fun (w,weight) -> if (mem ~vkind:vkind c w) && f(v,w,weight)
                                                                          then Some (v,w,weight)
                                                                          else None))
    let find_edge_exn ?vkind:(vkind=AnyKind) (c) ~f =
      match find_edge ~vkind:vkind c ~f:f with
      | Some ans -> ans
      | None -> invalid_arg "There is no edge in the cluster satisfying f"

    (* Note: By checking f (v,w, d) and f(w,v,d) we ensure that both copies of each edge are kept/removed together.*)
    let filter_edge c ~f =
      {frozen = c.frozen
      ;unfrozen= c.unfrozen
      ;quiver = Int.Map.mapi c.quiver ~f:(fun ~key:vi ~data:nbr -> Int.Map.filteri nbr ~f:(fun ~key:wi ~data:d -> f(vi,wi,d)|| f(wi,vi,Weights.neg d)))}


    let degree ?vkind:(vkind=AnyKind) ?nbrkind:(nbrkind=AnyNbr) (c) (v) =
      Map.fold (nbr ~vkind:vkind ~nbrkind:nbrkind c v) ~init:Weights.zero ~f:(fun ~key:_ ~data acc -> Weights.(acc + abs data))

    let matrix ?(vorder=None) c =
      let verts = match vorder with None -> List.append (Set.to_list c.unfrozen) (Set.to_list c.frozen) | Some order -> order in
      List.map verts ~f:(fun v -> List.map verts ~f:(fun w -> edge_weight c ~inp:v ~out:w))


    (* Helper functions for Isomorphism Checking *)
    let group_by_degree directed (m : nbrs) =
      Weights.Map.of_alist_fold
        ~init:Int.Set.empty
        ~f:Int.Set.add
        (Map.fold m ~init:[] ~f:(fun ~(key) ~(data:Weights.t) acc -> ((if directed then data else Weights.abs data), key)::acc))
    let compatible (frozen1,m1) (frozen2, m2) =
      ((frozen1 && frozen2) || (not frozen1 && not frozen2)) &&
        Map.fold2 m1 m2 ~init:true
          ~f:(fun ~key:_ ~data acc -> match data with `Both (d1,d2) -> Set.length d1 = Set.length d2 && acc | `Right _ -> false | `Left _ -> false)
    let least_choices ~key ~data acc =
      match acc with
      | None -> Some(key, data)
      | Some (v,l) -> if Set.length data < Set.length l
                      then Some (key, data)
                      else Some (v,l)


    let isomorphism ?vkind:(vkind=Unfrozen) ?directed:(directed = true) c1 c2 =
      let c2_verts = verts ~vkind:vkind c2 in
      let c2_by_degree = Set.to_map c2_verts ~f:(fun v -> group_by_degree directed (nbr ~vkind:vkind c2 v)) in
      let c1_verts = verts ~vkind:vkind c1 in
      let iso_possible =
        Set.to_map c1_verts ~f:(fun v -> Set.filter c2_verts
                                           ~f:(fun w -> compatible (Set.mem c1.frozen v, group_by_degree directed (nbr ~vkind:vkind c1 v))
                                                          (Set.mem c2.frozen w, Map.find_exn c2_by_degree w))) in
      let prune possible v w =
        let wnbr = Map.find_exn c2_by_degree w in
        Map.mapi (Map.remove possible v) ~f:(fun ~key:v' ~data:set -> let weight =  edge_weight c1 ~inp:v ~out:v' in
                                                                      if Weights.equal weight Weights.zero then Set.remove set w
                                                                      else  Set.inter (Set.remove set w)
                                                                              (Map.find_exn wnbr (if directed then weight else Weights.abs weight))) in
      let rec loop possible =
        match Map.fold possible ~init:None ~f:least_choices with
        | None -> Some []
        | Some(v,s) -> choose possible v (Set.to_list s)
      and choose possible v l =
        match l with
        | [] -> None
        | w::ws ->
           (match loop (prune possible v w) with
            | None ->  choose possible v ws
            | Some partial -> Some( (v,w) :: partial) )
      in Option.map (loop iso_possible) ~f:(fun l -> let m = Int.Map.of_alist_exn l in (fun v -> match Map.find m v with | Some w -> w | None -> v))

    let is_isomorphic ?vkind:(vkind=Unfrozen) ?directed:(directed = true) c1 c2 =
      match isomorphism ~vkind:vkind ~directed:directed c1 c2 with
      | Some _ -> true
      | None -> false

    let automorphisms ?vkind:(vkind = Unfrozen) ?directed:(directed = true) c =
      let c_verts = verts ~vkind:vkind c in
      let c_by_degree = Set.to_map c_verts ~f:(fun v -> group_by_degree directed (nbr ~vkind:vkind c v)) in
      let iso_possible =
        Map.mapi c_by_degree ~f:(fun ~key:v ~data:vgroups -> Set.filter c_verts ~f:(fun w -> compatible (Set.mem c.frozen v, vgroups) (Set.mem c.frozen w,Map.find_exn c_by_degree w))) in
      let prune possible v w =
        let wnbr = Map.find_exn c_by_degree w in
        Map.mapi (Map.remove possible v) ~f:(fun ~key:v' ~data:set -> let weight =  edge_weight c ~inp:v ~out:v' in
                                                                      if Weights.equal weight Weights.zero then  Set.remove set w
                                                                      else  Set.inter (Set.remove set w)
                                                                              (Map.find_exn wnbr (if directed then weight else Weights.abs weight))) in
      let rec loop possible =
        match Map.fold possible ~init:None ~f:least_choices with
        | None -> [[]]
        | Some (v,s) -> List.concat_map (Set.to_list s) ~f:(fun w -> List.map ~f:(fun partial -> (v,w) :: partial) (loop (prune possible v w))) in
      List.map (loop iso_possible) ~f:(fun l -> let m = Int.Map.of_alist_exn l in (fun v -> match Map.find m v with | Some w -> w | None -> v))


    let non_zero i = if Weights.(i = zero) then None else Some i

    let mutate c (vi : int)  =
      let formula ~bxy ~bxv ~bvy = non_zero Weights.(bxy + ( (abs(bxv) * bvy + bxv * abs(bvy)) / (Weights.one + Weights.one))) in
      match Map.find c.quiver vi with
      | None -> c
      | Some vnbrs ->
       let mutate_edge ~key:x ~data:nbr =
         if Int.equal x vi
         then Map.map nbr ~f:Weights.neg
         else
           match Map.find nbr vi with
           | None -> nbr
           | Some bxv ->
            (* Map.add_exn ~key:vnew ~data:(-bxv) (Map.remove nbr v)*)
              Map.add_exn (Map.merge (Map.remove nbr vi) vnbrs ~f:(fun ~key:_ -> function `Left bxy -> formula ~bxy:bxy ~bxv:bxv ~bvy:Weights.zero
                                                                                       | `Right bvy -> formula ~bxy:Weights.zero ~bxv:bxv ~bvy:bvy
                                                                                       | `Both(bxy, bvy) -> formula ~bxy:bxy ~bxv:bxv ~bvy:bvy))
                ~key:vi
                ~data:(Weights.neg bxv) in
       let seed_fix_edges = Map.mapi c.quiver ~f:mutate_edge in
       {quiver = seed_fix_edges
       ;frozen = c.frozen
       ;unfrozen = c.unfrozen}

    let mutate_multi q l = List.fold ~init:q ~f:mutate l



    type edge = int [@@deriving equal,sexp]
    let n q = q.unfrozen |> Set.to_list |> List.map ~f:(fun v -> (mutate q v, v))
    let follow q l = (mutate_multi q l, List.rev l)



    let label c = String.concat ~sep:"\n" (List.map ~f:(String.concat ~sep:" ") (List.map ~f:(List.map ~f:Weights.to_string) (cVectors c)))
    let id c = String.concat ~sep:"" (List.map ~f:(String.concat ~sep:"") (List.map ~f:(List.map ~f:Weights.to_string) (cVectors c)))

    include Comparable.Make(T)
  end
