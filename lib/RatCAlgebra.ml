open Algebra
open Core
     
module Make(Arg: sig val vars : string list end) =
  struct
    module Polys = Flint.BigIntMPoly.Make(struct let vars = Arg.vars let ord = Flint.BigIntMPoly.Ord.LEX end)
    module Rats = Rationals.Make(Polys)
    let vars = Arg.vars |> List.map ~f:Rats.of_string
    let v i = match List.nth vars i with
        Some x -> x
      | None -> invalid_arg "i is larger then supplied numbers of variables"
    include CAlg.Make(RingVertex.Make(Rats))
  end
