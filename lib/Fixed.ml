module Vert = FixedVertex
module Clust = FixedCluster

include CAlg.ExtendIndexed(struct module Vert = Vert module Clust = Clust end)
