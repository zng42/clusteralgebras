open Core
open Algebra
module Perm = Permutation.MapPermutation

module Group =
  struct
    module T =
      struct
        type t = int list * Perm.t [@@deriving sexp,compare]
      end
    include T
    include Comparable.Make(T)
    let one  = ([], Perm.one)

    let mult (path1,f1) (path2,f2) = (List.append path1 (List.map ~f:(Perm.apply f1) path2), Perm.mult f1 f2)
    let inv (path,f) = (List.rev_map ~f:(Perm.apply (Perm.inv f)) path, Perm.inv f)
    let pow g n =
      match Ordering.of_int(Int.compare n 0) with
      | Less -> Fn.apply_n_times ~n:(Int.abs n) (mult (inv g)) one
      | Equal -> one
      | Greater -> Fn.apply_n_times ~n:n (mult g) one
    let to_string (path,f) =
      "[" ^ String.concat ~sep:"," (List.map path ~f:Int.to_string) ^ "]" ^
        Perm.to_string f

    let pp = (fun ppf g -> Format.fprintf ppf "%s" (to_string g))
    let conjugate ~by:g h = mult g (mult h (inv g))

  end


module Make(ICluster : IndexedCluster.S) =
  struct
    module ISearch = ClusterSearch.Make(ICluster)
    let apply c (path,f) =
      ICluster.re_index ~f:(Perm.apply (Perm.inv f)) (ICluster.mutateI_multi c path)

    let apply_list c gs = List.fold ~init:c ~f:apply gs

    let modular_group ?(max_dist=Int.max_value) ~cond istart =
      let autos = ICluster.automorphisms istart in
      let paths = ISearch.paths ~max_dist:max_dist ~cond:cond istart in
      let group_fun iso auto i =
        match ICluster.at_ivertex istart i with
        | None -> i
        | Some v -> ICluster.vertex_to_i (iso(auto(v))) in
      List.concat(Map.data(
                      Map.filter_mapi paths ~f:(fun ~key ~data -> match ICluster.isomorphism istart key with
                                                                  | Some iso ->
                                                                     Some(List.map autos
                                                                            ~f:(fun f -> (List.map ~f:(fun (v) -> ICluster.vertex_to_i v) data
                                                                                      , Perm.of_fun (Set.length(ICluster.unfrozen istart)) (group_fun iso f))))
                                                                  | None -> None)))

    let change_base c1 c2 =
      match ISearch.shortest c1 c2 with
      | Some (_,path) ->
         let g = (List.rev_map path ~f:(fun (v) -> ICluster.vertex_to_i v), Perm.one) in
         Group.conjugate g
      | None -> invalid_arg "No path between clusters"


    module GroupSearch =
      struct
        module Node =
          struct
            module T =
              struct
                type t = ICluster.t * Group.t list [@@deriving sexp]
                let compare (n1,_) (n2,_) = ICluster.compare_ordered n1 n2
              end
            include T
            include Comparable.Make(T)

            let label (node,_) = ICluster.label node
            let id (node,_)  = ICluster.id_ordered node

            type edge = Group.t [@@deriving equal]
            let edge_of_sexp = Group.t_of_sexp
            let sexp_of_edge = Group.sexp_of_t

            let follow (node,groups) es = ((apply_list node es, groups),List.rev_map ~f:Group.inv es)
            let n (node,groups) = List.map groups ~f:(fun g -> ((apply node g, groups),g) )

          end
        include SearchBase.Make(Node)
      end

  end

include Group
