open Core
module Heap = Pairing_heap

module Make (N : Search.NodeS) :
sig
  include Search.S with module Node := N
end =
  struct
    module Node = N
    type graph = (Node.t * Node.edge) list Node.Map.t [@@deriving sexp]

    let induced_subgraph (g : graph) (s : Node.Set.t) =
      Map.filter_mapi g ~f:(fun ~key:v -> fun ~data:nbrs -> if Set.mem s v
                                                        then Some(List.filter nbrs ~f:(fun (w,_) -> Set.mem s w))
                                                         else None)

    let induced g = induced_subgraph g (Node.Set.of_list (Map.keys g))

    let comparedist (i, _) (j, _) = Int.compare i j

    let pi1 ?(max_size=Int.max_value) ?(cond= (fun (_(*c*), _(*e*)) -> true)) start =
      let visited = String.Table.create () in
      let _ = Hashtbl.add_exn visited ~key:(Node.id start) ~data:None in
      let rec dfs l cycles =
        if Hashtbl.length visited > max_size
        then cycles
        else
          match l with
          | [] -> cycles
          | (v,vparent)::vs ->
             match Hashtbl.find visited (Node.id v) with
             | Some _ -> dfs vs cycles
             | None ->
                let (found,unfound) = v |> Node.n |> List.filter ~f:cond
                                      |> List.map ~f:fst
                                      |> List.partition_tf ~f:(fun w -> not(Node.equal w vparent) &&
                                                                          w |> Node.id |> Hashtbl.mem visited) in
                let () = Hashtbl.add_exn visited ~key:(Node.id v) ~data:(Some vparent) in
                dfs ((List.map ~f:(fun nbr -> (nbr,v)) unfound) @ vs) ((List.map ~f:(fun nbr -> (v,nbr)) found) @ cycles) 
      in
      let unwrap (u,v) =
        let rec loop u acc =
          match Hashtbl.find_exn visited (Node.id u) with
          | Some upartent -> if Node.equal v upartent then u::acc else loop upartent (u::acc)
          | None -> invalid_arg ("loop didn't close from " ^ Node.id u ^ "--" ^ Node.id v)
        in loop u [v]
      in
      let back_edge = dfs (start |> Node.n |> List.map ~f:fst |> List.map ~f:(fun v -> (v,start))) [] in
      back_edge |> List.map ~f:unwrap 
    
    let search_base
          (start : Node.t list)
          ~(heuristic : Node.t -> int)
          ~(cond : int -> Node.t -> Node.edge -> bool)
          ~(save : Node.edge list -> Node.t -> 'a)
          ~(stop : 'tbl -> Node.t -> bool)
          ~(process : Node.t option -> 'tbl -> 'b) =
      let cur_dist = ref 0 in
      let tbl = String.Table.create () in
      let bad = String.Hash_set.create () in
      let q = (Heap.of_list (List.map start ~f:(fun node ->(heuristic node,(node,[])))) ~cmp:comparedist) in
      let cadd (dist, (nbr, path))  =
        let id = Node.id nbr in
        if Hashtbl.mem tbl id
        then ()
        else (if not(Hash_set.mem bad id) &&  cond dist nbr (List.hd_exn path)
              then  Heap.add q (dist, (nbr, path))
              else Hash_set.add bad id) in
      let () = Printf.printf "%2i" 0 in
      let rec loop () =
        match Heap.pop q with
        | None -> process None tbl
        | Some ((dist : int), (v,path)) ->
           match Hashtbl.find tbl (Node.id v) with
           | Some _  -> loop ()
           | None ->
              let () = if Hashtbl.length tbl mod 5000 = 0
                       then
                         let () = if List.length(path) > !cur_dist
                                  then (Out_channel.newline stdout; Printf.printf "%2i" (List.length path); cur_dist := (List.length path))
                                  else ()
                         in (Out_channel.output_char stdout '.'; Out_channel.flush stdout)
                       else () in
               let () = Hashtbl.add_exn tbl ~key:(Node.id v) ~data:(save path v) in
               let relax (nbr, mu)  =  (1+dist+heuristic nbr, (nbr, mu :: path)) in
               let () = List.iter (List.map (Node.n v) ~f:relax) ~f:cadd in
               if stop tbl v then process (Some v) tbl else loop ()
      in loop ()

    let save_graph _(*path*) v = (v, Node.n v)
    let stop_never _(*tbl*) _(*v*) = false
    let stop_size max_size tbl _(*v*) = Hashtbl.length tbl > max_size
    let process_map _(*vopt*) tbl = Node.Map.of_alist_exn (Hashtbl.data tbl)
    let trivial_heuristic _(*v*) = 0

    let graph_collect start ~init  ~mem ~add =
      let collection = ref init in
      search_base [start]
        ~heuristic:trivial_heuristic
        ~cond:(fun _(*dist*) c _(*e*) -> not(mem !collection c))
        (* Since we are doing bfs need to double check the info in c hasn't been found since it was queued *)
        ~save:(fun path -> fun c -> (if mem !collection c then () else collection := add !collection c; save_graph path c))
        ~stop:stop_never
        ~process:(fun vopt -> fun tbl -> (!collection, process_map vopt tbl))

    let shortest ?cond:(cond = (fun _(*dist*) _(*c*) _(*e*) -> true)) ?(heuristic=trivial_heuristic) start (stop : Node.t -> bool) =
      search_base start
        ~heuristic:heuristic
        ~cond:cond
        ~save:(fun path v -> (v,List.rev path))
        ~stop:(fun _(*tbl*) v -> stop v)
        ~process:(fun vopt tbl -> match vopt with None -> None | Some v -> Hashtbl.find tbl (Node.id v))
      

    let graph_mstart ?(max_size=Int.max_value) ?(max_dist=Int.max_value) ?(cond= (fun _(*c*) _(*e*) -> true)) start =
      search_base
        start
        ~heuristic:trivial_heuristic
        ~cond:(fun dist c e -> dist <= max_dist && cond c e)
        ~save:save_graph
        ~stop:(stop_size max_size)
        ~process:process_map

    let graph ?(max_size=Int.max_value) ?(max_dist=Int.max_value) ?(cond= (fun _(*c*) _(*e*) -> true)) start =
      graph_mstart ~max_size:max_size ~max_dist:max_dist ~cond:cond [start]

    let paths_mstart
          ?(max_size=Int.max_value)
          ?(max_dist=Int.max_value)
          ?(cond= (fun _(*c*) _(*e*) -> true))
          ?(heuristic=trivial_heuristic)
          (start:Node.t list) =
      search_base
        start
        ~heuristic:heuristic
        ~cond:(fun dist c e -> dist <= max_dist && cond c e)
        ~save:(fun path (v:Node.t) -> (v,List.rev path))
        ~stop:(stop_size max_size)
        ~process:process_map

    let paths
          ?(max_size=Int.max_value)
          ?(max_dist=Int.max_value)
          ?(cond= (fun _(*c*) _(*e*) -> true))
          ?(heuristic=trivial_heuristic)
          (start) =
      paths_mstart ~max_size:max_size ~max_dist:max_dist ~cond:cond ~heuristic:heuristic [start]

    let next cond node =
      let possible = List.filter_map (Node.n node) ~f:(fun (c,e) -> if cond (c,e)  then Some c else None) in
      match possible with
      | [] -> invalid_arg ("Condition isn't true for " ^ (Node.label node))
      | _ -> List.nth_exn possible (Random.int (List.length possible))

    let rand_search ?cond:(cond = fun _ -> true) max_count stop start =
      let rec loop max_count node =
        match max_count with
        | 0 -> None
        | _ ->
           let () = if max_count mod 5000 = 0 then (Out_channel.output_char stdout '.'; Out_channel.flush stdout) else () in
           if stop node then Some node else loop (max_count - 1) (next cond node)
      in
      loop max_count start

    let rand_search_all ?cond:(cond = fun _ -> true) max_count stop start =
      let rec loop max_count node found =
        match max_count with
        | 0 -> found
        | _ ->
           let () = if max_count mod 5000 = 0 then (Out_channel.output_char stdout '.'; Out_channel.flush stdout) else ()  in
           if stop node
           then loop (max_count - 1) (next cond node) (node :: found)
           else loop (max_count - 1) (next cond node) found
      in
      Node.Set.of_list (loop max_count start [])

    let rand_search_max ?cond:(cond = fun _ -> true) max_count (prop : Node.t -> int) start =
      let rec loop max_count node (cur_val, cur_node) =
        match max_count with
        | 0 -> (cur_val, cur_node)
        | _ ->
           let () = if max_count mod 5000 = 0 then (Out_channel.printf "%d." cur_val; Out_channel.flush stdout) else ()  in
           if prop node > cur_val
           then loop (max_count - 1) (next cond node) (prop node, node)
           else loop (max_count - 1) (next cond node) (cur_val, cur_node)
      in
      loop max_count start (prop start, start)

    let tab = "    "
    let label_of_node (node : Node.t) = "\"" ^ Node.id node ^ "\""
    let line_edge edge_weight edge_color (vin : Node.t) (vout, e ) =
      String.concat ~sep:"" [ tab
                            ; label_of_node vin
                            ; " -- "
                            ; label_of_node vout
                            ; " ["
                            ; "len = 3.0, "
                            ; "penwidth=\"" ^  Int.to_string edge_weight  ^ "\", "
                            ; "color = " ^ edge_color (vin, vout, e)
                            ; "]"
                            ; ";\n" ]
    let line_vertex fLabels vertex_color v =
      String.concat ~sep:"" [ tab
                            ; label_of_node v
                            ; " ["
                            ; "color = " ^ vertex_color v
                            ; ", "
                            ; "penwidth = 5"
                            ; ", "
                            ; if fLabels
                              then "label = " ^ "\"" ^ (Node.label v) ^ "\""
                              else "label=\"\",shape=\"point\",width=.75 "
                            ; "]"
                            ; ";\n"]
    let write_graphviz
          ?fLabels:(fLabels=true)
          ?edge_weight:(edge_weight = 5)
          ?palette:(palette = Color.Category)
          ?node_color:(node_color=(fun _ -> 0))
          ?edge_color:(edge_color=(fun _ -> 0))
          filename
          graph =
      let vcolor v = Color.choose palette (node_color v) in
      let ecolor e = Color.choose palette (edge_color e) in
      Out_channel.with_file filename
        ~f:(fun graphfile ->
          let () = Out_channel.output_string graphfile "strict graph G {\n" in
          let () = Map.iter_keys graph ~f:(fun v -> Out_channel.output_string graphfile (line_vertex fLabels vcolor v)) in
          let () = Map.iteri graph
                     ~f:(fun ~key:v ~data:es -> List.iter es ~f:(fun e -> Out_channel.output_string graphfile (line_edge edge_weight ecolor v e)))
          in
          let () = Out_channel.output_string graphfile "}"
          in ())

(*
    let write_graphviz ?fLabels:(fLabels=true) ?edge_weight:(edge_weight=1) filename graph  =
      write_graphviz_color_all ~fLabels:fLabels ~edge_weight:edge_weight filename  ~node_color:(fun _ -> 0) ~edge_color:(fun _ -> 0) graph

    let write_graphviz_color_nodes
          ?fLabels:(fLabels=true)
          ?edge_weight:(edge_weight=1)
          ?palette:(palette = Color.Category)
          filename
          ~node_color:(scheme : Node.t -> int) graph =
      write_graphviz_color_all ~fLabels:fLabels ~edge_weight:edge_weight ~palette:palette filename  ~node_color:scheme ~edge_color:(fun _ -> 0) graph

    let write_graphviz_color_edges
          ?fLabels:(fLabels=true)
          ?edge_weight:(edge_weight = 5)
          ?palette:(palette = Color.Category)
          filename
          ~edge_color:(e_scheme : Node.t * Node.t * Node.edge -> int)
          graph =
      write_graphviz_color_all ~fLabels:fLabels ~edge_weight:edge_weight ~palette:palette filename graph ~node_color:(fun _ -> 0) ~edge_color:e_scheme
 *)

    let write_picture
          ?fLabels:(fLabels=true)
          ?image_size:(isize=None)
          ?edge_weight:(edge_weight=5)
          ?palette:(palette = Color.Category)
          ?node_color:(node_color = (fun _ -> 0))
          ?edge_color:(edge_color = (fun _ -> 0))
          filename
          graph =
      let () = write_graphviz ~fLabels:fLabels
                 ~edge_weight:edge_weight
                 ~palette:palette
                 filename
                 ~node_color:node_color
                 ~edge_color:edge_color graph in
      match isize with
      | None -> Caml.Sys.command ("neato \"" ^ filename ^ "\" -Tpng -O")
      | Some(xin,yin,dpi) ->
         let _:int=Caml.Sys.command ("neato \"" ^ filename ^ "\" -Gsize="^Int.to_string xin^","^Int.to_string yin ^ "\\! -Gdpi=" ^ Int.to_string dpi ^ " -Tpng -O")
         in Caml.Sys.command ("convert \"" ^ filename^".png\" -gravity center -background white -extent "
                        ^ Int.to_string (xin*dpi)^"x"^Int.to_string(yin*dpi) ^ " \"" ^ filename^".png\"")
  end

module MakeFold(Node : Search.NodeS) =
  struct
    module Node =
      struct
        module T =
          struct
            type t = Node.t * (Node.edge list list) [@@deriving sexp]
            let compare (n1,_) (n2,_) = Node.compare n1 n2
          end
        include T
        include Comparable.Make(T)

        let label (node, _(*groups*)) = Node.label node
        let id (node,_(*groups*)) = Node.id node


        type edge = Node.edge list [@@deriving sexp, equal]

        let remove_i l i  = (List.take l i) @ (List.drop l (i+1))

        let follow (node,groups) egs =
          List.fold egs ~init:((node,groups),[])
            ~f:(fun ((curC,curGroups),l) (group) -> let (newc,newgroup) = Node.follow curC group
                                                    in ((newc,newgroup::(List.filter curGroups ~f:(Fn.non (List.equal Node.equal_edge group))))
                                                       ,newgroup::l)
            )
        let n (node,groups) =
          List.mapi groups ~f:(fun i group -> let (newc,newgroup) = Node.follow node group
                                           in ((newc, newgroup::(remove_i groups i)), group))
      end
    include Make(Node)
  end
