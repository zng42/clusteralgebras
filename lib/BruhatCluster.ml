open Core

type cartan =
  (* Finite simply laced *)
  | A of int
  | D of int
  | E6
  | E7
  | E8
  (* Finite folded *)
  | B of int
  | C of int
  | F4
  | G2
  (* Affine simply laced *)
  | AffA of int * int
  | AffD of int
  | AffE6
  | AffE7
  | AffE8
  (* Affine folded *)
  | AffB of int
  | AffC of int
  | AffF4
  | AffG2
  (* Doubly extended simply laced *)
  | DblA1
  | DblD4
  | DblE6
  | DblE7
  | DblE8
  (* Doubly extended folded *)
  | DblBC1_41
  | DblB2_21
  | DblBC2_42
  | DblG2
  | DblG2_31
  | DblB3
  | DblF4
  | DblF4_21
  | Cross of cartan list [@@deriving compare, sexp]

module CartanMap = Map.Make(struct type t = cartan [@@deriving sexp] let compare = compare_cartan end)
let (|.) f g x = f(g(x))

let cartan_to_texstring (c : cartan) =
  let rec loop c =
    match c with
    (* Finite Simply Laced *)
    | A n -> "A_{" ^ Int.to_string n ^ "}"
    | D n -> "D_{" ^ Int.to_string n ^ "}"
    | E6 -> "E_{6}"
    | E7 -> "E_{7}"
    | E8 -> "E_{8}"
    (* Finite folded *)
    | B n -> "B_{" ^ Int.to_string n  ^ "}"
    | C n -> "C_{" ^ Int.to_string n ^ "}"
    | F4 -> "F_{4}"
    | G2 -> "G_{2}"
    (* Affine Simply laced*)
    | AffA (p, q) -> "\\Affine{A_{" ^ Int.to_string p ^ "," ^ Int.to_string q ^ "}}"
    | AffD n -> "\\Affine{D_{" ^ Int.to_string n ^ "}}"
    | AffE6 -> "\\Affine{E_{6}}"
    | AffE7 -> "\\Affine{E_{7}}"
    | AffE8 -> "\\Affine{E_{8}}"
    (* Affine folded *)
    | AffB n -> "\\Affine{B_{"^Int.to_string n ^ "}}"
    | AffC n -> "\\Affine{C_{"^Int.to_string n ^ "}}"
    | AffF4 -> "\\Affine{F_{4}}"
    | AffG2 -> "\\Affine{G_{2}}"
    (* Doubly extended simply laced *)
    | DblA1 -> "\\db{A}{1}"
    | DblD4 -> "\\db{D}{4}"
    | DblE6 -> "\\db{E}{6}"
    | DblE7 -> "\\db{E}{7}"
    | DblE8 -> "\\db{E}{8}"
    (* Doubly extended folded *)
    | DblBC1_41 -> "\\dbf{BC}{1}{4}{1}"
    | DblB2_21 -> "\\dbf{B}{2}{2}{1}"
    | DblBC2_42 -> "\\dbf{BC}{2}{4}{2}"
    | DblG2 -> "\\db{G}{2}"
    | DblG2_31 -> "\\dbf{G}{2}{3}{1}"
    | DblB3 -> "\\db{B}{3}"
    | DblF4 -> "\\dbf{F}{4}"
    | DblF4_21 -> "\\dbf{F}{4}{2}{1}"
    | Cross l ->
       let power l =
         match l with
         | [] -> ""
         | [c'] -> loop c'
         | c' :: _ -> loop c' ^ "^{" ^ Int.to_string(List.length l) ^ "}" in
       String.concat ~sep:" \\times "
                     (List.map (List.group l ~break:(fun c1 c2 -> not(compare_cartan c1 c2 = 0))) ~f:power)
  in
  "$" ^ loop c ^ "$"

let cartan_to_string (c : cartan) =
  let rec loop c =
    match c with
    | A n -> "A" ^ Int.to_string n
    | B n -> "B" ^ Int.to_string n
    | C n -> "C" ^ Int.to_string n
    | D n -> "D" ^ Int.to_string n
    | E6 -> "E6"
    | E7 -> "E7"
    | E8 -> "E8"
    | F4 -> "F4"
    | G2 -> "G2"
    | AffA (p,q) -> "AffA" ^ Int.to_string p ^ Int.to_string q
    | AffD n -> "AffD" ^ Int.to_string n
    | AffE6 -> "AffE6"
    | AffE7 -> "AffE7"
    | AffE8 -> "AffE8"
    | AffB n -> "AffB"^Int.to_string n
    | AffC n -> "AffC"^Int.to_string n
    | AffF4 -> "AffF4"
    | AffG2 -> "AffG2"
    | DblA1 -> "DblA1"
    | DblD4 -> "DblD4"
    | DblE6 -> "DblE6"
    | DblE7 -> "DblE7"
    | DblE8 -> "DblE8"
    | DblBC1_41 -> "DblBC1_41"
    | DblB2_21 -> "DblB2_21"
    | DblBC2_42 -> "DblBC2_42"
    | DblG2 -> "DblG2"
    | DblG2_31 -> "DblG2_31"
    | DblB3 -> "DblB3"
    | DblF4 -> "DblF4"
    | DblF4_21 -> "DblF4_21"
    | Cross l -> String.concat ~sep:"x" (List.map l ~f:loop)
  in
  loop c

let rec rank (c : cartan) : int =
  match c with
  | A (r) -> r
  | B (r) -> r
  | C (r) -> r
  | D (r) -> r
  | E6 -> 6
  | E7 -> 7
  | E8 -> 8
  | F4 -> 4
  | G2 -> 2
  | Cross l -> List.fold l ~init:1 ~f:(fun acc -> fun c -> rank c * acc)
  | AffA (p,q) -> p+q
  | AffD n -> n+1
  | AffE6 -> 7
  | AffE7 -> 8
  | AffE8 -> 9
  | AffB n -> n+1
  | AffC n -> n+1
  | AffF4 -> 5
  | AffG2 -> 3
  | DblA1 -> 3
  | DblD4 -> 6
  | DblE6 -> 8
  | DblE7 -> 9
  | DblE8 -> 10
  | DblBC1_41 -> 3
  | DblB2_21 -> 4
  | DblBC2_42 -> 4
  | DblG2 -> 4
  | DblG2_31 -> 4
  | DblB3 -> 5
  | DblF4 -> 6
  | DblF4_21 -> 6

let rec standardize (c : cartan) : cartan =
  match c with
  | Cross l ->
     (match (List.filter (List.concat_map l ~f:(fun c -> match standardize c with Cross l -> l | cnew -> [cnew])) ~f:(function A 0 -> false | _ -> true)) with
      | [] -> Cross []
      | [c] -> c
      | cs ->Cross(List.rev(List.sort ~compare:compare_cartan cs)))
  | B 0 -> A 0
  | B 1 -> A 1
  | C 0 -> A 0
  | C 1 -> A 1
  | D 1 -> invalid_arg "D1 shouldn't exist"
  | D 2 -> Cross([A 1; A 1])
  | D 3 -> A 3
  | AffA (p,q) -> AffA(Int.min p q, Int.max p q)
  | AffD 3 -> AffA(2,2)
  | AffC 2 -> AffB 2
  | _ -> c

let rec coexter_number (c: cartan) =
  match c with
  | A n -> n+1
  | B n -> 2*n
  | C n -> 2*n
  | D n -> 2*n - 2
  | E6 -> 12
  | E7 -> 18
  | E8 -> 30
  | F4 -> 12
  | G2 -> 6
  | Cross l -> List.fold l ~f:(fun acc c -> coexter_number c + acc) ~init:0
  | _ -> invalid_arg "Coexeter number not defined for nonfinite types"

(* Assume special vertex is at low numbers. So double edge in Bn is 1=2-... and Dn is 12>3-4-... *)
let rec longest_word_inductive c =
  match c with
  | A 1 -> [1]
  | A n -> List.init n ~f:(fun i -> i+1) @ longest_word_inductive (A (n-1))
  | B 1 -> [1]
  | B n -> List.init n ~f:(fun i -> n-i) @ List.init (n-1) ~f:(fun i -> (i+2)) @ longest_word_inductive (B (n-1))
  | D 2 -> [1;2]
  | D n -> longest_word_inductive (B (n-1)) |> List.concat_map ~f:(fun i -> if i > 1 then [i+1] else [1;2])
  | _ -> invalid_arg "NYI inductive longest word in this case"

let longest_word_repeated c =
  match c with
  | A n -> (List.init ((n+1)/2) ~f:(fun _ -> (List.range ~stride:2 1 (n+1))@(List.range ~stride:2 2 (n+1))) |> List.concat)
           @ (if n mod 2 = 0 then List.range ~stride:2 1 (n+1) else [])
  | B n -> List.init n ~f:(fun _ -> (List.range ~stride:2 1 (n+1))@(List.range ~stride:2 2 (n+1))) |> List.concat
  | D 1 -> [1]
  | D n -> List.init (n-1) ~f:(fun _ -> [1;2]@(List.range ~stride:2 4 (n+1))@(List.range ~stride:2 3 (n+1))) |> List.concat
  | _ -> invalid_arg "NYI longest word for this case"

let longest_word = longest_word_inductive

type wordmove =
  Two of int
| Three of int
| Four of int [@@deriving sexp,equal]

let wordmove_to_string m =
  match m with
  | Two i -> "2:"^Int.to_string i
  | Three i -> "3:"^Int.to_string i
  | Four i -> "4:"^Int.to_string i

let rec reverseBn n =
  let rec swap n shift =
    match n with
    | 1 -> []
    | 2 -> [Four(shift+1)]
    | _ -> [[Three(shift+2*n-2)]
           ; List.range 3 (2*n-2) |> List.rev |> List.map ~f:(fun i -> Two(shift+i))
           ; List.range (2*n) (4*n-5) |> List.map ~f:(fun i -> Two(shift+i))
           ; [Three(shift+1)]
           ; swap (n-1) (shift+2)
           ; [Three(shift+4*n-6)]
           ; List.range 2 (2*n-3) |> List.map ~f:(fun i -> Two(shift+i))
           ; List.range (2*n-1) (4*n-6) |> List.rev |> List.map ~f:(fun i -> Two(shift+i))
           ; [Three(shift + 2*n-3)]] |> List.concat
  in
  let rec bswap i j ~shift =
    if Int.(i=j+1) then swap i shift
    else [List.init (2*j-1) ~f:(fun k -> Two(shift+2*i-1 + k)); bswap (i-1) j ~shift:(shift+1); List.init (2*j-1) ~f:(fun k -> Two(shift+k+1))] |> List.concat
  in
  if Int.(n = 1) then [] else (List.concat (List.init (n-1) ~f:(fun i -> bswap n (n-(i+1)) ~shift:((2*n-2-i)*i)))) @ (reverseBn (n-1))

let rec reverseDn n =
  (* Dn is block (n+1),...,3,1,2,3,...,(n+1), D1=12*)
  let blocksize n = 2*n in
  let rec swap n shift =
    match n with
    | 1 -> []
    | 2 -> [Two(shift+5);Three(shift+3);Three(shift+1);Two(shift+3);Three(shift+4);Three(shift+2);Two(shift+4)]
    | _ -> [[Three(shift+2+blocksize (n-2)+1)]
           ; List.range 3 (2+blocksize (n-2)+1) |> List.rev |> List.map ~f:(fun i -> Two(shift+i))
           ; List.range (3+blocksize(n-2)+2) (3+blocksize(n-2)+2+blocksize(n-2)-1+1) |> List.map ~f:(fun i -> Two(shift+i))
           ; [Three(shift+1)]
           ; swap (n-1) (shift+2)
           ; [Three(shift+2+blocksize(n-2)+blocksize(n-1))]
           ; List.range 2 (2+blocksize(n-2)-1 +1) |> List.map ~f:(fun i -> Two(shift+i))
           ; List.range (1+blocksize(n-2)+3) (1+blocksize(n-2)+2+blocksize(n-2)+1) |> List.rev |> List.map ~f:(fun i -> Two(shift+i))
           ; [Three(shift + 1+blocksize(n-2) + 1)]] |> List.concat
  in
  let rec bswap i j ~shift =
    if Int.(i=j+1) then swap i shift
    else [List.init (blocksize j) ~f:(fun k -> Two(shift+blocksize i + k)); bswap (i-1) j ~shift:(shift+1); List.init (blocksize j) ~f:(fun k -> Two(shift+k+1))] |> List.concat
  in   let revBlock n shift = [Two(shift+n)] in
  let maxsize = n-1 in
  if Int.(maxsize = 1) then revBlock maxsize 0
  else (List.concat (List.init (maxsize-1) ~f:(fun i -> bswap maxsize (maxsize-(i+1)) ~shift:((2*maxsize-1-i)*i))))
       @ revBlock maxsize (maxsize*(maxsize-1))
       @ (reverseDn (n-1))

(* Assumption is dynkin diagram labeled as follows
 * An: 1-...-n
 * Bn: 1=2-...-n
 * Cn: 1=2-...-n
 * Dn: 1-3-...-n
 *     2/
 * En: 1-2-4-...-n
         3/
 *)
let rec dynkinNbr c i j =
  if i > j then dynkinNbr c j i else
  match c with
  | A _ -> Int.abs(i-j) = 1
  | B _ -> Int.abs(i-j) = 1
  | C _ -> Int.abs(i-j) = 1
  | D _ -> Int.abs(i-j)=1 || (i=1 && j=3)
  | E6  -> Int.abs(i-j)=1 || (i=2 && j=4)
  | E7  -> Int.abs(i-j)=1 || (i=2 && j=4)
  | E8  -> Int.abs(i-j)=1 || (i=2 && j=4)
  | _ -> invalid_arg "valid2 NYI"

module WordNode(Cartan:sig val c : cartan end) =
  struct
    module T =
      struct
        type t = int list [@@deriving sexp,compare]
      end
    include T
    let label w = w |> List.map ~f:Int.to_string |> String.concat ~sep:","

    let int_to_digit i =
      if Int.between ~low:1 ~high:9 i then Char.of_string(Int.to_string i)
      else if Int.between ~low:10 ~high:35 i then Char.of_int_exn (Char.to_int 'a' + (i-10))
      else invalid_arg ("Can't process int larger than 35 "^ Int.to_string i)

    let digit_to_int c =
      if Char.is_digit c then Int.of_string (Char.to_string c)
      else if Char.is_lowercase c then Char.to_int c - Char.to_int 'a' + 10
      else invalid_arg ("Only mapped 1-9, a-z to int" ^ Char.to_string c)

    let id w = w |> List.map ~f:int_to_digit |> String.of_char_list

    let c = Cartan.c

    let valid2 i j = not (dynkinNbr c i j)

    let valid3 i j k =
      i=k &&
        match c with
        | A _ -> Int.abs(i-j) = 1
        | B _ -> Int.abs(i-j) = 1 && not(i=1) && not(j=1)
        | C _ -> Int.abs(i-j) = 1 && not(i=1) && not(j=1)
        | D _ -> not(valid2 i j)
        | _ -> invalid_arg "valid 3 NYI"
    let valid4 i j k l =
      match c with
      | A _ -> false
      | B _ -> (i=1 && j=2 && k=1 && l=2) || (i=2 && j=1 && k=2 && l=1)
      | D _ -> false
      | _ -> invalid_arg "valid 4 NYI"

    type edge = wordmove [@@deriving sexp,equal]

    let n word =
      let rec loop n word =
        match word with
        | [] -> []
        | [_] -> []
        | [i;j] -> if valid2 i j then [[j;i],Two n] else []
        | [i;j;k] -> (if valid3 i j k then [[j;i;j],Three n]
                      else if valid2 i j then [[j;i;k],Two n]
                      else [])
                     @ List.map ~f:(fun (w,e) -> (i::w,e)) (loop (n+1) [j;k])
      | i::j::k::l::rest ->
         (if valid4 i j k l then [j::i::j::i::rest,Four n]
          else if valid3 i j k then [j::i::j::l::rest,Three n]
          else if valid2 i j then [j::i::k::l::rest,Two n]
          else [])
         @ List.map ~f:(fun (w,e) -> (i::w,e)) (loop (n+1) (j::k::l::rest)) in
      loop 1 word

    let move w e =
      match e with
      | Two n -> (match List.split_n w (n-1) with
                  | (_,[]) -> w
                  | (_,[_]) -> w
                  | (p,i::j::rest) -> p@ (j::i::rest))
      | Three n -> (match List.split_n w (n-1) with
                  | (_,[]) -> w
                  | (_,[_]) -> w
                  | (_,[_;_]) -> w
                  | (p,i::j::_::rest) -> p@ (j::i::j::rest))
      | Four n -> (match List.split_n w (n-1) with
                  | (_,[]) -> w
                  | (_,[_]) -> w
                  | (_,[_;_]) -> w
                  | (_,[_;_;_]) -> w
                  | (p,i::j::_::_::rest) -> p@ (j::i::j::i::rest))

    let follow w es = (List.fold ~init:w ~f:move es,List.rev es)
    include Comparable.Make(T)
  end

let rec count_subalg (c : cartan) (codim) : (cartan * float) list =
  let h c = (* Float.of_int (coexter_number c + 2))/. 2.0 *)
    match c with
    (* Coxeter number: order of full sources/sink move *)
    | A(n) -> ((Float.of_int n) +. 3.0) /. 2.0
    | B(n) | C(n) -> (Float.of_int n) +. 1.0
    | D(n) -> (Float.of_int n)
    | E6 -> 7.0
    | E7 -> 10.0
    | E8 -> 16.0
    | F4 -> 7.0
    | G2 -> 4.0
    (* This is 1/chi which is number of repeated copies in affine associahedron *)
    | AffA (p,q) -> Float.of_int (p*q) /. Float.of_int(p+q)
    | AffD (n) -> Float.of_int(n-2)
    | AffE6 -> 6.0
    | AffE7 -> 12.0
    | AffE8 -> 30.0
    | AffB n -> Float.of_int(n-1)
    | AffC n -> Float.of_int(n) /. 2.0
    | AffF4 -> 6.0
    | AffG2 -> 2.0
    (* Size of quotient by N and `normal closure of gamma' *)
    | DblA1 -> 1.0
    | DblD4 -> 6.0
    | DblE6 -> 12.0
    | DblE7 -> 24.0
    | DblE8 -> (* 6.0 *) (*18.0*) (24.0)
    | DblBC1_41 -> 2.0
    | DblB2_21 -> 2.0
    | DblBC2_42 -> 2.0
    | DblG2 -> 6.0
    | DblG2_31 -> 3.0
    | DblB3 -> 6.0
    | DblF4 -> 12.0
    | DblF4_21 -> 8.0
    | _ -> invalid_arg "'h' number for this kind of subalgebra not supported"
  in
  (* Asssume: c is standardized *)
  let rec codim1 (c : cartan) =
    match c with
    | A(0) -> []
    | A(1) -> [(A 0, h c)]
    | A(n) ->
       if (n mod 2) = 0
       then List.map (List.range ~stop:`inclusive 0 ((n-2) / 2)) ~f:(fun i -> (standardize (Cross([A(n-i-1); A(i)])), 2.0 *. (h c)))
       else
         (Cross([A ((n-1)/2); A((n-1)/2)]), h c)
         :: (List.map (List.range ~stop:`inclusive 0 ((n-3) / 2)) ~f:(fun i -> (standardize(Cross([A(n-i-1); A(i)])), 2.0 *. (h c))))
    | B(n) -> List.map (List.range 0 n) ~f:(fun i -> (standardize(Cross [B i; A(n-i-1)]), h c))
    | C(n) -> List.map (List.range 0 n) ~f:(fun i -> (standardize(Cross [C i; A(n-i-1)]), h c))
    (* Since c is standardized in this case n > 3 *)
    | D(n) -> (A(n-1), 2.0 *. h c) :: (List.map (List.range 1 (n-1)) ~f:(fun i -> (standardize (Cross([D(n-i); A(i-1)])), h c)))
    | E6 -> [(D 5, 2.0 *. h c); (A 5, h c); (Cross [A 4; A 1], 2.0 *. h c) ; (Cross [A 2; A 2; A 1], h c)]
    | E7 -> [(E6, h c); (D 6, h c); (A 6, h c); (Cross [D 5; A 1], h c) ; (Cross [A 5; A 1], h c); (Cross [A 4; A 2], h c); (Cross [A 3; A 2; A 1], h c)]
    | E8 -> List.map ~f:(fun subc -> (subc, h c))
                     [E7 ; D 7 ; A 7 ; Cross [E6; A 1] ; Cross [A 6; A 1] ; Cross [D 5 ; A 2] ; Cross [A 4; A 2; A 1] ; Cross [A 4; A 3]]
    | F4 -> [(B 3, 2.0 *. h c); (Cross [A 2; A 1], 2.0 *. h c)]
    | G2 -> [(A 1, 2.0 *. h c)]
    | AffA(p,q) -> [(A (p+q-1), (Float.of_int (p+q)) *. h c)]
                   @ List.init (p-1) ~f:(fun i -> (standardize(Cross [A(i); AffA(p-i-1,q)]), Float.of_int p))
                   @ List.init (q-1) ~f:(fun i -> (standardize(Cross [A(i); AffA(p,q-i-1)]), Float.of_int q))
    | AffD n ->
       (if (n+1) mod 2 = 0
        then
          (D n, 4.0 *. h c) :: List.map (List.range 0 ((n-3)/2)) ~f:(fun k -> (standardize(Cross [D (2+k); D (n-2-k)]), 2.0 *. h c))
        else
          (D n, 4.0 *. h c)
          :: (Cross [D(n/2); D(n/2)], h c)
          :: List.map (List.range 0 ((n-4)/2)) ~f:(fun k -> (standardize(Cross [D (2+k); D(n-2-k)]), 2.0 *. h c)))
       @ (AffA(2,n-2), 2.0*.2.0) :: List.map (List.range 0 (n-3)) ~f:(fun i -> (standardize(Cross [AffD(n-i-1);A(i)]), Float.of_int n -. 2.0))
    | AffE6 -> [(E6, 3.0 *. h c) ; (Cross [A 5; A 1], 3.0 *. h c) ; (Cross [A 2; A 2; A 2], h c)]
               @ [(AffA(3,3),2.0); (AffD(5),2.0*.3.0) ; (Cross [AffA(2,3); A 1], 2.0*.3.0)]
    | AffE7 -> [(E7, 2.0 *. h c) ; (Cross [D 6; A 1], 2.0 *. h c) ; (Cross [A 5; A 2], 2.0 *. h c) ; (Cross [A 3; A 3; A 1], h c) ; (A 7 , h c)]
               @ [(AffA(3,4),2.0); (AffD(6),3.0);(Cross [AffA(2,4);A 1],3.0); (AffE6,4.0);(Cross [AffD 5; A 1],4.0);(Cross[AffA(2,3);A 2],4.0)]
    | AffE8 -> List.map ~f:(fun subc -> (subc, h c))
                 [E8 ; Cross [E7; A 1] ; Cross [E6; A 2] ; Cross [D 5; A 3] ; Cross [A 4; A 4] ; Cross [A 5; A 2; A 1] ; Cross [A 7; A 1] ; D 8 ; A 8]
               @ [(AffA(3,5),2.0); (AffD(7),3.0);(Cross [AffA(2,5);A 1],3.0)]
               @ [(AffE7,5.0);(Cross [AffE6;A 1],5.0);(Cross [AffD 5;A 2],5.0);(Cross [AffA(2,3);A 3],5.0)]

    | AffB n -> (B n, 2.0 *. h c) :: (List.map (List.range 0 (n-1)) ~f:(fun i -> (standardize (Cross [D(2+i); B(n-i-2)]), h c)))
                      @ (AffA(1,n-1),  2.0) :: List.init (n-2) ~f:(fun i -> (standardize (Cross [AffB (n-i-1);A i]), Float.of_int(n-1)))
    | AffC n -> List.map (List.range ~stop:`inclusive 0 n) ~f:(fun i -> (standardize (Cross [C i; C (n-i)]), h c))
                      @ List.init (n-1) ~f:(fun i -> (standardize (Cross[AffC (n-i-1); A i]), Float.of_int n))
    | AffF4 -> [(F4, h c); (Cross [C 3; A 1], h c); (Cross[A 2; A 2], h c);(Cross[A 3; A 1], h c); (B 4, h c)]
               @ [(AffC 3, 2.0); (AffB 3, 3.0); (Cross[AffA(1,2); A 1], 3.0)]
    | AffG2 -> [(G2, h c); (Cross[A 1; A 1], h c); (A 2, h c)]
               @ [(AffA(1,1),2.0)]
    | DblA1 -> [(AffA(1,1),3.0)] |> List.map ~f:(fun (t,count) -> (t,count *. h c))
    | DblD4 -> [(AffD(4), 4.0*.(2.0/.2.0))] |> List.map ~f:(fun (t,count) -> (t,count *. h c))
    | DblE6 -> [(AffE6, 3.0*.(3.0/.3.0)); (Cross [AffA(3,3); A 1], 3.0*.(3.0/.3.0))] |> List.map ~f:(fun (t,count) -> (t,count *. h c))
    | DblE7 -> [(AffE7, 2.0*.(4.0/.4.0)); (Cross [AffD 6;A 1],2.0*.(4.0/.4.0)); (Cross [AffA(2,4);A 2], 2.0*.(4.0/.4.0)); (AffA(4,4), (2.0/.4.0))]
               |> List.map ~f:(fun (t,count) -> (t,count *. h c))
    | DblE8 -> [(AffE8,(6.0/.6.0)); (Cross[AffE7;A 1],(6.0/.6.0));(Cross[AffE6;A 2],(6.0/.6.0));(Cross [AffD 5; A 3], 6.0/.6.0);(Cross[AffA(2,3);A 4],6.0/.6.0)
                ; (AffD 8, (3.0/.6.0)) ; (Cross[AffA(2,6);A 1], (3.0/.6.0))
                ; (AffA(3,6), 2.0/.6.0)]
               |> List.map ~f:(fun (t,count) -> (t,count *. h(c)))
    | DblBC1_41 -> [(AffA(1,1),2.0)] |> List.map ~f:(fun (t,count) -> (t,count *. h c))
    | DblB2_21 -> [(AffB(2), 2.0*.2.0)] |> List.map ~f:(fun (t,count) -> (t,count *. h c)) |> List.map ~f:(fun (t,c) -> (t,c*.2.0)) (* Self Dual *)
    | DblBC2_42 -> [(AffC 2, 2.0*.2.0)] |> List.map ~f:(fun (t,count) -> (t,count *. h c)) |> List.map ~f:(fun (t,c) -> (t,c*.2.0)) (* Self Dual *)
    | DblG2 -> [(AffG2,2.0/.2.0); (AffA(1,2),2.0/.2.0)] |> List.map ~f:(fun (t,count) -> (t,count *. h c))
    | DblG2_31 -> [(AffG2,3.0); (Cross[AffA(1,1);A 1],3.0)]
                  |> List.map ~f:(fun (t,count) -> (t,count *. h c))
                  |> List.map ~f:(fun (t,c) -> (t,c*.2.0)) (* Self Dual *)
    | DblB3 -> [(AffB 3, 2.0*.(2.0/.2.0)); (AffA(2,2),2.0/.2.0)] |> List.map ~f:(fun (t,count) -> (t,count *. h c))
    | DblF4 -> [(AffF4,3.0/.3.0);(Cross[AffC 3;A 1],3.0/.3.0); (AffB 4, 3.0/.3.0);(Cross[AffA(1,3);A 1],3.0/.3.0)]
               |> List.map ~f:(fun (t,count) -> (t,count *. h c))
    | DblF4_21 -> [(AffC 4, 2.0); (AffF4, 4.0);(Cross[AffB 3;A 1],4.0);(Cross[AffA(1,2);A 2],4.0)]
                  |> List.map ~f:(fun (t,count) -> (t,count *. 2.0/.4.0)) (* count*w1/n1 *)
                  |> List.map ~f:(fun (t,count) -> (t,count *. h c))
                  |> List.map ~f:(fun (t,c) -> (t,c*.2.0)) (* Self Dual *)
    | Cross cs ->
       List.concat_mapi
         cs
         ~f:(fun i -> fun c -> let rest = List.append (List.take cs i) (List.drop cs (i+1)) in
                            List.map (codim1 c) ~f:(fun (subc, m) -> (standardize (Cross (subc :: rest)), m) ))
  in
  let combine l = Map.to_alist (CartanMap.of_alist_reduce l ~f:(+.)) in
  let c = standardize c in
  match codim with
  | 0 -> [(c, 1.0)]
  | 1 -> combine (codim1 c)
  | _ -> List.map
           (combine (List.concat_map (codim1 c) ~f:(fun (subc, m) -> List.map (count_subalg subc (codim -1))~f:(fun (subsubc, subm) -> (subsubc, m *. subm)))))
           ~f:(fun (subc, m) -> (subc, m /. (Float.of_int codim) ))

let rec cartan_matrix c ((i ,j  ) : int * int) :int =
  match c with
  | A (r) -> if Int.between i ~low:1 ~high:r && Int.between j ~low:1 ~high:r
             then
               if Int.equal i j
               then 2
               else
                 if Int.equal i  (j +1) || Int.equal i (j -1)
                 then -1
                 else 0
             else invalid_arg "Asked for matrix entries beyond dimension"
  | B(r) -> if not (Int.between i ~low:1 ~high:r && Int.between j ~low:1 ~high:r)
            then invalid_arg "Asked for matrix entries beyond dimension"
            else
              (match (Int.equal i j, Int.equal i 2 && Int.equal j 1, Int.equal i (j+1) || Int.equal i (j-1)) with
              | (true, false, false) -> 2
              | (false, true, true) -> -2
              | (false, false, true) -> -1
              | _ -> 0)
  | C(r) -> if not (Int.between i ~low:1 ~high:r && Int.between j ~low:1 ~high:r)
            then invalid_arg "Asked for matrix entries beyond dimension"
            else
              (match (Int.equal i j, Int.equal i 1 && Int.equal j (2), Int.equal i (j+1) || Int.equal i (j-1)) with
              | (true, false, false) -> 2
              | (false, true, true) -> -2
              | (false, false, true) -> -1
              | _ -> 0)
  | D(r) -> if not (Int.between i ~low:1 ~high:r && Int.between j ~low:1 ~high:r)
            then invalid_arg "Asked for matrix entries beyond dimension"
            else
              (match (Int.equal i j
                     , (Int.equal i 2 && Int.equal j 1) || (Int.equal i 1 && Int.equal j 2)
                     , (Int.equal i 3 && Int.equal j 1) || (Int.equal i 1 && Int.equal j 3)
                     , Int.equal i (j+1) || Int.equal i (j-1)) with
              | (true, false, false, false) -> 2
              | (false, true, false, true) -> 0
              | (false, false, true, false) -> -1
              | (false, false, false, true) -> -1
              | _ -> 0)
  | E6 -> List.nth_exn (List.nth_exn [[ 2;-1; 0; 0; 0; 0]
                                     ;[-1; 2;-1; 0; 0; 0]
                                     ;[ 0;-1; 2;-1; 0;-1]
                                     ;[ 0; 0;-1; 2;-1; 0]
                                     ;[ 0; 0; 0;-1; 2; 0]
                                     ;[ 0; 0;-1; 0; 0; 2]] (5-(i-1))) (5-(j-1))

  | E7 -> List.nth_exn (List.nth_exn [[ 2;-1; 0; 0; 0; 0; 0]
                                     ;[-1; 2;-1; 0; 0; 0; 0]
                                     ;[ 0;-1; 2;-1; 0; 0; 0]
                                     ;[ 0; 0;-1; 2;-1; 0;-1]
                                     ;[ 0; 0; 0;-1; 2;-1; 0]
                                     ;[ 0; 0; 0; 0;-1; 2; 0]
                                     ;[ 0; 0; 0;-1; 0; 0; 2]] (6-(i-1))) (6-(j-1))

  | E8 -> List.nth_exn (List.nth_exn [[ 2;-1; 0; 0; 0; 0; 0; 0]
                                      ;[-1; 2;-1; 0; 0; 0; 0; 0]
                                      ;[ 0;-1; 2;-1; 0; 0; 0; 0]
                                      ;[ 0; 0;-1; 2;-1; 0; 0; 0]
                                      ;[ 0; 0; 0;-1; 2;-1; 0;-1]
                                      ;[ 0; 0; 0; 0;-1; 2;-1; 0]
                                      ;[ 0; 0; 0; 0; 0;-1; 2; 0]
                                      ;[ 0; 0; 0; 0;-1; 0; 0; 2]] (7-(i-1))) (7-(j-1))

  | F4 -> List.nth_exn (List.nth_exn [[ 2;-1; 0; 0]
                                     ;[-1; 2;-2; 0]
                                     ;[ 0;-1; 2;-1]
                                     ;[ 0; 0;-1; 2]] (i-1)) (j-1)
  | G2 -> List.nth_exn (List.nth_exn [[ 2; -1]
                                     ;[-3; 2]] (i-1)) (j-1)
  | AffA _ -> invalid_arg "Affine A clusters don't have Cartan Matrices"
  | AffD _ -> invalid_arg "Affine D clusters don't have Cartan Matrices"
  | AffE6  -> invalid_arg "Affine E6 clusters don't have Cartan Matrices"
  | AffE7  -> invalid_arg "Affine E7 clusters don't have Cartan Matrices"
  | AffE8  -> invalid_arg "Affine E8 clusters don't have Cartan Matrices"
  | AffB _ -> invalid_arg "Affine B clusters don't have Cartan Matrices"
  | AffC _ -> invalid_arg "Affine C clusters don't have Cartan Matrices"
  | AffF4 -> invalid_arg "Affine F4 clusters don't have Cartan Matrices"
  | AffG2 -> invalid_arg "Affine G2 clusters don't have Cartan Matrices"
  | DblA1  -> invalid_arg "Doubly extended A1 clusters don't have Cartan Matrices"
  | DblD4  -> invalid_arg "Doubly extended D4 clusters don't have Cartan Matrices"
  | DblE6  -> invalid_arg "Doubly extended E6 clusters don't have Cartan Matrices"
  | DblE7  -> invalid_arg "Doubly extended E7 clusters don't have Cartan Matrices"
  | DblE8  -> invalid_arg "Doubly extended E8 clusters don't have Cartan Matrices"
  | DblBC1_41 -> invalid_arg "Doubly extended BC1_41 clusters don't have Cartan Matrices"
  | DblB2_21 -> invalid_arg "Doubly extended B2_21 clusters don't have Cartan Matrices"
  | DblBC2_42 -> invalid_arg "Doubly extended BC2_42 clusters don't have Cartan Matrices"
  | DblG2 -> invalid_arg "Doubly extended G2 clusters don't have Cartan Matrices"
  | DblG2_31 -> invalid_arg "Doubly extended G2_31 clusters don't have Cartan Matrices"
  | DblB3 -> invalid_arg "Doubly extended B3 clusters don't have Cartan Matrices"
  | DblF4 -> invalid_arg "Doubly extended F4 clusters don't have Cartan Matrices"
  | DblF4_21 -> invalid_arg "Doubly extended F4_21 clusters don't have Cartan Matrices"
  | Cross [] -> 0
  | Cross (c :: cs) ->
     (match (i <= rank c, j <= rank c) with
      | (true, true) -> cartan_matrix c (i, j)
      | (false, false) -> cartan_matrix (Cross cs) (i - rank c, j - rank c)
      | (false, true) -> 0
      | (true, false) -> 0)


let ranks c =
  let codimi = List.map (List.range ~stop:`inclusive 1 (rank c)) ~f:(count_subalg c) in
  List.map codimi ~f:(fun l -> List.fold ~init:0.0 ~f:(fun acc -> fun (_, m) -> acc +. m) l)

let euler c = List.foldi ~init:0.0 ~f:(fun i acc rank -> if Int.(i mod 2 = 0) then acc +. rank else acc -. rank) (ranks c)

let latex_table c =
  let gcd i j =
    let rec loop i j =
      match (i,j) with
      | (0,_) -> j
      | (_,0) -> i
      | _ -> if i > j then loop (i mod j) j else loop i (j mod i)
    in loop (Int.abs i) (Int.abs j) in
  let lcm i j = i * j / (gcd i j) in
  let counts = List.map (List.range 1 (rank c + 1)) ~f:(count_subalg c) in
  let numcols = List.fold ~init:1 ~f:lcm (List.map ~f:List.length counts) in
  let count_to_tex codim l =
    let (algs, sizes) = List.unzip l in
    let colsize = numcols / (List.length l) in
    let total = Float.to_int(List.fold ~init:0.0 ~f:(+.) sizes) in
    let wrap_multicolumn i s = String.concat ["\\multicolumn{" ; Int.to_string i ; "}{|c|}{" ; s ; "}"] in
    String.concat
      [ "\\multirow{2}{*}{" ^ Int.to_string (codim +1) ^ "}"
      ; "& "
      ; (String.concat ~sep:" & " (List.map algs ~f:((wrap_multicolumn colsize) |. cartan_to_texstring)))
      ; " & "
      ; "\\multirow{2}{*}{" ^ Int.to_string total ^ "} \\\\ \n \\cline{2-" ^ Int.to_string (1 + numcols) ^ "}\n"
      ;" & "
      ; (String.concat ~sep:" & " (List.map sizes ~f:((wrap_multicolumn colsize) |. Int.to_string |. Float.to_int)))
      ; " & "]
  in
  String.concat
  [ "\\begin{table}[p]\n"
  ; "\\centering"
  ; "\\caption{Subalgebras of " ^ cartan_to_texstring c ^ "}\n"
  ; "\\label{tab:Subalg" ^ cartan_to_string c ^ "}\n"
  ; "\\begin{tabular}{|c *{" ^ Int.to_string numcols ^ "}{|c|} c|}\n"
  ; "\\hline \n Codimension & \\multicolumn{"^Int.to_string numcols ^"}{|c|}{Subalgebras} & Total \\\\ \n \\hline \\hline \n"
  ; String.concat ~sep:"\\\\ \\hline\n" (List.mapi counts ~f:count_to_tex)
  ; "\\\\ \\hline\n \\end{tabular}\n"
  ; "\\end{table}\n"]

(* module R = Flint.BigIntMRats.Make(struct let vars = List.init 64 ~f:(fun i -> "M"^(Int.to_string(i+1))) let ord = Flint.BigIntMPoly.LEX end)
module Vert = RingVertex.Make(R)
include IMapCluster.Make(Vert)
 *)
include MapCluster.Make(BruhatVertex)
let of_word (c : cartan) (i : int list) =
  let open Int in
  let r = rank c in
  let epsilon k = if Int.is_positive k then 1 else -1 in
  let w = List.append (List.range ~stop:`inclusive (-r) (-1)) i in
  let i_sub k = List.nth_exn w (k-1) in
  let plus k =
    let i_k = i_sub k in
    match List.findi (List.drop w k) ~f:(fun _(*i*) l -> Int.equal (Int.abs l) (Int.abs i_k)) with
    | Some (kplus,_) -> kplus+ k + 1
    | None -> List.length w + 1 in
  let i_exchangeable (k : int) =  Int.between (k - r) ~low:1 ~high:(List.length i) && Int.between (plus k - r) ~low:1 ~high:(List.length i) in
  let edge_dir ink inl =
    let sign = if ink < inl  then 1 else -1 in
    let k = Int.min ink inl in
    let l = Int.max ink inl in
    let kplus = plus k in
    let lplus = plus l in
    if not (i_exchangeable k || i_exchangeable l)
    then 0
    else
      if Int.equal l kplus (* horizontal edge *)
      then if Int.equal (epsilon (i_sub l))  1 then sign else -1 * sign
      else
        if List.exists ~f:(fun b -> b)
                       [ l < kplus
                         && kplus < lplus
                         && Int.equal (epsilon (i_sub l)) (epsilon (i_sub kplus))
                         && (cartan_matrix c) (Int.abs (i_sub k), Int.abs (i_sub l)) < 0
                       ; l < lplus
                         && lplus < kplus
                         && Int.equal (epsilon (i_sub l)) (-(epsilon (i_sub lplus)))
                         && (cartan_matrix c) (Int.abs (i_sub k), Int.abs (i_sub l)) < 0
                       ]
            then if (epsilon (i_sub l) = -1) then sign else -1 * sign
        else 0
  in let make_edges (k : int) =
       let nbrs = List.fold (List.range 1 (List.length w + 1))
                            ~init: BruhatVertex.Map.empty
                            ~f:(fun nbr -> fun l -> match edge_dir k l with
                                                    | 0 -> nbr
                                                    | d -> BruhatVertex.Map.add_exn nbr ~key:l ~data:d)
       in (k, nbrs)
     in
     let verts = List.range ~stop:`inclusive 1 (List.length w) in
     of_list ~graph:(List.map verts ~f:make_edges)
             ~frozen:(BruhatVertex.Set.of_list (List.filter verts ~f:(not |. i_exchangeable)))

let of_word_z (c : cartan) (i : int list) =
  let open Int in
  let r = rank c in
  let epsilon k = if Int.is_positive k then 1 else -1 in
  let w = i in
  let i_sub k = if k>=1 then List.nth_exn w (k-1) else 1 (*Treat 0 index as normal root *) in
  let minus k =
    let w_k = i_sub k in
    match List.findi (List.rev (List.take w (k-1))) ~f:(fun _(*i*) wl -> Int.equal (Int.abs wl) (Int.abs w_k)) with
    | Some (kminus,_) -> k-(kminus+1)
    | None -> 0 in
  let edge (k,l) =
    let lminus = minus l in
    let kminus = minus k in
    if k=lminus then if epsilon (i_sub k) = 1 then Some(k,l) else Some(l,k)
    else if (kminus <= lminus && lminus < k && epsilon (i_sub lminus) = epsilon (i_sub k) && dynkinNbr c (i_sub k) (i_sub l))
           || (lminus <= kminus && kminus < k && epsilon (i_sub kminus) = -1* epsilon (i_sub k) && dynkinNbr c (i_sub k) (i_sub l)) then
      if epsilon(i_sub k) = -1 then Some(k,l) else Some(l,k)
    else None  in
  let edges = List.filter_map (List.concat_mapi w ~f:(fun k _ -> List.init (List.length w - k-1) ~f:(fun ldiff -> (k+1,k+1+ldiff+1)))) ~f:edge in
  let frozen = List.range ~stop:`inclusive 1 r |>  List.map ~f:(fun wi -> List.findi w ~f:(fun _ wk -> Int.abs wk = wi))
               |> List.filter_map ~f:(function Some (i,_) -> Some(i+1) | None -> None) in
  of_edgelist ~graph:edges ~frozen:(BruhatVertex.Set.of_list frozen)

let m2tgraph_of_word (c : cartan) (i : int list) =
  let open Int in
  let r = rank c in
  let epsilon k = if Int.is_positive k then 1 else -1 in
  let w = i in
  let i_sub k = if k>=1 then List.nth_exn w (k-1) else 1 (*Treat 0 index as normal root *) in
  let minus k =
    let w_k = i_sub k in
    match List.findi (List.rev (List.take w (k-1))) ~f:(fun _(*i*) wl -> Int.equal (Int.abs wl) (Int.abs w_k)) with
    | Some (kminus,_) -> k-(kminus+1)
    | None -> 0 in
  let edge (k,l) =
    let lminus = minus l in
    let kminus = minus k in
    if k=lminus then if epsilon (i_sub k) = 1 then Some(k,l) else Some(l,k)
    else if (lminus < k && epsilon (i_sub lminus) = epsilon (i_sub k) && dynkinNbr c (i_sub k) (i_sub l))
           || (kminus < k && epsilon (i_sub kminus) = -1* epsilon (i_sub k) && dynkinNbr c (i_sub k) (i_sub l)) then
      if epsilon(i_sub k) = -1 then Some(k,l) else Some(l,k)
    else None  in
  let edges = List.filter_map (List.concat_mapi w ~f:(fun k _ -> List.init (List.length w - k-1) ~f:(fun ldiff -> (k+1,k+1+ldiff+1)))) ~f:edge in
  let frozen = List.range ~stop:`inclusive 1 r |>  List.map ~f:(fun wi -> List.findi w ~f:(fun _ wk -> Int.abs wk = wi))
               |> List.filter_map ~f:(function Some (i,_) -> Some(i+1) | None -> None) in
  of_edgelist ~graph:edges ~frozen:(BruhatVertex.Set.of_list frozen)

let writeAnosovMats folder c = 
  let mat = of_word_z c (longest_word_inductive c) in 
  let m2t = m2tgraph_of_word c (longest_word_inductive c) in 
  ["{"
  ; (mat |> matrix |> List.map ~f:(List.map ~f:Int.to_string) |> List.map ~f:(String.concat ~sep:",")
    |> List.map ~f:(fun s -> "{"^s^"}") |> String.concat ~sep:"," |> (fun s-> "{"^s^"}"))
  ; "," 
  ;(m2t |> matrix |> List.map ~f:(List.map ~f:Int.to_string) |> List.map ~f:(String.concat ~sep:",")
    |> List.map ~f:(fun s -> "{"^s^"}") |> String.concat ~sep:"," |> (fun s-> "{"^s^"}"))
  ; "}"] |> Out_channel.write_lines (folder ^ "/inductiveMats" ^ (cartan_to_string c) ^ ".txt")

let writeDnPath folder rank = 
  [longest_word_inductive (D rank) |> List.map ~f:Int.to_string |> String.concat ~sep:" "
  ;reverseDn rank |> List.map ~f:wordmove_to_string |> String.concat ~sep:","] 
  |> Out_channel.write_lines (folder ^ "/inductivePathD"^(Int.to_string rank) ^ ".txt")


let of_longest c = of_word_z c (longest_word c)
