open Core
module Heap = Pairing_heap

module type S =
  sig
    module Cluster : Cluster.S
    include Search.S with module Node = Cluster
    module XVert : XCoord.S with module ACoord = Cluster.Vertex
    val stoch_find : int -> Cluster.Vertex.Set.t -> Cluster.t -> Cluster.t
    val stoch_find_cond : int -> cond:(Cluster.Vertex.t -> bool) -> Cluster.Vertex.Set.t -> Cluster.t -> Cluster.t
    val all_shortest : Node.t -> Node.t -> Cluster.Vertex.t list list
    val all_coords : 'a Node.Map.t -> Cluster.Vertex.Set.t
    (* Returns a map from Vertices to the set of vertices which never appear together in a cluster *)
    val incompatMap : 'a Node.Map.t -> Cluster.Vertex.Set.t Cluster.Vertex.Map.t
    val xcoord : Cluster.t -> Cluster.Vertex.t -> XVert.t
    val xcoords: Cluster.t -> XVert.Set.t
    val all_xcoords : 'a Cluster.Map.t -> XVert.Set.t
    val all_xcoords_induced : 'a Cluster.Map.t  -> XVert.Set.t

    val xpair : Cluster.t -> Cluster.Vertex.t -> XVert.t * XVert.t
    val xpairs: Cluster.t -> XVert.t XVert.Map.t
    val all_xpairs : 'a Cluster.Map.t -> XVert.t XVert.Map.t
    val all_xpairs_induced : 'a Cluster.Map.t  -> XVert.t XVert.Map.t
    val casimir_factors : Cluster.t -> (int*XVert.t) list list

    val max_finite_size : int -> int
    val algebra_name : graph -> string
    val subalgebras : int -> graph -> (Cluster.Vertex.Set.t * graph) list
    val subalgebras_named : int -> graph -> (string * Cluster.Vertex.Set.t * graph) list

    val write_subalgebras : int -> string -> graph -> unit
    val write_path : ?fwriteFrozen:bool -> string -> Node.t -> Cluster.edge list -> unit
  end

module type ClusterNodeS =
  sig
    module Cluster : Cluster.S
    include Search.NodeS
    val mutate : Cluster.t -> Cluster.Vertex.t -> Cluster.t
  end

module Make (Clust : Cluster.S) : (S with module Cluster = Clust) =
  struct
    module Cluster = Clust
    include SearchBase.Make(Cluster)
    module Node = Cluster
    module Vertex = Cluster.Vertex
    module XVert = XCoord.Make(Vertex)

    let comparedist (i,_ ) (j,_ ) = Int.compare i j

(*
    let shortest_paths_dist (start: Cluster.t) (max_dist : int)  =
      let rec loop (d: Vertex.t list Cluster.Map.t) (q : (int * (Cluster.t * Vertex.t list)) Heap.t) =
        match Heap.pop q with
        | None -> d
        | Some ((dist : int), (v,path)) ->
           (match Map.find d v with
            | Some _ -> loop d q
            | None ->
               let dnew = Map.add_exn d ~key:v ~data:path in
               let relax (nbr, (mu,_))  =  (1+dist, (nbr, mu :: path)) in
               let cadd q (dist, path) = if dist > max_dist then () else Heap.add q (dist, path) in
               let () = List.iter (List.map (Cluster.n v) ~f:relax) ~f: (cadd q) in
               loop dnew q
           )
      in loop Cluster.Map.empty  (Heap.of_list [(0,(start,[]) )] ~cmp:comparedist)
 *)

    let all_shortest (start : Cluster.t) (target : Cluster.t) : Vertex.t list list =
      let rec loop
                (d: (int * (Cluster.t * Cluster.Vertex.t) list) Cluster.Map.t)
                (q : (int * (Cluster.t * (Cluster.t * Cluster.Vertex.t) option)) Heap.t) =
        match Heap.pop q with
        | None -> d
        | Some ((dist, (v , None))) ->
           (match Map.find d v with
              Some _ -> invalid_arg "Only Starting vertex should be added to queue with None"
            | None ->
               let dnew = Map.add_exn d ~key:v ~data:(dist,[]) in
               let relax (nbr, (mu))  =  (1+dist, (nbr, Some (v,mu))) in
               let () = List.iter (List.map (Cluster.n v) ~f:relax) ~f: (Heap.add q) in
               loop dnew q)
        | Some ((dist : int), (v, Some e)) ->
           (match Map.find d v with
            | Some (vdist, es) -> if vdist = dist then loop (Map.add_exn d ~key:v ~data:(dist, e::es)) q else loop d q
            | None ->
               let dnew = Map.add_exn d ~key:v ~data:(dist,[e]) in
               let relax (nbr, (mu))  =  (1+dist, (nbr, Some (v,mu))) in
               let () = List.iter (List.map (Cluster.n v) ~f:relax) ~f: (Heap.add q) in
               loop dnew q
           ) in
      let rec build_rev_paths (v: Cluster.t) (d : (int * (Cluster.t * Vertex.t) list) Cluster.Map.t) : Vertex.t list list =
        match Map.find d v with
        | None -> []
        | Some (_, []) -> [[]]
        | Some (_, bes) ->
           List.concat_map ~f:(fun (bv, be) -> List.map ~f:(fun path -> be:: path) (build_rev_paths bv d)) bes in
      let path_dict = loop Cluster.Map.empty  (Heap.of_list [(0,(start,None))] ~cmp:comparedist)
      in List.map ~f:List.rev (build_rev_paths target path_dict)


    let write_path ?fwriteFrozen:(fwriteFrozen = true) dirname start path =
      let (cend, vend) = List.foldi
                   ~init:(start,None)
                   ~f:(fun i -> fun (c, vprev) -> fun (v) ->
                                         let (cnew,vnew) = Cluster.mutate_name c v in
                                         let () = Cluster.write_cluster
                                                   (String.concat [dirname ; Int.to_string i; "_" ; Cluster.Vertex.to_string v ])
                                                   (if fwriteFrozen then c else Cluster.removeFrozen c)
                                                   ~node_color:(fun w -> if Cluster.Vertex.equal v w
                                                             then 3
                                                             else match vprev with
                                                                  | None -> 0
                                                                  | Some vprev -> if Cluster.Vertex.equal vprev w then 2 else 0)
                                         in (cnew, Some vnew))
                   path
      in Cluster.write_cluster (String.concat [dirname; Int.to_string (List.length path); "_End"])
           (if fwriteFrozen then cend else Cluster.removeFrozen cend)
           ~node_color:(fun w -> match vend with None -> 0 | Some vend -> if Cluster.Vertex.equal vend w then 2 else 0)

    let xcoord cluster v  = XVert.of_nbr (Cluster.nbr cluster v)

    let xcoords cluster = XVert.Set.map (Cluster.unfrozen cluster) ~f:(xcoord cluster)

    let all_coords (graph) =
      Map.fold graph ~init:Vertex.Set.empty ~f:(fun ~key ~data:_ acc -> Set.union (Cluster.unfrozen key) acc)

    let incompatMap graph =
      let coords = all_coords graph in
      coords
      |> Set.to_map ~f:(fun v -> Set.filter coords ~f:(fun w ->
                                     Map.existsi graph ~f:(fun ~key:c ~data:_ -> Cluster.mem c v && Cluster.mem c w))
                                 |> Set.diff coords)
    
    let all_xcoords (graph : 'a Cluster.Map.t) =
      Map.fold graph ~init:XVert.Set.empty ~f:(fun ~key ~data:_ acc -> Set.union (xcoords key) acc)

    let all_xcoords_induced (graph) =
      let xcoords_induced cluster =
        XVert.Set.filter_map (Cluster.unfrozen cluster) ~f:(fun v -> if Map.mem graph (Cluster.mutate cluster v) then Some (xcoord cluster v) else None)
      in
      Map.fold graph ~init:XVert.Set.empty ~f:(fun ~key ~data:_ acc -> Set.union (xcoords_induced key) acc)

    let xpair cluster v =
      let x = XVert.of_nbr (Cluster.nbr cluster v) in
      (x
      , XVert.of_list ~top:[v; Vertex.mutate v ~inp:(Cluster.iNbr cluster v) ~out:(Cluster.oNbr cluster v)]
          ~bot:(XVert.bot_list x))

    let xpairs cluster =
      XVert.Map.of_alist_exn (List.map (Set.to_list (Cluster.unfrozen cluster)) ~f:(xpair cluster))

    let all_xpairs (graph) =
      Map.fold graph ~init:XVert.Map.empty ~f:(fun ~key ~data:_ acc -> Map.merge ~f:(fun ~key:_ -> function `Left a -> Some a | `Right b -> Some b | `Both(a,_) -> Some a) (xpairs key) acc)

    let all_xpairs_induced (graph) =
      let xpairs_induced cluster =
        XVert.Map.of_alist_exn (List.filter_map (Set.to_list (Cluster.unfrozen cluster))
                                  ~f:(fun v -> if Map.mem graph (Cluster.mutate cluster v)
                                            then Some (xpair cluster v)
                                            else None))
      in
      Map.fold graph ~init:XVert.Map.empty
        ~f:(fun ~key ~data:_ acc -> Map.merge ~f:(fun ~key:_ -> function `Left a -> Some a
                                                                       | `Right b -> Some b
                                                                       | `Both(a,_) -> Some a)
                                      (xpairs_induced key) acc)

    let casimir_factors q =
      let vorder = Set.to_list (Cluster.unfrozen q) in
      let xs = List.map ~f:(xcoord q) vorder in
      q |> Cluster.casimir_vectors ~vorder:(Some vorder)
      |> List.map ~f:(fun cs -> List.zip_exn cs xs |> List.filter ~f:(function (0,_) -> false | _ -> true))


    let stoch_find max_count goal start =
      let next_cluster save c =
        let possible = Set.filter (Set.diff (Cluster.unfrozen c) save)
                                  ~f:(fun v -> Vertex.canExchange v ~inp:(Cluster.iNbr c v) ~out:(Cluster.oNbr c v))
        in  if Set.is_empty possible
            then c
            else Cluster.mutate c (List.nth_exn (Set.to_list possible) (Random.int (Set.length possible))) in
      let rec loop count c =
        if Set.is_subset goal ~of_:(Set.union (Cluster.unfrozen c) (Cluster.frozen c)) || count > max_count
        then let () = print_endline (Int.to_string count) in  c
        else loop (count + 1) (next_cluster goal c)
      in loop 0 start;;

    let stoch_find_cond max_count ~cond goal start =
      let next_cluster save c =
        let possible = Set.filter (Set.diff (Cluster.unfrozen c) save)
                                  ~f:(fun v -> Vertex.canExchange v ~inp:(Cluster.iNbr c v) ~out:(Cluster.oNbr c v))
        in  if Set.is_empty possible
            then c
            else let (cnew, vnew) = Cluster.mutate_name c (List.nth_exn (Set.to_list possible) (Random.int (Set.length possible))) in
                 if cond vnew then cnew else c in
      let rec loop count c =
        if Set.is_subset goal ~of_:(Set.union (Cluster.unfrozen c) (Cluster.frozen c)) || count > max_count
        then let () = print_endline (Int.to_string count) in  c
        else loop (count + 1) (next_cluster goal c)
      in loop 0 start;;


(*
    let algebra_name graph =
      let dim = Set.length (Cluster.unfrozen ((fun (x,_) -> x) (Map.min_elt_exn graph)))
      in
      Int.to_string dim ^ "_" ^ (
        match Map.length graph with
        (* 0 *)
        | 1 -> "A0"
        (* 1 *)
        | 2 -> "A1"
        (* 2 *)
        | 5 -> "A2"
        | 4 -> "A1xA1"
        (* 3 *)
        | 14 -> "A3"
        | 10 -> "A2xA1"
        | 8  -> "A1xA1xA1"
        (* 4 *)
        | 42 -> "A4"
        | 28 -> "A3xA1"
        | 25 -> "A2xA2"
        | 20 -> "A2xA1xA1"
        | 16 -> "A1xA1xA1xA1"
        | 50 -> (match dim with 4 -> "D4" | 5 ->  "A2xA2xA1" | _ -> "Unknown50")
        (* 5 *)
        | 132 -> "A5"
        | 84 -> "A4xA1"
        | 70 -> "A3xA2"
        | 56 -> "A3xA1xA1"
        | 40 -> "A2xA1xA1xA1"
        | 32 -> "A1xA1xA1xA1xA1"
        | 182 -> "D5"
        | 100 -> (match dim with 5 -> "D4xA1" | 6 -> "A2xA2xA1xA1" | _ -> "Unknown100")
        (* 6 *)
        | 429 -> "A6"
        | 264 -> "A5xA1"
        | 210 -> "A4xA2"
        | 168 -> "A4xA1xA1"
        | 196 -> "A3xA3"
        | 140 -> "A3xA2xA1"
        | 112 -> "A3xA1xA1xA1"
        | 80 -> "A2xA1xA1xA1xA1"
        | 64 -> "A1xA1xA1xA1xA1xA1"
        | 672 -> "D6"
        | 364 -> "D5xA1"
        | 250 -> (match dim with 6 -> "D4xA2" | 7 -> "A2xA2xA2xA1" | _ -> "Unknown250")
        | 200 -> (match dim with 6 -> "D4xA1xA1" | 7 -> "A2xA2xA1xA1xA1" | _ -> "Unknown200")
        | 833 -> "E6"
        (* 7 *)
        | 1430 -> "A7"
        | 858 -> "A6xA1"
        | 660 -> "A5xA2"
        | 588 -> "A4xA3"
        | 528 -> "A5xA1xA1"
        | 420 -> "A4xA2xA1"
        | 392 -> "A3xA3xA1"
        | 350 -> "A3xA2xA2"
        | 336 -> "A4xA1xA1xA1"
        | 280 -> "A3xA2xA1xA1"
        | 224 -> "A3xA1xA1xA1xA1"
        | 160 -> "A2xA1xA1xA1xA1xA1"
        | 128 -> "A1xA1xA1xA1xA1xA1xA1"
        | 2508 -> "D7"
        | 1344 -> "D6xA1"
        | 910 -> "D5xA2"
        | 700 -> "D4xA3"
        | 728 -> "D5xA1xA1"
        | 4160 -> "E7"
        | 1666 -> "E6xA1"
        (* 8 *)
        | 25080 -> "E8"
        | n -> "Unknown"^ (Int.to_string n)
      )
 *)
    let max_finite_size dim =
      match dim with
      | 0 -> 1
      | 1 -> 2
      | 2 -> 5
      | 3 -> 14
      | 4 -> 50
      | 5 -> 182
      | 6 -> 833
      | 7 -> 4160
      | _ -> invalid_arg ("Largest finite size not recorded for dimension " ^ (Int.to_string dim))


    let subalgebras codim graph =
      let rec choose k l =
        match (k,l) with
        | (0,_) -> [[]]
        | (_,[]) -> invalid_arg ("l doesn't have " ^ Int.to_string k ^ " elements")
        | (_,x::xs) -> if k = List.length l
                       then [l]
                       else List.map ~f:(fun g -> x::g) (choose (k-1) xs) @ (choose k xs) in 
      let groups = graph |> Map.keys |> List.concat_map ~f:(fun c -> c |> Clust.unfrozen |> Set.to_list |> choose codim)
                   |> List.dedup_and_sort ~compare:(List.compare Cluster.Vertex.compare) in
      let subalg (vs : Cluster.Vertex.t list) =
        let contains_v =
          Cluster.Set.of_list (List.filter_map (Map.keys graph) ~f:(fun c ->
                                                 if List.for_all vs ~f:(Set.mem (Cluster.unfrozen c))
                                                 then Some(Cluster.freeze c vs)
                                                 else None)) in
        if Set.length(contains_v) > 0
        then  Some (Cluster.Vertex.Set.of_list vs, induced_subgraph (Set.to_map contains_v ~f:Cluster.n) contains_v)
        else None
      in
      List.filter_map groups ~f:subalg

    let algebra_name graph =
      match List.find (Map.keys graph) ~f:(fun c -> Cluster.fAffineDynkin c || Cluster.is_Tpqr c) with
      | Some c -> Cluster.dynkin_type c
      | None -> "Unknown_"^(Int.to_string(Cluster.dim (List.hd_exn (Map.keys graph))))

    let subalgebras_named codim graph =
      List.map (subalgebras codim graph) ~f:(fun (v,subalg) -> (algebra_name subalg, v, subalg))

    let  write_subalgebras maxcodim dir_init graph_init =
      let rec loop codim dir string_frozen graph =
        if codim >= maxcodim then ()
        else
          let codim1 : (Cluster.Vertex.t * graph) list = subalgebras 1 graph |> List.map ~f:(fun (vs,graph) -> (Set.choose_exn vs, graph)) in
          let write (v,subalg) =
            let dirname = dir ^ "/" ^ (algebra_name subalg) ^ "_" ^ (Vertex.to_string v) in
            (* let frozen = Cluster.frozen ((fun (x,_) -> x) (Map.min_elt_exn subalg)) in *)
            let string_frozen_new = string_frozen ^ "_" ^ Vertex.to_string v in
            let () = match Caml.Sys.file_exists dirname && Caml.Sys.is_directory dirname with true -> () | false -> Caml.Sys.mkdir dirname 764 in
            (* let () = write_graph (dirname ^ "/graph") subalg in *)
            (* let () = write_graph_color_all (dir ^ "/graph_" ^ (algebra_name subalg)  ^ string_frozen_new)
                             (fun c -> if Map.mem subalg (Cluster.of_quiver ~quiver:(Cluster.quiver c) ~frozen:frozen)
                                       then 2
                                       else 0)
                             (fun (c1, c2, v) ->
                                       if Map.mem subalg (Cluster.of_quiver ~quiver:(Cluster.quiver c1) ~frozen:frozen)
                                             && Map.mem subalg (Cluster.of_quiver ~quiver:(Cluster.quiver c2) ~frozen:frozen)
                                       then 2
                                       else 0)
                              graph_init in
           *)
          let () = Out_channel.write_all (dirname ^ "/saved") ~data:(Sexp.to_string (sexp_of_graph subalg)) in
          let () = Out_channel.write_lines (dirname ^ "/acoord") (List.map (Set.to_list (all_coords subalg)) ~f:Vertex.to_string) in
          let () = Out_channel.write_lines (dirname ^ "/xcoord") (List.map (Set.to_list (all_xcoords subalg)) ~f:XVert.to_string) in
          (* let () = Out_channel.write_lines (dirname ^ "/clusters") (List.map (Map.keys subalg) ~f:Cluster.name) in *)
          (* let () = List.iter (Map.keys subalg) (fun c -> Cluster.write_cluster (dirname ^ "/" ^ Cluster.name c) c) in *)
          let () =  (fun c -> Cluster.write_cluster (dirname ^ "/" ^ Cluster.name c) c) (List.hd_exn (Map.keys subalg)) in
          loop (codim + 1) dirname string_frozen_new subalg
        in
        if List.length codim1 = 2
        then ()
        else List.iter codim1 ~f:write
      in
      loop 1 dir_init "" graph_init


  end
