open Core
include Vertex.S with type t = Int.Set.t
val hash : t -> int

val of_list : int list -> t
val rotate : int -> t -> t
val a : i:int -> t -> t
val apply : Algebra.Rings.IntPoly.t list list -> t -> Algebra.Rings.IntPoly.t
val eval_mat : t -> Algebra.Rings.LacamlMat.t -> float
