open Core
let cvec_compare = List.compare Int.compare


module FrameWithFrozen (Cluster : Cluster.S) :
sig
  include Search.S with type Node.t = Cluster.t * int list list and type Node.edge = Cluster.Vertex.t
end =
  struct
    module Node =
      struct
        module T =
          struct
            type t = Cluster.t * int list list [@@deriving sexp]
            let compare (_,f1) (_,f2) = List.compare cvec_compare f1 f2
            let label (_,f) = String.concat ~sep:"\n" (List.map ~f:(String.concat ~sep:" ") (List.map ~f:(List.map ~f:Int.to_string) f))
            let id (_,f) = String.concat ~sep:"" (List.map ~f:(String.concat ~sep:"") (List.map ~f:(List.map ~f:Int.to_string) f))
          end
        include T
        include Comparable.Make(T)
        type edge = Cluster.Vertex.t [@@deriving sexp,equal]
        let follow (c,_) es = let (cnew,backPath) = Cluster.follow c es in ((cnew, Cluster.cVectors cnew),backPath)
        let n (c,_) = List.map (Cluster.n c) ~f:(fun (c, v) -> ((c, Cluster.cVectors c), v))
      end
    include SearchBase.Make(Node)
  end

module MakeNormalized (Arg:sig val normalize : int list  -> int list end) =
  struct
    module Node =
      struct
        module T =
          struct
            type t = FixedCluster.t * int list list [@@deriving sexp]
            let compare (_,f1) (_,f2) = List.compare cvec_compare f1 f2
            let label (_,f) = String.concat ~sep:"\n" (List.map ~f:(String.concat ~sep:" ") (List.map ~f:(List.map ~f:Int.to_string) f))
            let id (_,f) = String.concat ~sep:"" (List.map ~f:(String.concat ~sep:"") (List.map ~f:(List.map ~f:Int.to_string) f))
          end
        include T
        include Comparable.Make(T)
        type edge = FixedCluster.Vertex.t  [@@deriving sexp,equal]
        let follow (c,_) es = let (cnew,backPath) = FixedCluster.follow c es in
                          ((cnew,List.sort ~compare:cvec_compare (List.map ~f:Arg.normalize (FixedCluster.cVectors c))), backPath)
        let n (c,_) =
          List.map (FixedCluster.n c) ~f:(fun (c, e) -> ((c, List.sort ~compare:cvec_compare (List.map ~f:Arg.normalize (FixedCluster.cVectors c))), e))
      end
    include SearchBase.Make(Node)

    let normalize = Arg.normalize
end

module Make (Cluster : Cluster.S) =
  struct
    module FramedVertex =
      struct
        module T =
          struct
            type t = Frame of int | Original of Cluster.Vertex.t [@@deriving sexp, compare]
          end
        include T
        include Comparable.Make(T)
        let weight (v) =
          match v with
          | Frame _ -> 1
          | Original v -> Cluster.Vertex.weight v

        let filterNbr nbr  =
          Cluster.Vertex.Map.of_alist_exn
            (List.filter_map (Map.to_alist nbr) ~f:(fun (key,data) -> match key with Frame _-> None | Original v ->  Some(v,data)))
(*         let fFramed v nbr =
           not(Map.is_empty (Map.filter_keys nbr ~f:(function Frame _ -> true | Original _ -> false)))
*)
        let canExchange v ~inp ~out =
          match v with
          | Frame _ -> false
          | Original v ->  Cluster.Vertex.canExchange v ~inp:(filterNbr inp) ~out:(filterNbr out)
        let mutate v ~inp ~out =
          match v with
          | Frame _ -> v
          | Original v -> Original(Cluster.Vertex.mutate v ~inp:(filterNbr inp)  ~out:(filterNbr out))
        let id v =
          match v with
          | Frame i -> "F"^Int.to_string i
          | Original v -> "O"^Cluster.Vertex.id v
        let to_string v =
          match v with
          | Frame i -> "F"^Int.to_string i
          | Original v -> Cluster.Vertex.to_string v

        let pp ppf v = Format.fprintf ppf "%s" (to_string v)

        let of_string s=
          match String.prefix s 1 with
          | "F" -> Frame(Int.of_string (String.suffix s 1))
          | "O" -> Original(Cluster.Vertex.of_string (String.suffix s 1))
          | _ -> invalid_arg ("Framed vertices should be F<i> or O<v> not " ^ s)
      end

    module FramedCluster = MapCluster.Make(FramedVertex)

    let frameVectors c =
      FramedCluster.cVectors ~frozenlist:(Some (Set.to_list (Set.filter (FramedCluster.frozen c)
                                                            ~f:(function FramedVertex.Frame _ -> true| FramedVertex.Original _ -> false)))) c
    module Node =
      struct
        module T =
          struct
            type t = FramedCluster.t * int list list [@@deriving sexp]
            let compare (_,f1) (_,f2) = List.compare cvec_compare f1 f2
            let label (_,f) = String.concat ~sep:"\n" (List.map ~f:(String.concat ~sep:" ") (List.map ~f:(List.map ~f:Int.to_string) f))
            let id (_,f) = String.concat ~sep:"" (List.map ~f:(String.concat ~sep:"") (List.map ~f:(List.map ~f:Int.to_string) f))
          end
        include T
        include Comparable.Make(T)
        type edge = FramedCluster.Vertex.t [@@deriving sexp,equal]
        let follow (c,_) es = let (cnew,groupnew) = FramedCluster.follow c es in ((cnew,frameVectors cnew), groupnew)

        let n (c,_) = List.map (FramedCluster.n c) ~f:(fun (c, e) -> ((c, frameVectors c), e))

      end
    include SearchBase.Make(Node)


    (* Takes a cluster and add frozen vertices to create a "frame" of frozen vertices paired with each unfrozen vertex by an edge*)
    let frame (c : Cluster.t) : Node.t  =
      let unfrozen = Cluster.Vertex.Set.to_list (Cluster.unfrozen c) in
      let unfrozenToFrameMap = Cluster.Vertex.Map.of_alist_exn (List.mapi unfrozen ~f:(fun i v -> (v, FramedVertex.Frame(i+1)))) in
      let frameSet = FramedVertex.Set.of_list (Map.data unfrozenToFrameMap) in
      let frozenSet = FramedVertex.Set.map (Cluster.frozen c) ~f:(fun v -> FramedVertex.Original v) in
      let make_original (m : int Cluster.Vertex.Map.t) : int FramedVertex.Map.t =
        FramedVertex.Map.of_alist_exn (List.map (Map.to_alist m) ~f:(fun (v, data) -> (FramedVertex.Original v, data))) in
      let graph = List.filter_map
                    (Map.to_alist (Cluster.quiver c))
                    ~f:(fun (v, nbr) ->
                      match Map.find unfrozenToFrameMap v with
                      | None -> Some(FramedVertex.Original v, make_original nbr)
                      | Some f -> Some (FramedVertex.Original v, FramedCluster.Vertex.Map.add_exn (make_original nbr) ~key:f ~data:(-1) )) in
      let frozenFrame = List.map unfrozen ~f:(fun v -> (Map.find_exn unfrozenToFrameMap v, FramedVertex.Map.singleton (FramedVertex.Original v) 1)) in
      let cluster = FramedCluster.of_list ~graph:(List.append graph frozenFrame) ~frozen:(Set.union frameSet frozenSet) in
      (cluster, frameVectors cluster)

    let unframe c =
      Cluster.of_quiver ~frozen:(Cluster.Vertex.Set.filter_map (FramedCluster.frozen c) ~f:(function FramedVertex.Original v -> Some v
                                                                                                | FramedVertex.Frame _ -> None))
                        ~quiver:(Cluster.Vertex.Map.of_alist_exn
                                   (List.filter_map (Map.to_alist (FramedCluster.quiver c))
                                                    ~f:(fun (v, nbr) -> match v with
                                                                     | FramedVertex.Original v -> Some(v, FramedVertex.filterNbr nbr)
                                                                     | FramedVertex.Frame _ -> None )))
  end

module MakeDouble (Cluster : Cluster.S) =
  struct
    module FramedVertex =
      struct
        module T =
          struct
            type t = OFrame of int | IFrame of int | Original of Cluster.Vertex.t [@@deriving sexp, compare]
          end
        include T
        include Comparable.Make(T)
        let weight (v) =
          match v with
          | IFrame _ -> 1
          | OFrame _ -> 1
          | Original v -> Cluster.Vertex.weight v

        let filterNbr nbr  =
          Cluster.Vertex.Map.of_alist_exn
            (List.filter_map (Map.to_alist nbr) ~f:(fun (key,data) -> match key with IFrame _-> None | OFrame _ -> None | Original v ->  Some(v,data)))

        let canExchange v ~inp ~out =
          match v with
          | IFrame _ -> false
          | OFrame _ -> false
          | Original v ->  Cluster.Vertex.canExchange v ~inp:(filterNbr inp) ~out:(filterNbr out)
        let mutate v ~inp ~out =
          match v with
          | IFrame _ -> v
          | OFrame _ -> v
          | Original v -> Original(Cluster.Vertex.mutate v ~inp:(filterNbr inp)  ~out:(filterNbr out))
        let id v =
          match v with
          | OFrame i -> "OF" ^ Int.to_string i
          | IFrame i -> "IF"^Int.to_string i
          | Original v -> "OR" ^ Cluster.Vertex.id v
        let to_string v =
          match v with
          | OFrame i -> "OF"^Int.to_string i
          | IFrame i -> "IF" ^ Int.to_string i
          | Original v -> Cluster.Vertex.to_string v
        let pp ppf v = Format.fprintf ppf "%s" (to_string v)
        let of_string s=
          match String.prefix s 2 with
          | "OF" -> OFrame(Int.of_string (String.suffix s 2))
          | "IF" -> IFrame(Int.of_string (String.suffix s 2))
          | "OR" -> Original(Cluster.Vertex.of_string (String.suffix s 2))
          | _ -> invalid_arg ("Framed vertices should be OF<i> IF<i> or OR<v> not " ^ s)

      end

    module FramedCluster = MapCluster.Make(FramedVertex)


    let iframeVectors c =
      FramedCluster.cVectors ~frozenlist:(Some (Set.to_list (Set.filter (FramedCluster.frozen c)
                                                            ~f:(function FramedVertex.IFrame _ -> true | _ -> false)))) c
    let oframeVectors c =
      FramedCluster.cVectors ~frozenlist:(Some (Set.to_list (Set.filter (FramedCluster.frozen c)
                                                              ~f:(function FramedVertex.OFrame _ -> true | _ -> false)))) c

    let frameVectors c =
      FramedCluster.cVectors ~frozenlist:(Some (Set.to_list (Set.filter (FramedCluster.frozen c)
                                                            ~f:(function FramedVertex.Original _ -> false | _ -> true)))) c

    module Node =
      struct
        module T =
          struct
            type t = FramedCluster.t * int list list [@@deriving sexp]
            let compare (_,f1) (_,f2) = List.compare cvec_compare f1 f2
            let label (_,f) = String.concat ~sep:"\n" (List.map ~f:(String.concat ~sep:" ") (List.map ~f:(List.map ~f:Int.to_string) f))
            let id (_,f) = String.concat ~sep:"" (List.map ~f:(String.concat ~sep:"") (List.map ~f:(List.map ~f:Int.to_string) f))
          end
        include T
        include Comparable.Make(T)
        type edge = FramedCluster.Vertex.t  [@@deriving sexp,equal]
        let follow (c,_) es = let (cnew,groupnew) = FramedCluster.follow c es in ((cnew,frameVectors cnew), groupnew)
        let n (c,_) = List.map (FramedCluster.n c) ~f:(fun (c, v) -> ((c, frameVectors c), v))

      end
    include SearchBase.Make(Node)


    (* Takes a cluster and replaces frozen vertices with a "frame" of frozen vertices paired with each unfrozen vertex by an edge*)
    let frame (c : Cluster.t) : Node.t  =
      let unfrozen = Cluster.Vertex.Set.to_list (Cluster.unfrozen c) in
      let unfrozenToFrameMap =
        Cluster.Vertex.Map.of_alist_exn (List.mapi unfrozen ~f:(fun i -> fun v -> (v, (FramedVertex.IFrame(i+1),FramedVertex.OFrame(i+1)) ))) in
      let frameSet = FramedVertex.Set.of_list (List.concat_map (Map.data unfrozenToFrameMap) ~f:(fun (v,w) -> [v;w])) in
      let frozenSet = FramedVertex.Set.map (Cluster.frozen c) ~f:(fun v -> FramedVertex.Original v) in
      let make_original (m : int Cluster.Vertex.Map.t) : int FramedVertex.Map.t =
        FramedVertex.Map.of_alist_exn (List.map (Map.to_alist m) ~f:(fun (v, data) -> (FramedVertex.Original v, data))) in
      let graph = List.filter_map
                    (Map.to_alist (Cluster.quiver c))
                    ~f:(fun (v, nbr) ->
                      match Map.find unfrozenToFrameMap v with
                      | None -> Some(FramedVertex.Original v, make_original nbr)
                      | Some (iFrame, oFrame) ->
                         Some (FramedVertex.Original v, FramedCluster.Vertex.Map.add_exn
                                                          (FramedCluster.Vertex.Map.add_exn (make_original nbr) ~key:iFrame ~data:(-1))
                                                          ~key:oFrame
                                                          ~data:1 )) in
      let frozenFrame =
        List.concat_map unfrozen ~f:(fun v -> let (iFrame, oFrame) = Map.find_exn unfrozenToFrameMap v in
                                              [(iFrame, FramedVertex.Map.singleton (FramedVertex.Original v) 1)
                                              ; (oFrame, FramedVertex.Map.singleton (FramedVertex.Original v) (-1))]) in
      let cluster = FramedCluster.of_list ~graph:(List.append graph frozenFrame) ~frozen:(Set.union frameSet frozenSet) in
      (cluster, frameVectors cluster)

    let unframe c =
      Cluster.of_quiver ~frozen:(Cluster.Vertex.Set.filter_map (FramedCluster.frozen c) ~f:(function FramedVertex.Original v -> Some v
                                                                                                | FramedVertex.IFrame _ -> None
                                                                                                | FramedVertex.OFrame _ -> None))
                        ~quiver:(Cluster.Vertex.Map.of_alist_exn
                                   (List.filter_map (Map.to_alist (FramedCluster.quiver c))
                                                    ~f:(fun (v, nbr) -> match v with
                                                                     | FramedVertex.Original v -> Some(v, FramedVertex.filterNbr nbr)
                                                                     | FramedVertex.OFrame _ -> None
                                                                     | FramedVertex.IFrame _ -> None)))
  end
