open Core
open Algebra

type 'a lform = W of (int*'a) list [@@deriving compare,sexp]

module type Extract =
  sig
    type rmod
    type cluster
    type xvert
    val extractA2rel : int -> (cluster, 'b, 'c) Map.t -> rmod
    val extractCasA3 : (cluster, 'b, 'c) Map.t -> xvert
    val termsCasA3 : int -> (cluster, 'b, 'c) Map.t -> xvert * rmod
    val termsCasA5 : int -> (cluster, 'b, 'c) Map.t -> xvert * rmod
    val extractA3 : int -> (cluster, 'b, 'c) Map.t -> rmod
    val extractA3sym : int -> (cluster, 'b, 'c) Map.t -> rmod
    val extractA3normalized : int -> (cluster, 'b, 'c) Map.t -> rmod
    val extractA4sym : int -> (cluster, 'b, 'c) Map.t -> rmod
    val extractCasA3_splits : cluster -> xvert -> (int * (xvert * xvert)) list
    val extractA5 : (cluster, 'b, 'c) Map.t -> rmod
end

module Make(Args:sig module CSearch:ClusterSearch.S module Lmod : RMod.S with type Index.t =  CSearch.XVert.t lform and type R.t = Int.t end):
  Extract with type rmod := Args.Lmod.t and type cluster := Args.CSearch.Cluster.t and type xvert := Args.CSearch.XVert.t  =
  struct
    module CSearch = Args.CSearch
    module Cluster = CSearch.Cluster
    module Vertex = Cluster.Vertex
    module XVertex = CSearch.XVert

    module Lmod = Args.Lmod

    let set_to_one s =
      match Set.to_list s with
      | [x] -> x
      | _ -> invalid_arg ("Expected set to have 1 element when instead it had " ^ Int.to_string(Set.length s))

    let set_to_pair s =
      match Set.to_list s with
      | [x;y] -> (x,y)
      | _ -> invalid_arg ("Expected set to have 2 elements when instead it had " ^ Int.to_string(Set.length s))

    let list_to_pair s =
      match s with
      | [x;y] -> (x,y)
      | _ -> invalid_arg ("Expected list to have 3 elements when instead it had " ^ Int.to_string(List.length s))

    let list_to_triple s =
      match s with
      | [x;y;z] -> (x,y,z)
      | _ -> invalid_arg ("Expected list to have 3 elements when instead it had " ^ Int.to_string(List.length s))

    let extractA2rel n graph = Lmod.of_list (List.map (Map.keys graph) ~f:(fun c -> (1, W[n,CSearch.xcoord c (Set.min_elt_exn (Cluster.sources c))])))
    let extractCasA3 graph =
      let c = List.find_exn (Map.keys graph) ~f:Cluster.fAffineDynkin in
      let (l,r) = set_to_pair (Cluster.sources c) in
      XVertex.mult(CSearch.xcoord c l) (XVertex.inv (CSearch.xcoord c r))

    let termsCasA3 n a3 =
      let extractCas c =
        let cas = XVertex.mult (CSearch.xcoord (c) (Set.choose_exn (Cluster.sources c))) (CSearch.xcoord c (Set.choose_exn (Cluster.sinks c))) in
        (cas, (-(n-1),W [n, cas])) in
      let extractL21path c =
        let (so,si) = (CSearch.xcoord c (Set.choose_exn (Cluster.sources c)), CSearch.xcoord c (Set.choose_exn (Cluster.sinks c)))
        in [(-1,W(List.zip_exn [n-1;1] [XVertex.mult so si; XVertex.inv si])); (1,W(List.zip_exn [n-1;1] [XVertex.mult so si; XVertex.inv so]))]
           @ [(Int.pow (-1) n, W[n,XVertex.inv si]); (Int.pow (-1) n, W[n,XVertex.inv so])] in
      let cycles = List.filter (Map.keys a3) ~f:(fun c -> Set.length (Cluster.sources c) = 0 && Set.length(Cluster.sinks c) =0) in
      let paths = List.map (Set.to_list (Cluster.unfrozen (List.hd_exn cycles))) ~f:(fun v -> Cluster.mutate (List.hd_exn cycles) v) in
      let (cas,casTerm) = extractCas (List.hd_exn paths) in
      (cas, Lmod.of_list (casTerm :: List.concat_map paths ~f:extractL21path))

    let termsCasA5 n a5 =
      let acoords c =
        let aM = set_to_one(Set.filter (Cluster.unfrozen c) ~f:(fun v -> Cluster.oDegree ~frozen:false c v = 2)) in
        let (aL,aR) = set_to_pair(Set.filter (Cluster.unfrozen c) ~f:(fun v -> Cluster.oDegree ~frozen:false c v = 1)) in
        let aML = set_to_one(Cluster.oNbrSet ~frozen:false c aL) in
        let aMR = set_to_one(Cluster.oNbrSet ~frozen:false c aR) in
        (aL,aML,aM,aMR,aR) in
      let dynkins = List.filter (Map.keys a5) ~f:Cluster.fAffineDynkin in
      let casA5 =
        let c = List.hd_exn dynkins in
        let (aL,aML,aM,aMR,aR) = acoords c in
        let (xL_1234, _(*xML_1245*), xM_1256, _(*xMR_1245*), xR_1234) =
          (CSearch.xcoord c aL, CSearch.xcoord c aML, CSearch.xcoord c aM,CSearch.xcoord c aMR, CSearch.xcoord c aR) in
         XVertex.mult xM_1256 (XVertex.inv (XVertex.mult xL_1234 xR_1234)) in
      let extract c =
        let (aL,aML,aM,aMR,aR) = acoords c in
        let (xL, xM, xR) =
          (CSearch.xcoord c aL,CSearch.xcoord c aM, CSearch.xcoord c aR) in
        let casA5 = XVertex.mult xM (XVertex.inv (XVertex.mult xL xR)) in
        let (cML,_(*aML_new*)) = Cluster.mutate_name c aML in
        let (xL_cML,xM_cML) =  (CSearch.xcoord cML aL, CSearch.xcoord cML aM) in
        let (cMR,_(*aMR_new*)) = Cluster.mutate_name c aMR in
        let (xM_cMR, xR_cMR) =  (CSearch.xcoord cMR aM, CSearch.xcoord cMR aR) in
          [(1, W[n-2,casA5; 1,xL; 1,XVertex.inv xM]) ; (1, W[n-2,casA5; 1,xR; 1, XVertex.inv xM])]
(*        @ [(1,W[n-1,XVertex.mult xM (XVertex.inv xL);1,XVertex.inv xM]); (-1,W[n-1,XVertex.mult xM (XVertex.inv xL);1, xL])
        ; (-1*Int.pow (-1) n, W[n, XVertex.inv xM]); (-1*Int.pow (-1) n, W[n, xL]); ((n-1),W[n,XVertex.mult xM (XVertex.inv xL)])]
        @ [(1, W[n-1,XVertex.mult xM (XVertex.inv xR);1, XVertex.inv xM]);  (-1, W[n-1,XVertex.mult xM (XVertex.inv xR);1,  xR])
        ; (-1*Int.pow (-1) n, W[n,XVertex.inv xM]); (-1*Int.pow (-1) n, W[n,xR]); ((n-1),W[n,XVertex.mult xM (XVertex.inv xR)])]
 *)
        @ [(-1,W[n-2,casA5; 1,xL_cML; 1,XVertex.inv xM_cML]) ; (-1, W[n-2,casA5; 1,xR; 1,XVertex.inv xM_cML])]
(*        @ [(-1,W[n-1,XVertex.mult xM_cML (XVertex.inv xL_cML);1,XVertex.inv xM_cML]); (1,W[n-1,XVertex.mult xM_cML (XVertex.inv xL_cML);1,xL_cML])
          ; (Int.pow (-1) n, W[n,XVertex.inv xM_cML]); (Int.pow (-1) n, W[n,xL_cML]) ; (-(n-1),W[n,XVertex.mult xM_cML (XVertex.inv xL_cML)])]
        @ [(-1, W[n-1,XVertex.mult xM_cML (XVertex.inv xR);1, XVertex.inv xM_cML]); (1, W[n-1,XVertex.mult xM_cML (XVertex.inv xR);1, xR])
          ; (Int.pow (-1) n, W[n,XVertex.inv xM_cML]); (Int.pow (-1) n, W[n,xR]); (-(n-1), W[n,XVertex.mult xM_cML (XVertex.inv xR)])]
 *)
        @ [(-1,W[n-2,casA5; 1,xL; 1,XVertex.inv xM_cMR]) ; (-1, W[n-2,casA5; 1,xR_cMR; 1,XVertex.inv xM_cMR])]
(*        @ [(-1,W[n-1,XVertex.mult xM_cMR (XVertex.inv xL);1,XVertex.inv xM_cMR]); (1,W[n-1,XVertex.mult xM_cMR (XVertex.inv xL);1, xL])
          ; (Int.pow (-1) n, W[n,XVertex.inv xM_cMR]); (Int.pow(-1) n, W[n, xL]); (-(n-1), W[n, XVertex.mult xM_cMR (XVertex.inv xL)])]
        @ [(-1, W[n-1,XVertex.mult xM_cMR (XVertex.inv xR_cMR);1, XVertex.inv xM_cMR]); (1, W[n-1,XVertex.mult xM_cMR (XVertex.inv xR_cMR);1, xR_cMR])
          ; (Int.pow (-1) n, W[n,XVertex.inv xM_cMR]); (Int.pow (-1) n, W[n,xR_cMR]); (-(n-1), W[n,XVertex.mult xM_cMR (XVertex.inv xR_cMR)])] *)
      in
      (casA5, Lmod.of_list ((-12,W[n,casA5]):: List.concat_map ~f:extract dynkins))
    (* n=5 -> -12 * W[5,cas]*)

    let extractA3 n graph =
      let extractCas c =
        (n+1,W[n, (XVertex.mult (CSearch.xcoord (c) (Set.choose_exn (Cluster.sources c))) (CSearch.xcoord c (Set.choose_exn (Cluster.sinks c))))]) in
      let extractL21 c =
        let (x,y) = (CSearch.xcoord c (Set.choose_exn (Cluster.sources c)), CSearch.xcoord c (Set.choose_exn (Cluster.sinks c)))
        in (1,W(List.zip_exn [n-1;1]  [XVertex.mult x y; XVertex.inv y])) in
      let extractL3dynkin c = (-1,W[n, CSearch.xcoord c (Set.choose_exn (Cluster.sources c))]) in
      let extractL3cycle c = List.map (Set.to_list (Cluster.unfrozen c)) ~f:(fun v -> (1, W[n, CSearch.xcoord c v])) in
      let cycles = List.filter (Map.keys graph) ~f:(fun c -> Set.length (Cluster.sources c) = 0 && Set.length(Cluster.sinks c) =0) in
      let paths = List.map (Set.to_list (Cluster.unfrozen (List.hd_exn cycles))) ~f:(fun v -> Cluster.mutate (List.hd_exn cycles) v) in
      let dynkins = List.map paths ~f:(fun c -> Cluster.mutate c (Set.choose_exn (Cluster.sources c))) in
      Lmod.of_list ((extractCas (List.hd_exn paths)) :: List.map paths ~f:extractL21
                    @ List.concat_map cycles ~f:extractL3cycle
                    @ List.map dynkins ~f:extractL3dynkin)

    let extractA3sym n graph =
      let extractCas c =
        (-(n-1),W [n, (XVertex.mult (CSearch.xcoord (c) (Set.choose_exn (Cluster.sources c))) (CSearch.xcoord c (Set.choose_exn (Cluster.sinks c))))]) in
      let extractL21path c =
        let (so,si) = (CSearch.xcoord c (Set.choose_exn (Cluster.sources c)), CSearch.xcoord c (Set.choose_exn (Cluster.sinks c)))
        in [(-1,W(List.zip_exn [n-1;1] [XVertex.mult so si; XVertex.inv si]));(1,W(List.zip_exn [n-1;1] [XVertex.mult so si; XVertex.inv so]))] in
      let extractL3dynkin c = (2,W [n,CSearch.xcoord c (Set.choose_exn (Cluster.sources c))]) ::
                                List.map (Set.to_list (Cluster.sinks c)) ~f:(fun v -> (1, W [n,CSearch.xcoord c v])) in
      let extractL3cycle c = List.map (Set.to_list (Cluster.unfrozen c)) ~f:(fun v -> (-2, W [n,CSearch.xcoord c v])) in
      let cycles = List.filter (Map.keys graph) ~f:(fun c -> Set.length (Cluster.sources c) = 0 && Set.length(Cluster.sinks c) =0) in
      let paths = List.map (Set.to_list (Cluster.unfrozen (List.hd_exn cycles))) ~f:(fun v -> Cluster.mutate (List.hd_exn cycles) v) in
      let dynkins = List.map paths ~f:(fun c -> Cluster.mutate c (Set.choose_exn (Cluster.sources c))) in
      Lmod.of_list ((extractCas (List.hd_exn paths))
                     :: List.concat_map paths ~f:extractL21path
                     @ List.concat_map cycles ~f:extractL3cycle
                     @ List.concat_map dynkins ~f:extractL3dynkin)

    let extractA3normalized n graph =
      let extractCas c =
        let cas = XVertex.mult (CSearch.xcoord (c) (Set.choose_exn (Cluster.sources c))) (CSearch.xcoord c (Set.choose_exn (Cluster.sinks c))) in
        [(n+1,W[n,cas]);(n+1,W[3,XVertex.inv cas])]  in
      let extractPath c =
        let (sources,sinks,neithers) = Cluster.sources_sinks_neithers c in
        let (xso,xsi,xne) = match List.map ~f:(fun s -> CSearch.xcoord c (Set.choose_exn s)) [sources;sinks;neithers] with
          | [xso;xsi;xne] -> (xso,xsi,xne)
          | _ -> invalid_arg "I messed this up"
        in [(1,W(List.zip_exn [n-1;1] [XVertex.mult xso xsi; XVertex.inv xsi])); (1,W[n,xne]); (1,W[n,XVertex.inv xne])] in
      let extractL3dynkin c =
        let x = CSearch.xcoord c (Set.choose_exn (Cluster.sources c)) in
        [(-1,W[n,x]); (-1,W[n,XVertex.inv x])] in
      let paths = List.filter (Map.keys graph) ~f:(fun c -> Set.length (Cluster.sources c) = 1 && Set.length (Cluster.sinks c) = 1) in
      let dynkins = List.filter (Map.keys graph) ~f:(fun c -> Set.length (Cluster.sources c) = 1 && Set.length (Cluster.sinks c) = 2) in
      Lmod.of_list ((extractCas (List.hd_exn paths))
                     @ List.concat_map paths ~f:extractPath
                     @ List.concat_map dynkins ~f:extractL3dynkin)



    let extractA4sym n graph =
      let cycles = List.filter (Map.keys graph) ~f:(fun c -> Set.length (Cluster.sources c) = 0 && Set.length(Cluster.sinks c) =1) in
      let dynkins = List.filter (Map.keys graph) ~f:(fun c -> Set.length (Cluster.sources c) = 2 && Set.length(Cluster.sinks c) =2) in
      let extract_cycle c =
        let center = set_to_one (Set.filter (Cluster.unfrozen c) ~f:(fun v -> Cluster.oDegree ~frozen:false c v = 2)) in
        let inp = set_to_one (Cluster.iNbrSet ~frozen:false c center) in
        let out = set_to_one (Cluster.iNbrSet ~frozen:false c inp) in
        let sink = set_to_one (Cluster.sinks c) in
        let (xcenter,xinp,xout,xsink) =
          match List.map ~f:(CSearch.xcoord c) [center;inp;out;sink] with
          | [xcenter;xinp;xout;xsink] -> (xcenter,xinp,xout,xsink)
          | _ -> invalid_arg "Out list isn't same size as in list" in
        let cas = XVertex.mult xsink xinp in
        let (a3next,path_next) = Cluster.mutate_multi_name c [inp;center;sink] in
        let (sink_next,center_next,inp_next) = list_to_triple path_next in
        let (a3last,path_last) = Cluster.mutate_multi_name a3next [inp_next;center_next;sink_next] in
        let (sink_last,_(*center_last*),inp_last) = list_to_triple path_last in
        [ (2,W[n,xcenter]); (-1,W[n,xout]); (1,W[n,xinp]); (-(n-1),W[n,cas])
        ; (1,W(List.zip_exn [n-1;1] [cas;XVertex.inv xinp])); (-1,W(List.zip_exn [n-1;1] [cas;XVertex.inv xsink]))
        ; (-1,W(List.zip_exn [n-1;1] [cas;CSearch.xcoord a3next inp_next])); (1,W(List.zip_exn [n-1;1] [cas; CSearch.xcoord a3next sink_next]))
        ; (1,W(List.zip_exn [n-1;1] [cas;XVertex.inv(CSearch.xcoord a3last inp_last)]))
        ; (-1,W(List.zip_exn [n-1;1] [cas;XVertex.inv(CSearch.xcoord a3last sink_last)]))] in
      let extract_dynkin c = List.map (Set.to_list (Cluster.sinks c)) ~f:(fun v -> (1,W[n, CSearch.xcoord c v])) in
      Lmod.of_list (List.concat_map cycles ~f:extract_cycle @ List.concat_map dynkins ~f:extract_dynkin)


    (* REQUIRES: c0 is quiver for an A3 dynking diagram with 1 sources and 2 sinks
     *           cas is one of the two choices of casimir for the A3 diagram *)
    let extractCasA3_splits c0 cas =
      let c1 = Cluster.mutate_multi c0 ((Set.to_list (Cluster.sources c0)) @ (Set.to_list (Cluster.sinks c0))) in
      let c2 = Cluster.mutate_multi c1 ((Set.to_list (Cluster.sources c1)) @ (Set.to_list (Cluster.sinks c1))) in
      [c0;c1;c2] |> List.map ~f:(fun c -> let (x,y) = set_to_pair(XVertex.Set.map ~f:(CSearch.xcoord c) (Cluster.sinks c)) in
                                          if XVertex.equal cas (XVertex.mult x (XVertex.inv y))
                                          then [(-1,(cas,XVertex.inv x)); (1,(cas, y))]
                                          else [(1,(cas, x)); (-1,(cas, XVertex.inv y))])
      |> List.concat

    let extractA5 graph =
      let acoords c =
        let aM = set_to_one(Set.filter (Cluster.unfrozen c) ~f:(fun v -> Cluster.oDegree ~frozen:false c v = 2)) in
        let (aL,aR) = set_to_pair(Set.filter (Cluster.unfrozen c) ~f:(fun v -> Cluster.oDegree ~frozen:false c v = 1)) in
        let aML = set_to_one(Cluster.oNbrSet ~frozen:false c aL) in
        let aMR = set_to_one(Cluster.oNbrSet ~frozen:false c aR) in
        (aL,aML,aM,aMR,aR) in

      let dynkins = List.filter (Map.keys graph) ~f:Cluster.fAffineDynkin in
      let extract c =
        let (aL,aML,aM,aMR,aR) = acoords c in
        let (xL_1234, xML_1245, xM_1256, xMR_1245, xR_1234) =
          (CSearch.xcoord c aL, CSearch.xcoord c aML, CSearch.xcoord c aM,CSearch.xcoord c aMR, CSearch.xcoord c aR) in
        let casA5 = XVertex.mult xM_1256 (XVertex.inv (XVertex.mult xL_1234 xR_1234)) in

        let casA3_i78L = XVertex.mult xM_1256 (XVertex.inv xL_1234) in
        let splits_C2i78L = extractCasA3_splits (Cluster.freeze (Cluster.mutate c aML) [aMR;aR]) casA3_i78L in
        let casA3_i78R = XVertex.mult xM_1256 (XVertex.inv xR_1234) in
        let splits_C2i78R = extractCasA3_splits (Cluster.freeze (Cluster.mutate c aMR) [aML;aL]) casA3_i78R in
        let casA3_i48L = XVertex.mult xML_1245 (XVertex.inv xMR_1245) in
        let splits_C2i48L = extractCasA3_splits (Cluster.freeze c [aL;aR]) casA3_i48L in
        let casA3_i48R = XVertex.mult xMR_1245 (XVertex.inv xML_1245) in
        let splits_C2i48R = extractCasA3_splits (Cluster.freeze c [aL;aR]) casA3_i48R in

        let (cM,aM_new) = Cluster.mutate_name c aM in
        let x1235L = CSearch.xcoord cM aML in
        let x1235R = CSearch.xcoord cM aMR in
        let cM_MLMR = Cluster.mutate_multi cM [aML;aMR] in
        let x1357 = CSearch.xcoord cM_MLMR aM_new in

        let (cML,aML_new) = Cluster.mutate_name c aML in
        let (xL_cML,xM_cML_1236) =  (CSearch.xcoord cML aL, CSearch.xcoord cML aM) in
        let x1257L = CSearch.xcoord (Cluster.mutate cML aM) aML_new in
        let (cMR,aMR_new) = Cluster.mutate_name c aMR in
        let (_(*xL_cMR*),xM_cMR_1236,xR_cMR) =  (CSearch.xcoord cMR aL, CSearch.xcoord cMR aM, CSearch.xcoord cMR aR) in
        let x1257R = CSearch.xcoord (Cluster.mutate cMR aM) aMR_new in

        let (cL,aL_cL) = Cluster.mutate_name c aL in
        let (xML_cL_1237,_(*xM_cL*),xMR_cL,_(*xR_cL*)) = (CSearch.xcoord cL aML, CSearch.xcoord cL aM, CSearch.xcoord cL aMR, CSearch.xcoord cL aR) in
        let casA3_i47L = XVertex.mult xML_cL_1237 (XVertex.inv xMR_cL) in
        let splits_C2i47L = extractCasA3_splits (Cluster.freeze cL [aL_cL;aR]) casA3_i47L in

        let cL_ML = Cluster.mutate cL aML in
        let x1246L = CSearch.xcoord cL_ML aM in
        let cL_ML_M = Cluster.mutate cL_ML aM in
        let x1247L = CSearch.xcoord cL_ML_M aMR in

        let (cR,aR_cR) = Cluster.mutate_name c aR in
        let (_(*xL_cR*),xML_cR,_(*xM_cR*),xMR_cR_1237) = (CSearch.xcoord cR aL, CSearch.xcoord cR aML, CSearch.xcoord cR aM, CSearch.xcoord cR aMR) in
        let casA3_i47R = XVertex.mult xMR_cR_1237 (XVertex.inv xML_cR) in
        let splits_C2i47R = extractCasA3_splits (Cluster.freeze cR [aL;aR_cR]) casA3_i47R in

        let cR_MR = Cluster.mutate cR aMR in
        let x1246R = CSearch.xcoord cR_MR aM in
        let cR_MR_M = Cluster.mutate cR_MR aM in
        let x1247R = CSearch.xcoord cR_MR_M aML in

        let (cLR,path_cLR) = Cluster.mutate_multi_name cL [aMR; aR] in
        let (aR_cLR,aMR_cLR) = list_to_pair path_cLR in
        let (xML_cLR, xMR_cLR) = (CSearch.xcoord cLR aML, CSearch.xcoord cLR aMR_cLR) in
        let casA3_i57L = XVertex.mult xML_cLR (xMR_cLR) in
        let splits_C2i57L = extractCasA3_splits (Cluster.freeze (Cluster.mutate cLR aMR_cLR) [aR_cLR;aL_cL]) casA3_i57L in

        let (cRL,path_cRL) = Cluster.mutate_multi_name cR [aML; aL] in
        let (aL_cRL,aML_cRL) = list_to_pair path_cRL in
        let (xML_cRL, xMR_cRL) = (CSearch.xcoord cRL aML_cRL, CSearch.xcoord cRL aMR) in
        let casA3_i57R = XVertex.mult xMR_cRL (xML_cRL) in
        let splits_C2i57R = extractCasA3_splits (Cluster.freeze (Cluster.mutate cRL aML_cRL) [aL_cRL;aR_cR]) casA3_i57R in

        let termsCasA5 =
          [(3,W[5,casA5])]
          @ [(2, W [3,casA5;1,xL_1234;1,XVertex.inv xM_1256]) ; (2, W[3,casA5;1,xR_1234;1, XVertex.inv xM_1256])]
          @ [(-2,W [3,casA5;1,xL_cML; 1, XVertex.inv xM_cML_1236]) ; (-2, W[3,casA5;1,xR_1234;1, XVertex.inv xM_cML_1236])]
          @ [(-2,W [3,casA5;1,xL_1234; 1, XVertex.inv xM_cMR_1236]) ; (-2, W[3,casA5;1,xR_cMR;1, XVertex.inv xM_cMR_1236])] in

        let terms_C2i78 = (-4,W[5,casA3_i78L]) :: (-4,W[5,casA3_i78R]) :: (List.map ~f:(fun (c,(x,y)) -> (c,W[4,x;1,y])) (splits_C2i78L @ splits_C2i78R)) in
        let terms_C2i48 = (4,W[5,casA3_i48L]) :: (4, W[5,casA3_i48R]) :: (List.map ~f:(fun (c,(x,y)) -> (-1*c,W[4,x;1,y])) (splits_C2i48L @ splits_C2i48R)) in
        let terms_C2i47 = (-8,W[5,casA3_i47L]) :: (-8,W[5,casA3_i47R]) :: (List.map ~f:(fun (c,(x,y)) -> (2*c,W[4,x;1,y])) (splits_C2i47L @ splits_C2i47R)) in
        let terms_C2i57 = (8,W[5,casA3_i57L]) :: (8,W[5,casA3_i57R]) :: (List.map ~f:(fun (c,(x,y)) -> (-2*c,W[4,x;1,y])) (splits_C2i57L @ splits_C2i57R)) in
        let terms_x = [ (1,W[5 ,xL_1234]); (1,W[5,xR_1234])
                        ; (2,W[5,x1357])
                        ; (2,W[5,xM_1256])
                        ; (2,W[5,xML_1245]);(2,W[5,xMR_1245])
                        ; (2,W[5,x1247L]);(2,W[5,x1247R])
                        ; (-2,W[5,x1235L]);(-2,W[5,x1235R])
                        ; (-2,W[5,xML_cL_1237]);(-2,W[5,xMR_cR_1237])
                        ; (-2,W[5,x1246L]);(-2,W[5,x1246R])
                        ; (-2,W[5,x1257L]);(-2,W[5,x1257R])] in
        termsCasA5 @ terms_C2i78 @ terms_C2i48 @ terms_C2i47 @ terms_C2i57 @ terms_x in
      Lmod.of_list (List.concat_map ~f:extract dynkins)

  end
(*
module Make(CSearch: ClusterSearch.S) =
  struct
    module Cluster = CSearch.Cluster
    module Vertex = Cluster.Vertex
    module XVertex = CSearch.XVert

    module L2mod =
      FreeRMod.Make
        (struct
          module  T =
            struct
              type t = XVertex.t l2 [@@deriving compare,sexp]
            end
          include T
          include Comparable.Make(T)
          let to_string x =
            match x with
            | L2 x -> "L2_"^(String.concat (List.map (XVertex.top_list x) Vertex.to_string))
          let of_string s = invalid_arg "NYI"
        end)
        (IntRing)
    let extractA2rel graph = L2mod.of_list (List.map (Map.keys graph) (fun c -> (1, L2( CSearch.xcoord c (Set.min_elt_exn (Cluster.sources c))))))

    module L3mod =
      FreeRMod.Make
        (struct
          module T =
            struct
              type t = XVertex.t l3 [@@deriving sexp,compare]
            end
          include T
          include Comparable.Make(T)
          let to_string x =
            match x with
            | L3 x -> "L3_"^(String.concat (List.map (XVertex.top_list x) Vertex.to_string))
            | L21 (x,y) -> "L21_"^ (String.concat (List.map (XVertex.top_list x) Vertex.to_string))
                           ^ "_" ^ (String.concat (List.map (XVertex.top_list y) Vertex.to_string))
          let of_string s = invalid_arg "NYI"
        end)
        (IntRing)

    let set_to_one s =
      match Set.to_list s with
      | [x] -> x
      | _ -> invalid_arg ("Expected set to have 1 element when instead it had " ^ Int.to_string(Set.length s))

    let set_to_pair s =
      match Set.to_list s with
      | [x;y] -> (x,y)
      | _ -> invalid_arg ("Expected set to have 2 elements when instead it had " ^ Int.to_string(Set.length s))

    (* REQUIRES: c0 is quiver for an A3 dynking diagram with 1 sources and 2 sinks
     *           cas is one of the two choices of casimir for the A3 diagram *)
    let extractCasA3_splits c0 cas =
      let c1 = Cluster.mutate_multi c0 ((Set.to_list (Cluster.sources c0)) @ (Set.to_list (Cluster.sinks c0))) in
      let c2 = Cluster.mutate_multi c1 ((Set.to_list (Cluster.sources c1)) @ (Set.to_list (Cluster.sinks c1))) in
      [c0;c1;c2] |> List.map ~f:(fun c -> let (x,y) = set_to_pair(XVertex.Set.map ~f:(CSearch.xcoord c) (Cluster.sinks c)) in
                                          if XVertex.equal cas (XVertex.mult x (XVertex.inv y))
                                          then [(-1,(cas,XVertex.inv x)); (1,(cas, y))]
                                          else [(1,(cas, x)); (-1,(cas, XVertex.inv y))])
      |> List.concat


    let extractCasA3 graph =
      let c = List.find_exn (Map.keys graph) ~f:Cluster.fAffineDynkin in
      let (l,r) = set_to_pair (Cluster.sources c) in
      XVertex.mult(CSearch.xcoord c l) (XVertex.inv (CSearch.xcoord c r))

    let extractA3 graph =
      let extractCas c =
        (n+1,L3 (XVertex.mult (CSearch.xcoord (c) (Set.choose_exn (Cluster.sources c))) (CSearch.xcoord c (Set.choose_exn (Cluster.sinks c))))) in
      let extractL21 c =
        let (x,y) = (CSearch.xcoord c (Set.choose_exn (Cluster.sources c)), CSearch.xcoord c (Set.choose_exn (Cluster.sinks c)))
        in (1,L21 (XVertex.mult x y, XVertex.inv y)) in
      let extractL3dynkin c = (-1,L3 (CSearch.xcoord c (Set.choose_exn (Cluster.sources c)))) in
      let extractL3cycle c = List.map (Set.to_list (Cluster.unfrozen c)) (fun v -> (1, L3(CSearch.xcoord c v))) in
      let cycles = List.filter (Map.keys graph) ~f:(fun c -> Set.length (Cluster.sources c) = 0 && Set.length(Cluster.sinks c) =0) in
      let paths = List.map (Set.to_list (Cluster.unfrozen (List.hd_exn cycles))) ~f:(fun v -> Cluster.mutate (List.hd_exn cycles) v) in
      let dynkins = List.map paths (fun c -> Cluster.mutate c (Set.choose_exn (Cluster.sources c))) in
      L3mod.of_list ((extractCas (List.hd_exn paths)) :: List.map paths extractL21 @ List.concat_map cycles extractL3cycle @ List.map dynkins extractL3dynkin)

    let extractA3sym graph =
      let extractCas c =
        (-2,L3 (XVertex.mult (CSearch.xcoord (c) (Set.choose_exn (Cluster.sources c))) (CSearch.xcoord c (Set.choose_exn (Cluster.sinks c))))) in
      let extractL21path c =
        let (so,si) = (CSearch.xcoord c (Set.choose_exn (Cluster.sources c)), CSearch.xcoord c (Set.choose_exn (Cluster.sinks c)))
        in [(-1,L21 (XVertex.mult so si, XVertex.inv si));(1,L21(XVertex.mult so si, XVertex.inv so))] in
      let extractL3dynkin c = (2,L3 (CSearch.xcoord c (Set.choose_exn (Cluster.sources c)))) ::
                                List.map (Set.to_list (Cluster.sinks c)) (fun v -> (1, L3(CSearch.xcoord c v))) in
      let extractL3cycle c = List.map (Set.to_list (Cluster.unfrozen c)) (fun v -> (-2, L3(CSearch.xcoord c v))) in
      let cycles = List.filter (Map.keys graph) ~f:(fun c -> Set.length (Cluster.sources c) = 0 && Set.length(Cluster.sinks c) =0) in
      let paths = List.map (Set.to_list (Cluster.unfrozen (List.hd_exn cycles))) ~f:(fun v -> Cluster.mutate (List.hd_exn cycles) v) in
      let dynkins = List.map paths (fun c -> Cluster.mutate c (Set.choose_exn (Cluster.sources c))) in
      L3mod.of_list ((extractCas (List.hd_exn paths))
                     :: List.concat_map paths extractL21path
                     @ List.concat_map cycles extractL3cycle
                     @ List.concat_map dynkins extractL3dynkin)

    let extractA3normalized graph =
      let extractCas c =
        let cas = XVertex.mult (CSearch.xcoord (c) (Set.choose_exn (Cluster.sources c))) (CSearch.xcoord c (Set.choose_exn (Cluster.sinks c))) in
        [(4,L3 (cas));(4,L3(XVertex.inv cas))] in
      let extractPath c =
        let (sources,sinks,neithers) = Cluster.sources_sinks_neithers c in
        let [xso;xsi;xne] = List.map ~f:(fun s -> CSearch.xcoord c (Set.choose_exn s)) [sources;sinks;neithers]
        in [(1,L21 (XVertex.mult xso xsi, XVertex.inv xsi)); (1,L3 (xne)); (1,L3(XVertex.inv xne))] in
      let extractL3dynkin c =
        let x = CSearch.xcoord c (Set.choose_exn (Cluster.sources c)) in
        [(-1,L3 (x)); (-1,L3(XVertex.inv x))] in
      let paths = List.filter (Map.keys graph) (fun c -> Set.length (Cluster.sources c) = 1 && Set.length (Cluster.sinks c) = 1) in
      let dynkins = List.filter (Map.keys graph) (fun c -> Set.length (Cluster.sources c) = 1 && Set.length (Cluster.sinks c) = 2) in
      L3mod.of_list ((extractCas (List.hd_exn paths))
                     @ List.concat_map paths extractPath
                     @ List.concat_map dynkins extractL3dynkin)


    module L4mod =
      FreeRMod.Make
        (struct
          module T =
            struct
              type t = XVertex.t l4 [@@deriving compare,sexp]
            end
          include T
          include Comparable.Make(T)
          let to_string x =
            match x with
            | L4 x -> "L4_"^(String.concat (List.map (XVertex.top_list x) Vertex.to_string))
            | L31 (x,y) -> "L31_"^ (String.concat (List.map (XVertex.top_list x) Vertex.to_string))
                           ^ "_" ^ (String.concat (List.map (XVertex.top_list y) Vertex.to_string))
          let of_string s = invalid_arg "NYI"
        end)
        (IntRing)

    let extractA4sym graph =
      let cycles = List.filter (Map.keys graph) ~f:(fun c -> Set.length (Cluster.sources c) = 0 && Set.length(Cluster.sinks c) =1) in
      let dynkins = List.filter (Map.keys graph) ~f:(fun c -> Set.length (Cluster.sources c) = 2 && Set.length(Cluster.sinks c) =2) in
      let extract_cycle c =
        let center = set_to_one (Set.filter (Cluster.unfrozen c) (fun v -> Cluster.oDegree ~frozen:false c v = 2)) in
        let inp = set_to_one (Cluster.iNbrSet ~frozen:false c center) in
        let out = set_to_one (Cluster.iNbrSet ~frozen:false c inp) in
        let sink = set_to_one (Cluster.sinks c) in
        let [xcenter;xinp;xout;xsink] = List.map ~f:(CSearch.xcoord c) [center;inp;out;sink] in
        let cas = XVertex.mult xsink xinp in
        let (a3next,[sink_next;center_next;inp_next]) = Cluster.mutate_multi_name c [inp;center;sink] in
        let (a3last,[sink_last;center_last;inp_last]) = Cluster.mutate_multi_name a3next [inp_next;center_next;sink_next] in
        [ (2,L4(xcenter)); (-1,L4(xout)); (1,L4(xinp)); (-3,L4(cas))
        ; (1,L31(cas,XVertex.inv xinp)); (-1,L31(cas,XVertex.inv xsink))
        ; (-1,L31(cas,CSearch.xcoord a3next inp_next)); (1,L31(cas, CSearch.xcoord a3next sink_next))
        ; (1,L31(cas,XVertex.inv(CSearch.xcoord a3last inp_last))); (-1,L31(cas, XVertex.inv(CSearch.xcoord a3last sink_last)))] in
      let extract_dynkin c = List.map (Set.to_list (Cluster.sinks c)) (fun v -> (1,L4 (CSearch.xcoord c v))) in
      L4mod.of_list (List.concat_map cycles extract_cycle @ List.concat_map dynkins extract_dynkin)


    module L5mod =
      FreeRMod.Make(struct
          module T =
            struct
              type t = XVertex.t l5 [@@deriving compare,sexp]
            end
          include T
          include Comparable.Make(T)
          let to_string x =
            match x with
            | L5 x -> "L5_"^(String.concat (List.map (XVertex.top_list x) Vertex.to_string))
            | L41 (x,y) -> "L41_"^ (String.concat (List.map (XVertex.top_list x) Vertex.to_string))
                           ^ "_" ^ (String.concat (List.map (XVertex.top_list y) Vertex.to_string))
            | L32 (x,y) -> "L32_"^ (String.concat (List.map (XVertex.top_list x) Vertex.to_string))
                           ^ "_" ^ (String.concat (List.map (XVertex.top_list y) Vertex.to_string))
            | L311 (x,y,z) -> "L311_"^ (String.concat (List.map (XVertex.top_list x) Vertex.to_string))
                              ^ "_" ^ (String.concat (List.map (XVertex.top_list y) Vertex.to_string))
                              ^ "_" ^ (String.concat (List.map (XVertex.top_list z) Vertex.to_string))
          let of_string s = invalid_arg "NYI"
        end) (IntRing)

    let extractCasA5 graph =
      let c = List.find_exn (Map.keys graph) ~f:Cluster.fAffineDynkin in
      let aM = set_to_one(Set.filter (Cluster.unfrozen c) (fun v -> Cluster.oDegree ~frozen:false c v = 2)) in
      let (aL,aR) = set_to_pair(Set.filter (Cluster.unfrozen c) (fun v -> Cluster.oDegree ~frozen:false c v = 1)) in
      let aML = set_to_one(Cluster.oNbrSet ~frozen:false c aL) in
      let aMR = set_to_one(Cluster.oNbrSet ~frozen:false c aR) in
      let (xL_1234, xML_1245, xM_1256, xMR_1245, xR_1234) =
        (CSearch.xcoord c aL, CSearch.xcoord c aML, CSearch.xcoord c aM,CSearch.xcoord c aMR, CSearch.xcoord c aR) in
      XVertex.mult xM_1256 (XVertex.inv (XVertex.mult xL_1234 xR_1234))


    let extractA5 graph =
      let acoords c =
        let aM = set_to_one(Set.filter (Cluster.unfrozen c) (fun v -> Cluster.oDegree ~frozen:false c v = 2)) in
        let (aL,aR) = set_to_pair(Set.filter (Cluster.unfrozen c) (fun v -> Cluster.oDegree ~frozen:false c v = 1)) in
        let aML = set_to_one(Cluster.oNbrSet ~frozen:false c aL) in
        let aMR = set_to_one(Cluster.oNbrSet ~frozen:false c aR) in
        (aL,aML,aM,aMR,aR) in

      let dynkins = List.filter (Map.keys graph) Cluster.fAffineDynkin in
      let extract c =
        let (aL,aML,aM,aMR,aR) = acoords c in
        let (xL_1234, xML_1245, xM_1256, xMR_1245, xR_1234) =
          (CSearch.xcoord c aL, CSearch.xcoord c aML, CSearch.xcoord c aM,CSearch.xcoord c aMR, CSearch.xcoord c aR) in
        let casA5 = XVertex.mult xM_1256 (XVertex.inv (XVertex.mult xL_1234 xR_1234)) in

        let casA3_i78L = XVertex.mult xM_1256 (XVertex.inv xL_1234) in
        let splits_C2i78L = extractCasA3_splits (Cluster.freeze (Cluster.mutate c aML) [aMR;aR]) casA3_i78L in
        let casA3_i78R = XVertex.mult xM_1256 (XVertex.inv xR_1234) in
        let splits_C2i78R = extractCasA3_splits (Cluster.freeze (Cluster.mutate c aMR) [aML;aL]) casA3_i78R in
        let casA3_i48L = XVertex.mult xML_1245 (XVertex.inv xMR_1245) in
        let splits_C2i48L = extractCasA3_splits (Cluster.freeze c [aL;aR]) casA3_i48L in
        let casA3_i48R = XVertex.mult xMR_1245 (XVertex.inv xML_1245) in
        let splits_C2i48R = extractCasA3_splits (Cluster.freeze c [aL;aR]) casA3_i48R in

        let (cM,aM_new) = Cluster.mutate_name c aM in
        let x1235L = CSearch.xcoord cM aML in
        let x1235R = CSearch.xcoord cM aMR in
        let cM_MLMR = Cluster.mutate_multi cM [aML;aMR] in
        let x1357 = CSearch.xcoord cM_MLMR aM_new in

        let (cML,aML_new) = Cluster.mutate_name c aML in
        let (xL_cML,xM_cML_1236) =  (CSearch.xcoord cML aL, CSearch.xcoord cML aM) in
        let x1257L = CSearch.xcoord (Cluster.mutate cML aM) aML_new in
        let (cMR,aMR_new) = Cluster.mutate_name c aMR in
        let (xL_cMR,xM_cMR_1236,xR_cMR) =  (CSearch.xcoord cMR aL, CSearch.xcoord cMR aM, CSearch.xcoord cMR aR) in
        let x1257R = CSearch.xcoord (Cluster.mutate cMR aM) aMR_new in

        let (cL,aL_cL) = Cluster.mutate_name c aL in
        let (xML_cL_1237,xM_cL,xMR_cL,xR_cL) = (CSearch.xcoord cL aML, CSearch.xcoord cL aM, CSearch.xcoord cL aMR, CSearch.xcoord cL aR) in
        let casA3_i47L = XVertex.mult xML_cL_1237 (XVertex.inv xMR_cL) in
        let splits_C2i47L = extractCasA3_splits (Cluster.freeze cL [aL_cL;aR]) casA3_i47L in

        let cL_ML = Cluster.mutate cL aML in
        let x1246L = CSearch.xcoord cL_ML aM in
        let cL_ML_M = Cluster.mutate cL_ML aM in
        let x1247L = CSearch.xcoord cL_ML_M aMR in

        let (cR,aR_cR) = Cluster.mutate_name c aR in
        let (xL_cR,xML_cR,xM_cR,xMR_cR_1237) = (CSearch.xcoord cR aL, CSearch.xcoord cR aML, CSearch.xcoord cR aM, CSearch.xcoord cR aMR) in
        let casA3_i47R = XVertex.mult xMR_cR_1237 (XVertex.inv xML_cR) in
        let splits_C2i47R = extractCasA3_splits (Cluster.freeze cR [aL;aR_cR]) casA3_i47R in

        let cR_MR = Cluster.mutate cR aMR in
        let x1246R = CSearch.xcoord cR_MR aM in
        let cR_MR_M = Cluster.mutate cR_MR aM in
        let x1247R = CSearch.xcoord cR_MR_M aML in

        let (cLR,[aR_cLR;aMR_cLR]) = Cluster.mutate_multi_name cL [aMR; aR] in
        let (xML_cLR, xMR_cLR) = (CSearch.xcoord cLR aML, CSearch.xcoord cLR aMR_cLR) in
        let casA3_i57L = XVertex.mult xML_cLR (xMR_cLR) in
        let splits_C2i57L = extractCasA3_splits (Cluster.freeze (Cluster.mutate cLR aMR_cLR) [aR_cLR;aL_cL]) casA3_i57L in

        let (cRL,[aL_cRL;aML_cRL]) = Cluster.mutate_multi_name cR [aML; aL] in
        let (xML_cRL, xMR_cRL) = (CSearch.xcoord cRL aML_cRL, CSearch.xcoord cRL aMR) in
        let casA3_i57R = XVertex.mult xMR_cRL (xML_cRL) in
        let splits_C2i57R = extractCasA3_splits (Cluster.freeze (Cluster.mutate cRL aML_cRL) [aL_cRL;aR_cR]) casA3_i57R in

        let termsCasA5 =
          [(3,L5(casA5))]
          @ [(2, L311(casA5,xL_1234,XVertex.inv xM_1256)) ; (2, L311(casA5,xR_1234, XVertex.inv xM_1256))]
          @ [(-2,L311(casA5,xML_cL_1237, XVertex.inv xM_cML_1236)) ; (-2, L311(casA5,xR_1234, XVertex.inv xM_cML_1236))]
          @ [(-2,L311(casA5,xL_1234, XVertex.inv xM_cMR_1236)) ; (-2, L311(casA5,xMR_cR_1237, XVertex.inv xM_cMR_1236))] in

        let terms_C2i78 = (-4,L5(casA3_i78L)) :: (-4,L5(casA3_i78R)) :: (List.map ~f:(fun (c,(x,y)) -> (c,L41(x,y))) (splits_C2i78L @ splits_C2i78R)) in
        let terms_C2i48 = (4,L5(casA3_i48L)) :: (4, L5(casA3_i48R)) :: (List.map ~f:(fun (c,(x,y)) -> (-1*c,L41(x,y))) (splits_C2i48L @ splits_C2i48R)) in
        let terms_C2i47 = (-8,L5(casA3_i47L)) :: (-8,L5(casA3_i47R)) :: (List.map ~f:(fun (c,(x,y)) -> (2*c,L41(x,y))) (splits_C2i47L @ splits_C2i47R)) in
        let terms_C2i57 = (8,L5(casA3_i57L)) :: (8,L5(casA3_i57R)) :: (List.map ~f:(fun (c,(x,y)) -> (-2*c,L41(x,y))) (splits_C2i57L @ splits_C2i57R)) in
        let terms_x = [ (1,L5 (xL_1234)); (1,L5(xR_1234))
                        ; (2,L5(x1357))
                        ; (2,L5(xM_1256))
                        ; (2,L5(xML_1245));(2,L5(xMR_1245))
                        ; (2,L5(x1247L));(2,L5(x1247R))
                        ; (-2,L5(x1235L));(-2,L5(x1235R))
                        ; (-2,L5(xML_cL_1237));(-2,L5(xMR_cR_1237))
                        ; (-2,L5(x1246L));(-2,L5(x1246R))
                        ; (-2,L5(x1257L));(-2,L5(x1257R))] in
        termsCasA5 @ terms_C2i78 @ terms_C2i48 @ terms_C2i47 @ terms_C2i57 @ terms_x in
      L5mod.of_list (List.concat_map ~f:extract dynkins)


  end
 *)
