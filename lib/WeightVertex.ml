open Core

module Make(Args : sig val max_weight : int end) : sig include Vertex.PS with type t = int list end =
  struct
    type t = int list [@@deriving compare, sexp]

    let of_plucker n s =
      List.map (List.range 1 n ~stop:`inclusive) ~f:(fun i -> if Set.mem s i then 1 else 0)

    let mutate v ~inp ~out =
      let zeros = List.map (List.range 0 (List.length v)) ~f:(fun _ -> 0) in
      let combine ~key ~data acc = List.map2_exn ~f:(+) acc (List.map key ~f:(fun i -> i * data)) in
      let outcount = Map.fold out ~init:zeros ~f:combine in
      let inpcount = Map.fold inp ~init:zeros ~f:combine in
      let () = if List.equal Int.equal outcount inpcount then () else invalid_arg "Verex has unbalanced in/out" in
      List.map2_exn outcount v ~f:(fun a -> fun b -> a - b)

    let canExchange v ~inp ~out =
      List.exists (mutate v ~inp:inp ~out:out) ~f:(fun i -> i <= Args.max_weight)

    let weight v = List.fold v ~init:0 ~f:Int.max

    let id v = String.concat ~sep:"," (List.map ~f:Int.to_string v)
    let to_string = id
    let of_string s = List.map (String.split ~on:',' s) ~f:Int.of_string
    let pp ppf v = Format.fprintf ppf "%s" (to_string v)

    module Comp = Comparable.Make(struct type t = int list [@@deriving compare, sexp] end)
    include Comp
  end
