open Core
include PartVertex

let exoticX = List.init 7 ~f:(fun i -> PartVertex.a ~i:(i+1) [[2;1;1];[1;1;1];[1;1;0]])
let exoticY = List.init 7 ~f:(fun i -> PartVertex.a ~i:(i+1) [[2;2;1];[2;1;0];[1;0;0]])
let convert_zickert s =
  match s with
  | "y71" -> "x7"
  | "y62" -> "x6"
  | "y51" -> "x5"
  | "y42" -> "x4"
  | "y31" -> "x3"
  | "y22" -> "x2"
  | "y11" -> "x1"
  | "y72" -> "y2"
  | "y61" -> "y6"
  | "y52" -> "y5"
  | "y41" -> "y4"
  | "y32" -> "y3"
  | "y21" -> "y2"
  | "y12" -> "y1"
  | _ -> s

let to_string v =
  match PartVertex.weight v with
  | 1 -> Plucker.Vert.to_string (to_plucker 3 7 v)
  | 2 -> (match List.findi exoticX ~f:(fun _ -> PartVertex.equal v) with
          | Some (i,_)  -> "e2x" ^ Int.to_string (i+1)
          | None ->
             (match List.findi exoticY ~f:(fun _ -> PartVertex.equal v) with
              | Some (i,_) -> "e2y" ^ Int.to_string (i+1)
              | None -> invalid_arg ("Weight 2 vertex isn't in gr37" ^ PartVertex.to_string v)))
  | _ -> invalid_arg ("No weight about 2 vertices in in gr37" ^ PartVertex.to_string v)
let to_string_short = to_string
let pp ppf v = Format.fprintf ppf "%s" (to_string v)

let of_string s =
    match String.to_list s with
  | ['p';i;j;k] -> PartVertex.of_plucker 7 (Int.Set.of_list (List.map [i;j;k] ~f:(fun c -> Int.of_string(Char.to_string c))))
  | ['e';'2';'x';si] -> List.nth_exn exoticX (Int.of_string (Char.to_string si) -1)
  | ['e';'2';'y';si] -> List.nth_exn exoticY (Int.of_string (Char.to_string si) -1)
  | _ -> invalid_arg (s ^ " has unknown prefix")


(* Orbits for Higher weight vertices:
 * x1 y2 x3 y4 x5 y6 x7
 * y1 x2 y3 x3 y4 x5 y7 *)
let rotate v =
  match PartVertex.weight v with
  | 1 -> PartVertex.of_plucker 7 (Plucker.Vert.rotate 7 (PartVertex.to_plucker 3 7 v))
  | 2 -> (match List.findi exoticX ~f:(fun _ u -> PartVertex.equal u v) with
          | Some (6, _) -> List.nth_exn exoticX 0
          | Some (i, _) -> List.nth_exn exoticY ((i+1) mod 7)
          | None -> (match List.findi exoticY ~f:(fun _ u -> PartVertex.equal u v) with
                     | Some (6,_) -> List.nth_exn exoticY 0
                     | Some (i,_) -> List.nth_exn exoticX ((i+1) mod 7)
                     | None -> invalid_arg ("Can't rotate unknown weight 2 coordinate " ^ (PartVertex.to_string v)))
         )
  | _ -> invalid_arg ("No weight higher then 2 vertices in gr37 " ^ PartVertex.to_string v)


let eval_mat v mat =
  match PartVertex.weight v with
  | 1 -> Plucker.Vert.eval_mat (to_plucker 3 7 v) mat
  | 2 -> (match List.findi exoticX ~f:(fun _ -> PartVertex.equal v) with
          | Some (i,_)  ->
             (Plucker.Vert.eval_mat (Plucker.Vert.a ~i:(i+1) (Int.Set.of_list [1;3;4])) mat) *. (Plucker.Vert.eval_mat (Plucker.Vert.a ~i:(i+1) (Int.Set.of_list [2;5;6])) mat)
             -. (Plucker.Vert.eval_mat (Plucker.Vert.a ~i:(i+1) (Int.Set.of_list [1;5;6])) mat) *. (Plucker.Vert.eval_mat (Plucker.Vert.a ~i:(i+1) (Int.Set.of_list [2;3;4])) mat)
          | None ->
             (match List.findi exoticY ~f:(fun _ -> PartVertex.equal v) with
              | Some (i,_) ->
                 (Plucker.Vert.eval_mat (Plucker.Vert.a ~i:(i+1) (Int.Set.of_list [1;3;6])) mat) *. (Plucker.Vert.eval_mat (Plucker.Vert.a ~i:(i+1) (Int.Set.of_list [2;4;5])) mat)
             -. (Plucker.Vert.eval_mat (Plucker.Vert.a ~i:(i+1) (Int.Set.of_list [1;2;6])) mat) *. (Plucker.Vert.eval_mat (Plucker.Vert.a ~i:(i+1) (Int.Set.of_list [3;4;5])) mat)
              | None -> invalid_arg ("Weight 2 vertex isn't in gr37" ^ PartVertex.to_string v)))
  | _ -> invalid_arg ("No weight about 2 vertices in in gr37" ^ PartVertex.to_string v)
