open Core

module Make ( V: Vertex.S )  (* : sig include Vertex.S with type t = V.t with module Set = V.Set with module Map = V.Map
                                   val value : t -> float
                                   val fvalue : t-> bool
                                   val init : Set.t -> unit end *)=
  struct
    include V
    let values = ref(V.Map.empty)
    let value v =
      match Map.find !values v with
      | Some value -> value
      | None -> invalid_arg ("Need to mutate to " ^ V.to_string v)
    let fvalue v = Map.mem !values v

    let init s = (values := V.Set.to_map s ~f:(fun _ -> Random.float 1.0))

    let mutate v ~inp ~out =
      let w = V.mutate v ~inp ~out
      in match Map.find !values w with
         | Some _ -> w
         | None ->
            if Map.for_alli inp ~f:(fun ~key ~data:_ -> Map.mem !values key) && Map.for_alli out ~f:(fun ~key ~data:_ -> Map.mem !values key)
            then
              let newval = ((Map.fold inp ~init:1.0
                               ~f:(fun ~key ~data acc -> Fn.apply_n_times ~n:(Int.abs data) ( ( *.) (value key)) acc))
                             +. (Map.fold out ~init:1.0
                                   ~f:(fun ~key ~data acc -> Fn.apply_n_times ~n:(Int.abs data) ( ( *.) (value key)) acc)))
                           /. (value v) in
              let () = values := (Map.add_exn !values ~key:w ~data:newval) in
              w
            else w



  end
