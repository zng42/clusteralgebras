open Core
let (|.) f g = fun x -> f (g x)
let mapreduce l ~(init:'b) ~map ~(f: 'b -> 'b -> 'b) = List.fold l ~init:init ~f:(fun acc -> fun x -> f acc (map x))

module N36Vert = NVertex.Make(struct let n = 6 let k = 3 let epsilon = 0.000000001 end)
module N36 = CAlgGenPlucker.Make(N36Vert)

module N48Vert = NVertex.Make(struct let n = 8 let k = 4 let epsilon = 0.000000001 end)
module N48 = CAlgGenPlucker.Make(N48Vert)

module NW48Vert = ProductVertex.MakeP (N48.Vert) (WeightVertex.Make(struct let max_weight = 9 end))
module NW48 = CAlgGenPlucker.Make(NW48Vert)


(*module PartCluster =
  struct
    include GeneralizePlucker.Make(PartVertex)
    let gr k n = of_pcluster n (Grassman.gr k n)
  end
module PartSearch = ClusterSearch.Make(PartCluster);;
module PartFrame = FrameSearch.Make(struct include PartCluster  end);;
module XPartVertex =
  struct
    include PartSearch.XVert
    let to_table k n x =
      PartVertex.div (List.fold (top_list x) ~init:(List.init k ~f:(fun _ -> List.init (n - k) ~f:(fun _ -> 0))) ~f:PartVertex.mult)
        (List.fold (bot_list x) ~init:(List.init k ~f:(fun _ -> List.init (n - k) ~f:(fun _ -> 0))) ~f:PartVertex.mult)
  end
module PartSymbol = Symbol.Make(XPartVertex)
 *)
(* #install_printer PartSymbol.ATensor.pp *)
(*
module XPartPairSet = Set.Make(struct type t = XPartVertex.t * XPartVertex.t [@@deriving compare, sexp] end)

module PartIndexedCluster = IndexedCluster.Make(PartCluster)
module PartModularGroup = ModularGroup.Make(PartIndexedCluster)
module PartIndexedSearch = PartModularGroup.ISearch
module PartFoldSearch = FoldSearch.Make(PartCluster)


module VPartVertex = ValuedVertex.Make(PartVertex)
module VPartCluster = MapCluster.Make(VPartVertex)
module VPartSearch = ClusterSearch.Make(VPartCluster)
*)
(*
module A2DoubleVertex = NumericVertex.Make(struct let epsilon = 0.0000000001 end);;
module A2DoubleCluster = MapCluster.Make(A2DoubleVertex);;
module A2DoubleSearch = ClusterSearch.Make(A2DoubleCluster);;
 *)
(*
module FixedFrame =
  struct
    let frame (c : FixedCluster.t) : (FixedCluster.t * int list list) =
      let frozen = List.map (Set.to_list (FixedCluster.unfrozen c)) ~f:(fun i ->  (-i, Int.Map.singleton i 1)) in
      let graph = List.filter_map (Map.to_alist (FixedCluster.quiver c))
                                  ~f:(fun (v, nbr) -> if Set.mem (FixedCluster.unfrozen c) v
                                                      then Some(v, FixedCluster.Vertex.Map.of_alist_exn
                                                               ((-v,-1)::List.filter (Map.to_alist nbr) ~f:(fun (w, i) -> Set.mem(FixedCluster.unfrozen c) w)))
                                                      else None) in
      let c =  FixedCluster.of_list ~graph:(List.append graph frozen) ~frozen:(Int.Set.map (FixedCluster.unfrozen c) ~f:(fun i -> -i)) in
      (c, FixedCluster.cVectors c)

    module Node = FrameSearch.FrameWithFrozen(FixedCluster)
    include SearchBase.Make(Node)
  end
 *)
(*
module FixedFoldSearch =
  struct
    module Node =
      struct
        module T =
          struct
            type t = FixedFrame.Node.t * int list list [@@deriving sexp]
            let compare (n1,_) (n2,_) = FixedFrame.Node.compare n1 n2
          end
        include T
        include Comparable.Make(T)

        let label (n,groups) = FixedFrame.Node.label n
        let id (n, groups) = FixedFrame.Node.id n

        type edge = int list [@@deriving sexp]
        let n ((c,f),groups) =
          List.map groups ~f:(fun path -> let newc = FixedCluster.mutate_multi c path in (((newc, FixedCluster.cVectors newc), groups), path))
      end
    include SearchBase.Make(Node)
  end
 *)
(* module FixedModularGroup = ModularGroup.Make(FixedCluster)
module FixedModularGroupSearch = FixedModularGroup.GroupSearch
 *)






(*
module FixedFrameA = FrameSearch.MakeNormalized(
                         struct
                           let normalize cVec =
                             let min = cvec_sign cVec * List.fold ~init:Int.max_value ~f:(fun m -> fun i -> Int.min (Int.abs i) m) cVec
                             in  List.map cVec (fun i -> i -  min)
                           let qAaffine m n =
                             let (m,n) = (Int.min m n, Int.max m n) in
                             let sources_sinks = List.map (List.range ~stop:`inclusive 1 (2*m - 1)) (fun i -> match i with
                                                                                                             | 1 -> (1, ([],[2;m+n]))
                                                                                                             | j-> if j mod 2 = 0
                                                                                                                   then (j, ([j-1;j+1],[]))
                                                                                                                   else (j, ([],[j-1;j+1])) ) in
                             let tail = List.map (List.range ~stop:`inclusive (2*m) (m+n)) (fun i -> if i = (m+n)
                                                                                                      then (i, ([i-1;1],[]))
                                                                                                      else (i, ([i-1], [i+1])) )
                             in FixedCluster.of_nbrlist ~frozen:Int.Set.empty ~graph:(List.append sources_sinks tail)


                         end)
module FixedFrameD =
  struct
    include FrameSearch.MakeNormalized(
                struct
                  let normalize cvec =
                    let sign_init = cvec_sign cvec in
                    let rec loop v =
                      let next = List.mapi ~f:(fun index -> fun i -> i - (if (List.length v - index) <= 4 then 1 else 2)*sign_init) v in
                      if sign_init = cvec_sign next
                      then loop next
                      else v
                    in
                    loop cvec
                end)
    let qDaffine n =
      let tails = [ (1, ([5],[])) ; (2, ([5],[]))
                    ; (3, if n mod 2 = 0 then ([n+1],[]) else ([],[n+1])) ; (4, if n mod 2 = 0 then ([n+1],[]) else ([],[n+1]))] in
      let path = List.map (List.range 6 (n+1)) (fun i -> (i, if i mod 2 = 0 then ([i-1;i+1],[]) else ([],[i-1;i+1]))) in
      let deg3 = if n = 4 then [(5, ([],[1;2;3;4]))] else [(5,([],[1;2;6])) ; (n+1, if n mod 2 = 0 then ([],[3;4;n]) else ([3;4;n],[]))]
      in
      FixedCluster.of_nbrlist ~frozen:Int.Set.empty ~graph:(List.append (List.append tails path) deg3)
  end
 *)
(*
module Gr36Cluster =
  struct
    include GeneralizePlucker.Make(Gr36Vertex)
    let qD4 = of_pcluster 6 (Grassman.gr 3 6)
  end;;
module Gr36Search = ClusterSearch.Make(Gr36Cluster);;
 *)
(*
module Gr37Cluster =
  struct
    include GeneralizePlucker.Make(Gr37Vertex)
    let qE6 = of_pcluster 7 (Grassman.gr 3 7)
  end;;
module Gr37Search = ClusterSearch.Make(Gr37Cluster);;
 *)

(*
module Gr38Cluster =
  struct
    include GeneralizePlucker.Make(Gr38Vertex)
    let qE8 = of_pcluster 8 (Grassman.gr 3 8)
  end;;
module Gr38Search = ClusterSearch.Make(Gr38Cluster);;
 *)

open Algebra
module IntPoly = Rings.IntPoly
module CorrolatorsIndex =
  struct
    module T =
      struct
        type t = (IntPoly.t * int) list [@@deriving sexp]
        let compare = List.compare (fun (p1,i1) (p2,i2) -> match Int.compare i1 i2 with 0 -> IntPoly.compare p1 p2 | c -> -c)
      end
    include T
    include Comparable.Make(T)
    let to_string cor =
      let (p,l)=List.unzip cor
      in "{" ^ String.concat ~sep:"," (List.map ~f:IntPoly.to_string p) ^ "}_" ^ "[" ^ String.concat ~sep:"," (List.map ~f:Int.to_string l) ^ "]"
    let of_string _(*s*) = invalid_arg "NYI"
  end
module Corrolators = FreeRMod.Make (CorrolatorsIndex) (Rings.Int)
module CorrolatorsForm = MapForms.Make(Corrolators)
let oneMinus p = IntPoly.add IntPoly.one (IntPoly.neg p)

let del cor =
  match cor with
  | [] -> CorrolatorsForm.zero
  | [(_,1)] -> CorrolatorsForm.zero
  | [(p,2)] -> CorrolatorsForm.primitive 1 [[(oneMinus p,1)]; [(p,1)]]
  | [(p,n)] -> CorrolatorsForm.primitive 1 [[(p,n-1)];[(p,1)]]
  | [(x,1);(y,1)] -> List.fold ~init:CorrolatorsForm.zero ~f:CorrolatorsForm.add
                       [CorrolatorsForm.primitive 1 [[(x,1)];[(oneMinus (IntPoly.mult x y),1)]]
                       ;CorrolatorsForm.primitive 1 [[(oneMinus y,1)];[(oneMinus (IntPoly.mult x y),1)]]
                       ;CorrolatorsForm.primitive 1 [[(oneMinus (IntPoly.mult x y),1)];[(oneMinus x,1)]]]
  | [(x,2);(y,1)] -> List.fold ~init:CorrolatorsForm.zero ~f:CorrolatorsForm.add
                       [CorrolatorsForm.primitive 1 [[(x,1);(y,1)]; [(x,1)]]
                       ;CorrolatorsForm.primitive (-1) [[(x,2)]; [(oneMinus (IntPoly.mult x y),1)]]
                       ;CorrolatorsForm.primitive 1 [[(x,2)]; [(oneMinus y,1)]]
                       ;CorrolatorsForm.primitive (-1) [[(y,2)]; [(oneMinus (IntPoly.mult x y),1)]]
                       ;CorrolatorsForm.primitive (-1) [[(IntPoly.mult x y, 2)]; [(oneMinus y, 1)]] ]
  | _ -> invalid_arg "NYI"

(* Non Commutative Cluster Algebras *)
module FloatTRing =
  struct
    include Rings.Float
    let id v = to_string (Float.round_decimal ~decimal_digits:8 v)
    let transpose = Fn.id
  end

module FloatNonCom = NonComCluster.Make(FloatTRing)
module FloatNonComSearch = SearchBase.Make(FloatNonCom)
module LacamlMat2 = Gl.Make(LacamlMatRing) (struct let dim = 2 end)
module MatCluster = NonComCluster.Make(LacamlMat2)
module MatSearch =SearchBase.Make(MatCluster)

module PolyMat = Rings.PolyMat
module PolyMat2 = Gl.Make(PolyMat) (struct let dim = 2 end)
module PolyMatCluster = NonComCluster.Make(PolyMat2)
module PolyMatSearch = SearchBase.Make(PolyMatCluster)
let cA1poly =
  PolyMatCluster.qA1_double (PolyMat.of_string "{{1, 1*t},{0, 1}}")
    (PolyMat.of_string "{{1, 0},{0, 1}}")
    (PolyMat.of_string "{{1, (0-1*t)},{0, 1}}")
