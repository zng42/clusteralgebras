open Core

module Base(V:Vertex.S) =
  struct
    module Vert = V
    module Clust = MapCluster.Make(V)
  end


module Extend(Base: sig module Vert:Vertex.S module Clust: Cluster.S end) =
  struct
    open Base
    module CSearch = ClusterSearch.Make(Clust)
    module XVert = CSearch.XVert

    module Frame = FrameSearch.Make(Clust)
    module Symbol = Symbol.Make(XVert)

    module XPairSet = Set.Make(struct type t = XVert.t*XVert.t [@@deriving compare,sexp] end)

    module IClust = IndexedCluster.Make(Clust)
    module ModG = ModularGroup.Make(IClust)
    module GroupSearch = ModG.GroupSearch
    module ISearch = ModG.ISearch
    module FoldSearch = FoldSearch.Make(Clust)

    module ValuedVert = ValuedVertex.Make(Vert)
    module ValuedClust = MapCluster.Make(ValuedVert)
    module ValuedSearch = ClusterSearch.Make(ValuedClust)
  end
module ExtendIndexed(Base:sig module Vert:Vertex.S module Clust : IndexedCluster.S end) =
  struct
    open Base
    module CSearch = ClusterSearch.Make(Clust)
    module XVert = CSearch.XVert

    module Frame = FrameSearch.FrameWithFrozen(Base.Clust)
    module Symbol = Symbol.Make(XVert)

    module XPairSet = Set.Make(struct type t = XVert.t*XVert.t [@@deriving compare,sexp] end)

    module IClust = Clust
    module ModG = ModularGroup.Make(IClust)
    module GroupSearch = ModG.GroupSearch
    module ISearch = ModG.ISearch
    module FoldSearch = FoldSearch.Make(Clust)

    module ValuedVert = ValuedVertex.Make(Vert)
    module ValuedClust = MapCluster.Make(ValuedVert)
    module ValuedSearch = ClusterSearch.Make(ValuedClust)


  end

module Make(V:Vertex.S) =
  struct
    module Vert = V
    module Clust = MapCluster.Make(V)
    include Extend(struct module Vert = Vert module Clust = Clust end)
  end

module MakeFrameWithFrozen(V: Vertex.S) =
  struct
    include Make(V)
    module Frame = FrameSearch.FrameWithFrozen(Clust)
  end
