(*open Core
open Algebra

type 'a l2 = L2 of 'a [@@ deriving compare,sexp]
type 'a l3 = L3 of 'a | L21 of 'a * 'a [@@ deriving compare, sexp]
type 'a l4 = L4 of 'a | L31 of 'a * 'a [@@ deriving compare, sexp]
type 'a l5 = L5 of 'a | L41 of 'a * 'a | L32 of 'a * 'a | L311 of 'a * 'a * 'a [@@ deriving compare, sexp]

type 'a lform = W of (int*'a) list [@@deriving compare,sexp]

module MakeLmod (CSearch:ClusterSearch.S) =
  FreeRMod.Make(
      struct
        module T =
          struct
            type t = CSearch.XVert.t lform [@@deriving compare,sexp]
          end
        include T

        let to_string  = function
          | W(arg) ->
             let (ns,zs) = List.unzip arg in
             "w[" ^ String.concat ~sep:"," (List.map ~f:Int.to_string ns) ^ "][" ^ String.concat ~sep:"," (List.map ~f:CSearch.XVert.to_string zs)^"]"
        let of_string s =
          match String.chop_prefix s ~prefix:"w" with
          | None -> invalid_arg ("Need to start with w: " ^ s)
          | Some s -> (match s |> String.split_on_chars ~on:['[';']'] |> List.filter ~f:(fun s-> Int.(String.length s > 0)) with
                       | [nstring;zstring] -> W(List.zip_exn (nstring |> String.split ~on:',' |> List.map ~f:Int.of_string)
                                                  (zstring |> String.split ~on:',' |> List.map ~f:CSearch.XVert.of_string))
                       |  _ -> invalid_arg ("Arguments should be two lists seperated by [] not" ^ s)
                      )
        include Comparable.Make(T)
      end)
    (Rings.Int)

module PFormMod = MakeLmod(Plucker.CSearch)
module PLogRel = Make(struct module CSearch = Plucker.CSearch module Lmod = PFormMod end)
 *)
