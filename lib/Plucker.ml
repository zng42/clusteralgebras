open Core
open Algebra

 module Vert = PluckerVert
 module Clust = MapCluster.Make(Vert)
 include CAlg.Extend(struct module Vert=Vert module Clust = Clust end)
 type loc = Begin | Middle | End
 let loc_of_index ~(length : int) ~(index : int) =
   if index = 1 then Begin
   else if index = length then End
   else Middle

 let frozen n k = Vert.Set.of_list
                    (List.map (List.range 1 (n+1))
                       ~f:(fun i -> Int.Set.of_list(List.map (List.range i (i+k)) ~f:(fun x -> match x mod n with 0 -> n | i -> i))))


 let gr (k:int) (n:int) : Clust.t =
   let l = n - k in
   let range = List.range ~stop:`inclusive in
   let f (i, j) : Int.Set.t =
     if i <= l - j + 1
     then Int.Set.of_list (List.append (range (i + 1) k) (range (k + j) (k + j + i - 1)))
     else Int.Set.of_list (List.append (List.append (range 1 (i - (l - j + 1)) ) (range (i + 1) k)) (range (k + j) (k + l))) in
   let extra = Int.Set.of_list (range 1 k) in
   let grid = List.concat_map ~f:(fun i -> List.map ~f:(fun j -> (i,j)) (List.rev (range 1 l))) (range 1 k) in
   let edge (i,j)   =
     match l with
       1 -> (*In this case there is only one column *)
        (match (loc_of_index ~index:i ~length:k) with
         | Begin -> ( [extra] ,   [f(i+1,j)])
         | Middle -> ( [f(i-1,j)] ,  [f(i+1,j)])
         | End -> ( [f(i-1,j)] ,   [extra]))
     | _  ->
        (match (loc_of_index ~index:i ~length:k, loc_of_index ~index:(l+1 - j) ~length:l) with
         | (Begin, Begin) -> ( [f(i+1, j-1); extra]
                             ,  [f(i+1,j); f(i, j-1)] )
         | (Begin, Middle) -> (  [f(i+1, j-1); f(i, j+1)]
                              ,  [f(i+1,j); f(i,j-1)])
         | (Begin, End) -> ( [f(i,j+1)] ,  [f(i+1,j)])
         | (Middle,Begin) -> (  [f(i-1,j); f(i+1,j-1)]
                             ,  [f(i+1,j); f(i, j-1)])
         | (Middle, Middle) -> (  [f(i-1,j); f(i, j+1); f(i+1,j-1)]
                               ,  [f(i+1,j) ; f(i, j-1) ; f(i-1, j+1)])
         | (Middle, End) -> (  [f(i-1,j); f(i, j+1)]
                            ,  [f(i+1, j) ; f(i-1, j+1)])
         | (End, Begin) -> (  [f(i-1,j)] ,  [extra])
         | (End, Middle) | (End, End) -> (  [f(i-1,j)]
                                         ,  [f(i-1,j+1)]))
   in
   Clust.of_nbrlist
     ~graph:( (extra, ([f (k,l)] , [f(1,l)])) :: (List.map ~f:(fun (i,j) -> (f(i,j) , edge(i,j))) grid))
     ~frozen:(frozen n k)


 let collapse_path (start : Clust.t) (path : Vert.t list) =
   let rec collapse_path' (start : Clust.t)
             (path : Vert.t list)
             ((s,acc) : Vert.Set.t * Vert.Set.t list) : Vert.Set.t list =
     match (path, acc) with
       ([], _) -> acc
     | (v :: vs, []) ->
        let (cnew,vnew) = Clust.mutate_name start v in
        collapse_path' cnew vs (PluckerVert.Set.singleton vnew,  [PluckerVert.Set.singleton v])
     | (v :: vs, path_s :: rest) ->
        let nbrs = Set.union (Clust.iNbrSet start v) (Clust.oNbrSet start v) in
        (*let () = print_endline (Sexp.to_string (Plucker.Set.sexp_of_t nbrs)) in*)
        let (cnew, vnew) = Clust.mutate_name start v in
        if Set.is_empty (Set.inter nbrs s)
        then collapse_path' cnew vs (PluckerVert.Set.add s vnew , (PluckerVert.Set.add path_s v) :: rest)
        else collapse_path' cnew vs (PluckerVert.Set.singleton vnew, (PluckerVert.Set.singleton v) :: path_s :: rest)
   in List.rev(collapse_path' start path (PluckerVert.Set.empty, []))



 let boundaries_cluster k n graph =
   let grkn_1 = gr k (n-1) in
   let grkn_1s = List.map (List.range 1 (n+1)) ~f:(fun i -> Map.keys (CSearch.graph (Clust.map grkn_1 ~f:(PluckerVert.a ~i:i)))) in
   let found grkn i grkn_1s = if List.exists ~f:(fun grkn_1 -> Clust.is_subset grkn_1 ~of_:grkn) grkn_1s then Some (i+1) else None in
   Map.mapi graph ~f:(fun ~key:grkn ~data:_ -> List.filter_mapi grkn_1s  ~f:(found grkn))


 module F = MapPolynomial.Make (PluckerVert) (Rings.Int)
 module WF = MapForms.Make(F)
 let w c =
   List.fold ~init:WF.zero ~f:WF.add
     (List.concat_map (Set.to_list (Clust.verts c))
        ~f:(fun a -> List.map (Set.to_list (Clust.oNbrSet c a))
                       ~f:(fun b -> WF.wedge (WF.primitive 1 [[(a,1)]]) (WF.primitive 1 [[(b,1)]]))))

 let sum_form i l = List.fold ~init:WF.zero ~f:WF.add (List.map ~f:(fun p -> WF.primitive i [[(p,1)]]) l);;
 let x_to_L1 x = WF.add (sum_form 1 (XVert.top_list x)) (sum_form (-1) (XVert.bot_list x));;

 let l44 c26 = WF.wedge (w c26) (w c26)

 let l34 (c, v) =
   let x = CSearch.xcoord c v in
   ( WF.add
       (sum_form 1 (XVert.top_list x))
       (sum_form (-1) (XVert.bot_list x))
   , WF.add (w c) (w (Clust.mutate c v)))

 let delOl34 (c, v) =
   let x = CSearch.xcoord c v in
   let v2 = PluckerVert.mutate v ~inp:(Clust.iNbr c v) ~out:(Clust.oNbr c v) in
   WF.wedge
     (WF.wedge (WF.add (sum_form 1 (XVert.top_list x)) (sum_form (-1) (XVert.bot_list x)))
        (WF.add (sum_form 1 [v; v2]) (sum_form (-1) (XVert.bot_list x))))
     (WF.add (w c) (w (Clust.mutate c v)))
 let l44Od (c,v) = WF.add (l44 c) (WF.scale (-1) (l44 (Clust.mutate c v)))
