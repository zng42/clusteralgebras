open Core
module Perm = Algebra.Permutation.MapPermutation

(* If findi2 f l = ((i,j),x) then f(nth2_exn l (i,j)) = true *)
let findi2 l ~f = List.find_mapi ~f:(fun i l -> Option.map ~f:(fun (j,x) -> ((i,j),x)) (List.findi ~f:(fun _ -> f) l)) l
let nth2_exn l (i,j) = List.nth_exn (List.nth_exn l i) j
let sup (i,j) v = PartVertex.a ~i:(i+1 + j+1) (PartVertex.a ~i:(i+1) v)
let sup_plucker (i,j) v = Plucker.Vert.a ~i:(i+1 + j+1) (Plucker.Vert.a ~i:(i+1) (Int.Set.of_list v))

include PartVertex
let exoticX = List.init 7 ~f:(fun i -> List.init (7-i) ~f:(fun j -> sup (i,j) [[2;1;1];[1;1;1];[1;1;0]]))
let exoticY = List.init 7 ~f:(fun i -> List.init (7-i) ~f:(fun j -> sup (i,j) [[2;2;1];[2;1;0];[1;0;0]]))
let exoticA = [ [[3;3;3;2;2];[3;2;1;1;1];[2;1;1;1;0]] ; [[3;3;2;2;1];[2;2;2;2;1];[2;2;1;0;0]] ; [[3;3;3;2;1];[3;3;2;1;0];[2;1;0;0;0]]
                ; [[3;3;2;1;1];[3;2;1;1;1];[2;1;1;1;0]] ; [[3;2;2;2;1];[2;2;2;1;0];[2;2;1;0;0]] ; [[3;3;3;2;1];[3;2;1;0;0];[2;1;0;0;0]]
                ; [[3;3;2;1;1];[2;1;1;1;1];[2;1;1;0;0]] ; [[3;2;2;2;1];[2;2;2;1;0];[1;1;0;0;0]] ]

let exoticB = [ [[3;3;3;2;1];[3;2;1;1;1];[2;1;1;1;0]] ; [[3;3;2;2;1];[2;2;2;2;0];[2;2;1;0;0]] ; [[3;3;3;2;1];[3;3;2;0;0];[2;1;0;0;0]]
                ; [[3;3;2;1;1];[3;2;1;1;1];[2;1;1;0;0]] ; [[3;2;2;2;1];[2;2;2;1;0];[2;2;0;0;0]] ; [[3;3;3;2;1];[3;2;1;0;0];[2;0;0;0;0]]
                ; [[3;3;2;1;1;];[2;1;1;1;1];[1;1;1;0;0]] ; [[3;3;3;3;2];[3;3;2;1;0];[2;1;0;0;0]] ]
let exoticBflip =
  [ [[3;3;3;2;1];[3;2;1;0;0];[1;0;0;0;0]];
    [[3;2;2;2;1];[2;2;2;1;0];[2;1;0;0;0]];
    [[3;3;2;1;1];[3;1;1;1;1];[2;1;1;0;0]];
    [[3;3;3;2;1];[3;3;1;0;0];[2;1;0;0;0]];
    [[3;3;2;2;1];[2;2;2;1;0];[2;2;1;0;0]];
    [[3;3;3;1;1];[3;2;1;1;1];[2;1;1;1;0]];
    [[3;3;3;3;1];[3;3;2;1;0];[2;1;0;0;0]];
    [[3;3;2;2;2];[2;2;2;2;1];[2;2;1;0;0]]]

let to_string v =
  match PartVertex.weight v with
  | 1 -> Plucker.Vert.to_string (to_plucker 3 8 v)
  | 2 -> (match findi2 exoticX ~f:(PartVertex.equal v) with
          | Some ((i,j),_)  -> "e2x" ^ Int.to_string (i+1) ^ Int.to_string (i+1 + j+1)
          | None ->
             (match findi2 exoticY ~f:(PartVertex.equal v) with
              | Some ((i,j),_) -> "e2y" ^ Int.to_string (i+1) ^ Int.to_string (i+1 + j+1)
              | None -> invalid_arg ("Weight 2 vertex isn't in gr38: " ^ PartVertex.to_string v)))
  | 3 -> (match List.findi exoticA ~f:(fun _ -> PartVertex.equal v) with
         | Some (i,_) -> "e3Ar" ^ Int.to_string (i)
         | None -> (match List.findi exoticB ~f:(fun _ -> PartVertex.equal v) with
                    | Some (i,_) -> "e3Br" ^ Int.to_string (i)
                    | None -> (match List.findi exoticBflip ~f:(fun _ -> PartVertex.equal v) with
                               | Some (i,_) -> "e3Bsr" ^ Int.to_string (i)
                               | None -> invalid_arg ("This weight 3 vertex isn't in gr38: " ^ PartVertex.to_string v))))
  | _ -> invalid_arg ("No vertices above weight 3 in in gr38: " ^ PartVertex.to_string v)
let pp ppf v = Format.fprintf ppf "%s" (to_string v)
let to_string_short = to_string


let of_string s =
  match String.to_list s with
  | ['p';i;j;k] -> PartVertex.of_plucker 8 (Int.Set.of_list (List.map [i;j;k] ~f:(fun c -> Int.of_string(Char.to_string c))))
  | ['e';'2';'x';si;sj] -> let (i,j) = (Int.of_string (Char.to_string si),Int.of_string(Char.to_string sj)) in nth2_exn exoticX (i-1, j-i-1)
  | ['e';'2';'y';si;sj] -> let (i,j) = (Int.of_string (Char.to_string si),Int.of_string(Char.to_string sj)) in nth2_exn exoticY (i-1, j-i-1)
  | ['e';'3';'A';'r';si] -> List.nth_exn exoticA (Int.of_string(Char.to_string si))
  | ['e';'3';'B';'r';si] -> List.nth_exn exoticB (Int.of_string(Char.to_string si))
  | ['e';'3';'B';'s';'r';si] -> List.nth_exn exoticBflip (Int.of_string(Char.to_string si))
  | _ -> invalid_arg (s ^ " has unknown prefix")

let convert_zickert s =
  match String.to_list s with
  | 'a'::rest -> String.of_char_list ('p' :: rest)
  | 'x'::rest -> String.of_char_list ('e'::'2'::'x':: rest)
  | 'y'::rest -> String.of_char_list ('e'::'2'::'y':: rest)
  | ['A';si] -> String.of_char_list ('e'::'3'::'A'::'r'::[]) ^ (Int.to_string (Int.of_string (Char.to_string si) -1))
  | ['B';'9'] -> "e3Bsr1"
  | ['B';si] ->  "e3Br"^(Int.to_string (Int.of_string (Char.to_string si) -1))
  | ['B';'1';si] -> "e3Bsr" ^ Int.to_string((2 + Int.of_string(Char.to_string si)) mod 8)
  | _ -> invalid_arg (s ^ " has unknown prefix")

let rotate v =
  match PartVertex.weight v with
  | 1 -> PartVertex.of_plucker 8 (Plucker.Vert.rotate 8 (PartVertex.to_plucker 3 8 v))
  | 2 -> (match findi2 exoticX ~f:(PartVertex.equal v) with
          | Some ((i,j), _) ->
             if Int.equal (i+1 +j+1) 8
             then nth2_exn exoticX (0,i)
             else nth2_exn exoticY (i+1, j)
          | None ->
             (match findi2 exoticY ~f:(PartVertex.equal v) with
              | Some ((i,j),_) ->
                 if Int.equal (i+1 + j+1) 8
                 then nth2_exn exoticY (0,i)
                 else nth2_exn exoticX (i+1,j)
              | None -> invalid_arg ("Can't rotate unknown weight 2 coordinate " ^ (PartVertex.to_string v)))
         )
  | 3 -> (match List.findi exoticA ~f:(fun _ -> PartVertex.equal v) with
          | Some (i,_) -> List.nth_exn exoticA ((i+1) mod 8)
          | None ->
             (match List.findi exoticB ~f:(fun _ -> PartVertex.equal v) with
              | Some (i,_) -> List.nth_exn exoticB ((i+1) mod 8)
              | None ->
                 (match List.findi exoticBflip ~f:(fun _ -> PartVertex.equal v) with
                  | Some (i,_) -> List.nth_exn exoticBflip (if Int.((i-1) < 0) then 7 else i-1)
                  | None ->invalid_arg ("Can't rotate unknown weight 3 coordinate " ^ PartVertex.to_string v))))
  | _ -> invalid_arg ("No weight higher then 3 vertices in gr38 " ^ PartVertex.to_string v)

let flip8 i = Perm.apply (Perm.of_cycles [[1;8];[2;7];[3;6];[4;5]]) i

let flip v =
  match PartVertex.weight v with
  | 1 -> PartVertex.of_plucker 8 (Int.Set.map ~f:flip8 (PartVertex.to_plucker 3 8 v))
  | 2 -> (match findi2 exoticX ~f:(PartVertex.equal v) with
          | Some ((i,j), _) ->
             let (inew,jnew) = (flip8 (i+1), flip8 (i+1+ j+1)) in
             nth2_exn exoticX (Int.min inew jnew -1, Int.max inew jnew - Int.min inew jnew -1)
          | None ->
             (match findi2 exoticY ~f:(PartVertex.equal v) with
              | Some ((i,j),_) ->
                 let (inew,jnew) = (flip8 (i+1), flip8 (i+1+ j+1)) in
                 nth2_exn exoticY (Int.min inew jnew -1, Int.max inew jnew - Int.min inew jnew -1)
              | None -> invalid_arg ("Unknown weight 2 coordinate " ^ PartVertex.to_string v))
         )
  | 3 -> (match List.findi exoticA ~f:(fun _ -> PartVertex.equal v) with
          | Some (i,_) -> List.nth_exn exoticA (flip8 (i+1) -1)
          | None ->
             (match List.findi exoticB ~f:(fun _ -> PartVertex.equal v) with
              | Some (i,_) -> List.nth_exn exoticBflip i
              | None ->
                 (match List.findi exoticBflip ~f:(fun _ -> PartVertex.equal v) with
                  | Some (i,_) -> List.nth_exn exoticB i
                  | None ->invalid_arg ("Can't rotate unknown weight 3 coordinate " ^ PartVertex.to_string v))))
  | _ -> invalid_arg ("No weight higher then 3 vertices in gr38 " ^ PartVertex.to_string v)




let eval_mat v mat =
  match PartVertex.weight v with
  | 1 -> Plucker.Vert.eval_mat (to_plucker 3 8 v) mat
  | 2 -> (match findi2 exoticX ~f:(PartVertex.equal v) with
          | Some ((i,j),_)  ->
             (Plucker.Vert.eval_mat (sup_plucker (i,j) [1;3;4]) mat) *. (Plucker.Vert.eval_mat (sup_plucker (i,j) [2;5;6]) mat)
             -. (Plucker.Vert.eval_mat (sup_plucker (i,j) [1;5;6]) mat) *. (Plucker.Vert.eval_mat (sup_plucker (i,j) [2;3;4]) mat)
          | None ->
             (match findi2 exoticY ~f:(PartVertex.equal v) with
              | Some ((i,j),_) ->
                 (Plucker.Vert.eval_mat (sup_plucker (i,j) [1;3;6]) mat) *. (Plucker.Vert.eval_mat (sup_plucker (i,j) [2;4;5]) mat)
                 -. (Plucker.Vert.eval_mat (sup_plucker (i,j) [1;2;6]) mat) *. (Plucker.Vert.eval_mat (sup_plucker (i,j) [3;4;5]) mat)
              | None -> invalid_arg ("Weight 2 vertex isn't in gr38: " ^ PartVertex.to_string v)))
  | 3 -> (match List.findi exoticA ~f:(fun _ -> PartVertex.equal v) with
          | Some (i,_) -> let p l = Plucker.Vert.eval_mat (Fn.apply_n_times ~n:i (Plucker.Vert.rotate 8) (Plucker.Vert.of_list l)) mat in
                          p [1;3;4] *. (p[2;5;8] *. p[1;6;7] -. p[6;7;8] *. p[1;2;5]) -. p[1;5;8]*.p[2;3;4]*.p[1;6;7]
          | None ->
             (match List.findi exoticB ~f:(fun _ -> PartVertex.equal v) with
              | Some (i,_) ->
                 let p l = Plucker.Vert.eval_mat (Fn.apply_n_times ~n:i (Plucker.Vert.rotate 8) (Plucker.Vert.of_list l)) mat in
                 p[2;5;8] *. p[1;3;4]*.p[2;6;7] -. p[2;3;4]*.(p[1;2;8]*.p[5;6;7] +. p[2;5;8]*.p[1;6;7])
              | None ->
                 (match List.findi exoticBflip ~f:(fun _ -> PartVertex.equal v) with
                  | Some (i,_) ->
                     let p l = Plucker.Vert.eval_mat (Fn.apply_n_times ~n:i (Plucker.Vert.rotate 8) (Int.Set.map ~f:flip8 (Plucker.Vert.of_list l))) mat in
                     p[2;5;8] *. p[1;3;4]*.p[2;6;7] -. p[2;3;4]*.(p[1;2;8]*.p[5;6;7] +. p[2;5;8]*.p[1;6;7])
                  | None -> invalid_arg ("This weight 3 vertex isn't in gr38: " ^ PartVertex.to_string v))))
  | _ -> invalid_arg ("No weight above 3 vertices in in gr38" ^ PartVertex.to_string v)
