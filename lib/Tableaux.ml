(*
open Core

let map2_default x l1 l2 ~f:f =
  let (n) = Int.max (List.length l1) (List.length l2) in
  List.map2_exn (l1 @ List.init (n - List.length l1) ~f:(fun _ -> x))
    (l2 @ List.init (n- List.length l2) ~f:(fun _ -> x)) ~f:f

let rec ragged_transpose cols =
  cols |>  List.map ~f:(fun l -> List.split_n l 1)
  |> List.unzip
  |> (fun (row,rest) -> (List.concat row) :: (if List.for_all rest ~f:(List.is_empty) then [] else ragged_transpose rest))

module PartOrd =
  struct
    type t =
      LESS
    | EQUAL
    | GREAT
    | INCOMP [@@deriving sexp,compare]
  end
module Parition =
  struct
    type t= int list [@@deriving sexp]
    let partsum p = List.folding_map ~init:0 ~f:(fun acc x -> (acc+x,acc+x))
    let dominate p1 p2 =
      in match List.map2default 0 (partsum p1) (partsum p2) ~f:(Int.compare) |> List.dedup_and_sort ~compare:Int.compare with
         | [] -> PartOrd.EQUAL
         | [1] -> PartOrd.GREAT
         | [-1]-> PartOrd.LESS
         | [0] -> PartOrd.EQUAL
         | _ -> PartOrd.INCOMP
    let compare = List.compare Int.compare
      
  end

module Tabl =
  struct

    module T =
      struct
        type t = int list list [@@deriving sexp]

        let shape t = t |> ragged_transpose |> List.map ~f:List.length
        let restrict i t = List.map ~f:(List.filter ~f:(fun j -> j <= i))
        let maxelt t = t |> List.map ~f:(List.last_exn) |> List.fold ~init:0 ~f:(Int.max) 
        let compare t1 t2 =
          let m = Int.max (maxelt t1) (maxelt t2) in
          let subords = List.init m ~f:(fun i -> Partition.dominate (t1 |> restrict (i+1) |> shape) (t2 |> restrict (i+1) |> shape))
                        |> List.dedup_and_sort ~compare:PartOrd.compare in
          match subords with
          | [] -> 0
          | [PartOrd.LESS] -> -1
          | [PartOrd.EQUAL] -> 0
          | [PartOrd.GREAT]-> 1
          | _ -> invalid_arg "incomparable tableaux"
          
      end
    include(T)
    include (Comparable.Make(T))

    let is_trivial t = 
    let reduce t = 
      match t with
      | [] -> []
      | c::cs -> c |> List.group ~break:(fun i j -> not(Int.(i+1=j))) |> 
    let add = max
    let mult t1 t2 = map2_default [] t1 t2 ~f:(List.append) |> List.map ~f:(List.sort ~compare:Int.compare)


  end
  *)
