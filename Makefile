SOURCES = Vertex.ml Plucker.ml MapCluster.ml Cluster.ml XCoord.ml Search.ml ClusterSearch.ml

main:
	corebuild $(SOURCES) foo.native
clean:
	corebuild -clean
	rm *~
