open Core

module Make(R: Ring.S) =
  struct

    module T =
      struct
        type t = R.t * R.t [@@deriving sexp,compare]
        let to_string (x,y) =
          match R.equal x R.zero , R.equal y R.zero with
          | (true,true) -> "0"
          | (true,false) -> R.to_string y ^ "I"
          | (false,true) -> R.to_string x
          | (false,false) -> R.to_string x ^ "+" ^ R.to_string y ^ "I"
      end

    include T
    include Comparable.Make(T)

    let pp ppf x = Format.fprintf ppf "%s" (to_string x)

    let zero = (R.zero,R.zero)
    let one = (R.one,R.zero)
    let i = (R.zero,R.one)
    let add (x1,y1) (x2,y2) = (R.add x1 x2, R.add y1 y2)
    let neg (x,y) = (R.neg x, R.neg y)
    let mult (x1,y1) (x2,y2) = R.(sub (mult x1 x2) (mult y1 y2), add (mult x1 y2) (mult x2 y1))

    let norm2 (x,y) = R.(add (mult x x) (mult y y))
    
    let inv (x,y) =
      match R.inv( norm2 (x,y)) with
        None -> None
      | Some ninv -> Some (R.mult ninv x, R.mult ninv (R.neg y))

    let inv_exn z =
      match inv z with
      | Some i -> i
      | None -> invalid_arg ("No inverse for " ^ to_string z)

    let sub (x1,y1) (x2,y2) = (R.sub x1 x2, R.sub y1 y2)
    let pow i n =
      match Ordering.of_int (Int.compare 0 n) with
      | Greater -> Fn.apply_n_times ~n:Int.(n-1) (mult i) i
      | Equal -> one
      | Less ->
         (match inv i with
          | Some j -> Fn.apply_n_times ~n:Int.(abs n - 1) (mult j) j
          | None -> invalid_arg ("Can't take negative power of noninvertable elment " ^ to_string i))

    let conj (x,y) = (x,R.neg y)
    let re (x,_) = x
    let im (_,y) = y
    let of_re x = (x,R.zero)
    let of_im y = (R.zero,y)
    let is_re (_,y) = R.equal y R.zero
    let is_re_pos (x,y) = is_re (x,y) && R.(x > R.zero)
    let is_re_neg (x,y) = is_re (x,y) && R.(x < R.zero) 
    let is_im (x,_) = R.equal x R.zero
    let is_im_pos (x,y) = is_im (x,y) && R.(y > R.zero)
    let is_im_neg (x,y) = is_im (x,y) && R.(y < R.zero)

    
    
    let of_string s =
      let parse_term a =
        match String.chop_suffix ~suffix:"I" (String.strip a) with
        | Some y -> if Int.(String.length y = 0) then i
                    else if String.(y = "-") then neg i
                    else of_im (R.of_string y)
        | None -> of_re(R.of_string a) in
      s |> String.split ~on:'+' |> List.map ~f:parse_term |> List.fold ~init:zero ~f:add

    let (+) = add
    let ( * ) = mult
    let (-) = sub
    
  end


    
        
