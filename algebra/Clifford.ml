open Core


module type ArgS =
  sig
    module VIndex: Index.S
    val vars : string list
    val vecs : VIndex.t list
    val norms : (VIndex.t*VIndex.t*int) list
    val frozen : VIndex.t list
  end


module Make (Arg: ArgS) =
  struct
    module VIndex = Arg.VIndex
    let vecs = Arg.vecs
    let default_frozen = VIndex.Set.of_list Arg.frozen
    let defaultName v w =
      if VIndex.equal v w
      then "Q"^ VIndex.to_string v
      else "B" ^ VIndex.to_string (VIndex.min v w) ^ "_" ^ VIndex.to_string (VIndex.max v w)

    let normMap = Arg.norms |> List.concat_map ~f:(fun (v,w,q) -> if VIndex.equal v w then [(v,(w,q))] else [(v,(w,q));(w,(v,q))])
                  |> VIndex.Map.of_alist_multi |> VIndex.Map.map ~f:VIndex.Map.of_alist_exn
    let partialQ v w = VIndex.Map.find normMap v |> Option.bind ~f:(fun lookUp -> VIndex.Map.find lookUp w)
    let normNames = (List.zip_exn vecs vecs) @ (vecs |> List.concat_mapi ~f:(fun i v -> List.drop vecs (i+1) |> List.map ~f:(fun w -> (v,w))))
                    |> List.filter_map ~f:(fun (v,w) -> match partialQ v w with
                                                        | Some _ -> None
                                                        | None -> Some (defaultName v w))
    module BaseR = Flint.BigIntMPoly.Make(struct let vars = Arg.vars @ normNames let ord =Flint.BigIntMPoly.Ord.LEX end)
    module R = Rationals.Make(BaseR)

    let inner v w =
      match partialQ v w with
      | Some r -> r |> Flint.BigInt.of_int |> BaseR.const |> R.of_r
      | None -> R.of_string(defaultName v w)
    let q v = inner v v

    type vec = VIndex.t

    module Index : Index.S with type t = VIndex.t list =
      struct
        module T =
          struct
            type t=VIndex.t list [@@deriving compare,sexp]
          end
        include T
        let to_string s =
          s |> List.map ~f:(fun x -> VIndex.to_string x)
          |>  String.concat ~sep:"**"

        let of_string s =
          s |> Str.split (Str.regexp_string "**")
          |> List.map ~f:String.strip
          |> List.map ~f:VIndex.of_string

        include Comparable.Make(T)
      end


    let rec clearAdj r l =
      match l with
      | [] -> (r,[])
      | [v] -> (r,[v])
      | u::v::vs -> if VIndex.equal u v
                    then clearAdj (R.mult r (q u))  vs
                    else (match clearAdj r (v::vs) with
                          | (rnew,[]) -> (rnew,[u])
                          | (rnew,vnew::vsnew) -> if VIndex.equal u vnew
                                                  then (R.mult rnew (q u), vsnew)
                                                  else (rnew, u::vnew::vsnew))


    let standardize ?frozen:(frozen=default_frozen) r l =
      let rec loop r l =
        match l with
        | [] -> [[],r]
        | [v] -> [[v],r]
        | v1::v2::vs ->
             (match Ordering.of_int (VIndex.compare v1 v2) with
              | Less -> loop r (v2::vs) |> List.concat_map ~f:(prepend v1)
              | Equal -> loop (R.mult r (q v1)) vs
              | Greater ->
                 if VIndex.Set.mem frozen v1 || VIndex.Set.mem frozen v2
                 then loop r (v2::vs) |> List.concat_map ~f:(prepend v1)
                 else (loop (R.mult (R.add r r) (inner v1 v2)) vs)  @ (loop (R.neg r) (v1::vs) |> List.concat_map ~f:(prepend v2) ))
      and prepend v1 (m,s) =
        match m with
        | [] -> [([v1],s)]
        | w::ws ->
           (match Ordering.of_int(VIndex.compare v1 w) with
            | Less -> [(v1::w::ws, s)]
            | Equal -> [(ws,R.mult (q v1) s)]
            | Greater -> if VIndex.Set.mem frozen v1 || VIndex.Set.mem frozen w
                         then [(v1::w::ws,s)]
                         else loop s (v1::w::ws) ) in
      loop r l


    let add x y = Index.Map.merge x y ~f:(fun ~key:_ -> function `Both(r1,r2) ->  let s = R.add r1 r2 in if R.(equal zero s) then None else Some s
                                                               | `Left(r) -> Some r
                                                               | `Right(r) -> Some r)
    let simplify ?frozen:(frozen=default_frozen) v =
      v |> Index.Map.to_alist
      |> List.concat_map ~f:(fun (v,r) -> standardize ~frozen:frozen r v)
      |> Index.Map.of_alist_reduce ~f:R.add
      |> Index.Map.filter ~f:(Fn.non (R.equal R.zero))

    module T =
      struct
        type t = R.t Index.Map.t [@@deriving sexp] (* Invariants: R.t is never zero *)
        let compare v1 v2 = Index.Map.compare R.compare (simplify ~frozen:default_frozen v1) (simplify ~frozen:default_frozen v2)
      end
    include T

    let zero = Index.Map.empty
    let one = Index.Map.singleton [] R.one
    let of_vec v = Index.Map.singleton [v] R.one
    let primitive ?frozen:(frozen=default_frozen) r l =
      if R.equal R.zero r
      then zero
      else standardize ~frozen:frozen r l |> Index.Map.of_alist_reduce ~f:R.add |> Index.Map.filter ~f:(Fn.non (R.equal R.zero))
    (* let (rNew,vs) = clearAdj r l in Index.Map.singleton vs rNew *)

    let to_string w =
      let primitive_to_string (s, r) =
        (R.to_string r)
        ^ if List.length s = 0
          then ""
          else "*." ^ Index.to_string s in
      if Index.Map.is_empty w
      then R.to_string (R.zero)
      else String.concat ~sep:" ++ " (List.map (Index.Map.to_alist w) ~f:primitive_to_string)

    let id v = to_string (simplify ~frozen:VIndex.Set.empty v)
    (* TODO: add appropriate breaks *)
    let pp ppf v = Format.fprintf ppf "%s" (to_string v)

    let scale r x = Index.Map.filter_map x ~f:(fun s -> let c = R.mult r s in
                                                        if R.equal R.zero c then None else Some c)
    let neg x = Index.Map.map ~f:R.neg x

    let mult w1 w2 =
      let pairs = List.cartesian_product (Map.to_alist w1) (Map.to_alist w2) in
      let combined = List.map pairs
                       ~f:(fun ((b1,r1), (b2,r2)) -> primitive ~frozen:default_frozen (R.mult r1 r2) (b1@b2))
      in
      List.fold combined ~init:zero ~f:add

    (* TODO: For speed shouldn't need to reapply primitive and add since every list should be adjacent duplicate free and unique already *)
    let transpose  x =
      x |> Index.Map.to_alist |> List.map ~f:(fun (v,r) -> primitive ~frozen:(default_frozen) r (List.rev v)) |> List.fold ~init:zero ~f:add

    let norm x = mult x (transpose  x)
    let inv  x =
      let q = simplify ~frozen:(VIndex.Set.empty) (norm x) in
      match Index.Map.find q [] with
      | Some r -> if Index.Map.length q =1
                  then Option.map (R.inv r) ~f:(fun rinv -> scale rinv (transpose  x))
                  else None
      | None -> None
    let inv_exn  x =
      match inv x with
      | Some ix -> ix
      | None -> invalid_arg (to_string x ^ " isn't invertible")
    let sub x y = add x (neg y)
    let pow  p n =
      if n >= 0
      then Fn.apply_n_times ~n:n (mult p) one
      else match inv p with
           | Some q -> Fn.apply_n_times ~n:(Int.abs n) (mult q) one
           | None -> invalid_arg ("Can't take negative power of: " ^ to_string p)

    let substitute ~f x =
      x |> Index.Map.to_alist |> List.map ~f:(fun (vs,r) -> scale r (List.fold ~init:one ~f:(fun acc v -> mult (f v) acc) vs)) |> List.fold ~f:add ~init:zero

    let of_string ?frozen:(frozen=default_frozen) s =
      s |> Str.split (Str.regexp_string "++")
      |> List.map ~f:(Str.split (Str.regexp_string "*."))
      |> List.map ~f:(function [] -> zero
                             | [r] -> primitive ~frozen:frozen (R.of_string (String.strip r)) []
                             | l ->  let (rs,v) = List.split_n l (List.length l - 1) in
                                     let r = rs |> List.map ~f:(fun r -> r |> String.strip |> R.of_string) |> List.reduce_exn ~f:R.mult in
                                     primitive ~frozen:frozen r (Index.of_string (String.strip (List.hd_exn v))))
      |> List.fold ~f:add ~init:zero
    include Comparable.Make(T)
  end


module MakeExplicit (Mod : sig include RMod.S val q : t -> R.t end) =
  struct
    module R =Mod.R
    module Index =
      struct
        include Mod.Index.Set
        let to_string s =
          s |> Core.Set.to_list
          |> List.map ~f:(fun x -> Mod.Index.to_string x)
          |>  String.concat ~sep:"*"

        let of_string _ = invalid_arg "NYI"

        include Comparable.Make(Mod.Index.Set)
      end
    module T =
      struct
        type t = R.t Index.Map.t [@@deriving compare,sexp] (* invariant, R.t should never be zero *)
      end
    include T
    let to_string w =
      let primitive_to_string (s, r) =
        (R.to_string r)
        ^ if Index.is_empty s
          then ""
          else "*" ^ Index.to_string s in
      if Index.Map.is_empty w
      then R.to_string (R.zero)
      else String.concat ~sep:" + " (List.map (Index.Map.to_alist w) ~f:primitive_to_string)

    let id = to_string

    let of_string _ = invalid_arg "NYI"
    let sign (l : Mod.Index.t list) =
      let tails = List.mapi l ~f:(fun i -> fun x -> (x, List.drop l i) ) in
      let totals =
        List.map tails ~f:(fun (x, tail) -> List.count tail ~f:(fun y -> Mod.Index.compare x y > 0)) in
      if (List.fold totals ~init:0 ~f:(fun x -> fun y -> x + y)) mod 2  = 0
      then R.one
      else R.neg (R.one)
    let minusOneToN n = if n mod 2 = 0 then R.one else R.neg(R.one)

    let zero = Index.Map.empty
    let one = Index.Map.singleton (Index.empty) R.one


    let primitive (c : R.t) (b : Mod.Index.t list) =
      if R.equal R.zero c
      then zero
      else
        let processed =
          b |> List.sort ~compare:Mod.Index.compare
          |> List.map ~f:(fun i -> (i,List.count b ~f:(Mod.Index.equal i)))
          |> List.map ~f:(fun (i,n) -> if n mod 2 = 0
                                       then (None, R.pow (Mod.q (Mod.of_list [R.one,i])) (n/2))
                                       else (Some i, R.pow (Mod.q (Mod.of_list [R.one,i])) ((n-1)/2))) in
        let coef = List.fold ~init:R.one ~f:(fun acc (_,r) -> R.mult acc r) processed in
        let bset = processed |> List.filter_map ~f:fst |> Index.of_list in
        Index.Map.singleton (bset) (R.mult (R.mult (sign b) c) coef)

    let of_vec v =
      v |> Mod.to_list
      |> List.filter_map ~f:(fun (r,i) -> if R.equal r R.zero then None else Some(Index.singleton i,r))
      |> Index.Map.of_alist_exn


    let add w1 w2 =
      Index.Map.merge w1 w2
        ~f:(fun ~key:_ -> function `Left a -> Some a
                               | `Right b -> Some b
                               | `Both (a,b) -> let c = R.add a b in
                                                if R.equal R.zero c then None else Some c)
    let scale r x = Index.Map.filter_map x ~f:(fun s -> let c = R.mult r s in
                                                        if R.equal R.zero c then None else Some c)
    let neg x = Index.Map.map ~f:R.neg x

    let mult w1 w2 =
      let pairs = List.cartesian_product (Map.to_alist w1) (Map.to_alist w2) in
      let combined = List.map pairs
                       ~f:(fun ((b1,r1), (b2,r2)) -> primitive (R.mult r1 r2)
                                                       (List.append (Set.to_list b1) (Set.to_list b2)))
      in
      List.fold combined ~init:zero ~f:add

    let transpose x =
      Index.Map.mapi x ~f:(fun ~key:bs ~data:c ->
          primitive c (List.rev (Index.to_list bs)) |> Index.Map.min_elt_exn |> snd)

    let norm x = mult x (transpose x)
    let inv x =
      let q = norm x in
      match Index.Map.find q Index.empty with
      | Some r -> if  Index.Map.length q =1
                  then Option.map (R.inv r) ~f:(fun rinv -> scale rinv (transpose x))
                  else None
      | None -> None
    let inv_exn x = match inv x with Some ix -> ix | None -> invalid_arg (to_string x ^ " isn't invertible")
    let sub x y = add x (neg y)
    let pow p n =
      if n >= 0
      then Fn.apply_n_times ~n:n (mult p) one
      else match inv p with
           | Some q -> Fn.apply_n_times ~n:(Int.abs n) (mult q) one
           | None -> invalid_arg ("Can't take negative power of: " ^ to_string p)

    (* TODO: Add appropriate breaks *)
    let pp ppf v = Format.fprintf ppf "%s" (to_string v)
    include Comparable.Make(T)
  end

  (*
module Make(VSpace : RMod.S) =
  struct
    module R = VSpace.R

    type vec =
      Vect of VSpace.t
    | Var of char  [@@deriving compare,sexp]
    type scalar =
      Const of R.t
    | Q of vec
    | SProd of scalar list [@@deriving compare,sexp]
    type t =
      Prod of scalar * (vec list)
    | Sum of t list [@@deriving compare, sexp]


  end
   *)
