open Core
module SQ =
  struct
    include Q
    let sexp_of_t x = Sexp.Atom(Q.to_string x)
    let t_of_sexp s =
      match s with
      | Sexp.Atom s -> Q.of_string s
      | Sexp.List _ -> of_sexp_error "BigRat.of_sexp: atom needed" s
  end
include SQ
let pp = pp_print
let mult = mul
let inv x =
  if SQ.equal x zero
  then None
  else Some(SQ.inv x)

let inv_exn = SQ.inv
let pow x n =
  Fn.apply_n_times ~n:(Int.abs n) (mult (if Int.(n>=0) then x else SQ.inv x)) (SQ.of_int 1)

let standard_unit x  = (SQ.inv x, SQ.of_int 1)
let div_rem p d = (p/d, SQ.of_int 0)
let gcd _ _ = SQ.of_int 1
let lcm p q = mult p q
include Comparable.Make(SQ)

let of_r x = of_bigint x
let of_rs x y = (of_bigint x) / (of_bigint y)
