open Core
module type S =
  sig
    type t
    include Comparable.S with type t:=t
    val t_of_sexp : Sexp.t -> t
    val sexp_of_t : t -> Sexp.t

    val to_string : t -> string
    val to_list : t -> int list
    val to_cycles : t -> int list list

    val pp : Format.formatter -> t -> unit

    val of_list : int list -> t
    val of_pairlist : (int * int) list -> t
    val of_cycles : int list list -> t
    val of_fun : int -> (int -> int) -> t

    val mult : t -> t -> t
    val inv : t -> t
    val one :  t

    (* apply (mult p1 p2) i === p2 . p1 i *)
    val apply : t -> int -> int
    val orbit : t -> int -> int list

  end

module MapPermutation : S =
  struct
    module T =
      struct
        (* All values not in the map are assumed to be fixed by permutation *)
        type t = int Int.Map.t [@@deriving sexp]
        let compare p1 p2 =
          Map.fold2 p1 p2 ~init:0 ~f:(fun ~key:i ~data acc ->
                         match acc with
                         | 0 -> (match data with
                                    | `Left a -> Int.compare a i
                                    | `Right b -> Int.compare i b
                                    | `Both(a,b) -> Int.compare a b)
                         | _ -> acc)
      end
    include T


    let apply (p: t)  (i : int) =
      match Int.Map.find p i with
      | None -> i
      | Some j -> j

    let to_list p =
      match Int.Map.max_elt p with
      | None -> []
      | Some (n,_) -> List.init n ~f:(fun i -> apply p (i+1) )

    let to_string p =
      match Int.Map.max_elt p with
      | None -> "<>"
      | Some (_,_) -> "<" ^ String.concat ~sep:"," (List.map ~f:Int.to_string (to_list p)) ^ ">"

    let pp ppf p = Format.fprintf ppf "%s" (to_string p)

    let of_list l =
      match List.find_a_dup ~compare:Int.compare l with
      | None -> Int.Map.of_alist_exn (List.mapi l ~f:(fun i j -> (i+1,j) ))
      | Some v -> invalid_arg ("Map isn't a permutation due to duplicate" ^ Int.to_string v)

    let of_pairlist l =
      match Int.Map.of_alist l with
      | `Ok p -> (match List.find_a_dup ~compare:Int.compare (Int.Map.data p) with
                  | None -> p
                  | Some v -> invalid_arg ("Map isn't a permutation due to duplicate image " ^ Int.to_string v))
      | `Duplicate_key v -> invalid_arg ("Map isn't a permutation due to duplicate key " ^ Int.to_string v)

    let of_cycles (l : int list list) =
      of_pairlist(List.concat_map l ~f:(fun cycle -> List.mapi cycle ~f:(fun i j -> if i = List.length cycle-1
                                                                  then (j, List.nth_exn cycle 0)
                                                                  else (j, List.nth_exn cycle (i+1)))) )
    let of_fun n f = of_list (List.init n ~f:(fun i -> f(i+1)))

    let mult p1 p2 =
      Int.Map.merge p1 p2 ~f:(fun ~key:_ data ->
          match data with
          | `Both(_,v) -> Some(apply p1 v)
          | `Left(v) -> Some(v)
          | `Right(v) -> Some(apply p1 v))

    let inv p = Int.Map.of_alist_exn (List.map ~f:(fun (i,j) -> (j,i)) (Int.Map.to_alist p))
    let one = Int.Map.empty

    let  orbit p i =
      let rec loop cur acc  =
        if cur = i
        then acc
        else loop (apply p cur) (cur::acc)
      in List.rev (loop (apply p i) [i])

    let to_cycles p =
      match Int.Map.max_elt p with
      | None -> []
      | Some (n,_) ->
         List.init n ~f:(fun i -> orbit p (i+1))
         |> List.filter ~f:(fun group -> List.length group > 1)
         |> List.dedup_and_sort ~compare:(fun g1 g2 -> Int.compare (List.fold ~init:Int.max_value ~f:Int.min g1) (List.fold ~init:Int.max_value ~f:Int.min g2))

    include Comparable.Make(T)
  end
