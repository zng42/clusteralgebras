open Core

module Make(R : Ring.S) : (Matrix.S with module R = R)=
  struct

    module R = R
    module T =
      struct
        type t = R.t list list [@@deriving sexp, compare] (* List of rows *)
      end
    include T
    include Comparable.Make(T)

    module Index = MatrixIndex

    let dim mat = (List.length mat, List.length (List.hd_exn mat))
    let row mat i = List.nth_exn mat i
    let col mat j = List.map ~f:(fun r -> List.nth_exn r j) mat
    let rows mat = mat
    let cols mat = List.transpose_exn mat
    let init (m,n) ~f = List.init m ~f:(fun i -> List.init n ~f:(fun j -> f i j))

(*     let zero = [[R.zero]]
       let one = [[R.one]]
 *)
    let zero n = init (n,n) ~f:(fun _ _  -> R.zero)
    let one n = init (n,n) ~f:(fun i j -> if Int.(i = j) then R.one else R.zero)


    let of_rows rows = rows
    let of_cols cols = List.transpose_exn cols
    let of_string s =
      s |> String.split_on_chars ~on:['{';'}';'\n']
      |> List.map ~f:(String.strip ~drop:(function ','-> true | c -> Char.is_whitespace c))
      |> List.filter ~f:(Fn.non String.is_empty)
      |> List.map ~f:(fun r -> r |> String.split ~on:',' |> List.map ~f:(fun v -> v |> String.strip |> R.of_string))
    let to_string mat = "{" ^ String.concat ~sep:"\n" (List.map mat ~f:(fun row -> String.concat ~sep:" , " (List.map row ~f:R.to_string))) ^"}\n"
    let id = to_string

    (* TODO: Replace with pp that properly blocks matrix elements *)
    let pp = (fun ppf x -> Format.fprintf ppf "%s" (to_string x))

    let transpose m = List.transpose_exn m

    let add m1 m2 = List.map2_exn m1 m2 ~f:(fun r1 r2 -> List.map2_exn r1 r2 ~f:R.add)
    let sub m1 m2 = List.map2_exn m1 m2 ~f:(fun r1 r2 -> List.map2_exn r1 r2 ~f:R.sub)
    let scale c mat = List.map ~f:(List.map ~f:(R.mult c)) mat
    let mult m1 m2 =
      let dot row col =
        match List.fold2 row col ~f:(fun acc r c -> R.add acc (R.mult r c)) ~init:R.zero with
        | List.Or_unequal_lengths.Ok x -> x
        | List.Or_unequal_lengths.Unequal_lengths -> invalid_arg "Numbers of columns doesn't match number of rows"  in
      let tm2 = transpose m2 in
      List.map m1 ~f:(fun row -> List.map tm2 ~f:(dot row))
    let neg m = List.map ~f:(List.map ~f:R.neg) m
    let pow m k =
      match k with
      | 0 ->  one (fst (dim m))
      | _ ->  Fn.apply_n_times ~n:(k-1) (mult m) m

    let at m ~i:(i,j) = List.nth_exn (List.nth_exn m i) j

    let det m =
      match dim(m) with
      | (1,1) -> at m ~i:(0,0)
      | (2,2) -> let (a,b,c,d) = (at m ~i:(0,0), at m ~i:(0,1), at m ~i:(1,0), at m ~i:(1,1)) in
                 R.(sub (mult a d) (mult b c))
      | _ -> invalid_arg "Det for this size NYI"

    let schurComp m i =
      match dim m with
      | (2,2) ->
         let (a,b,c,d) = (at m ~i:(0,0), at m ~i:(0,1), at m ~i:(1,0), at m ~i:(1,1)) in
         (match i with
          | (0,0) -> a |> R.inv |> Option.map ~f:(fun ainv -> (ainv,R.(sub d (mult c (mult ainv b)))))
          | (0,1) -> b |> R.inv |> Option.map ~f:(fun binv -> (binv,R.(sub c (mult d (mult binv a)))))
          | (1,0) -> c |> R.inv |> Option.map ~f:(fun cinv -> (cinv,R.(sub b (mult a (mult cinv d)))))
          | (1,1) -> d |> R.inv |> Option.map ~f:(fun dinv -> (dinv,R.(sub a (mult b (mult dinv c)))))
          | _ -> invalid_arg "Need i to be between (0,0) and (1,1)" )
      | _ -> invalid_arg "shcur comp not implemented for non 2x2 matrices"

    let inv m =
      match dim(m) with
      | (1,1) -> R.inv (at m ~i:(0,0)) |> Option.map ~f:(fun ainv -> [[ainv]])
      | (2,2) ->
         let (a,b,c,d) = (at m ~i:(0,0), at m ~i:(0,1), at m ~i:(1,0), at m ~i:(1,1)) in
         (*match R.inv (det m) with
          | Some detInv -> [[d; R.neg b];[R.neg c; a]] |> scale detInv |> (fun m -> Some m)
          | None -> None*)
         (match (schurComp m (0,0), schurComp m (0,1), schurComp m (1,0), schurComp m (1,1))  with
          | (Some (ainv, schur),_,_,_) ->
             schur |> R.inv |> Option.map ~f:(fun schurInv ->
                                   [[R.(add ainv (mult (mult ainv b) (mult (mult schurInv c) ainv))) ; R.(neg (mult (mult ainv b) schurInv))]
                                   ;[R.(neg (mult schurInv (mult c ainv)))                           ; schurInv]])
          | (_ ,Some (binv,schur),_,_)->
             schur |> R.inv |> Option.map ~f:(fun schurInv ->
                                   R.[[neg (mult (mult schurInv d) binv)                          ; schurInv]
                                     ;[add binv (mult (mult (mult binv a) schurInv) (mult d binv)); neg (mult (mult binv a) schurInv)]])
          | (_, _, Some (cinv,schur),_) ->
             schur |> R.inv |> Option.map ~f:(fun schurInv ->
                                   R.[[neg (mult (mult cinv d) schurInv); add cinv (mult (mult (mult cinv d) schurInv) (mult a cinv))]
                                     ;[schurInv                         ; neg (mult (mult schurInv a) cinv)]])
          | (_,_,_,Some(dinv,schur)) ->
             schur |> R.inv |> Option.map ~f:(fun schurInv ->
                                   R.[[schurInv                         ; neg (mult (mult schurInv b) dinv)]
                                     ;[neg (mult (mult dinv c) schurInv); add dinv (mult (mult (mult dinv c) schurInv) (mult b dinv))]])
          | (None,None,None,None) -> None)
      | _ -> invalid_arg "NYI"

    let inv_exn i =
      match inv i with
      | Some iinv -> iinv
      | None -> invalid_arg (to_string i ^ " is not invertible")


    let trace m = List.fold ~f:(R.add) ~init:R.zero (List.init (List.length m) ~f:(fun i -> at m ~i:(i,i)))

    let to_list m =
      m
      |> rows
      |> List.concat_mapi ~f:(fun i -> List.mapi ~f:(fun j x -> (x,(i,j))))
      |> List.filter ~f:(fun (x,_) -> not(R.equal x R.zero))

    let of_list l =
      let lookup = l |> List.map ~f:(fun (x,y) -> (y,x)) |> Index.Map.of_alist_exn in
      let m =
        match l |> List.map ~f:(fun (_,(i,_)) -> i) |> List.max_elt ~compare:Int.compare with
        | Some m -> m+1
        | None -> 0 in
      let n =
        match l |> List.map ~f:(fun (_,(_,j)) -> j) |> List.max_elt ~compare:Int.compare with
        | Some n -> n+1
        | None -> 0
      in
      init (m,n) ~f:(fun i j -> match Index.Map.find lookup (i,j) with Some x -> x | None -> R.zero)

    let terms l = l |> to_list |> List.map ~f:snd
    let all_terms l = l |> List.concat_map ~f:terms|> List.dedup_and_sort ~compare:Index.compare
    let to_mat ls =
      let terms = all_terms ls in
      List.transpose_exn (List.map ls ~f:(fun l -> List.map terms ~f:(fun i -> at l ~i:i)))

    let to_sparse_mat ls =
      let index_position = Index.Map.of_alist_exn (List.mapi (all_terms ls) ~f:(fun i x -> (x,i))) in
      List.concat_mapi ls ~f:(fun j l -> List.map (terms l) ~f:(fun index -> (Index.Map.find_exn index_position index, j ,at l ~i:index)))

    let mapi ~f m = init (dim m) ~f:(fun i j -> f (i,j) (at ~i:(i,j) m))
    let map_basis ~f m = init (dim m) ~f:(fun i j -> at ~i:(f (i,j)) m)
    let filteri ~f m = init (dim m) ~f:(fun i j -> if f (i,j) (at ~i:(i,j) m) then at ~i:(i,j) m  else R.zero)

    module GL (Dim : sig val dim:int end): (Ring.S with type t = t)=
      struct
        include T
        include Comparable.Make(T)
        let zero = zero Dim.dim
        let one = one Dim.dim
        let add = add
        let neg = neg
        let mult = mult
        let inv = inv
        let inv_exn = inv_exn
        let sub = sub
        let pow = pow
        let to_string = to_string
        let of_string = of_string
        let pp = pp
      end

  end
