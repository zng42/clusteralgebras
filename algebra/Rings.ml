open Core

module Int  =
  struct
    include Core.Int
    let add i j = i + j
    let sub i j = i - j
    let neg i = -i
    let mult i j = i * j
    let inv i =
      match i with
      | 1 -> Some 1
      | -1 -> Some (-1)
      | _ -> None
    let inv_exn i =
      match inv i with
      | Some iinv ->  iinv
      | None -> invalid_arg (to_string i ^ " is not invertible")

    let div_rem p d = (p / d, p mod d)
    let gcd a b =
      let rec loop x y =
        if x = 0 then y
        else loop (y mod x) x
      in if a < b then loop a b else loop b a
    let lcm a b = (a / (gcd a b)) *b
    let standard_unit x =
      if x > 0
      then (1,x)
      else (-1, -1*x)
    let pp = (fun ppf x -> Format.fprintf ppf "%s" (to_string x))
    let inv_modn p ~n = List.find (List.init n ~f:(fun i -> i)) ~f:(fun i -> (p*i) mod n = 1)
  end

module Float : Ring.EuclideanDomain with type t = float=
  struct
    include Core.Float
    let add i j = i +. j
    let neg i = -. i
    let sub i j = i -. j
    let mult i j = i *. j
    let inv i =
      match Core.Float.robustly_compare i 0.0 with
      | 0 -> None
      | _ -> Some (1.0 /. i)
    let inv_exn i =
      match inv i with
      | Some iinv -> iinv
      | None -> invalid_arg ("Can't invert 0")
    let pow i n =
      match Ordering.of_int (Int.compare 0 n) with
      | Greater -> Fn.apply_n_times ~n:Int.(n-1) (mult i) i
      | Equal -> 1.0
      | Less ->
         (match inv i with
          | Some j -> Fn.apply_n_times ~n:Int.(abs n - 1) (mult j) j
          | None -> invalid_arg "Can't take negative power of 0")

    let div_rem p d = (p /. d, 0.)
    let gcd _ _ = 1.0
    let lcm a b = a *. b
    let standard_unit x = (1. /. x, 1.)

    let pp = (fun ppf x -> Format.fprintf ppf "%s" (to_string x))
  end

module BigInt = BigIntRing
module BigRat = BigRatRing

module MatrixPoly = MapPolynomial.Make(Indeces.IntPair) (Int)
module FMatrixPoly = MapPolynomial.Make(Indeces.IntPair) (Float)

module IntPoly = MapPolynomial.Make (Indeces.Letter) (Int)
module IntLaurent = MapLaurent.Make (Indeces.Letter) (Int)
module BigIntPoly = MapPolynomial.Make (Indeces.Letter) (BigInt)

module FloatPoly = MapPolynomial.Make (Indeces.Letter) (Float)

module BigRatUPoly = MapPolynomial.Make (Indeces.Single) (BigRatRing)

module RationalFunc =
  struct
    include Rationals.Make(BigRatUPoly)
    let degree r = BigRatUPoly.degree (numerator r) - BigRatUPoly.degree (denominator r)
    let eval r x = BigRatRing.div (BigRatUPoly.eval (numerator r) (fun _ -> x)) (BigRatUPoly.eval (denominator r) (fun _ -> x))
    let leading_term r = of_rs (r |> numerator |> BigRatUPoly.leading_term |> (fun (r, ts) ->  BigRatUPoly.of_mono r ts))
                           (r |> denominator |> BigRatUPoly.leading_term |> (fun (r,ts) -> BigRatUPoly.of_mono r ts))
  end

module PolyMat = ListMatrix.Make(RationalFunc)
module LacamlMat = LacamlMatRing
