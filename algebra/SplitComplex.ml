open Core

module Make(R: sig include Ring.EuclideanDomain val abs: t -> t end) =
struct
  module T =
    struct
      type t = R.t * R.t [@@deriving compare,sexp]
      let to_string (a,b) = R.to_string a ^" + " ^ R.to_string b ^ "I"
      let of_string s =
        s |> String.split ~on:'+'
        |> List.map ~f:String.strip |> List.map ~f:(fun s -> match String.chop_suffix s ~suffix:"I" with
                                                             | Some "" -> (R.zero, R.one)
                                                             | Some s' -> (R.zero, R.of_string s')
                                                             | None -> (R.of_string s, R.zero))
        |> List.fold ~init:(R.zero,R.zero) ~f:(fun (i1,i2) (j1,j2) -> (R.add i1 j1,R.add i2 j2))
      let pp = (fun ppf x -> Format.fprintf ppf "%s" (to_string x))
    end
  include T

  let zero = (R.zero,R.zero)
  let one = (R.one,R.zero)
  let add (i1,i2) (j1,j2) = (R.add i1 j1, R.add i2 j2)
  let neg (a,b) = (R.neg a,R.neg b)
  let mult (i1,i2) (j1,j2) = (R.(add (mult i1 j1) (mult i2 j2)), R.(add (mult i1 j2) (mult i2 j1)))
  let scale lam (a,b) = (R.mult lam a, R.mult lam b)


  let conjugate (a,b) = (a,R.neg b)
  let modulus (a,b) = R.(sub (mult a a) (mult b b))

  let inv x =
    match R.inv (modulus x) with
    | Some xmodInv -> Some(scale xmodInv (conjugate x))
    | None -> None

  let inv_exn i =
    match inv i with
    | Some iinv -> iinv
    | None -> invalid_arg (to_string i ^ " is not invertible")

  let sub (i1,i2) (j1,j2) = (R.sub i1 i2, R.sub j1 j2)
  let div_rem x y =
    let m = modulus y in
    let (a,b) = mult x (conjugate y) in
    let (q1,r1) = R.div_rem a m in
    let (q2,r2) = R.div_rem b m in
    ((q1,q2), (r1, r2))

  let div p q = div_rem p q |> fst
  let gcd _ _ = invalid_arg "NYI"
  let lcm _ _ = invalid_arg "NYI"
  let standard_unit (_,_) = invalid_arg "NYI"

  let (+) = add
  let ( * ) = mult
  let (-) = sub
  let (/) = div

  let pow i n =
    match Ordering.of_int (Int.compare 0 n) with
    | Greater -> Fn.apply_n_times ~n:Int.(n-1) (mult i) i
    | Equal -> one
    | Less ->
       (match inv i with
        | Some j -> Fn.apply_n_times ~n:Int.(abs n - 1) (mult j) j
        | None -> invalid_arg "Can't take negative power of 0")

  let abs (a,b) = (R.abs a, R.abs b)

  let re (a,_) = a
  let of_re a = (a,R.zero)
  let is_re (_,b) = R.(b = zero)

  let im (_,b) = b
  let of_im b = (R.zero,b)
  let is_im (a,_) = R.(a=zero)

  let of_pair (a,b) = (a,b)


  include Comparable.Make(T)
end

module Int = Make(IntRing)
module BigInt = Make(BigIntRing)
