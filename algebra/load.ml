(*#mod_use "Index.ml";;
#mod_use "MatrixIndex.ml";;
#mod_use "Ring.ml";;
#mod_use "Rationals.ml";;
#mod_use "RMod.ml";;
#mod_use "RAlg.ml";;
#mod_use "Polynomial.ml";;
#mod_use "MapPolynomial.ml";;
#mod_use "Laurent.ml";;
#mod_use "MapLaurent.ml";;
#mod_use "FreeRMod.ml";;
#mod_use "Forms.ml";;
#mod_use "MapForms.ml";;
#mod_use "Tensor.ml";;
#mod_use "MapTensor.ml";;
#mod_use "Permutation.ml";;

#mod_use "Matrix.ml";;
#mod_use "ListMatrix.ml";;
#mod_use "Gl.ml";;

(* Requires Zarith *)
#require "zarith.top";;
#mod_use "BigIntRing.ml";;
#mod_use "BigRatRing.ml";;

#mod_use "Rationals.ml";;
 *)
open Core

       (*
module IntRing : Ring.EuclideanDomain with type t = int=
  struct
    include Core.Int
    let add i j = i + j
    let sub i j = i - j
    let neg i = -i
    let mult i j = i * j
    let inv i =
      match i with
      | 1 -> Some 1
      | -1 -> Some (-1)
      | _ -> None
    let div_rem p d = (p / d, p mod d)
    let gcd a b =
      let rec loop x y =
        if x = 0 then y
        else loop (y mod x) x
      in if a < b then loop a b else loop b a
    let lcm a b = (a / (gcd a b)) *b
    let standard_unit x =
      if x > 0
      then (1,x)
      else (-1, -1*x)
    let pp = (fun ppf x -> Format.fprintf ppf "%s" (to_string x))
  end

let inv_modn p n = List.find (List.init n ~f:(fun i -> i)) ~f:(fun i -> (p*i) mod n = 1)

module FloatRing : Ring.EuclideanDomain with type t = float=
  struct
    include Core.Float
    let add i j = i +. j
    let neg i = -. i
    let sub i j = i -. j
    let mult i j = i *. j
    let inv i =
      match Core.Float.robustly_compare i 0.0 with
      | 0 -> None
      | _ -> Some (1.0 /. i)

    let pow i n =
      match Ordering.of_int (Int.compare 0 n) with
      | Greater -> Fn.apply_n_times ~n:Int.(n-1) (mult i) i
      | Equal -> 1.0
      | Less ->
         (match inv i with
          | Some j -> Fn.apply_n_times ~n:Int.(abs n - 1) (mult j) j
          | None -> invalid_arg "Can't take negative power of 0")

    let div_rem p d = (p /. d, 0.)
    let gcd _ _ = 1.0
    let lcm a b = a *. b
    let standard_unit x = (1. /. x, 1.)

    let pp = (fun ppf x -> Format.fprintf ppf "%s" (to_string x))
  end

module IntPairIndex =
  struct
    module T =
      struct
        type t = int*int [@@deriving compare, sexp]
      end
    include T
    include Comparable.Make(T)
    let to_string (i,j) = "x" ^ Int.to_string i ^"_" ^ Int.to_string j
    let of_string s =
      match String.split (String.drop_prefix s 1) ~on:'_' with
      | [i;j] -> (Int.of_string i, Int.of_string j)
      | _ -> invalid_arg (s ^ " is not xi_j")
  end
module MatrixPoly = MapPolynomial.Make(IntPairIndex) (IntRing)
module FMatrixPoly = MapPolynomial.Make(IntPairIndex) (FloatRing)
        *)
module I3 = FreeRMod.MakeFinite (struct let dim = 3 end) (Rings.Int)
module Q3 = FreeRMod.MakeFinite (struct let dim = 3 end) (Rings.BigRat)
module Wedge = MapForms.Make (I3)
module I3Tensor = MapTensor.Make(I3)
(* #install_printer I3Tensor.pp *)


module SingleIndex =
  struct
    module T =
      struct
        type t = unit [@@deriving compare,sexp]
      end
    include T
    include Comparable.Make(T)
    let to_string () = "t"
    let of_string _ = ()
  end

module LetterIndex =
  struct
    module T =
      struct
        type t = int [@@deriving compare, sexp]
      end
    include T
    include Comparable.Make(T)
    let to_string i = String.of_char(Char.of_int_exn (65+i))
    let of_string s= Char.to_int(Char.of_string s)-65
  end
module LetterSubIndex =
  struct
    module T =
      struct
        type t = int * (int option) [@@deriving compare, sexp]
      end
    include T
    include Comparable.Make(T)
    let to_string (i,j) =
      match j with
      |Some j -> String.of_char(Char.of_int_exn (65+i)) ^ Int.to_string j
      | None -> String.of_char(Char.of_int_exn (65+i))

    let of_string s=
      match String.to_list s with
      | [] -> invalid_arg "Index should be character followed by a number"
      | [c] -> (Char.to_int c - 65, None)
      | c :: cs -> (Char.to_int(c)-65, Some(Int.of_string(String.of_char_list cs)))

  end
module FreeInt = FreeRMod.Make (LetterIndex) (Rings.Int)
module IntPoly = MapPolynomial.Make (LetterIndex) (Rings.Int)
module IntLaurent = MapLaurent.Make (LetterIndex) (Rings.Int)
module BigIntPoly = MapPolynomial.Make (LetterIndex) (Rings.BigInt)

module FloatPoly = MapPolynomial.Make (LetterIndex) (Rings.Float)

module BigRatUPoly = MapPolynomial.Make (SingleIndex) (Rings.BigRat)
module Perm = Permutation.MapPermutation

module RationalFunc =
  struct
    include Rationals.Make(BigRatUPoly)
    let degree r = BigRatUPoly.degree (numerator r) - BigRatUPoly.degree (denominator r)
    let eval r x = BigRatRing.div (BigRatUPoly.eval (numerator r) (fun _ -> x)) (BigRatUPoly.eval (denominator r) (fun _ -> x))
  end
module PolyMat = ListMatrix.Make(RationalFunc)
(* #install_printer FreeInt.pp
#install_printer IntLaurent.pp
#install_printer IntPoly.pp
#install_printer BigIntPoly.pp
#install_printer FloatPoly.pp
#install_printer MatrixPoly.pp
#install_printer BigRatUPoly.pp
#install_printer RationalFunc.pp
#install_printer PolyMat.pp
#install_printer Perm.pp
 *)

(* Requires Lacaml *)
(* #require "lacaml" *)
open Lacaml.D;;
(* #mod_use "LacamlMatRing.ml";;
#install_printer LacamlMatRing.pp
 *)
let minor ~m mat = Mat.of_col_vecs_list (List.map m ~f:(Mat.col mat))
let det_minor ~l mat = LacamlMatRing.det (minor ~m:l mat)

let nullspace threshold mat =
  let m = Mat.of_list (List.map ~f:(List.map ~f:Float.of_int) mat) in
  let (sigma,_,vt) = gesvd (Mat.of_col_vecs (Mat.to_col_vecs m)) in
  List.drop (Mat.to_list vt) (List.count (Vec.to_list sigma) ~f:(fun s -> Float.(abs s > threshold)))
let cmod k n = let x = k mod n in if x < 0 then x + n else x
