(* open Core *)
module type S =
  sig
    module RMod : RMod.S

    type t
    val primitive : RMod.R.t -> RMod.Index.t list -> t

    val add : t -> t -> t
    val wedge : t -> t -> t
    val d : t -> t

    val scale : RMod.R.t -> t -> t

    val star : t -> t
  end
