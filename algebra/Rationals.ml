open Core
module Make(R : Ring.EuclideanDomain)  =
  struct
    module T =
      struct
        type t = R.t * R.t [@@deriving sexp]
        let compare (p1,q1) (p2,q2) =
          R.compare (R.mult p1 q2) (R.mult p2 q1)
      end
    include T
    include Comparable.Make(T)

    let normalize (p,q) =
      if R.equal p R.zero then (R.zero, R.one)
      else
        let g = R.gcd p q in
        let (u,den) = R.standard_unit (fst(R.div_rem q g)) in
        let num = fst(R.div_rem p g) in
        (R.mult u num, den)

    let to_string (p,q) =
      if R.equal q R.one
      then R.to_string p
      else "(" ^ R.to_string p ^  ")" ^ "/" ^ "(" ^ R.to_string q ^ ")"
    let id = to_string
    (* TODO: work out proper pretty print boxing for num/denom *)
    let pp = (fun ppf x -> Format.fprintf ppf "%s" (to_string x))
    let of_string s =
      match String.split ~on:'/' s with
      | [ps] -> (ps |> String.strip ~drop:(function '(' -> true | ')' -> true | c -> Char.is_whitespace c) |> R.of_string, R.one)
      | [ps;qs] ->
         let p = ps |> String.strip ~drop:(function '(' -> true | ')' -> true | c -> Char.is_whitespace c) |> R.of_string in
         let q = qs |> String.strip ~drop:(function '(' -> true | ')' -> true | c -> Char.is_whitespace c) |> R.of_string in
         normalize (p,q)
      | _ -> invalid_arg ("Rational expressions can only have one /: " ^ s)


    let numerator (p,_) = p
    let denominator (_,q) = q
    let of_r p = (p,R.one)
    let of_rs p q = (p,q) |> normalize

    let zero = (R.zero,R.one)
    let one = (R.one, R.one)
    let add (p1,q1) (p2,q2) = R.(add (mult p1 q2) (mult p2 q1),mult q1 q2) |> normalize
    let neg (p,q) = (R.neg p, q)
    let mult (p1,q1) (p2,q2) =
(*      let x1 = R.gcd p1 q2 in
      let x2 = R.gcd p2 q1 in *)
      R.(mult p1 p2, mult q1 q2) |> normalize
    let inv (p,q) = if R.equal p R.zero then None else Some ((q,p)|> normalize)
    let inv_exn i =
      match inv i with
      | Some iinv ->  iinv
      | None -> invalid_arg ("zero is not invertible")

    let sub (p1,q1) (p2,q2) = R.(sub (mult p1 q2) (mult p2 q1),mult q1 q2) |> normalize
    let pow (p,q) k =
      if Int.(k > 0)
      then (R.pow p k , R.pow q k)
      else (R.pow q (Int.abs k), R.pow p (Int.abs k))

    let gcd _ _ = one
    let div_rem (p1,q1) (p2,q2) = (mult (p1,q1) (q2,p2), zero)
    let standard_unit (p,q) = ((q,p), one)
  end
