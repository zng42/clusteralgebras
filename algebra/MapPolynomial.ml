open Core

module MakeMonomial (I : Index.S) =
  struct
    (* Add an extra variable Bot that is before any user defined variables for use in groebner basis gcd computations *)
    module IB =
      struct
        module T =
          struct
            type t = Bot | I of I.t [@@deriving sexp]
            let compare x y =
              match (x,y) with
              | (Bot,Bot) -> Equal |> Ordering.to_int
              | (Bot,I _) -> Less |> Ordering.to_int
              | (I _, Bot) -> Greater |> Ordering.to_int
              | (I x, I y) -> I.compare x y
          end
        include T
        include Comparable.Make(T)
        let to_string x =
          match x with
          | Bot -> invalid_arg "All computations involving Bot should be internal"
          | I x -> I.to_string x
        let of_string s = I (I.of_string s)
      end


    let one = IB.Map.empty
    let is_one = IB.Map.is_empty
    let degree m = IB.Map.fold m ~f:(fun ~key:_ ~data acc -> data + acc) ~init:0
    let idegree m ~i = match IB.Map.find m (IB.I i) with Some d -> d | None -> 0

    module T =
      struct
        type monomial = int IB.Map.t
        type t = monomial

        (* Lexographic order (AB^2C > ABC^3) *)
        let lex_compare m1 m2 =
          let ord = Map.fold2 m1 m2 ~init:None ~f:(fun ~key:_ ~data acc ->
                        match acc with
                        | Some ord -> Some ord
                        | None ->
                           (match data with
                            | `Left _ -> Some Greater
                            | `Right _ -> Some Less
                            | `Both (e1,e2) ->
                               (match Ordering.of_int (Int.compare e1 e2) with
                                | Equal -> None
                                | ord -> Some ord)) )
          in match ord with
             | Some ord -> Ordering.to_int ord
             | None -> Ordering.to_int Equal (* Only can occur in both monomials are empty and thus both represent 1 *)

        let grlex_compare m1 m2 =
          match Ordering.of_int (Int.compare (degree m1) (degree m2)) with
          | Less -> Less |> Ordering.to_int
          | Greater -> Greater |> Ordering.to_int
          | Equal -> lex_compare m1 m2

        let compare = lex_compare
        let sexp_of_t = IB.Map.sexp_of_t Int.sexp_of_t
        let t_of_sexp = IB.Map.t_of_sexp Int.t_of_sexp
      end
    include T
     let to_string (m : monomial) : string =
      if IB.Map.is_empty m
      then "1"
      else String.concat ~sep:"*" (IB.Map.data (IB.Map.mapi m ~f:(fun ~key -> fun ~data -> IB.to_string key ^
                                                                                           (if data = 1
                                                                                            then ""
                                                                                            else "^" ^ (Int.to_string data)))))
    let of_list l =
      l |> List.map ~f:(fun (x,i) -> (IB.I x,i))
      |> IB.Map.of_alist_reduce ~f:(fun x -> fun y -> x + y) |> IB.Map.filteri ~f:(fun ~key:_ ~data -> not(Int.equal 0 data))
    let to_alist m =
      m |> IB.Map.to_alist |> List.map ~f:(function (IB.I x,d) -> (x,d) | (IB.Bot,_) -> invalid_arg "Users shouldn't see Bot")
    let of_string s =
      String.split s ~on:'*' |> (List.filter_map  ~f:(fun v -> match String.split v ~on:'^' with
                                                               | [] -> invalid_arg (v ^" is not a v^d")
                                                               | [""] -> None
                                                               | ["1"] -> None
                                                               | [x] -> Some(I.of_string (String.strip x), 1)
                                                               | ["1";_] -> None
                                                               | [x;d] -> Some(I.of_string (String.strip x), Int.of_string (String.strip d))
                                                               | _ -> invalid_arg (v ^ " is not of form v^d")))
      |> of_list
    let mult (m1 : monomial) (m2 : monomial) : monomial =
      IB.Map.merge m1 m2 ~f:(fun ~key:_ -> function `Left x -> Some x | `Right y -> Some y | `Both (x,y) -> if x +y = 0 then None else Some (x + y))
    let div (m : monomial) (n : monomial) =
      IB.Map.merge m n ~f:(fun ~key:_ -> function `Both(m_degree, n_degree) -> if m_degree = n_degree then None else Some(m_degree - n_degree)
                                              | `Left(m_degree) -> Some(m_degree)
                                              | `Right(n_degree) -> Some(-n_degree))
    let gcd m1 m2 = IB.Map.merge m1 m2 ~f:(fun ~key:_ -> function `Left _ -> None | `Right _ -> None | `Both(x,y) -> Some(Int.min x y))
    let lcm m1 m2 = IB.Map.merge m1 m2 ~f:(fun ~key:_ -> function `Left x -> Some x| `Right y -> Some y | `Both(x,y) -> Some(Int.max x y))

    let fold m ~init ~f =
      IB.Map.fold m ~init:init ~f:(fun ~key ~data acc -> match key with IB.Bot -> invalid_arg "User shouldn't have Bot" | IB.I i -> f ~key:i ~data:data acc)

    let is_positive m = IB.Map.for_all m ~f:(fun x -> x > 0)

    let bot = IB.Map.singleton IB.Bot 1
    include Comparable.Make(T)
  end


module Make
         (I : Index.S )
         (Coef : Ring.EuclideanDomain )
       : Polynomial.S with type index = I.t with module R = Coef =
  struct
    module R = Coef
    type index = I.t

    type coef = Coef.t

    module Monomial = MakeMonomial(I)

      (* struct

                            (* Lexographic order (AB^2C > ABC^3) *)
                            let compare m1 m2 =
                              (* TODO: Rewrite using Map.fold2 after checking this folds with increasing key *)
                              let compare_keys (k1,v1) (k2,v2) =
                                match Ordering.of_int(I.compare k1 k2) with
                                | Less ->  Greater (* A > B as A^1 > A^0 B *)
                                | Greater -> Less
                                | Equal -> Ordering.of_int (Int.compare v1 v2) in
                              let rec loop opt1 opt2 =
                                match (opt1, opt2) with
                                | (None, None) -> Equal
                                | (None, Some _) ->  Less
                                | (Some _ , None) -> Greater
                                | (Some (k1,v1), Some(k2,v2)) ->
                                   (match compare_keys (k1,v1) (k2,v2) with
                                    | Equal -> loop (Map.closest_key m1 `Greater_than k1) (Map.closest_key m2 `Greater_than k2)
                                    | Less -> Less
                                    | Greater -> Greater)

                              in
                              match loop (Map.min_elt m1) (Map.min_elt m2) with
                              | Greater -> 1
                              | Less -> -1
                              | Equal -> 0

                      end *)
    module MMap = Monomial.Map
    type poly = coef MMap.t
    type t = poly

    let to_string (p : poly) : string =
      if MMap.is_empty p
      then "0"
      else String.concat ~sep:" + " (MMap.data (MMap.mapi p ~f:(fun ~key -> fun ~data ->
                                                                            if Monomial.is_one key
                                                                            then Coef.to_string data
                                                                            else (Coef.to_string data)^ "*" ^ (Monomial.to_string key))))

    let zero = MMap.empty
    let const c = if Coef.equal c Coef.zero then zero else (MMap.singleton Monomial.one c)
    let one = const Coef.one
    let add  =
      let combine data =
        match data with
        | `Left x -> Some x
        | `Right y -> Some y
        | `Both (x,y) -> let v= Coef.add x y in
                         if Coef.equal v Coef.zero then None else Some(v)
      in  MMap.merge ~f:(fun ~key:_ -> combine)
    let neg p = MMap.map p ~f:(Coef.neg)
    let sub p q = add p (neg q)

    let leading (p : poly) =
      match Map.max_elt p with
      | None -> Coef.zero
      | Some (_,c) -> c

    let leading_term (p : poly) =
      match Map.max_elt p with
      | None -> (Coef.zero, [])
      | Some (m,c) -> (c, Monomial.to_alist m)

    let scale (s : Coef.t) (p : poly) =
      if Coef.equal s Coef.zero
      then zero
      else MMap.map p ~f:(fun coef -> Coef.mult s coef)

    let scale_mono (m : Monomial.t) (c1: Coef.t) (p : poly) : poly =
      if Coef.equal Coef.zero c1
      then zero
      else MMap.of_alist_exn (List.map ~f:(fun (m1,c) -> (Monomial.mult m m1, Coef.mult c c1)) (MMap.to_alist p))

    let mult (p1 : poly) (p2 : poly) : poly =
      MMap.fold ~init:MMap.empty p1
        ~f:(fun ~key:m1 -> fun ~data:c1 -> fun acc -> add acc (scale_mono m1 c1 p2))

    let syzgy f1 f2 =
      match (Map.max_elt f1, Map.max_elt f2) with
      | (None,None) -> zero
      | (None,Some _) -> f2
      | (Some _, None) -> f1
      | (Some (m1,c1), Some(m2,c2)) ->
         let (ca, a) = (R.lcm c1 c2, Monomial.lcm m1 m2) in
         let (x,_) = R.div_rem ca c1 in
         let (y,_) = R.div_rem ca c2 in
         sub (scale_mono (Monomial.div a m1) x f1) (scale_mono (Monomial.div a m2) y f2)


    let degree p =
      List.fold (List.map (MMap.keys p) ~f:Monomial.degree) ~init:Int.min_value ~f:(Int.max)

    let idegree p ~i =
      List.fold (List.map (MMap.keys p) ~f:(Monomial.idegree ~i:i)) ~init:Int.min_value ~f:Int.max



    let eval p f =
      let eval_mono m = Monomial.fold ~init:R.one m ~f:(fun ~key:i ~data:deg res -> R.mult res (R.pow (f i) deg)) in
      MMap.fold p ~init:R.zero  ~f:(fun ~key:m ~data:c res -> R.add (R.mult c (eval_mono m)) res)


    (* TODO: Fix for laurent polynomials *)
    let subs p ~f =
      let subs_mono m = Monomial.fold ~init:one m ~f:(fun ~key:i ~data:deg res -> Fn.apply_n_times ~n:deg (mult (f i)) res) in
      MMap.fold p ~init:zero  ~f:(fun ~key:m -> fun ~data:c -> fun res -> add (scale c (subs_mono m)) res)




    let of_mono (c : coef) (ms : (index * int) list) : poly = MMap.singleton (Monomial.of_list ms) c
    let of_list (l : (coef * ((index * int) list)) list) : poly =
      List.fold ~init:zero ~f:add (List.map l ~f:(fun (c, m) -> of_mono c m))

    let map_basis ~f p = of_list (List.map ~f:(fun (m,c) -> (c, f (Monomial.to_alist m))) (MMap.to_alist p))
    let mapi ~f p = MMap.mapi p ~f:(fun ~key:m ~data:c -> f (Monomial.to_alist m) c)
    let filteri ~f p = MMap.filteri p ~f:(fun ~key:m ~data:c -> f (Monomial.to_alist m) c)


    let of_string s =
      let mono_list =
        List.concat_map ~f:(fun s -> List.mapi (String.split ~on:'-' s) ~f:(fun i m -> if i = 0 then (true,m) else (false,m)))
          (String.split ~on:'+' s) in
      List.fold mono_list ~init:zero ~f:(fun acc (fAdd,m) ->
          let (cs,ms) = match String.lsplit2 ~on:'*' m with
            | Some (cs,ms) -> (cs,ms)
            | None -> (m, "") in
          let newp = MMap.singleton (Monomial.of_string ms) (R.of_string (String.strip cs)) in
          if fAdd then add acc newp else add acc (neg newp))
      |> MMap.filter ~f:(fun c -> not(R.equal c R.zero))

    module Index =
      struct
        module T =
          struct
            type t = (I.t * int) list  [@@deriving compare, sexp]
          end
        include T
        include Comparable.Make(T)
        let to_string l = Monomial.to_string(Monomial.of_list l)
        let of_string (s:string) : t =
          List.filter_map (String.split s ~on:'*') ~f:(fun v -> match String.split v ~on:'^' with
                                                                   | [] -> invalid_arg (v ^" is not a v^d")
                                                                   | ["1"] -> None
                                                                   | [x] -> Some(I.of_string x, 1)
                                                                   | ["1";_] -> None
                                                                   | [x;d] -> Some(I.of_string x, Int.of_string d)
                                                                   | _ -> invalid_arg (v ^ " is not of form v^d"))
      end

    let at p ~i =
      match MMap.find p (Monomial.of_list i) with
      | None -> Coef.zero
      | Some c -> c

    let to_list p = List.map (MMap.to_alist p) ~f:(fun (m, c) -> (c, Monomial.to_alist m))
    let terms p = List.map (MMap.to_alist p) ~f:(fun (m,_) -> Monomial.to_alist m)
    let all_terms ls = List.dedup_and_sort ~compare:Index.compare (List.concat_map ls ~f:terms)
    let to_mat ls =
      let terms = all_terms ls in
      List.transpose_exn (List.map ls ~f:(fun l -> List.map terms ~f:(fun i -> at l ~i:i)))

    let to_sparse_mat ls =
      let index_position = Index.Map.of_alist_exn (List.mapi (all_terms ls) ~f:(fun i x -> (x,i))) in
      List.concat_mapi ls
        ~f:(fun j l -> MMap.fold l ~init:[] ~f:(fun ~key:m ~data:c acc -> (Index.Map.find_exn index_position (Monomial.to_alist m), j ,c) :: acc))


    let length p = MMap.length p

    let inv (p : t) : t Option.t =
      if degree p = 0
      then let (_, c) = Map.min_elt_exn p in Option.map (Coef.inv c) ~f:const
      else None

    let inv_exn i =
      match inv i with
      | Some iinv -> iinv
      | None -> invalid_arg (to_string i ^ " is not invertible")

    let pow p n =
      if n >= 0
      then Fn.apply_n_times ~n:n (mult p) one
      else match inv p with
           | Some q -> Fn.apply_n_times ~n:(Int.abs n) (mult q) one
           | None -> invalid_arg ("Can't take negative power of: " ^ to_string p)



    let div_rem p d =
      let (d_lmono, d_lcoef) = Map.max_elt_exn d in
      let rec loop (p : poly) =
        if MMap.is_empty p (* p = 0 *)
        then (zero, zero)
        else
          let (p_lmono, p_lcoef) = Map.max_elt_exn p in
          let (coef, coef_rem) = Coef.div_rem p_lcoef d_lcoef in
          if not(Coef.equal coef_rem Coef.zero)
          then (zero, p)
          else
            let mono_quotient = Monomial.div p_lmono d_lmono  in
            if not(Monomial.is_positive  mono_quotient)
            then (zero, p)
            else
              let (q, r) = loop (add p (scale_mono mono_quotient (Coef.neg coef) d)) in
              (add q (MMap.singleton mono_quotient coef), r)
      in loop p

(*
    let ipoly (p: t) ~i =
      MMap.fold p ~init:Int.Map.empty ~f:(fun ~key:m ~data:c acc -> let ideg = match I.Map.find m i with Some d -> d | None -> 0 in
                                                                    let rem = MMap.singleton (Map.remove m i) c in
                                                                    Int.Map.update acc ideg ~f:(function None -> rem | Some q -> add q rem))
 *)

    let derivative _ = invalid_arg "NYI"
    (*       List.concat_map (to_list p) (fun (c,mono) -> List.init (List.length mono) (fun i -> (c* snd(List.nth_exn mono i),List.mapi mono (fun j (var,pow) -> if i = j then (var,pow-1) else (var,pow))))) *)


    let sexp_of_t = MMap.sexp_of_t (Coef.sexp_of_t)
    let t_of_sexp = MMap.t_of_sexp (Coef.t_of_sexp)
    let pp ppf poly = Format.fprintf ppf "%s" (to_string poly)

    include Comparable.Make(struct
                type t= poly
                let compare p1 p2 =
                  let rec loop opt1 opt2 =
                    match (opt1, opt2) with
                    | (None, None) -> Equal
                    | (None, Some _) ->  Less
                    | (Some _ , None) -> Greater
                    | (Some (m1,c1), Some (m2,c2)) ->
                       (match (Ordering.of_int(Monomial.compare m1 m2)) with
                        | Equal -> (match Ordering.of_int(R.compare c1 c2) with
                                    | Equal -> loop (MMap.closest_key p1 `Less_than m1) (MMap.closest_key p2 `Less_than m2)
                                    | o ->  o)
                        | o -> o
                       )
                  in loop (MMap.max_elt p1) (MMap.max_elt p2) |> Ordering.to_int
               let sexp_of_t = sexp_of_t let t_of_sexp = t_of_sexp end)

    let lead_reduction f l =
      match MMap.max_elt f with
      | None -> f
      | Some (fm, fc) ->
         let rec loop l =
           match l with
           | [] -> f
           | g::gs ->
              (match MMap.max_elt g with
               | None -> loop gs
               | Some (gm,gc) ->
                  let sm = Monomial.div fm gm in
                  let (sc,_) = R.div_rem fc gc in
                  if Monomial.is_positive sm
                  then sub f (scale_mono sm sc g)
                  else loop gs)
         in loop l
    let rec full_reduction f l =
      let g = lead_reduction f l in
      if equal f g
      then f
      else full_reduction g l

    let groebner ps =
      let rec loop ps pairs =
        let () = if Int.(List.length ps mod 10 = 0) then Out_channel.print_string "." in
        match pairs with
        | [] -> ps
        | (f1,f2):: pairs ->
           let s = syzgy f1 f2 in
           let red = full_reduction s ps in
           if equal red zero
           then loop ps pairs
           else loop (red::ps) (pairs @ List.map ps ~f:(fun x -> (x,red)))
      in loop ps (List.concat_mapi ps ~f:(fun i f -> List.map (List.drop ps i) ~f:(fun g -> (f,g))))

(*
    let gcd (p1: t) (p2:t) : t =
      let rec loop c psmall pbig =
(*      let () = Out_channel.print_endline (to_string psmall) in
        let () = Out_channel.print_endline (to_string pbig) in
        let () = Out_channel.print_endline (R.to_string c) in
 *)
        if equal psmall zero
        then  MMap.map pbig ~f:(fun a -> let (q,r) = Coef.div_rem a c in if Coef.equal r Coef.zero then q else invalid_arg "Divisoin doesn't work")
        else
          let lcsmall = leading psmall in
          (*           let lcbig = leading pbig in *)
          let () = (if Coef.equal lcsmall Coef.zero
              then invalid_arg (" WHAT: " ^ to_string pbig ^ "," ^ to_string psmall)
              else ()) in
          (* TODO: scale by gcd of leading terms instead of lcsmall *)
          let (_,r) = div_rem (scale R.one pbig) psmall in
          let () = if r > psmall then invalid_arg "gcd failed"  in
          loop c (*Coef.mult c lcsmall*) r psmall
      in loop Coef.one (min p1 p2) (max p1 p2)
 *)
    let t = MMap.singleton (Monomial.bot) R.one
    let t1 = sub (const R.one) t

    let lcm p1 p2 =
      let g = groebner [mult t p1; mult t1 p2] in
      List.hd_exn g
    let gcd p1 p2 =
      fst(div_rem (mult p1 p2) (lcm p1 p2))


    let standard_unit p =
      let (u,_) = R.standard_unit (leading p) in
      (const u, scale u p)
  end
