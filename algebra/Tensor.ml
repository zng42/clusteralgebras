(* open Core *)
module type S =
  sig
    type t
    module R : Ring.S
    module Base : RMod.S
    module Index : Index.S with type t = Base.Index.t list

    include RMod.S with type t := t with module R := R with module Index := Index

    val pp : Format.formatter -> t -> unit

    val primitive : R.t -> Index.t -> t
    val of_base : Base.t -> t

    val empty : t

    val tensor : t -> t -> t

    (* wedge p1 p2 of two primitive tensors results in
     *     p1 tensor p2                     if p1 < p2
     *     (-1)^len(p1) p2 tensor p1        if p1 > p2
     *     0                                if p1 = p2 and either is odd length *)
    val wedge : t -> t -> t


  end
