open Core

module Make (RMod : RMod.S) (*: Forms.S with module RMod = RMod *)=
  struct
    module RMod = RMod
    module R = RMod.R

    module BasisSet = Set.Make(RMod.Index)
    module CMap = Map.Make(BasisSet)
    type t = R.t CMap.t (* Invariant: This map should only contain keys corresponding to nonzero coefficients*)

    let sign (l : RMod.Index.t list) =
      let tails = List.mapi l ~f:(fun i -> fun x -> (x, List.drop l i) ) in
      let totals = List.map tails ~f:(fun (x, tail) -> List.count tail ~f:(fun y -> RMod.Index.compare x y > 0)) in
      if (List.fold totals ~init:0 ~f:(fun x -> fun y -> x + y)) mod 2  = 0
      then R.one
      else R.neg (R.one)
    let minusOneToN n = if n mod 2 = 0 then R.one else R.neg(R.one)

    let zero : t = CMap.empty

    let primitive (c : R.t) (b : RMod.Index.t list) =
      if List.contains_dup ~compare:RMod.Index.compare b || R.equal R.zero c
      then zero
      else CMap.singleton (BasisSet.of_list b) (R.mult (sign b) c)

    let add w1 w2 =
      CMap.merge w1 w2 ~f:(fun ~key:_ -> function `Left a -> Some a
                                           | `Right b -> Some b
                                           | `Both (a,b) -> let c = R.add a b in if R.equal R.zero c then None else Some c)

    let wedge w1 w2 =
      let pairs = List.cartesian_product (Map.to_alist w1) (Map.to_alist w2) in
      let combined = List.map pairs ~f:(fun ((b1,r1), (b2,r2)) -> primitive (R.mult r1 r2) (List.append (Set.to_list b1) (Set.to_list b2))) in
      List.fold combined ~init:zero ~f:add

    let scale r w =
      CMap.filter_map w ~f:(fun r2 -> let c = R.mult r r2 in if R.equal R.zero c then None else Some c)

    let primitive_apply ~f c l =
      let rec loop head tail =
        match tail with
        | [] -> []
        | [x] -> [wedge (primitive c head) (f x)]
        | x::xs ->
           (wedge (primitive c head) (wedge (f x) (primitive R.one xs))) :: (loop (head @ [x]) xs)
      in
      loop [] l

    let apply ~f (w:t) : t =
      CMap.fold w ~init:zero ~f:(fun ~key:iset ~data:c acc -> let ilist = Core.Set.to_list iset
                                                              in List.fold ~init:acc ~f:add (primitive_apply ~f:f c ilist))

    let to_string w =
      let primitive_to_string (s, r) =
        (R.to_string r)
        ^ if BasisSet.is_empty s
          then ""
          else "*" ^ String.concat ~sep:" ^ " (List.map (BasisSet.to_list s) ~f:(fun i ->  RMod.Index.to_string i)) in
      if CMap.is_empty w
      then R.to_string (R.zero)
      else String.concat ~sep:" + " (List.map (CMap.to_alist w) ~f:primitive_to_string)

    (* TODO: Add appropriate breaks *)
    let pp ppf v = Format.fprintf ppf "%s" (to_string v)
  end
