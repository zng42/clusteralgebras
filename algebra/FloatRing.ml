open Core

include Core.Float
let add i j = i +. j
let neg i = -. i
let sub i j = i -. j
let mult i j = i *. j
let inv i =
  match Core.Float.robustly_compare i 0.0 with
  | 0 -> None
  | _ -> Some (1.0 /. i)
let inv_exn i =
  match inv i with
  | Some iinv -> iinv
  | None -> invalid_arg (to_string i ^ " is not invertible")

let pow i n =
  match Ordering.of_int (Int.compare 0 n) with
  | Greater -> Fn.apply_n_times ~n:Int.(n-1) (mult i) i
  | Equal -> 1.0
  | Less ->
     (match inv i with
          | Some j -> Fn.apply_n_times ~n:Int.(abs n - 1) (mult j) j
          | None -> invalid_arg "Can't take negative power of 0")

let div_rem p d = (p /. d, 0.)
let gcd _ _ = 1.0
let lcm a b = a *. b
let standard_unit x = (1. /. x, 1.)

let pp = (fun ppf x -> Format.fprintf ppf "%s" (to_string x))
