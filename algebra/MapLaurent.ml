open Core
module Make
         (I : Index.S)
         (Coef : Ring.EuclideanDomain) : Laurent.S with type index = I.t with module R = Coef =
   struct
     module BasePoly =  MapPolynomial.Make (I) (Coef)
     include BasePoly
     module IMap = Core.Map.Make(I)

     let inv p =
       match to_list p with
       | [(c,m)] -> Option.map (Coef.inv c) ~f:(fun cinv -> of_mono cinv (List.map m ~f:(fun (i,d) -> (i,-d))))
       | _ -> None

     let denominator p =
       of_mono Coef.one (Core.Map.to_alist (IMap.of_alist_fold ~init:0 ~f:(fun acc d -> Int.max acc (-1 * d)) (List.concat (terms p))))
     let to_string p =
       let denom = denominator p in
       String.concat ["("; BasePoly.to_string (mult denom p) ; ")";"/"; "("; BasePoly.to_string denom ; ")"]


     let div_rem p d =
       let (p_denom, d_denom) = (denominator p, denominator d) in
       let (q,r) = BasePoly.div_rem (BasePoly.mult p_denom p) (BasePoly.mult d_denom d) in
       let new_denom = match inv p_denom with Some x -> x  | None -> invalid_arg "Denominator should always be a invertible monomial" in
       (mult (mult new_denom d_denom) q, mult new_denom r)


     let of_string _ = invalid_arg "Not implemented"
   end
