open Core
module MakeFinite (Dim : sig val dim : int end) (R : Ring.S) :
sig
  include RMod.S with module R = R and module Index = Int
  val of_alist : R.t list -> t
  val of_fun : (int -> R.t) -> t
  val dot : ?p:int -> t -> t-> R.t
  val pp : Format.formatter -> t -> unit
end =
  struct
    module R = R
    module Index = struct include Int let to_string i = "e" ^ Int.to_string i end
    type t = R.t list [@@deriving compare, sexp]
    let zero = List.map (List.range 0 Dim.dim) ~f:(fun _ -> R.zero)
    let of_fun f = List.map (List.range 0 Dim.dim) ~f:f
    let add v1 v2 = List.map2_exn v1 v2 ~f:R.add
    let sub v1 v2 = List.map2_exn v1 v2 ~f:R.sub

    let scale c v = List.map v ~f:(R.mult c)
    let at v ~i = List.nth_exn v i

    let to_string = List.to_string ~f:R.to_string
    let pp ppf v = Format.fprintf ppf "%s" (to_string v)
    let equal l1 l2 = List.for_all (List.map2_exn l1 l2 ~f:R.equal) ~f:(fun x -> x)

    let of_list l =
      let cmap = Int.Map.of_alist_exn (List.map ~f:(fun (r,i) -> (i,r)) l) in
      List.init Dim.dim ~f:(fun i -> match Map.find cmap i with None -> R.zero | Some r -> r)

    let to_list l = List.mapi l ~f:(fun i c -> (c,i))

    let terms l = List.filter_mapi l ~f:(fun i c -> if R.equal c R.zero then Some i else None)
    let all_terms ls = List.dedup_and_sort ~compare:Index.compare (List.concat_map ls ~f:terms)
    let to_mat ls =
      let terms = all_terms ls in
      List.transpose_exn (List.map ls ~f:(fun l -> List.map terms ~f:(fun i -> at l ~i:i)))

    let to_sparse_mat ls =
      let index_position = Index.Map.of_alist_exn (List.mapi (all_terms ls) ~f:(fun i x -> (x,i))) in
      List.concat_mapi ls ~f:(fun j l -> List.map (terms l) ~f:(fun index -> (Index.Map.find_exn index_position index, j ,at l ~i:index)))

    (* For any input list always returns list of length Dim.dim either by truncating or filling with R.zero *)
    let of_alist l =
      match Int.compare (List.length l) (Dim.dim) with
      | -1 -> List.append l (List.map (List.range 0 (Dim.dim - List.length l)) ~f:(fun _ -> R.zero))
      | 0 -> l
      | 1 -> List.take l Dim.dim
      | _ -> [] (* Should never happen as compare always returns -1,0,1 *)

    let map_basis ~f l = of_list (List.mapi l ~f:(fun i x -> (x, f i)))
    let mapi ~f (l: t) = List.mapi l ~f:f
    let filteri  ~f l = List.mapi l ~f:(fun i x -> if f i x then x else R.zero)

    let dot ?p:(p= Dim.dim) x y =
      let (pos,neg) =  List.split_n (List.map2_exn x y ~f:R.mult) p in
      R.sub (List.fold ~init:R.zero ~f:R.add pos) (List.fold ~init:R.zero ~f:R.add neg)
end


module Make (Index: Index.S) (R : Ring.S) : sig include RMod.S with module Index = Index and module R = R
                                                val pp : Format.formatter -> t -> unit end  =
  struct
    module R = R
    module Index = Index
    module IMap = Index.Map
    type t = R.t IMap.t [@@deriving compare, sexp]

    let equal = IMap.equal R.equal
    let to_string v =
      String.concat ~sep:" + " (List.map (Map.to_alist v) ~f:(fun (i,c) -> R.to_string c ^ "*" ^ Index.to_string i))

    let at v ~i =
      match Map.find v i with
      | Some r -> r
      | None -> R.zero

    let to_list v = List.map (IMap.to_alist v) ~f:(fun (i,c) -> (c,i))
    let terms v = List.map (IMap.to_alist v) ~f:(fun (i,_) -> i)
    let all_terms ls = List.dedup_and_sort ~compare:Index.compare (List.concat_map ls ~f:terms)
    let to_mat ls =
      let terms = all_terms ls in
      List.transpose_exn (List.map ls ~f:(fun l -> List.map terms ~f:(fun i -> at l ~i:i)))

    let to_sparse_mat ls =
      let index_position = Index.Map.of_alist_exn (List.mapi (all_terms ls) ~f:(fun i x -> (x,i))) in
      List.concat_mapi ls ~f:(fun j l -> IMap.fold l ~init:[] ~f:(fun ~key ~data acc -> (Index.Map.find_exn index_position key, j ,data) :: acc))



    let zero = IMap.empty
    let of_list l = IMap.filter ~f:(fun c -> not(R.equal c R.zero)) (IMap.of_alist_reduce ~f:R.add (List.map l ~f:(fun (c,i) -> (i,c))))

    let add v1 v2 =
      IMap.merge v1 v2 ~f:(fun ~key:_ -> function `Left a -> Some a
                                                | `Right b -> Some b
                                                | `Both(a,b) ->
                                                   let sum = R.add a b in
                                                   if R.equal R.zero sum then None else Some sum)

    let scale r v =
      if R.equal R.zero r
      then zero
      else IMap.map v ~f:(fun c -> R.mult c r)

    let sub v1 v2 = add v1 (scale (R.neg R.one) v2)
    let pp ppf v = Format.fprintf ppf "%s" (to_string v)

    let map_basis ~f l = of_list (List.map ~f:(fun (i,c) -> (c,f i)) (Map.to_alist l))
    let mapi ~f l = Map.mapi ~f:(fun ~key ~data  -> f key data) l
    let filteri ~f l = Map.filteri ~f:(fun ~key ~data -> f key data) l
  end
