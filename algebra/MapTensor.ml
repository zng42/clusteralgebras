open Core

module Make (RMod : RMod.S) : Tensor.S with module Base = RMod and module R = RMod.R =
  struct
    module R = RMod.R
    module Base = RMod
    module Index =
      struct
        module T =
          struct
            type t = Base.Index.t list [@@deriving sexp, compare]
          end
        include T
        include Comparable.Make(T)
        let to_string l = List.to_string ~f:Base.Index.to_string l
        let of_string s =
          List.map ~f:Base.Index.of_string
            (String.split ~on:' ' (String.strip ~drop:(function '(' -> true | ')' -> true | _ -> false) s))
      end

    (*     include FreeRMod.Make (IndexPrimitive) (RMod.R) *)
    type t = R.t Index.Map.t [@@deriving sexp,compare] (* Invariant: This map should only contain keys corresponding to nonzero coefficients*)
    let equal = Index.Map.equal R.equal
(*
    let sign (l : RMod.Index.t list) =
      let tails = List.mapi l ~f:(fun i -> fun x -> (x, List.drop l i) ) in
      let totals = List.map tails ~f:(fun (x, tail) -> List.count tail ~f:(fun y -> RMod.Index.compare x y > 0)) in
      if (List.fold totals ~init:0 ~f:(fun x -> fun y -> x + y)) mod 2  = 0
      then R.one
      else R.neg (R.one)
           *)
    let zero = Index.Map.empty
    let empty = Index.Map.singleton [] R.one

    let primitive (c : R.t) (b : RMod.Index.t list) =
      if R.equal R.zero c
      then zero
      else Index.Map.singleton b c


    let at v ~i =
      match Map.find v i with
      | Some r -> r
      | None -> R.zero

    let add w1 w2 =
      Index.Map.merge w1 w2 ~f:(fun ~key:_ -> function `Left a -> Some a
                                                     | `Right b -> Some b
                                                     | `Both (a,b) -> let c = R.add a b in if R.equal R.zero c then None else Some c)


    let of_base (rmod: RMod.t) =
      List.fold ~init:zero ~f: add (List.map (RMod.to_list rmod) ~f:(fun (c, index) -> primitive c [index]))

    let of_list l = Index.Map.filter ~f:(fun c -> not(R.equal c R.zero)) (Index.Map.of_alist_reduce ~f:R.add (List.map l ~f:(fun (c,i) -> (i,c))))

    let tensor w1 w2 =
      let pairs = List.cartesian_product (Map.to_alist w1) (Map.to_alist w2) in
      let combined = List.map pairs ~f:(fun ((b1,r1), (b2,r2)) -> primitive (R.mult r1 r2) (List.append b1 b2)) in
      List.fold combined ~init:zero ~f:add

    let wedge w1 w2 =
      let pairs = List.cartesian_product (Map.to_alist w1) (Map.to_alist w2) in
      let combined = List.map pairs ~f:(fun ((b1,r1), (b2,r2)) ->
                         match Ordering.of_int (List.compare RMod.Index.compare b1 b2) with
                         | Less -> primitive (R.mult r1 r2) (List.append b1 b2)
                         | Equal -> if (List.length b1 mod 2) = 0 && (List.length b2 mod 2) = 0
                                    then primitive (R.mult r1 r2) (List.append b1 b2)
                                    else zero
                         | Greater -> if (List.length b1 mod 2) = 0
                                      then primitive (R.mult r1 r2) (List.append b2 b1)
                                      else primitive (R.neg (R.mult r1 r2)) (List.append b2 b1)) in
      List.fold combined ~init:zero ~f:add

    let scale r w =
      Index.Map.filter_map w ~f:(fun r2 -> let c = R.mult r r2 in if R.equal R.zero c then None else Some c)
    let sub w1 w2 = add w1 (scale (R.neg R.one) w2)

    let to_string w =
      let primitive_to_string (s, r) =
        (R.to_string r)
        ^ if List.length s = 0
          then ""
          else "* " ^ String.concat ~sep:" @ " (List.map s ~f:(fun i ->  RMod.Index.to_string i)) in
      if Index.Map.is_empty w
      then R.to_string (R.zero)
      else String.concat ~sep:" + " (List.map (Index.Map.to_alist w) ~f:primitive_to_string)

    let map_basis ~f l = of_list (List.map ~f:(fun (i,c) -> (c,f i)) (Index.Map.to_alist l))
    let mapi ~f w = Index.Map.mapi w ~f:(fun ~key:index ~data:c -> f index c)
    let filteri ~f w = Index.Map.filteri w ~f:(fun ~key:index ~data:c -> f index c)

    let to_list v = List.map ~f:(fun (l,c) -> (c,l)) (Index.Map.to_alist v)
    let terms v = List.map ~f:(fun (l,_) -> l) (Index.Map.to_alist v)

    let all_terms ls = List.dedup_and_sort ~compare:Index.compare (List.concat_map ls ~f:terms)
    let to_mat ls =
      let terms = all_terms ls in
      List.transpose_exn (List.map ls ~f:(fun l -> List.map terms ~f:(fun i -> at l ~i:i)))

    let to_sparse_mat ls =
      let index_position = Index.Map.of_alist_exn (List.mapi (all_terms ls) ~f:(fun i x -> (x,i))) in
      List.concat_mapi ls ~f:(fun j l -> Index.Map.fold l ~init:[] ~f:(fun ~key ~data acc -> (Index.Map.find_exn index_position key, j ,data) :: acc))


    let pp ppf tensor =
      let pp_index index = String.concat ~sep:" @ " (List.map index ~f:RMod.Index.to_string) in
      match Map.to_alist tensor with
      | [] -> Format.fprintf ppf "<>"
      | (index,c):: tail ->
         let () = Format.fprintf ppf "@[%s@ *@ %s@]" (R.to_string c) (pp_index index)
         in List.iter tail ~f:(fun (index, c) -> Format.fprintf ppf "@ +@ @[%s@ *@ %s@]" (R.to_string c) (pp_index index))
  end
