open Core;;

  let sep = ' '
  
  module T =
    struct
      type t = int list [@@deriving sexp,compare]
      let to_string l = l |> List.map ~f:Int.to_string |> String.concat ~sep:(Char.to_string sep)
      let of_string s =
        s |> String.split ~on:sep |> List.map ~f:String.strip |> List.map ~f:Int.of_string
      
      let pp ppf l = Format.fprintf ppf "%s" (to_string l)
    end

  include T
  include Comparable.Make(T)

  let map2_pad ~pad ~f l1 l2 =
    if Int.(List.length l1 < List.length l2)
    then List.map2_exn ~f:f (l1 @ List.init (List.length l2-List.length l1) ~f:(fun _ -> pad)) l2
    else List.map2_exn ~f:f l1 (l2 @ List.init (List.length l1-List.length l2) ~f:(fun _ -> pad))
  
  let zero = [Int.max_value]
  let one = [0]
  let add = map2_pad ~pad:Int.max_value ~f:(Int.min)
  let mult = map2_pad ~pad:0 ~f:(fun x y -> x + y)
  let inv_exn = List.map ~f:(fun x -> -x)
  let inv l = Some(inv_exn l)
  let pow l i = List.map ~f:(fun x -> i * x) l


