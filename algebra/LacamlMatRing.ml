open Core
open Lacaml.D
let round10 = Float.round_decimal ~decimal_digits:10

module R = FloatRing
module Index =
  struct
    module T =
      struct
        type t = int*int [@@deriving compare, sexp]
      end
    include T
    include Comparable.Make(T)

    let to_string (i,j) = "e" ^ Int.to_string i ^"_" ^ Int.to_string j
    let of_string s =
      match String.split (String.drop_prefix s 1) ~on:'_' with
      | [i;j] -> (Int.of_string i, Int.of_string j)
      | _ -> invalid_arg (s ^ " is not ei_j")
  end


module T =
struct include Lacaml.D.Mat
  let compare m1 m2 = List.compare (List.compare (fun f1 f2 -> Float.(compare (round10 f1) (round10 f2)))) (Mat.to_list m1) (Mat.to_list m2)
  let sexp_of_t m = m |> Mat.to_list |> List.sexp_of_t (List.sexp_of_t Float.sexp_of_t)
  let t_of_sexp s = s |> List.t_of_sexp (List.t_of_sexp Float.t_of_sexp) |> Mat.of_list
end
include T

let copy m = Mat.of_list (Mat.to_list m)

let dim m = (dim1 m, dim2 m)
let row mat i = Mat.copy_row mat (i+1) |> Vec.to_list
let col mat i = List.nth_exn (Mat.to_col_vecs_list mat) (i) |> Vec.to_list
let rows mat = List.init (dim1 mat) ~f:(row mat)
let cols mat = mat |> Mat.to_col_vecs_list |> List.map ~f:Vec.to_list

let at m ~i:(i,j) = List.nth_exn (row m i) j


let of_rows rows = Mat.of_list rows
let of_cols cols = cols |> List.map ~f:Vec.of_list |> Mat.of_col_vecs_list
let init (m,n) ~f = Mat.init_rows m n (fun i j -> f (i-1) (j-1))
let mapi ~f m = init (dim m) ~f:(fun i j -> f (i,j) (at ~i:(i,j) m))
let map_basis ~f m = init (dim m) ~f:(fun i j -> at ~i:(f (i,j)) m)
let filteri ~f m = init (dim m) ~f:(fun i j -> if f (i,j) (at ~i:(i,j) m) then at ~i:(i,j) m else 0.0)


let to_list m =
  m
  |> rows
  |> List.concat_mapi ~f:(fun i -> List.mapi ~f:(fun j x -> (x,(i,j))))
  |> List.filter ~f:(fun (x,_) -> not(Float.equal (round10 x) 0.0))
let of_list l =
  let lookup = l |> List.map ~f:(fun (x,y) -> (y,x)) |> Index.Map.of_alist_exn in
  let m =
    match l |> List.map ~f:(fun (_,(i,_)) -> i) |> List.max_elt ~compare:Int.compare with
    | Some m -> m+1
    | None -> 0 in
  let n =
    match l |> List.map ~f:(fun (_,(_,j)) -> j) |> List.max_elt ~compare:Int.compare with
    | Some n -> n+1
    | None -> 0
  in
  init (m,n) ~f:(fun i j -> match Index.Map.find lookup (i,j) with Some x -> x | None -> 0.0)

let terms l = l |> to_list |> List.map ~f:snd
let all_terms l = l |> List.concat_map ~f:terms|> List.dedup_and_sort ~compare:Index.compare
let to_mat ls =
  let terms = all_terms ls in
  List.transpose_exn (List.map ls ~f:(fun l -> List.map terms ~f:(fun i -> at l ~i:i)))

let to_sparse_mat ls =
  let index_position = Index.Map.of_alist_exn (List.mapi (all_terms ls) ~f:(fun i x -> (x,i))) in
  List.concat_mapi ls ~f:(fun j l -> List.map (terms l) ~f:(fun index -> (Index.Map.find_exn index_position index, j ,at l ~i:index)))

let one n = Mat.identity n
let zero n= init (n,n) ~f:(fun _ _ -> 0.0)

let mult m1 m2 = gemm m1 m2
let add m1 m2 = add m1 m2
let neg m = neg m
let sub m1 m2 = sub m1 m2
let pow m i =
  match i with
  | 0 -> one (fst (dim m))
  | _ -> Fn.apply_n_times ~n:(i-1) (mult m) m
let scale c m =
  let scaled = copy m in
  let () = Mat.scal c scaled in
  scaled

let to_string m =
  m |> Mat.to_list
  |> List.map ~f:(List.map ~f:(fun x -> round10 x))
  |> (fun mat -> "{" ^ String.concat ~sep:"\n" (List.map mat ~f:(fun row -> String.concat ~sep:", " (List.map row ~f:Float.to_string_12))) ^"}\n")

let id m =
  m |> Mat.to_list
  |> List.map ~f:(List.map ~f:(fun x -> round10 x))
  |> (fun mat -> "{" ^ String.concat ~sep:"," (List.map mat ~f:(fun row -> "{"^String.concat ~sep:"," (List.map row ~f:Float.to_string_12)^"}")) ^"}")
let transpose m = Mat.transpose_copy m

let trace m = m |> Mat.copy_diag |> Vec.sum

let det m =
  let mat = copy m  in
  let tau = geqrf mat in
  let ones = Vec.init (Vec.dim tau) (fun _ -> 1.)
  in (Vec.prod(Vec.signum (Vec.sub tau ones))) *. Vec.prod (Mat.copy_diag mat)

let minor ~m mat = Mat.of_col_vecs_list (List.map m ~f:(Mat.col mat))
let det_minor ~l mat = det (minor ~m:l mat)


let inv m =
  match Float.(robustly_compare (abs(det m))) 0.0 |> Ordering.of_int with
  | Less -> None
  | Equal -> None
  | _ -> let minv = copy m in
         let () = getri minv in
         Some(minv)

let inv_exn i =
  match inv i with
  | Some iinv -> iinv
  | None -> invalid_arg (to_string i ^ " is not invertible")

let of_string s = s |> String.split_on_chars ~on:['{';'}']
      |> List.map ~f:(String.strip ~drop:(function ','-> true | c -> Char.is_whitespace c))
      |> List.filter ~f:(Fn.non String.is_empty)
      |> List.map ~f:(fun r -> r |> String.split ~on:',' |> List.map ~f:(fun v -> v |> String.strip |> Float.of_string))
      |> Mat.of_list


let nullspace ?threshold:(threshold = 0.0000001) m =
  let (sigma,_,vt) = gesvd (Mat.of_col_vecs (Mat.to_col_vecs m)) in
  List.drop (Mat.to_list vt) (List.count (Vec.to_list sigma) ~f:(fun s -> Float.(abs s > threshold)))

let solve ?threshold:(threshold = 0.0000001) _(*m*) _(*b*) = invalid_arg ("NYI"  ^ Float.to_string threshold)
(*   let epsilon = threshold *. threshold in
  let num_valid_rows = Mat.dim2 m in
  let target = Mat.of_col_vecs(Mat.to_col_vecs b)
  let rank = gelsd (Mat.of_col_vecs (Mat.to_col_vecs m))  target in
  let () = if rank < num_valid_rows
           then Out_channel.output_string stdout ("mA is underdetermined with rank " ^ Int.to_string rank ^ " < " ^ Int.to_string num_valid_rows)
           else () in
  List.filter_mapi (Mat.to_col_vecs_list target) ~f:(fun i v -> let error = Vec.sum (Vec.sqr ~ofsx:(num_valid_rows+1) v)  in
                                                            if Float.(error <= epsilon) then Some (i,error) else None)
  *)
let pp = pp_mat
include Comparable.Make(T)
