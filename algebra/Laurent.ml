module type S =
  sig
    include Polynomial.S
    (* Returns the monomial, m such that m*p is a polynomial
     *   i.e. all the variables to their highest negative powers *)
    val denominator : t -> t
  end
