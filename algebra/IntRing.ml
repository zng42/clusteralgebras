open Core
include Core.Int
let add i j = i + j
let sub i j = i - j
let neg i = -i
let mult i j = i * j
let inv i =
  match i with
  | 1 -> Some 1
  | -1 -> Some (-1)
  | _ -> None

let inv_exn i =
  match inv i with
  | Some iinv -> iinv
  | None -> invalid_arg (to_string i ^ " is not invertible")

let div_rem p d = (p / d, p mod d)
let gcd a b =
  let rec loop x y =
    if x = 0 then y
    else loop (y mod x) x
  in if a < b then loop a b else loop b a
let lcm a b = (a / (gcd a b)) *b
let standard_unit x =
  if x > 0
  then (1,x)
  else (-1, -1*x)
let pp = (fun ppf x -> Format.fprintf ppf "%s" (to_string x))
