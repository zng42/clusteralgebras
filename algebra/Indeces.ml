open Core

module Single =
  struct
    module T =
      struct
        type t = unit [@@deriving compare,sexp]
      end
    include T
    include Comparable.Make(T)
    let to_string () = "t"
    let of_string _ = ()
  end

module IntPair =
  struct
    module T =
      struct
        type t = int*int [@@deriving compare, sexp]
      end
    include T
    include Comparable.Make(T)
    let to_string (i,j) = "x" ^ Int.to_string i ^"_" ^ Int.to_string j
    let of_string s =
      match String.split (String.drop_prefix s 1) ~on:'_' with
      | [i;j] -> (Int.of_string i, Int.of_string j)
      | _ -> invalid_arg (s ^ " is not xi_j")
  end

module Letter =
  struct
    module T =
      struct
        type t = int [@@deriving compare, sexp]
      end
    include T
    include Comparable.Make(T)
    let to_string i = String.of_char(Char.of_int_exn (65+i))
    let of_string s= Char.to_int(Char.of_string s)-65
  end

module LetterSubscript =
  struct
    module T =
      struct
        type t = int * (int option) [@@deriving compare, sexp]
      end
    include T
    include Comparable.Make(T)
    let to_string (i,j) =
      match j with
      |Some j -> String.of_char(Char.of_int_exn (65+i)) ^ Int.to_string j
      | None -> String.of_char(Char.of_int_exn (65+i))

    let of_string s=
      match String.to_list s with
      | [] -> invalid_arg "Index should be character followed by a number"
      | [c] -> (Char.to_int c - 65, None)
      | c :: cs -> (Char.to_int(c)-65, Some(Int.of_string(String.of_char_list cs)))

  end

module MakeFinite(Arg:sig val names: string list end) : Index.S with type t = string =
  struct
    let err = "UNKOWN"
    let () (*ban_unknown*) =
      if List.mem Arg.names ~equal:String.equal err
      then invalid_arg ("Can't use the name: "^ err)
      else ()

    let ord = Arg.names |> List.mapi ~f:(fun i s -> (s,i)) |> String.Map.of_alist_exn
    let lookup n =
      match Map.find ord n with
      | Some i -> i
      | None -> List.length Arg.names + 1


    module T =
      struct
        type t= string [@@deriving sexp]
        let compare n1 n2 = Int.compare (lookup n1) (lookup n2)
      end


    let to_string s = s
    let of_string n =
      match Map.find ord n with
      | Some _ -> n
      | None -> invalid_arg (n ^ " is not  a valid name " )
    include T
    include Comparable.Make(T)
  end
