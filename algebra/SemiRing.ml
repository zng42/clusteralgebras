open Core;;

module type S =
  sig
    type t
    val t_of_sexp : Sexplib.Sexp.t -> t
    val sexp_of_t : t -> Sexplib.Sexp.t

    include Comparable.S with type t := t
    val to_string : t -> string
    val of_string : string -> t
    val pp : Format.formatter -> t -> unit

    val zero : t
    val one : t
    val add : t -> t -> t
    val mult : t -> t -> t
    val inv : t -> t Option.t
    val inv_exn : t -> t

    val pow : t -> int -> t
  end
