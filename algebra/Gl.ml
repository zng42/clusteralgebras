module Make (M: Matrix.S) (Dim: sig val dim : int end) :
sig
  include Ring.S with type t = M.t
  val transpose : t -> t
  val id : t -> string
end =
  struct
    include M

    let one = M.one Dim.dim
    let zero = M.zero Dim.dim
  end
