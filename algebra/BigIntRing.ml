open Core

  module SZ =
    struct
      include Z
      let sexp_of_t x = Sexp.Atom(Z.to_string x)
      let t_of_sexp s =
        match s with
        |  Sexp.Atom s -> Z.of_string s
        | Sexp.List _ -> of_sexp_error "BigInt.of_sexp: atom needed" s
    end
  include SZ
  let pp = pp_print
  let mult = mul
  let inv x =
    if equal (abs x) (of_int 1)
    then Some x
    else None
  let inv_exn i =
    match inv i with
    | Some iinv -> iinv
    | None -> invalid_arg (to_string i ^ " is not invertible")

  let standard_unit x  =
    if geq x (of_int 0)
    then (of_int 1, x)
    else (of_int (-1),neg x)
  include Comparable.Make(SZ)
