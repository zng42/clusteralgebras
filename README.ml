open Algebra
(*** Check if Test directory  exists and create it if not ***)
let () = match Caml.Sys.file_exists "./TestOutput" && Caml.Sys.is_directory "./TestOutput" with
  | true -> ()
  | false -> Caml.Sys.mkdir "./TestOutput" 764

(*** Steps to create a framed quiver and search all clusters with cVectors with entries less than 2 ***)
  let cluster = Fixed.Clust.of_edgelist ~graph:[(1,2);(2,3);(3,1)] ~frozen:(Int.Set.of_list []);;
  let a2affine = Fixed.Frame.graph (Fixed.Clust.frame cluster)
                   ~cond:(fun (c,frame) inv -> List.for_all frame ~f:(fun l -> List.for_all l ~f:(fun i -> Int.abs i <= 2)));;


(*** Steps to find all clusters in gr(3,8) with weight less than or equal to  2 and write to a file ***)
  let cluster38 = Partition.gr 3 8;;
  let gr38_weight2 =
    Partition.CSearch.graph cluster38 ~cond:(fun c inv -> Partition.Clust.for_all c ~f:(fun v -> Partition.Vert.weight v <= 2));;
  let () = Partition.CSearch.write_graphviz "./TestOutput/gr38_weight2.gv"
             ~node_color:(fun c -> Set.length(Partition.Clust.sinks c) + Set.length(Partition.Clust.sources c))
             (Partition.CSearch.induced gr38_weight2);;


(*** Steps to find a two colorable cluster in gr(4,8) ***)
  let cluster48 = Partition.gr 4 8;;
  let Some cluster_twocolor = Partition.CSearch.rand_search 5000 (fun c -> Option.is_some(Partition.Clust.two_color c))
                                ~cond:(fun (c,v) -> Partition.Clust.for_all c ~f:(fun v -> PartVertex.weight v <= 1))
                                cluster48;;
  let () = Partition.Clust.write_clav "./TestOutput/cluster48_twocolor" cluster_twocolor;;
  let () = Partition.Clust.write_cluster "./TestOutput/cluster48_twocolor" cluster_twocolor;;

(*** Steps to color clusters in gr(2,6) by isomorphism class ***)
  let gr26 = Plucker.CSearch.graph (Plucker.gr 2 6);;
  let iso_class = Set.group_by (Plucker.Clust.Set.of_list (Map.keys gr26)) ~equiv:(Plucker.Clust.is_isomorphic ~directed:true) ;;
  (* Option if you have every cluster in the groups you want *)
  let _:int =   Plucker.CSearch.write_picture "./TestOutput/gr26_isoclass.gv" ~node_color:(Color.by_group ~groups:iso_class ~found:Set.mem) gr26 ;;
  (* Option if you have a list of representatives *)
  let rep_list = List.map iso_class ~f:Set.min_elt_exn;;
  let _:int =   Plucker.CSearch.write_picture "./TestOutput/gr26_isoclass.gv"
               ~node_color:(Color.by_group ~groups:rep_list ~found:(Plucker.Clust.is_isomorphic ~directed:true))
               gr26;;

(*** Steps to fold D4 to create a G2 associhedron ***)
  let clusterD4 = Fixed.Clust.of_edgelist ~graph:[(1,2);(1,3);(1,4)] ~frozen:Int.Set.empty
  let g2 = Fixed.FoldSearch.graph (fst(Fixed.Clust.frame clusterD4), [[1];[2;3;4]])

(*** Steps to find all isomorphism classes in finite mutation type but not finite cluster algebra  ***)
  let qA13affine = Fixed.Clust.qAaffine 1 3
  let (iso_classA13,_) = Fixed.CSearch.graph_collect ~init:[]
                           ~add:(fun l v -> v::l)
                           ~mem:(fun l c -> List.exists l ~f:(Fixed.Clust.is_isomorphic c)) 
                           qA13affine  

(*** Calculate all the automorphisms of a cluster and create a picture coloring vertices by orbit ***)
let clusterD4double = Fixed.Clust.of_edgelist ~frozen:Int.Set.empty ~graph:[(1,2);(2,4);(4,1);(2,5);(5,1);(2,6);(6,1);(1,3);(3,2)]
let autos = Fixed.Clust.automorphisms clusterD4double
let orbits = Set.group_by (Fixed.Clust.unfrozen clusterD4double) ~equiv:(fun v1 v2 -> List.exists autos ~f:(fun f -> Fixed.Vert.equal (f v1) v2));;
let () = Fixed.Clust.write_cluster "./TestOutput/clusterD4double" clusterD4double ~node_color:(Color.by_group ~groups:orbits ~found:Set.mem)

(*** Compute a red to green sequence for a given cluster ***)
let qcyc = Fixed.Clust.of_edgelist ~graph:[(1,2);(2,3);(3,4);(4,5);(5,6);(6,1)] ~frozen:Int.Set.empty |> Fixed.Clust.frame |> fst
let true = Fixed.Clust.is_red qcyc
let Some redToGreen = Fixed.CSearch.shortest [qcyc] Fixed.Clust.is_green

(*** Find all elements of cluster modular group in a certain distance ***)
let iclusterD4 = Partition.IClust.index (Partition.gr 3 6)
let d4_group_gonch = Partition.ModG.modular_group ~cond:(fun c v -> true) iclusterD4
let d4_allgonch = List.map d4_group_gonch ~f:(fun g -> Partition.ModG.apply iclusterD4 g)
let d4_allgonch_unindexed = List.map d4_allgonch ~f:Partition.IClust.un_index

(*** Compute Cluster Modular Group Complex From Given Generators ***)
let clusterA5affine23 = Fixed.Clust.qAaffine 2 3
let group_gen = [([5],Perm.of_cycles [[5;1]]) ; ([2;1;5],Perm.of_cycles [[1;2;5]]) ; ([4;3;5;1],Perm.of_cycles [[1;5;3]])]
let group = group_gen @ List.map ~f:ModularGroup.inv group_gen
let a5_23 = Fixed.GroupSearch.graph ~max_dist:10 (clusterA5affine23, group)

(*** Work With Cluster Modular Group for gr48/ E7double***)
let vorder = [ [[1;1;1;1];[1;1;1;0];[1;1;1;0];[0;0;0;0]] ; [[2;2;1;1];[2;2;0;0];[2;1;0;0];[1;0;0;0]] ; [[2;2;1;1];[2;2;1;0];[2;1;0;0];[1;0;0;0]]
               ; [[2;2;1;1];[2;2;1;1];[2;1;0;0];[1;0;0;0]] ; [[1;1;0;0];[1;1;0;0];[1;1;0;0];[1;0;0;0]] ; [[2;2;2;1];[2;2;1;0];[2;1;1;0];[1;0;0;0]]
               ; [[1;1;1;1];[1;1;0;0];[1;0;0;0];[1;0;0;0]] ; [[1;0;0;0];[0;0;0;0];[0;0;0;0];[0;0;0;0]] ;[[1;1;1;1];[1;1;0;0];[1;1;0;0];[0;0;0;0]]]
let qStart = Partition.CSearch.stoch_find_cond 5000 ~cond:(fun v -> Partition.Vert.weight v <= 2) (Partition.Vert.Set.of_list vorder) (Partition.gr 4 8)
let iqStart = Partition.IClust.index ~vlist:vorder qStart
let gp = ([3;1;2;4;5], Perm.of_cycles [[1;3;2]])
let gq = ([6;1;2;7;8], Perm.of_cycles [[1;6;2]])
let gr = ([9;1;2], Perm.of_cycles [[1;9;2]])
let gX = ([], Perm.of_cycles [[3;6];[4;7];[5;8]])
let gY = ([3; 1; 2; 4; 5; 2; 3; 1; 4; 5; 9; 3; 2], Perm.of_cycles [[1;3];[2;9]])
let gZ = ([3; 1; 2; 4; 5; 8; 7; 6; 1; 3], Perm.of_cycles [[2;6];[4;7];[5;8]])
let gRhot = ([6; 8; 3; 4; 9; 1; 2; 7; 2; 3; 6; 8; 9; 5], Perm.of_cycles [[1;2;7];[3;8;9];[4;6;5]]) (* <2,7,8,6,4,5,1,9,3>*)
let gTheta = ([6; 8; 3; 4; 9; 1; 2; 9; 8; 3; 2; 7; 9; 5; 8; 3; 6], Perm.of_cycles [[1;7];[4;5];[9;8]]) (* <7,2,3,5,4,6,1,9,8>*)
let gIota = ([5; 4; 5; 8; 7; 8], Perm.of_cycles [[1;2]])
let gAlpha = ([1], Perm.of_cycles [[1;2]])
let gBeta = (Fn.apply_n_times ~n:12 (List.append [9;3;4;5;6;7;8;2]) [] @ [2], Perm.of_cycles [[1;2]])

let mobiusKantor = Partition.GroupSearch.graph (iqStart, [gX;gY;gZ])

(*** Find gR by conjugating rotation of a grid cluster ***)
let  iqgrid = Partition.ISearch.rand_search 50000
                ~cond:(fun (c, v) -> Partition.IClust.for_all c ~f:(fun v -> Partition.IClust.Vertex.weight v <= 2))
                (fun c -> Partition.IClust.rank_pi1 c = 4
                          && Partition.IClust.count ~frozen:false ~f:(fun v -> Poly.(Partition.IClust.degree ~frozen:false c v = (1,-1))) c = 4
                          && Partition.IClust.count c ~frozen:false ~f:(fun v -> Poly.(Partition.IClust.degree ~frozen:false c v = (2,-1)) ) = 2
                          && Partition.IClust.count c ~frozen:false ~f:(fun v -> Poly.(Partition.IClust.degree ~frozen:false c v = (1,-2)) ) = 2
                          && Partition.IClust.count c ~frozen:false ~f:(fun v -> Poly.(Partition.IClust.degree ~frozen:false c v = (1,-1)) ) = 4
                          && Option.is_some(Partition.IClust.two_color c) )
                (iqStart)
              |> Option.value_exn
let (_,path_grid) = Partition.ISearch.shortest [iqStart] (Partition.IClust.is_isomorphic iqgrid) |> Option.value_exn
let gGrid = (List.map ~f:(fun v -> Partition.IClust.vertex_to_i v) path_grid,Perm.one)
let (c1,c2) = Partition.IClust.two_color (Partition.ModG.apply iqStart gGrid) |> Option.value_exn
let gRpre = (List.map c2 ~f:Partition.IClust.vertex_to_i @ List.map c1 ~f:Partition.IClust.vertex_to_i, Perm.one)
let gR = ModularGroup.conjugate ~by:gGrid gRpre

(*** Find gR as a product of gX,gY,gZ, gRhot, gIota, and gTheta ***)
let name_group =
  let groupMap = ModularGroup.Map.of_alist_exn [(gp,"gp");(gq,"gq");(gr,"gr");
                                                    (gX,"gX"); (gY,"gY"); (gZ,"gZ");
                                                    (gRhot, "gRhot"); (gTheta, "gTheta"); (gIota, "gIota");
                                                    (gAlpha, "gAlpha"); (gBeta, "gBeta")] in
  (fun g -> match Map.find groupMap g with Some s -> s | None -> "Unknown " ^ ModularGroup.to_string g)
let targetR = Partition.ModG.apply iqStart gR
let rPath = Partition.GroupSearch.shortest [iqStart, [gX;gY;gZ;gRhot;gIota;gTheta]] (fun (c,_) -> Partition.IClust.equal c targetR) |> Option.value_exn |> snd
let rPath_named = List.map rPath ~f:name_group


(*** Creating Folded Cluster Algebras ***)
module PolyRing = Flint.BigIntMPoly.Make(struct let vars = List.init 6 ~f:(fun i -> "a"^Int.to_string (i+1)) let ord = Flint.BigIntMPoly.Ord.LEX end)
module PolyRat = Rationals.Make(PolyRing)
module PolyVert = RingVertex.Make(PolyRat)
module PolyClust = IMapCluster.Make(PolyVert)
module PolyCSearch = SearchBase.Make(PolyClust)
let qE6hat = PolyClust.of_edgelist ~graph:[(1,3); 3,2; 2,4; 4,1; 1,5; 2,6; 3,7; 4,8;] ~frozen:(Int.Set.empty) ~label:([1,"a1";2,"a1";3,"a2";4,"a2";5,"a3"; 6,"a3"; 7,"a4"; 8,"a4";]|> List.map ~f:(fun (i,s) -> (i, PolyRat.of_string s)))
let qE7hat = PolyClust.of_edgelist ~graph:[(1,3); 3,2; 2,4; 4,1; 1,5; 2,6; 3,7; 4,8; 9,7; 10,8;] ~frozen:(Int.Set.empty) ~label:([1,"a1";2,"a1";3,"a2";4,"a2";5,"a3"; 6,"a3"; 7,"a4"; 8,"a4"; 9,"a5"; 10,"a5" ]|> List.map ~f:(fun (i,s) -> (i, PolyRat.of_string s)))
let qE8hat = PolyClust.of_edgelist ~graph:[(1,3); 3,2; 2,4; 4,1; 1,5; 2,6; 3,7; 4,8; 9,7; 10,8; 9,11; 10,12] ~frozen:(Int.Set.empty) ~label:([1,"a1";2,"a1";3,"a2";4,"a2";5,"a3"; 6,"a3"; 7,"a4"; 8,"a4"; 9,"a5"; 10,"a5"; 11,"a6";12,"a6" ]|> List.map ~f:(fun (i,s) -> (i, PolyRat.of_string s)))
