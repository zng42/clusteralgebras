include Polynomial.S
val create_from_fun : (C.Function.Fmpz_poly.t -> unit) -> t
val create_from_fun_extra : (C.Function.Fmpz_poly.t -> 'a) -> ('a*t)
val const_ptr : t -> C.Function.Fmpz_poly.t
