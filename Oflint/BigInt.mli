include Ring.EuclideanDomain
val create_from_fun : (C.Function.Fmpz.t -> unit) -> t
val create_from_fun_extra : (C.Function.Fmpz.t -> 'a) -> ('a*t)
val create_from_funB : ?error:string -> (C.Function.Fmpz.t -> bool) -> t
val const_ptr : t -> C.Function.Fmpz.t
val of_int : int -> t
val is_pm1 : t -> bool
