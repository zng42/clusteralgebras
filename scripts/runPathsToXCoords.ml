open Partition

let n = 6
let iqDn =
  Sexp.load_sexp_conv_exn "./_Clusters/D10/startDynkin" Clust.t_of_sexp
  |> (fun c -> Clust.freeze c (["p1389";"p1379";"p2379";"p2349"] |> List.map ~f:Plucker.Vert.of_string
                               |>  List.map ~f:(Vert.of_plucker 9)))
  |> IClust.index ~vlist:(["p5679";"p4789";"p4679";"p3467";"p3457";"p3459";"p2349";"p2379";"p1379";"p1389"]
                          |> List.map ~f:Plucker.Vert.of_string
                          |> List.map ~f:(Vert.of_plucker 9))
  (* gr 3 6 |> CSearch.graph |> Map.keys |> List.filter ~f:Clust.fAffineDynkin |> List.hd_exn
           |> fun c -> IClust.index ~vlist:((c |> Clust.sources |> Set.to_list) @ (c |> Clust.sinks |> Set.to_list)) c
   *)


let aN = CSearch.graph (gr 2 (n+2))
let iqAn = aN |> Map.keys |> List.filter ~f:Clust.fAffineDynkin |> List.hd_exn
           |> (fun c -> IClust.index ~vlist:(c |> Clust.unfrozen
                                             |> Set.to_list
                                             |> List.map ~f:(Clust.tail c)
                                             |> List.filter ~f:(Fn.non List.is_empty)
                                             |> List.hd_exn)
                          c)

let aN_xcoords =
  aN |> CSearch.all_xcoords
  |> Set.to_map ~f:(fun x -> ISearch.shortest [iqAn] (fun c -> c |> IClust.un_index |> CSearch.xcoords|> fun s -> Set.mem s x)
                             |> Option.value_exn
                             |> (fun (ic,p) -> (ic |> IClust.unfrozen
                                                |> Set.find_exn ~f:(fun iv -> CSearch.xcoord (IClust.un_index ic) (IClust.vertex_to_v iv)
                                                                              |>  XVert.equal x))
                                               |> IClust.vertex_to_i
                                             , p |> List.map ~f:fst |> List.map ~f:IClust.vertex_to_i))

 
let tails =
  iqDn |> IClust.unfrozen
  |> Set.to_list
  |> List.map ~f:(fun v -> (v,IClust.tail iqDn v))
  |> List.filter ~f:(fun (_,l) -> List.length l = 2)
  |> List.map ~f:(fun (v,_) -> IClust.vertex_to_i v)

let modg = ((iqDn |> IClust.sources |> Set.to_list) @ (iqDn |> IClust.sinks |> Set.to_list)
            |> List.map ~f:IClust.vertex_to_i, Perm.of_cycles [[List.hd_exn tails;List.last_exn tails]])
let dynkins = List.fold ~init:[iqDn] ~f:(fun acc _ -> ModG.apply (List.hd_exn acc) modg ::acc) (List.range 1 n) |> List.rev

let extract_map iqDn i = 
  let iso = IClust.isomorphism iqAn (IClust.freeze iqDn [IClust.at_ivertex_exn iqDn i]) |> Option.value_exn in
  let iso_nice i = i |> IClust.at_ivertex_exn iqAn |> iso |> IClust.vertex_to_i in
  (IClust.at_exn iqDn i
  , aN_xcoords |> Map.map ~f:(fun (j,path) -> IClust.mutateI_multi iqDn (List.map ~f:iso_nice path)
                                              |> fun c -> CSearch.xcoord (IClust.un_index c) (IClust.at_exn c (iso_nice j))))  

let maps =  dynkins |> List.map ~f:(fun iqDn -> tails |> List.map ~f:(extract_map iqDn)) |> List.transpose_exn

let () =  maps |> List.iteri ~f:(fun tail convs ->
                      convs |> List.iteri ~f:(fun i (v,conv)-> conv |> Map.to_alist
                                                             |> List.map ~f:(fun (x,y) -> XVert.to_string x ^ " , " ^ XVert.to_string y)
                                                             |> Out_channel.write_lines
                                                                  ("./_Clusters/D"^Int.to_string n^"/xconversion"
                                                                   ^ Int.to_string i
                                                                   ^ (if Int.equal tail 0 then  "A" else "B")
                                                                   ^ "_" ^ Vert.to_string v)))
                                                                                                                   
