#require "Glpk"
#use "scripts/RunGr48verts.ml"

let aconv_rev8 = aconv_rev "./_Clusters/gr48/Framed/gr48_a8_amap.txt"
let xpairs = "./_Clusters/gr48/Framed/gr48_a8_xpairs.txt" |> In_channel.read_lines |> List.map ~f:string_to_xpair |> List.map ~f:(fun ((xT,xB),(x1pT,x1pB)) -> (Partition.XVert.of_list ~top:(List.map ~f:aconv_rev8 xT) ~bot:(List.map ~f:aconv_rev8 xB), Partition.XVert.of_list ~top:(List.map ~f:aconv_rev8 x1pT) ~bot:(List.map ~f:aconv_rev8 x1pB)))
let xP1 = let map = Partition.XVert.Map.of_alist_exn xpairs in (fun x -> Map.find_exn map x)
let xPlucker = xpairs |> List.map ~f:fst |> List.filter ~f:(fun x -> (Partition.XVert.top_list x @ Partition.XVert.bot_list x) |> List.for_all ~f:(fun a -> Partition.Vert.weight a =1))
let a48_big = xPlucker |> List.concat_map ~f:(fun x -> [x;xP1 x]) |> List.concat_map ~f:Partition.XVert.pow_list |> List.map ~f:fst |> List.dedup_and_sort ~compare:(fun v w -> match Ordering.of_int (Int.compare (Partition.Vert.weight v) (Partition.Vert.weight w)) with Less -> -1 | Equal -> if Partition.Vert.weight v = 1 then Plucker.Vert.compare (PartVertex.to_plucker 4 8 v) (PartVertex.to_plucker 4 8 w) else Partition.Vert.compare v w | Greater -> 1)
let a48_big_set = Partition.Vert.Set.of_list a48_big
let xBndedBig = xpairs |> List.map ~f:snd  |> List.filter ~f:(Partition.XVert.for_all ~f:(fun _ -> Set.mem a48_big_set)) |> List.map ~f:Partition.XVert.inv


let find_min_prod xs vs =
  let constrs = xs |> List.map ~f:(fun x -> List.map vs ~f:(fun a -> a |> Partition.XVert.exponent_of x |> Float.of_int))
                |> List.transpose_exn
                |> List.map ~f:Array.of_list |> Array.of_list in
  let xbounds = List.map xs ~f:(fun _ -> (0.0,1.0 /. 0.0)) |> Array.of_list in
  let zcoefs = List.map xs ~f:(fun _ -> 1.0) |> Array.of_list in
  (fun target ->
    let pbounds = List.map vs ~f:(Partition.XVert.exponent_of target) |> List.map ~f:(fun a -> (Float.of_int a , Float.of_int a)) |> Array.of_list in
    let prob = Glpk.make_problem Glpk.Minimize zcoefs constrs pbounds xbounds in
    let () = Glpk.use_presolver prob true in
    let () = Glpk.simplex prob in
    prob |> Glpk.get_col_primals |> Array.to_list |> List.zip_exn xs |> List.filter ~f:(fun (_,c) -> not(Float.equal c 0.0)))
