(*
open Gr36
let q = gr 3 6
let foldername = "Gr36"
let subName = "plucker"
let subsetQ v = Vert.weight v |> Int.equal 1
                                 *)

(*
open Gr37
let q = gr 3 7
let foldername = "Gr37"
let subName = "plucker"
let subsetQ v = Vert.weight v |> Int.equal 1
*)

(*
open Gr38
let q = gr 3 8
let foldername = "Gr38"
let subName = "plucker"
let subsetQ v = Vert.weight v |> Int.equal 1
 *)

(*
open Gr38
let q = gr 3 8
let foldername = "Gr38"
let subName = "weight2"
let subsetQ v = Vert.weight v |> (fun i -> i <= 2)
*)

let graph = CSearch.graph q 
let dynkins =
  graph |> Map.keys |> List.find_exn ~f:(Clust.is_SourceSink)
  |> (fun c -> Clust.follow_sources_path c 500)
  |> snd

(* let vars = dynkins |> List.map ~f:(Clust.sources) |> List.map ~f:Set.to_list |> List.concat*)
let allvars = vars @ (q |> Clust.frozen |> Set.to_list)
let checkvars = allvars |> List.filter ~f:(Fn.non subsetQ)


let uvars = dynkins
            |> List.map ~f:(fun c -> c |> Clust.sources |> Set.to_list
                                     |> List.map ~f:(fun v -> (v,CSearch.xpair c v |> (fun (y,yp1) -> XVert.mult y (XVert.inv yp1)))))
            |> List.concat
            |> Vert.Map.of_alist_exn

let makemat () = 
  vars |> List.map ~f:(fun v -> (v, Map.find_exn uvars v))
  |> List.map ~f:(fun (v,u) -> (v,checkvars |> List.map ~f:(XVert.exponent_of u)))
  |> List.filter_map ~f:(fun (v,l) -> if List.for_all l ~f:(Int.equal 0) then None else Some (v,l))
  |> List.unzip
  |> fun (vs,mat) -> (vs,List.transpose_exn mat)

let (colLabels,umat) = makemat()

let matString =
  let (colLabels,umat) = makemat() in
  ["amb_space " ^ Int.to_string (List.length colLabels)
  ;"/*" ^ (colLabels |> List.map ~f:Vert.to_string |> String.concat ~sep:" ") ^ "*/"
  ;"equations " ^ Int.to_string (List.length checkvars) 
  ]
  @ (umat |> List.map ~f:(List.map ~f:Int.to_string) |> List.map ~f:(String.concat ~sep:" "))
  @ ["/*" ^ (checkvars |> List.map ~f:Vert.to_string |> String.concat ~sep:" ") ^ "*/"]

let write_vars () =
  vars |> List.map ~f:(fun v -> Vert.to_string v ^ ", " ^ XVert.to_string (Map.find_exn uvars v))
  |> Out_channel.write_lines ("./_Clusters/"^foldername^"/uvars.txt")
let write_mat () = Out_channel.write_lines ("./_Clusters/"^foldername^"/umat_"^subName^".in") matString

let latticeBasis () =
  umat |> Flint.BigIntMat.of_rows_int
  |> Flint.BigIntMat.nullspace
  |> (fun (n,cols) -> if Signed.Long.equal n Signed.Long.zero
                      then invalid_arg "empty nullspace"
                      else cols |> Flint.BigIntMat.cols |> (fun m -> List.take m (Signed.Long.to_int n)) |> Flint.BigIntMat.of_rows
                           |> Flint.BigIntMat.lll
                           |> Flint.BigIntMat.rows)

let writeLattice () =
  ["//Cols (U vars)"
  ;colLabels |> List.map ~f:Vert.to_string |> String.concat ~sep:", "
  ;"//Matrix"
  ; latticeBasis() |> List.map ~f:(List.map ~f:Flint.BigInt.to_string) |> List.map ~f:(String.concat ~sep:",") |> String.concat ~sep:"\n"
  ]
  |>  Out_channel.write_lines ("./_Clusters/"^foldername^"/unullLat_"^subName^".txt") 

type parse_normaliz =
  | Begin
  | PassStars
  | ExtremeRays of int * (int list list)
  | Finished of int list list

let process_normaliz () =
  let res = 
    In_channel.fold_lines (In_channel.create  ("./_Clusters/"^foldername^"/umat_"^subName^".out"))
      ~init:Begin ~f:(fun time line -> match time with
                                       | Begin -> if String.for_all ~f:(Char.equal '*') line && String.length line > 1
                                                  then PassStars
                                                  else Begin
                                       | PassStars -> (match String.chop_suffix line ~suffix:" extreme rays:" with
                                                       | None ->  PassStars
                                                       | Some pref -> ExtremeRays(Int.of_string pref,[])) 
                                       |  ExtremeRays(t,vecs) -> if Int.(t > 0)
                                                                 then ExtremeRays(t-1, (line |> String.strip
                                                                                        |> String.split ~on:' '
                                                                                        |> List.filter ~f:(fun s -> String.length s > 0)
                                                                                        |> List.map ~f:Int.of_string)::vecs)
                                                                 else Finished(vecs)
                                       | Finished(vecs) -> Finished(vecs))
  in
  let cone = match res with
    | Finished cone -> cone
    | _ -> invalid_arg "Failed to generate cone for normaliz output" in
  let short_cone = cone |> List.map ~f:(fun l -> List.zip_exn l colLabels |> List.filter ~f:(fun (c,_) -> c>0)) in
  let () = short_cone
           |> List.map ~f:(fun l -> (l |> List.map ~f:(fun (c,v) -> Int.to_string c ^ "*" ^ Vert.to_string v) |> String.concat ~sep:" ")
                                    ^ "  -> "
                                    ^ (l |> List.map ~f:(fun (c,v) -> XVert.pow (Map.find_exn uvars v) c)
                                       |> List.reduce_exn ~f:XVert.mult
                                       |> XVert.to_string))
           |> Out_channel.write_lines ("./_Clusters/" ^foldername^"/extra"^(String.capitalize subName)^"RayFactorization.txt") in
  short_cone
(*

let lastring v =
  v |> Partition.Vert.to_string
  |> String.chop_prefix_exn ~prefix:"a" |> String.map ~f:(function 'w'-> '|' | c -> c) |> fun s -> "\\pl{"^s^"}"
let to_latex x =
  ["\\frac{"
  ; x |> XVert.top_list |> List.map ~f:lastring |> String.concat ~sep:" "
  ; "}{"
  ; x |> XVert.bot_list |> List.map ~f:lastring |> String.concat ~sep:" "; "}"
  ] |> String.concat
let uvarstrings () =
  dynkins |> List.hd_exn
  |> (fun c -> Clust.follow_sources_path c 1000)
  |> snd
  |>  List.map ~f:(fun c -> c |> Clust.sources |> Set.to_list
                            |> List.map ~f:(fun v -> (lastring v) ^ " & " ^ (v |> Map.find_exn uvars |> to_latex) ))
  |> List.map ~f:(String.concat ~sep:"\\\\ \\hline\n")
  |> String.concat ~sep:"\\\\ \\hline\\hline\n"

 *)
