open Clusters
open Core

let dtype = BruhatCluster.B 4
let file = Out_channel.create ~append:true ("./_LongestWord/path" ^ BruhatCluster.cartan_to_string dtype ^ ".txt")
module WNode = BruhatCluster.WordNode(struct let c = dtype end)
module WSearch = SearchBase.Make(WNode)

let path_to_string p = p |> List.map ~f:BruhatCluster.wordmove_to_string |> String.concat ~sep:" "
let newline s = s ^ "\n"

let start = BruhatCluster.longest_word dtype
let start_twoMove = WSearch.paths  ~cond:(fun _ e -> match e with BruhatCluster.Two _ -> true | _ -> false) start
let conj1 = (List.drop start 1)@(List.take start 1)
let conj1_twoMove = WSearch.paths ~cond:(fun _ e -> match e with BruhatCluster.Two _ -> true | _ -> false) conj1
let (ans1,pathConj1) = WSearch.shortest ([start]) (Map.mem conj1_twoMove) |> Option.value_exn

let () = Out_channel.output_string file (start |> WNode.id |> newline)
let (mid,_) = WNode.follow ans1 (List.rev pathConj1)
let () = Out_channel.output_string file (mid |> Map.find_exn start_twoMove |> path_to_string |> newline)
let () = Out_channel.output_string file (mid |> WNode.id |> newline)

let () = Out_channel.output_string file (pathConj1 |> path_to_string |> newline)
let () = Out_channel.output_string file (ans1 |> WNode.id |> newline)
let () = Out_channel.output_string file (ans1 |> Map.find_exn conj1_twoMove |> List.rev |> path_to_string |> newline)
let () = Out_channel.output_string file (conj1 |> WNode.id |> newline)
let () = Out_channel.flush file
let () = print_endline "Finished first conjugation"

let conj2 = (List.drop start 2)@(List.take start 2)
let conj2_twoMove = WSearch.paths ~cond:(fun _ e -> match e with BruhatCluster.Two _ -> true | _ -> false) conj2
let (ans2,pathConj2) = WSearch.shortest (Map.keys conj1_twoMove) (Map.mem conj2_twoMove) |> Option.value_exn
let (mid2,_) = WNode.follow ans2 (List.rev pathConj2)
let () = Out_channel.output_string file (mid2 |> Map.find_exn conj1_twoMove |> path_to_string |> newline)
let () = Out_channel.output_string file (mid2 |> WNode.id |> newline)
let () = Out_channel.output_string file (pathConj2 |> path_to_string |> newline)
let () = Out_channel.output_string file (ans2 |> WNode.id |> newline)
let () = Out_channel.output_string file (ans2 |> Map.find_exn conj2_twoMove |> List.rev |> path_to_string |> newline)
let () = Out_channel.output_string file (conj2 |> WNode.id |> newline)
let () = Out_channel.flush file
let () = print_endline "Finished second conjugation"

let conj3 = (List.drop start 3)@(List.take start 3)
let conj3_twoMove = WSearch.paths ~cond:(fun _ e -> match e with BruhatCluster.Two _ -> true | _ -> false) conj3
let (ans3,pathConj3) = WSearch.shortest (Map.keys conj2_twoMove) (Map.mem conj3_twoMove) |> Option.value_exn
let (mid3,_) = WNode.follow ans3 (List.rev pathConj3)
let () = Out_channel.output_string file (mid3 |> Map.find_exn conj2_twoMove |> path_to_string |> newline)
let () = Out_channel.output_string file (mid3 |> WNode.id |> newline)
let () = Out_channel.output_string file (pathConj3 |> path_to_string |> newline)
let () = Out_channel.output_string file (ans3 |> WNode.id |> newline)
let () = Out_channel.output_string file (ans3 |> Map.find_exn conj3_twoMove |> List.rev |> path_to_string |> newline)
let () = Out_channel.output_string file (conj3 |> WNode.id |> newline)
let () = Out_channel.flush file

let target_twoMove = WSearch.paths  ~cond:(fun _ e -> match e with BruhatCluster.Two _ -> true | _ -> false) (List.rev start)
let () = Out_channel.output_string file (conj3 |> Map.find_exn target_twoMove |> List.rev |> path_to_string |> newline)
let () = Out_channel.output_string file (start |> List.rev |> WNode.id |> newline)
let () = Out_channel.output_string file "\n"
let () = Out_channel.output_string file (((mid |> Map.find_exn start_twoMove)
                                          @ pathConj1
                                          @(ans1 |> Map.find_exn conj1_twoMove |> List.rev)
                                          @(mid2 |> Map.find_exn conj1_twoMove)
                                          @ pathConj2
                                          @(ans2 |> Map.find_exn conj2_twoMove |> List.rev)
                                          @(mid3 |> Map.find_exn conj2_twoMove)
                                          @ pathConj3
                                          @(ans3 |> Map.find_exn conj3_twoMove |> List.rev)
                                          @(conj3 |> Map.find_exn target_twoMove |> List.rev)) |> path_to_string |> newline)
let () = Out_channel.close file
let () = print_endline "Finished"
