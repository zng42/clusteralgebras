open Core
module Run = RunGr48verts
(*
let mat =
  [[IntPoly.of_string "1*0-1*t";IntPoly.zero;IntPoly.zero;IntPoly.zero; IntPoly.of_string "1*a";IntPoly.of_string "1*b";IntPoly.of_string "1*c";IntPoly.of_string "1*d"]
  ;[IntPoly.zero;IntPoly.one;IntPoly.zero;IntPoly.zero; IntPoly.of_string "1*e";IntPoly.of_string "1*f";IntPoly.of_string "1*g";IntPoly.of_string "1*h"]
  ;[IntPoly.zero;IntPoly.zero;IntPoly.scale (-1) IntPoly.one;IntPoly.zero; IntPoly.of_string "1*k";IntPoly.of_string "1*l";IntPoly.of_string "1*m";IntPoly.of_string "1*n"]
  ;[IntPoly.zero; IntPoly.zero;IntPoly.zero;IntPoly.one;IntPoly.of_string "1*p";IntPoly.of_string "1*q";IntPoly.of_string "1*r";IntPoly.of_string "1*s"]]
 *)
(*
module MathematicaIndex =
  struct
    type t = int*int [@@deriving compare, sexp]
    let to_string (i,j) = "mat[[" ^ Int.to_string (i+1) ^"," ^ Int.to_string (j+1) ^"]]"
    let of_string s =
      match String.split (String.drop_prefix (String.drop_suffix s 2) 5) ~on:'_' with
      | [i;j] -> (Int.of_string i - 1, Int.of_string j -1)
      | _ -> invalid_arg (s ^ " is not mat[[i,j]]")
                           end
module MPoly = MapPolynomial.Make(MathematicaIndex) (IntRing)
let mat = List.init 4 ~f:(fun i -> List.init 8 ~f:(fun j -> MPoly.of_mono 1 [((i,j),1)]))
 *)

(*
module MPoly = Algebra.Rings.IntPoly
let mat =
  let p1 = MPoly.one in
  let p0 = MPoly.zero in
  let x =  MPoly.of_string "1*x" in
  let y =  MPoly.of_string "1*y" in
  let z =  MPoly.of_string "1*z" in
  let w =  MPoly.of_string "1*w" in
  [[p1; p0; p0; p1; p1; p0; p0;  w]
  ;[p1; p1; p0; p0;  x; p1; p0; p0]
  ;[p0; p1; p1; p0; p0;  y; p1; p0]
  ;[p0; p0; p1; p1; p0; p0;  z; p1]]
  *)
module MPoly = Flint.BigIntMPoly.Make(struct let vars = List.init 16 ~f:(fun i -> "x"^Int.to_string(i+1))
                                             let ord = Flint.BigIntMPoly.Ord.LEX end)
let mat = "./_Clusters/gr48/Partition/BoundedRatio/weightMatrixSimple.txt" |> In_channel.read_lines |> List.map ~f:(String.split ~on:',') |> List.map ~f:(List.map ~f:String.strip)|> List.map ~f:(List.map ~f:MPoly.of_string);;

let rec det mat =
  match mat with
  | [] -> MPoly.one
  | row::rest ->
     List.foldi row ~init:MPoly.zero
       ~f:(fun i acc x -> MPoly.add acc (MPoly.mult (MPoly.scale (MPoly.R.pow (MPoly.R.of_int (-1)) i) x)
                                           (det (List.map rest ~f:(List.filteri ~f:(fun j _ -> not(i = j)))))))

let minor ~cols mat = det(List.map mat ~f:(List.filteri ~f:(fun i _ -> List.mem cols ~equal:Int.equal (i+1))));;

let alist = List.map (In_channel.read_lines "./_Clusters/gr48/Framed/gr48_a8_amap.txt")
              ~f:(fun l -> let (table,name) = Run.aname_of_string l in (name, Run.part_parse table))
let alist_plucker = List.map ~f:(fun (a, table) -> (a, (PartVertex.to_plucker 4 8) table)) (List.filter alist ~f:(fun (_,v) -> PartVertex.weight v = 1))
let amap_plucker = String.Map.of_alist_exn(List.map alist_plucker ~f:((fun (a,cols) -> (a,minor mat ~cols:(Set.to_list cols)))))

let xpairs = In_channel.read_lines "./_Clusters/gr48/Framed/gr48_a2_xpairs.txt"

let xpair_to_poly amap xpair =
  match xpair with
  | ((top,bot), ([a1;a2], _)) ->
      let p = MPoly.add
                (List.fold top ~init:MPoly.one ~f:(fun p a -> MPoly.mult (Map.find_exn amap a) p))
                (List.fold bot ~init:MPoly.one ~f:(fun p a -> MPoly.mult (Map.find_exn amap a) p)) in
      let (a,q) = match Map.find amap a1 with
        | Some q -> (a2,q)
        | None -> (a1, (Map.find_exn amap a2)) in
      if MPoly.equal q MPoly.zero || MPoly.equal p MPoly.zero
      then None
      else
        let (newp, rem) = MPoly.div_rem p q in
        if MPoly.equal MPoly.zero rem
        then Some (a,newp)
        else None
  | _ -> None

let rec process_xpairs amap xpairs =
  let fProcessable amap_cur xstring =
    let ((xtop,xbot),(x1top,_)) = Run.string_to_xpair xstring
    in List.for_all xtop ~f:(Map.mem amap_cur)
       && List.for_all xbot ~f:(Map.mem amap_cur)
       && List.count ~f:(Fn.non (Map.mem amap_cur)) x1top =1 in
  let rec loop amap xpairs =
    let (amap_new, skipped) =
      List.fold xpairs ~init:(amap,[])
        ~f:(fun  (amap,skipped) xpair ->
          let () = if Map.length amap mod 5000 = 0 then Out_channel.output_string stdout "." else () in
          if fProcessable amap xpair
          then
            match xpair_to_poly amap (Run.string_to_xpair xpair) with
            | Some (a,poly) ->
               (Map.update amap a ~f:(function None -> poly
                                             | Some p -> if MPoly.equal p poly
                                                         then p
                                                         else invalid_arg (a ^" has two different polys")), skipped)
            | None -> (amap, xpair::skipped)
          else (amap, xpair::skipped))
    in
    match skipped with
    | [] -> amap_new
    | _ -> if List.length skipped < List.length xpairs
           then
             let () = Out_channel.print_endline (Int.to_string (Map.length amap_new)) in
             loop amap_new skipped
           else amap_new
  in loop amap xpairs


let rec process_write_xpairs file_name amap xpairs =
  let fProcessable amap_cur xstring =
    let ((xtop,xbot),(x1top,_)) = Run.string_to_xpair xstring
    in List.for_all xtop ~f:(Map.mem amap_cur)
       && List.for_all xbot ~f:(Map.mem amap_cur)
       && List.count ~f:(Fn.non (Map.mem amap_cur)) x1top =1 in
  let file = Out_channel.create ~append:true file_name in
  let rec loop amap xpairs =
    let (amap_new, skipped) =
      List.fold xpairs ~init:(amap,[])
        ~f:(fun  (amap,skipped) xpair -> if fProcessable amap xpair
                                         then
                                           match xpair_to_poly amap (Run.string_to_xpair xpair) with
                                           | Some (a,poly) ->
                                              let () = Out_channel.output_string file ("{" ^ a ^ "," ^ MPoly.to_string poly ^"}")
                                              in (Map.update amap a ~f:(function None -> poly
                                                                               | Some p -> if MPoly.equal p poly
                                                                                           then p
                                                                                           else invalid_arg (a ^" has two different polys")), skipped)
                                           | None -> (amap, xpair::skipped)
                                         else (amap, xpair::skipped))
    in
    match skipped with
    | [] -> amap_new
    | _ -> if List.length skipped < List.length xpairs
           then loop amap_new skipped
           else amap_new
  in
  let output = loop amap xpairs in
  let () = Out_channel.output_string file "}" in
  let () = Out_channel.close file in
  output

let write_polynomials file amap =
  Out_channel.write_all file ~data:("{" ^ String.concat ~sep:"," (List.map (Map.to_alist amap) ~f:(fun (a, poly) -> "{" ^ a ^ "," ^ String.lowercase(MPoly.to_string poly) ^"}")) ^"}")
