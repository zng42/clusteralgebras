open Core
open Algebra
open Lacaml.D
(*
let minor ~m mat = Mat.of_col_vecs_list (List.map m (Mat.col mat))

let det_minor ~l mat =
  let det mat =
    let tau = geqrf mat in
    let ones = Vec.init (Vec.dim tau) (fun _ -> 1.)
    in (Vec.prod(Vec.signum (Vec.sub tau ones))) *. Vec.prod (Mat.copy_diag mat)
  in det (minor l mat)
     *)


let det_minor = Algebra.Rings.LacamlMat.det_minor

let create_test_mats (l: 'a list) (fa : 'a -> 'c -> float) (tests : 'c list)   =
  let mat = Mat.of_list (List.map tests ~f:(fun test -> List.map l ~f:(fun a -> fa a test))) in
  (fun (fb : 'b -> 'c -> float) (bs : 'b  list)  ->
    let bmat = Mat.of_list (List.map tests ~f:(fun test -> List.map bs ~f:(fun b -> fb b test))) in
    (Mat.log mat, Mat.log bmat))

let test_products ?threshold:(threshold = 0.0000001) (mA : Mat.t) (mB : Mat.t) =
  let epsilon = threshold *. threshold in
  let num_valid_rows = Mat.dim2 mA in
  let rank = gelsd mA mB in
  let () = if rank < num_valid_rows
           then Out_channel.output_string stdout ("mA is underdetermined with rank " ^ Int.to_string rank ^ " < " ^ Int.to_string num_valid_rows)
           else () in
  List.filter_mapi (Mat.to_col_vecs_list mB) ~f:(fun i v -> let error = Vec.sum (Vec.sqr ~ofsx:(num_valid_rows+1) v)  in
                                                            if Float.(error <= epsilon) then Some (i,error) else None)

let find_products ?threshold:(threshold = 0.0000001) (l : 'a list) (fa : 'a -> 'c -> float) (tests : 'c list)  =
  let test_mats = create_test_mats l fa tests in
  let num_rows = List.length l in
  let prod_list mat i = List.take(Vec.to_list (Mat.col mat (i+1))) num_rows in
  let get_factor i (expr:Float.t) = if Float.(abs(expr) > threshold) then Some(List.nth_exn l i, expr) else None in
  (fun (fb : 'b -> 'c -> float) (bs : 'b list) ->
    let (mA,mB) = test_mats fb bs in
    let prods = test_products ~threshold:threshold mA mB in
    List.map prods ~f:(fun (i,error) -> (List.nth_exn bs i,error, List.filter_mapi (prod_list mB i) ~f:get_factor))
  )


let vandermonde l k =
  let powers = Vec.init k (fun i -> Float.of_int i)
  in Mat.of_col_vecs_list (List.map l ~f:(fun p -> (Vec.pow (Vec.init k (fun _ -> p)) powers)))

let random_positive ?from:(from = 0.0) ?range:(range = 2.0) m n =
  let tests = List.map ~f:Set.to_list (Set.to_list (Plucker.Clust.verts (Plucker.gr m n))) in
  let rec loop () =
    let mat = Mat.add (Mat.random ~from:0.0 ~range:(range /. 100.) m n) (vandermonde (List.sort ~compare:Float.compare (List.init n ~f:(fun _ -> Random.float_range from (from +. range)))) m)
    in if List.for_all tests ~f:(fun a -> Float.(det_minor ~l:a mat > 0.))
       then mat
       else loop ()
  in loop ()

(*
module Run = RunGr48verts

let pluckerMap k n alist =
  String.Table.of_alist_exn
    (List.filter_map ~f:((fun (a,name) -> if Run.weight_a name = 1
                                          then Some(name, Set.to_list (PartVertex.to_plucker k n (Run.part_parse a)))
                                          else None)
                         |. Run.aname_of_string)
       alist)

module APoly = MapLaurent.Make (String) (Rings.Float)
let extend_polymap amap xpair =
  let apoly xtop xbot a =
    let p b = APoly.of_mono 1. [(b,1)] in
    APoly.mult (APoly.of_mono 1. [(a,-1)]) (APoly.add (List.fold xtop ~init:APoly.one ~f:(fun acc b -> APoly.mult acc (p b)))
                                              (List.fold xbot ~init:APoly.one ~f:(fun acc b -> APoly.mult acc (p b)))) in
  let ((xtop,xbot), (x1top,_)) = Run.string_to_xpair xpair in
  if List.for_all xtop ~f:(Map.mem amap) && List.for_all xbot ~f:(Map.mem amap) && List.count x1top ~f:(Fn.non (Map.mem amap)) = 1
  then Map.add_exn amap ~key:(List.find_exn x1top ~f:(Fn.non (Map.mem amap))) ~data:(apoly xtop xbot (List.find_exn x1top ~f:(Map.mem amap)))
  else amap

let create_polymap plucker_map xpairs =
  let plucker_polymap = String.Map.of_alist_exn (List.map (String.Table.keys plucker_map) ~f:(fun a -> (a,APoly.of_mono 1. [(a,1)]))) in
  List.fold xpairs ~init:plucker_polymap ~f:extend_polymap

(* Requires all polynomials in poly_map eventually reach a string in plucker_map. Should be true of any maps built from extend_polymap *)
let rec eval_astring plucker_map poly_map s mat =
  match String.Table.find plucker_map s with
  | Some l -> det_minor ~l:l mat
  | None -> match Map.find poly_map s with
            | Some p -> APoly.eval p (fun s -> eval_astring plucker_map poly_map s mat)
            | None -> invalid_arg ("Missing polynomial for " ^ s)

let eval_xstring ~f (top,bot) mat =
  (List.fold ~init:1. ~f:(fun acc a -> f a mat *. acc) top)
  /. (List.fold ~init:1. ~f:(fun acc a -> f a mat *. acc) bot)
 *)

let load_matrices file =
  List.map ~f:Mat.of_list (Sexp.load_sexps_conv_exn file (List.t_of_sexp (List.t_of_sexp Float.t_of_sexp)))
let save_matrices file list =
  Sexp.save_sexps file (List.map ~f:(fun m -> m |> Mat.to_list |> List.sexp_of_t (List.sexp_of_t Float.sexp_of_t)) list)


(*

(* Functions for products of x/y/z *)
let powVec_add l1 l2 = List.mapi (List.zip_exn l1 l2) (fun i (x,y) -> if i = 0 then (x+y) mod 2 else x+ y)
let powVec_to_uPoly l =
  List.foldi ~init:([],[]) ~f:(fun i (numer,denom) x ->
      if x> 0
      then ((Map.find_exn factor3Map (List.nth_exn ((FloatPoly.const (-1.))::factors3) i), x)::numer,denom)
      else
        if x < 0
        then (numer, (Map.find_exn factor3Map (List.nth_exn ((FloatPoly.const (-1.))::factors3) i), Int.abs(x))::denom)
        else (numer,denom)) l
 *)
