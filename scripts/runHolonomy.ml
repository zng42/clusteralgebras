open Clusters
open Core

module FlintRat = Algebra.Rationals.Make(Flint.BigIntPoly)
module FlintMat = Algebra.ListMatrix.Make(FlintRat)
module FlintMat2 = Algebra.Gl.Make(FlintMat)(struct let dim = 2 end)
module FlintCluster = NonComCluster.Make(FlintMat2)

let rotlist rot l =
  let (a,b) = List.split_n l rot in
  b @ a

let horizpath ?rot:(rot = 0) c i =
  let (tL,tR) = FlintCluster.triangles c i in
  FlintCluster.[gCross; gLeftTri tR; gCross; gRightTri (rotClockTri tL)]
  |> List.rev
  |> rotlist rot
  |> List.fold ~init:(FlintCluster.RMat.one 2) ~f:FlintCluster.RMat.mult

let vertpath ?rot:(rot = 0) c i =
  let (tL,tR) = FlintCluster.triangles c i in
  FlintCluster.[gCross; gRightTri tR; gCross; gLeftTri (rotCounterClockTri tL)]
  |> List.rev
  |> rotlist rot
  |> List.fold ~init:(FlintCluster.RMat.one 2) ~f:FlintCluster.RMat.mult
  |> FlintCluster.RMat.inv
  |> Option.value_exn

let c_test = FlintCluster.qA1_double (FlintMat2.one) (FlintMat2.one) (FlintMat2.one)
let vpath_test = vertpath c_test 1
let hpath_test = horizpath c_test 1

let () = Out_channel.print_endline ("Vpath is :" ^ FlintCluster.RMat.to_string vpath_test)
let () = Out_channel.print_endline ("Hpath is :" ^ FlintCluster.RMat.to_string hpath_test)

(*
let gXY [x;y;z] =
  let Some yinv = Gl2Z.inv y in
  let a11 = Gl2Z.(mult (transpose yinv) z) in
  let a12 = Gl2Z.one in
  let a21 = Gl2Z.(neg (mult yinv (transpose x))) in
  let a22 = Gl2Z.zero
  in [[a11;a12];[a21;a22]] |> FlintMatMat.of_rows

let gYX [x; y; z] =
  let Some yinv = FlintMat.inv y in
  let Some xinv = FlintMat.inv x in
  let a11 = Gl2Z.zero in
  let a12 = Gl2Z.(neg (mult (transpose xinv) y)) in
  let a21 = Gl2Z.one in
  let a22 = Gl2Z.(mult (mult (transpose yinv) z) (mult (transpose xinv)  y)) in
  [[a11;a12];[a21;a22]] |> FlintMatMat.of_rows |> FlintMatMat.neg

let cross = Gl2Z.[[zero; one];[neg one; zero]] |> FlintMatMat.of_rows

let horizpath [c;a;b] = FlintMatMat.(mult (mult cross (gXY [a;b;c])) (mult cross (gYX [a;b;c])))
let vertpath [c;a;b] = FlintMatMat.(mult (mult cross (gYX [c;a;b])) (mult cross (gXY [c;a;b])))


let horizpathAns [c;a;b] =
  match (Gl2Z.inv a, Gl2Z.inv b) with
  | (Some ainv,Somv binv) ->
       Gl2Z.[[mult binv (transpose a); mult (mult binv (transpose a)) (mult ainv (transpose c)) ]
            ;[mult (transpose binv) c; add (mult (mult (transpose binv) c) (mult ainv (transpose c))) (mult (transpose ainv) b)]]
       |> FlintMatMat.of_rows
       |> FlintMatMat.neg
  | _ -> invalid_arg "a and b must be invertible"

let vertpathAns [c;a;b] =
    match (Gl2Z.inv a, Gl2Z.inv c) with
  | (Some ainv,Somv cinv) ->
     Gl2Z.[[add (mult ainv (transpose c)) (mult (mult cinv (transpose b)) (mult (transpose ainv) b)); mult cinv (transpose b)]
          ;[mult(mult (transpose cinv) a) (mult (transpose ainv) b); mult (transpose cinv) a]]
  |> FlintMatMat.of_rows
  |> FlintMatMat.neg
 *)
