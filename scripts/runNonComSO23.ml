(*
module BaseRing = Flint.BigIntMPoly.Make(struct let vars = ["a";"b";"c";"d";"x";"y";"v1";"v2";"w1";"w2";"l1";"l2";"m1";"m2"]
                                                let ord = Flint.BigIntMPoly.Ord.LEX end)
module BaseRats = Algebra.Rationals.Make(BaseRing)
module V2 = Algebra.FreeRMod.MakeFinite (struct let dim = 2 end) (BaseRats)
module Cliff = Algebra.Clifford.MakeExplicit(struct include V2 let q v = V2.dot ~p:2 v v end)
let e = BaseRats.[one;zero] |> V2.of_alist |> Cliff.of_vec
let com v = Cliff.scale (BaseRats.of_string v) Cliff.one
let vec base = [base ^ "1"; base^ "2"] |> List.map ~f:BaseRats.of_string |> V2.of_alist |> Cliff.of_vec

let labelComm = [w11a, (com "b^2") ** (com "1/(a*b)") ** (com "v1+w1")
                ;w11b, (com "b^2") ** (com "1/(a*b)") ** (com "v1+w1") |> Cliff.transpose
                ;w21, (com "b^2") ** com "y" ** com "v1^2"
                ;b1a, [(com "a*b");com "y"; com "v1"; com "w1"] |> List.fold ~init:Cliff.one ~f:( ** )
                ;b1b, [(com "a*b");com "y"; com "v1"; com "w1"] |> List.fold ~init:Cliff.one ~f:( ** ) |> Cliff.transpose
                ;b2, [com "a^2"; com "x"; com "y" ;com "v1^2"] |> List.fold ~init:Cliff.one ~f:( ** )
                ;c1a, com "1/(a*b)"
                ;c1b, com "1/(a*b)" |> Cliff.transpose
                ;c2, com "1/a^2"
let qB2TriComm = qB2tri labelComm
*)
(* open Algebra *)
module QuadIndex = Indeces.MakeFinite(struct let names = ["M2";"M1";"V";"W";"Vp";"Wp";"L1";"L2";"Lp1";"Lp2";"e"] end)
module Cliff = Clifford.Make(struct
                   module VIndex = QuadIndex
                   let vecs = ["V";"W";"Vp";"Wp";"e";"L1";"L2";"Lp1";"Lp2";"M1";"M2"] |> List.map ~f:VIndex.of_string
                   let frozen = ["L1";"L2";"e";"M2";"M1";"Lp1";"Lp2"]
                   let norms = ["e";"L1";"L2";"Lp1";"Lp2";"M1";"M2"] |> List.map ~f:(fun v -> VIndex.(of_string v,of_string v,1))
                   let vars = ["a";"b";"x";"y";"c";"d";"ap";"bp";"xp";"yp"] end)
let e = Cliff.of_string "1*.e"
let (x,y,v,w) = Cliff.(of_string "x",of_string"y", of_string "1*.V", of_string "1*.W")
let (a,b,l1,l2) = Cliff.(of_string "a",of_string"b", of_string "1*.L1", of_string "1*.L2")
let (c,d,m1,m2) = Cliff.(of_string "c",of_string"d", of_string "1*.M1", of_string "1*.M2")

let (xp,yp,vp,wp) = Cliff.(of_string "xp",of_string"yp", of_string "1*.Vp", of_string "1*.Wp")
let (ap,bp,lp1,lp2) = Cliff.(of_string "ap",of_string"bp", of_string "1*.Lp1", of_string "1*.Lp2")

module CliffVert =
  struct
    include Cliff
    let of_string = of_string ~frozen:Cliff.default_frozen
    let transpose v = mult (mult e (Cliff.transpose v)) e
  end

module CliffClust = NonComCluster.Make(CliffVert)
let [w11a;w11b;w21;a1a;a1b;a2;v11a;v11b;v21;b1a;b1b;b2;c1a;c1b;c2;d1a;d1b;d2;e1a;e1b;e2] = List.init 21 ~f:Fn.id
let indexName =
  let map = [w11a,"W11";w11b,"W11";w21,"w21";a1a,"A1";a1b,"A1";a2,"a2";b1a,"B1";b1b,"B1";b2,"b2";c1a,"C1";c1b,"C1";c2,"c2"
            ;v11a,"V11";v11b,"V11";v21,"v21";d1a,"D1";d1b,"D1";d2,"d2";e1a,"E1";e1b,"E1";e2,"e2"] |> Int.Map.of_alist_exn
  in (fun i -> match Map.find map i with Some s -> s | _ -> ("U" ^ Int.to_string i))
let indexNameS =
  let map = [w11a,"W11a";w11b,"W11b";w21,"w21";a1a,"A1a";a1b,"A1b";a2,"a2";b1a,"B1a";b1b,"B1b";b2,"b2";c1a,"C1a";c1b,"C1b";c2,"c2"
            ;v11a,"V11a";v11b,"V11b";v21,"v21";d1a,"D1a";d1b,"D1b";d2,"d2";e1a,"E1a";e1b,"E1b";e2,"e2"] |> Int.Map.of_alist_exn
  in (fun i -> match Map.find map i with Some s -> s | _ -> ("U" ^ Int.to_string i))

let ( ++ ) = CliffVert.add
let ( ** ) = CliffVert.mult

let tau = CliffVert.transpose
let sigma = Cliff.transpose

let label = [w11a, y**v**e
            ;w11b, y**v**e |> sigma
            ;w21, x++y
            ;b1a, y**v**w
            ;b1b, y**v**w |> sigma
            ;b2, x**y**v**v
            ;c1a,Cliff.one
            ;c1b,Cliff.one |> sigma
            ;c2,Cliff.one]

let qB2tri label = CliffClust.of_nbrlist
                     ~graph:CliffClust.[w11a,(([T w21],[N b1a]),([T c1a],[N b2]))
                                       ;w11b,(([N b1b],[T w21]),([N b2],[T c1b]))
                                       ; w21,(([N c1a;N c1b],[N b2]),([N c2],[N w11a;N w11b]))] ~frozen:(Int.Set.of_list [b1a;b1b;b2;c1a;c1b;c2])
                     ~label:label


let labelBorelish = [w11a, l2**l1**e**(v++w)
                   ;w11b, l2**l1**e**(v++w) |> sigma |> tau
                   ;w21, y**v**v
                   ;b1a, y**v**w **e**l1**l2**e
                   ;b1b, y**v**w **e**l1**l2**e |> sigma |> tau
                   ;b2, x**y**v**v
                   ;c1a, l2**l1
                   ;c1b, l2**l1 |> sigma |> tau
                   ;c2,Cliff.one]
let labelBorel = [w11a, Cliff.of_string "b/a"**l2**l1**e**(v++w)
                 ;w11b, Cliff.of_string "b/a"**l2**l1**e**(v++w) |> tau |> sigma
                 ;w21, b**b**y**v**v
                 ;b1a, a**b**y**v**w **e**l1**l2**e
                 ;b1b, a**b**y**v**w **e**l1**l2**e |> tau |> sigma
                 ;b2, a**a**x**y**v**v
                 ;c1a, Cliff.of_string "1/(a*b)"**l2**l1
                 ;c1b, Cliff.of_string "1/(a*b)"**l2**l1 |> tau |> sigma
                 ;c2,Cliff.of_string "1/(a*a)"]

let qB2triBorell label =
   CliffClust.of_nbrlist
            ~graph:CliffClust.[w11a,(([N w21],[N c1a]),([N b1a; N c2],[]))
                              ;w11b,(([T w21],[N c1b]),([N b1b;N c2],[]))
                              ; w21,(([T b1b;N c2],[N b1a]),([N b2],[N w11b;T w11a]))] ~frozen:(Int.Set.of_list [b1a;b1b;b2;c1a;c1b;c2]) ~label:label

let labelFull = [w11a, Cliff.of_string "b/a"**l2**l1**e**(v++w) ** e**m1**m2**e**Cliff.of_string "1/(c*d)"
                 ;w11b, Cliff.of_string "b/a"**l2**l1**e** (v++w) ** e**m1**m2**e**Cliff.of_string "1/(c*d)" |> tau |> sigma
                 ;w21, b**b**y** v**v** Cliff.of_string "1/(c*c*d*d)"
                 ;a1a, Cliff.of_string "1/(c*d)"**m2**m1
                 ;a1b, Cliff.of_string "1/(c*d)"**m2**m1 |> tau |> sigma
                 ;a2, Cliff.of_string "1/(c*c)"
                 ;b1a, Cliff.of_string "1/(c*d)"**e**m2**m1**e ** y**v**w ** e**a**b**l1**l2**e
                 ;b1b, Cliff.of_string "1/(c*d)"**e**m2**m1**e ** y**v**w ** e**a**b**l1**l2**e |> tau |> sigma
                 ;b2, a**a ** x**y**v**v ** Cliff.of_string "1/(c*c)"
                 ;c1a, Cliff.of_string "1/(a*b)"**l2**l1
                 ;c1b, Cliff.of_string "1/(a*b)"**l2**l1 |> tau |> sigma
                 ;c2, Cliff.of_string "1/(a*a)"]

let qB2triFull label =
   CliffClust.of_nbrlist
            ~graph:CliffClust.[w11a,(([N w21],[N c1a]),([N b1a; N c2],[N a1a]))
                              ;w11b,(([T w21],[N c1b]),([N b1b;N c2],[N a1b]))
                              ; w21,(([T b1b;N c2;N a2],[N b1a]),([N b2],[N w11b;T w11a]))]
            ~frozen:(Int.Set.of_list [a1a;a1b;a2;b1a;b1b;b2;c1a;c1b;c2]) ~label:label

let labelFullAgain = [w11a, e**(v++w) ** Cliff.of_string "b/a"**l2**l1 ** m1**m2**Cliff.of_string "1/(c*d)"
                 ;w11b, e**(v++w) ** Cliff.of_string "b/a"**l2**l1 ** m1**m2**Cliff.of_string "1/(c*d)"  |> tau |> sigma
                 ;w21, b**b** y**v**v ** Cliff.of_string "1/(c*c*d*d)"
                 ;a1a, Cliff.of_string "1/(c*d)"**m1**m2
                 ;a1b, Cliff.of_string "1/(c*d)"**m1**m2 |> tau  |> sigma
                 ;a2, Cliff.of_string "1/(c*c)"
                 ;b1a, Cliff.of_string "1/(c*d)"**m2**m1 ** a**b**l1**l2 ** y**v**w
                 ;b1b, Cliff.of_string "1/(c*d)"**m2**m1 ** a**b**l1**l2 ** y**v**w |> tau |> sigma
                 ;b2, a**a ** x**y**v**v ** Cliff.of_string "1/(c*c)"
                 ;c1a, Cliff.of_string "1/(a*b)"** e**l2**l1**e
                 ;c1b, Cliff.of_string "1/(a*b)"** e**l2**l1**e |> tau |> sigma
                 ;c2, Cliff.of_string "1/(a*a)"]
let qB2triFullReal label =
   CliffClust.of_nbrlist
            ~graph:CliffClust.[w11a,(([N a1a],[T b1a; N c2]),([T c1a],[N w21]))
                              ;w11b,(([N a1b],[T b1b; T c2]),([T c1b],[T w21]))
                              ; w21,(([N b2],[N w11b;T w11a]),([T b1b;N b1a],[N c2;N a2]))]
            ~frozen:(Int.Set.of_list [a1a;a1b;a2;b1a;b1b;b2;c1a;c1b;c2]) ~label:label


let qB2quad label =
  CliffClust.of_nbrlist
    ~graph:CliffClust.[v11a,(([N b1a; T c2],[T a1a]),([T v21],[N c1a]))
                      ;v11b,(([N b1b; N c2],[T a1b]),([N v21],[N c1b]))
                      ; v21,(([N b2],[T v11b;N v11a]),([T b1b;N b1a],[N c2;N a2]))
                      ; a1a,(([N w11a],[T c1a]),([T d1a],[N v11a]))
                      ; a1b,(([N w11b],[T c1b]),([T d1b],[N v11b]))
                      ;  a2,(([T v21],[N w21]),([T w11a;N w11b],[N b2]))
                      ;w11a,(([N e1a],[T d1a;N a2]),([T w21],[T a1a]))
                      ;w11b,(([N e1b],[T d1b;T a2]),([N w21],[T a1b]))
                      ; w21,(([N w11a;T w11b],[N e2]),([T e1b;N e1a],[N d2;T a2]))]
    ~frozen:(Int.Set.of_list [e1a;e1b;e2;d1a;d1b;d2;b1a;b1b;b2;c1a;c1b;c2]) ~label:label

let m = m1**m2
let l = l1**l2
let lp = lp1 ** lp2

let labelQuad =
  [v11a, Cliff.of_string "1/(c*d)"** tau m ** Cliff.of_string "b/a"**(sigma(tau l)) **  e**(v++w)
  ;v11b, Cliff.of_string "1/(c*d)"** tau m ** Cliff.of_string "b/a"**(sigma(tau l)) **  e**(v++w)  |> tau |> sigma
  ;v21, b**b** y**v**v ** Cliff.of_string "1/(c*c*d*d)"
  ;a1a, Cliff.of_string "1/(c*d)"** m
  ;a1b, Cliff.of_string "1/(c*d)"** m |> tau  |> sigma
  ;a2, Cliff.of_string "1/(c*c)"
  ;b1a, Cliff.of_string "1/(c*d)"**(sigma m) **  a**b**l ** y**v**w
  ;b1b, Cliff.of_string "1/(c*d)"**(sigma m) **  a**b**l ** y**v**w |> tau |> sigma
  ;b2, a**a ** x**y**v**v ** Cliff.of_string "1/(c*c)"
  ;c1a, Cliff.of_string "1/(a*b)"** l |> tau
  ;c1b, Cliff.of_string "1/(a*b)"** l |> tau |> tau |> sigma
  ;c2, Cliff.of_string "1/(a*a)"
  ;w11a, Cliff.of_string "-bp/ap"**(tau lp)**(sigma(tau m))**e**(tau m)**(sigma(tau lp))**(vp++wp)**(tau lp)**(sigma (tau m))
  ;w11b, Cliff.of_string "-bp/ap"**(tau lp)**(sigma(tau m))**e**(tau m)**(sigma(tau lp))**(vp++wp)**(tau lp)**(sigma (tau m)) |> tau |> sigma
  ;w21, Cliff.of_string "-1/(c*c)" **( xp**(vp++wp)**(vp++wp) ++ yp **wp**wp)
  ;d1a, Cliff.of_string "1/(ap*bp)" ** lp
  ;d1b, Cliff.of_string "1/(ap*bp)" ** lp |> tau |> sigma
  ;d2,Cliff.of_string "1/ap"
  ;e1a, ap**bp**(sigma lp) ** Cliff.of_string "-1/(c*d)"** m **tau m **(sigma (tau lp))** yp**vp**wp** (tau lp)**(sigma (tau m))
  ;e1b, ap**bp**(sigma lp) ** Cliff.of_string "-1/(c*d)"** m **tau m **(sigma (tau lp))** yp**vp**wp** (tau lp)**(sigma (tau m)) |> tau |> sigma
  ;e2, Cliff.of_string "1/c" ** xp**yp**vp**vp]

(* w11 -> v11   w21 -> v21
 * w12 -> A1    w22 -> A2
 * w13 -> w11  w23 -> w21 *)
let muP = [[w21];[w11a;w11b]; [v21]; [v11a;v11b]]
let muFlipCore = [[w11a;w11b] ; [w21]; [a1a;a1b]; [a2]; [v11a;v11b]; [v21]; [w11a;w11b]; [w21]; [a1a;a1b]; [a2]; [w11a;w11b]; [w21]]
let mu03 = [ [v11a;v11b]; [a1a;a1b]; [w11a;w11b]; [v11a;v11b]; [a1a;a1b]; [v11a;v11b]; [w11a;w11b]; [a1a;a1b]; [v11a;v11b]
           ; [v21]; [a2]; [w21]; [v21]; [a2]; [v21]; [w21]; [a2]; [v21]]
let muFlipGrouped = muP@ muFlipCore @ mu03
let muFlip = List.concat muFlipGrouped
