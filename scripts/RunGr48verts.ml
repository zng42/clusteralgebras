open Core
open Load
let qgr48 () = Partition.Clust.t_of_sexp (Sexp.of_string (In_channel.read_all "./Clusters/gr48/Framed/save_quiverDynkin"))


let aname_of_string l =
  match (String.split l ~on:';') with
  | [a;name] -> (a,name)
  | _ -> invalid_arg l
let string_of_aname (a,name) = String.concat ~sep:";" [Partition.Vert.id a;name]
let part_parse a = List.map (String.split ~on:'-' a) ~f:(fun l -> List.map (String.split ~on:',' l) ~f:Int.of_string)

let aconv file =
  let table = String.Table.of_alist_exn (List.map ~f:(aname_of_string) (In_channel.read_lines file)) in
  (fun v -> match String.Table.find table (Partition.Vert.id v) with Some a -> a | None -> invalid_arg ("Can't find:" ^ (Partition.Vert.id v)))
let aconv_rev file =
  let table = String.Table.of_alist_exn (List.map ~f:((fun (a,b) -> (b,part_parse a)) |. (aname_of_string)) (In_channel.read_lines file)) in
  (fun name -> match String.Table.find table name with Some a -> a | None -> invalid_arg ("Can't find:" ^ (name)))

let weight_astring l =
  match (Int.of_string (String.prefix l (String.index_exn l ','))) with
  | 0 -> 1
  | w -> w
let weight_a a = Int.of_string (String.slice a 1 (String.index_exn a 'n'))

let a_from_xpair x =
  String.Set.of_list (List.filter ~f:(not |. String.is_empty) (String.split_on_chars ~on:[' ';'(';')';'*';',';'/'] x))


let string_to_x s =
  let clean = String.strip ~drop:(function '(' -> true | ')' -> true | c -> Char.is_whitespace c) in
  let (top,bot) = match String.split s ~on:'/' with [top;bot] -> (top, bot) | _ -> invalid_arg ("More than one / in " ^ s)
  in (List.map ~f:clean (String.split top ~on:'*'), List.map ~f:clean (String.split bot ~on:'*'))

let string_to_xpair s =
  let (x,x1) = match String.split s ~on:',' with [x;x1] -> (x,x1) | _ -> invalid_arg ("More than one , in " ^ s)
  in (string_to_x x, string_to_x x1)

let casimirPairs c = List.concat_map (Set.to_list (Partition.Clust.unfrozen c))
                       ~f:(fun v -> let oNbr = Partition.Clust.oNbr ~frozen:false c v in
                                    let goodPairs =
                                      Map.mapi oNbr ~f:(fun ~key:w ~data:e ->
                                          if e = 1
                                          then Map.keys(Map.filteri oNbr
                                                          ~f:(fun ~key:u ~data:f -> not(Partition.Vert.equal u w)
                                                                                    && f=1
                                                                                    && Partition.Clust.edge_weight c ~inp:u ~out:w = 0))
                                          else [])
                                    in
                                    Map.fold goodPairs ~init:[] ~f:(fun ~key:w ~data:us acc ->
                                        let xw = Partition.XVert.inv (Partition.CSearch.xcoord c w)
                                        in  List.map us ~f:(fun u -> (xw, Partition.CSearch.xcoord c u)) @ acc))

let go ~folder ~basename start max_weight  =
  let amap_filename = (folder ^ basename ^ "_amap.txt") in
  let acoord_names =
    if Sys.file_exists_exn amap_filename
    then String.Table.of_alist_exn (List.map (In_channel.read_lines amap_filename) ~f:aname_of_string)
    else String.Table.create () in
  Out_channel.with_file
    ~append:true
    amap_filename
    ~f:(fun afound_file ->
      let acoord_counts = Hashtbl.map (Int.Table.of_alist_multi (List.map (Hashtbl.keys acoord_names) ~f:(fun a -> (weight_astring a, ())))) ~f:List.length in
      let a_to_string a =
        match Hashtbl.find acoord_names (Partition.Vert.id a) with
        | Some name -> name
        | None ->
           let weight = Partition.Vert.weight a in
           let count = Hashtbl.find_or_add acoord_counts ~default:(fun () -> 0) weight in
           let () = Hashtbl.set acoord_counts ~key:weight ~data:(count + 1) in
           let name = String.concat ["a"; Int.to_string weight; "n" ; Int.to_string (count + 1)] in
           let () = Hashtbl.add_exn acoord_names ~key:(Partition.Vert.id a) ~data:name in
           let () = Out_channel.output_string afound_file (string_of_aname (a, name) ^ "\n") in
           name in
      let x_to_string x =
        String.concat [ "("
                      ; (String.concat ~sep:" * " (List.map (Partition.XVert.top_list x) ~f:a_to_string))
                      ; ")  / ("
                      ; (String.concat ~sep:" * " (List.map (Partition.XVert.bot_list x) ~f:a_to_string))
                      ; ")"] in
      let xfound_filename = (folder ^  basename ^ "_xfound.txt") in
      let xcoord_found =
        if Sys.file_exists_exn xfound_filename
        then String.Hash_set.of_list (In_channel.read_lines xfound_filename)
        else String.Hash_set.create () in
      Out_channel.with_file
        ~append:true
        xfound_filename
        ~f:(fun xfound_file ->
          Out_channel.with_file
            ~append:true
            (folder ^ basename ^ "_xpairs.txt")
            ~f:(fun xpairs_file ->
              let write_xpair (x1,x2) =
                if Hash_set.mem xcoord_found (Partition.XVert.id x1) && Hash_set.mem xcoord_found (Partition.XVert.id x2)
                then ()
                else let () = Out_channel.output_string xpairs_file ((x_to_string x1) ^ "  ,  " ^ (x_to_string x2) ^"\n") in
                     let () = Out_channel.output_string xfound_file (Partition.XVert.to_string x1 ^ "\n") in
                     let () = Hash_set.add xcoord_found (Partition.XVert.id x1) in
                     Hash_set.add xcoord_found (Partition.XVert.id x2)
              in
              Partition.CSearch.search_base start
                ~cond:(fun dist c v -> Set.for_all (Partition.Clust.unfrozen c) ~f:(fun v -> Partition.Vert.weight v <= max_weight))
                ~save:(fun path -> fun (c) -> List.iter (casimirPairs c) ~f:write_xpair)
                (* ~save:(fun path -> fun (c) -> List.iter (xpart_pairs c) write_xpair)*)
                ~stop:(fun tbl -> fun _ -> false)
                ~process:(fun _ -> fun tbl ->
                                   let () = Out_channel.newline stdout in
                                   let () = Out_channel.printf "XCount: %d\n" (Hash_set.length xcoord_found) in
                                   let () = Out_channel.printf "ACount: %d\n" (Hashtbl.length acoord_names) in
                                   let () = Out_channel.print_endline ((String.concat( List.filter_map ~f:(fun (i, count) ->
                                                                                          if count = 0
                                                                                          then None
                                                                                          else Some ((Int.to_string i)
                                                                                                     ^ ":"
                                                                                                     ^ Int.to_string count
                                                                                                     ^ " "))
                                                                                        (Hashtbl.to_alist acoord_counts))) ^ "\n")

                                   in (acoord_names, xcoord_found)))))

          (*
          PartFrame.search_base (PartFrame.frame start)
                                ~cond:(fun dist (c,f) v -> List.for_all f ~f:(List.for_all ~f:(fun i -> i <= max_weight)))
                                ~save:(fun path -> fun (c,f) -> List.iter (xpart_pairs (PartFrame.unframe c)) write_xpair)
                                ~stop:(fun tbl -> fun (f,cf) -> false)
                                ~process:(fun _ -> fun tbl ->
                                                   let _ = Out_channel.newline stdout in
                                                   let _ = Out_channel.printf "XCount: %d\n" (Hash_set.length xcoord_found) in
                                                   let _ = Out_channel.printf "ACount: %d\n" (Hashtbl.length acoord_names) in
                                                   let _ = Out_channel.print_endline ((String.concat( List.filter_map ~f:(fun (i, count) ->
                                                                                                                        if count = 0
                                                                                                                        then None
                                                                                                                        else Some ((Int.to_string i)
                                                                                                                                   ^ ":"
                                                                                                                                   ^ Int.to_string count
                                                                                                                                   ^ " "))
                                                                                                           (Hashtbl.to_alist acoord_counts))) ^ "\n")

                                                   in (acoord_names, xcoord_found)))))
           *)


let process_xpairs ~folder ~basename =
  let xpairs = In_channel.read_lines (folder ^ basename ^ "_xpairs.txt") in
  let amap = String.Table.create () in
  let xmap = Int.Table.create ~size:(List.length xpairs / 2) () in
  let () =  List.iteri xpairs ~f:(fun i x -> let aset = a_from_xpair x in
                                             let () = Hashtbl.add_exn xmap ~key:i ~data:aset in
                                             Set.iter aset ~f:(fun a -> Hashtbl.change amap a ~f:(function Some l -> Some(i :: l) | None -> Some [i] ))) in
  let agrouped = String.Set.group_by (String.Set.of_list (Hashtbl.keys amap)) ~equiv:(fun a1 a2 -> Int.equal (weight_a a1) (weight_a a2)) in
  let counts i =
    let lengths = List.map (Hashtbl.data (Hashtbl.filteri amap ~f:(fun ~key ~data -> weight_a key = i))) ~f:List.length in
      List.map ~f:(fun l -> (List.hd_exn l, List.length l))
             (List.group ~break:(fun i j -> not(Int.equal i j)) (List.sort ~compare:Int.compare lengths))
  in
  Out_channel.with_file (folder ^ basename ^ "_summary.txt") ~f:(fun sfile ->
                          let () = Out_channel.output_string sfile "A Count (weight, count)\n" in
                          let acounts =  List.sort ~compare:(fun (w1,_) (w2,_) -> Int.compare w1 w2)
                                                   (List.map agrouped ~f:(fun s -> (weight_a(Set.min_elt_exn s), Set.length s))) in
                          let () = List.iter acounts ~f:(fun (w,count) -> Printf.fprintf sfile "(%n,%n); " w count) in
                          let () = Out_channel.output_string sfile "\n\n" in
                          let () = Printf.fprintf sfile "X Count: %n\n" (List.length xpairs) in
                          let () = Out_channel.output_string sfile "\n" in
                          let () = Out_channel.output_string sfile "(Number of of X coordinates containing a fixed A, Number of times that size occurs)\n" in
                          let () = List.iter acounts ~f:(fun (w,_) ->
                                               ( Printf.fprintf sfile "weight A = %n\n    " w
                                               ; List.iteri (counts w) ~f:(fun i (len,count) ->
                                                              ( Printf.fprintf sfile "(%n, %n); " len count
                                                              ; if i mod 10 = 9 then Out_channel.output_string sfile "\n    " else ()) )
                                               ; Out_channel.output_string sfile "\n") ) in
                          ())


let add values c =
  match Set.find (Partition.Clust.unfrozen c) ~f:(not |. (Map.mem values)) with
  | None -> invalid_arg "Cluster isn't new"
  | Some v ->
     let iNbr = Partition.Clust.iNbr c v in
     let oNbr = Partition.Clust.oNbr c v in
     let old = Partition.Vert.mutate v ~inp:iNbr ~out:oNbr in
     Map.add_exn values ~key:v
       ~data:(((Map.fold iNbr ~init:0.0 ~f:(fun ~key ~data acc -> Fn.apply_n_times ~n:(Int.abs data) (( *.) (Map.find_exn values key)) acc))
               +. (Map.fold oNbr ~init:0.0 ~f:(fun ~key ~data acc -> Fn.apply_n_times ~n:(Int.abs data) (( *.) (Map.find_exn values key)) acc))
              )
              /. (Map.find_exn values old)
       )
