(*
(* Doubly Extended B3 (1,1) *)
let name = "B3_11"
let q = Fixed.Clust.of_edgelist ~frozen:Int.Set.empty ~graph:[1,2;1,2; 2,3; 3,1; 2,4; 4,1;  2,5;2,6; 5,1;6,1] |> Fixed.Clust.frame |> fst
let groups = [[1,1];[2,2];[3,3];[4,4];[5,5;6,6]]
 *)
(*
(* Doubly Extended B2 (2,1) *)
let name = "B2_21"
let q = Fixed.Clust.of_edgelist ~frozen:Int.Set.empty ~graph:[1,2;1,2; 2,3;2,4; 3,1;4,1; 2,5;2,6; 5,1;6,1] |> Fixed.Clust.frame |> fst
let groups = [[1,1];[2,2];[3,3;4,4];[5,5;6,6]]
 *)

(* Doubly Extended BC1 (4,1) *)
let name = "BC1_41"
let q = Fixed.Clust.of_edgelist ~frozen:Int.Set.empty ~graph:[1,2;1,2; 2,3;2,4;2,5;2,6; 3,1;4,1;5,1;6,1] |> Fixed.Clust.frame |> fst
let groups = [[1,1];[2,2];[3,3;4,4;5,5;6,6]]
 
(*
(* Doubly Extended BC2 (4,2) *)
let name = "BC2_42"
let q = Fixed.Clust.of_edgelist ~frozen:Int.Set.empty ~graph:[1,2;7,8; 1,8;7,2; 2,3;2,4;8,5;8,6; 3,1;4,7;5,1;6,7; 2,9;8,9; 9,1;9,7] |> Fixed.Clust.frame |> fst
let groups = [[1,1;7,7];[2,2;8,8];[3,3;4,4;5,5;6,6];[9,9]]
 *) 
(*
(* Doubly Extended F4 (1,1) *)
let name = "F4_11"
let q = Fixed.Clust.of_edgelist ~frozen:Int.Set.empty ~graph:[1,2;1,2; 2,3; 3,1; 3,4;  2,5;2,6; 5,1;6,1; 5,7;6,8] |> Fixed.Clust.frame |> fst
let groups = [[1,1];[2,2];[3,3];[4,4];[5,5;6,6];[7,7;8,8]]
 *)
(*
(* Doubly Extended F4 (2,1) *)
let name = "F4_21"
let q = Fixed.Clust.of_edgelist ~frozen:Int.Set.empty ~graph:[1,2;1,2; 2,3; 3,1;  2,4;2,5; 4,1;5,1; 4,6;5,7; 8,6;9,7] |> Fixed.Clust.frame |> fst
let groups = [[1,1];[2,2];[3,3];[4,4;5,5];[6,6;7,7];[8,8;9,9]]
 *)
(*
(* Doubly Extended G2 (1,1) *)
let name = "G2_11"
let q = Fixed.Clust.of_edgelist ~frozen:Int.Set.empty ~graph:[1,2;1,2; 2,3;2,4;2,5; 3,1;4,1;5,1; 2,6; 6,1] |> Fixed.Clust.frame |> fst
let groups = [[1,1];[2,2];[3,3;4,4;5,5];[6,6]]
 *)
(*
(* Doubly Extended G2 (3,1) *)
let name = "G2_31"
let q = Fixed.Clust.of_edgelist ~frozen:Int.Set.empty ~graph:[1,2;1,2; 2,3;2,4;2,5; 3,1;4,1;5,1; 3,6;4,7;5,8] |> Fixed.Clust.frame |> fst
let groups = [[1,1];[2,2];[3,3;4,4;5,5];[6,6;7,7;8,8]]
 *)





(* Computation *)

let gs = List.map ~f:(List.map ~f:fst) groups

let group_weight c u v = List.map u ~f:(fun ui -> v |> List.map ~f:(fun vi -> Fixed.Clust.edge_weight c ~inp:ui ~out:vi) |> List.fold ~init:0 ~f:(+)) |> List.fold ~init:0 ~f:(+) |> (fun mij -> mij/(List.length u) )
let groupindex v = List.findi_exn gs ~f:(fun _ group -> List.mem ~equal:Int.equal group v) |> fst |> (fun i -> i+1)
let group_mat q = gs |> List.map ~f:(fun u -> List.map gs ~f:(fun v -> group_weight q u v))
let is_group_Tpqr c =
  match gs |> List.concat_map ~f:(fun u -> gs |> List.map ~f:(fun v -> (u,v))) |> List.filter ~f:(fun (u,v) -> group_weight c u v = 2 && Int.abs(group_weight c u v) = Int.abs(group_weight c v u)) with
  | [] -> None
  | [(u,v)] -> if Fixed.Clust.is_SourceSink (Fixed.Clust.freeze c u) then Some (u,v) else None
  | _ -> None

let is_TnBC c =
  match (List.filter gs ~f:(fun group -> List.length group = 4), List.filter gs ~f:(fun group -> List.length group = 1)) with
  | ([],_) -> false
  | (_,[]) -> false
  | ([u],[v]) -> (group_weight c u v = 1)
                 && (gs |> List.map ~f:(fun w -> (group_weight c w u, group_weight c v w, List.length w)) |> List.for_all ~f:(fun (wu,vw,wweight) -> not(wweight = 2) || ( vw =2 && wu=2) ))
                 && Fixed.Clust.is_SourceSink (Fixed.Clust.freeze c u)
  | _ -> false

let () =  q |> Fixed.Clust.removeFrozen |> Fixed.Clust.write_cluster ("./_Clusters/Doubly" ^ name ^ "/q") ~node_color:(groupindex)  ~vlabel:(fun v -> groupindex v |> Int.to_string)

let (iso_class,_) = Fixed.FoldSearch.graph_collect
                      ~init:[]
                      ~add:(fun l v -> v::l)
                      ~mem:(fun l (c,_) -> List.exists l ~f:(fun (d,_) -> Fixed.Clust.is_isomorphic c d))
                      (q,groups) 

let () = iso_class |> List.iteri ~f:(fun i (c,gs) -> c |> Fixed.Clust.removeFrozen |> Fixed.Clust.write_cluster ("./_Clusters/Doubly" ^ name ^ "/qIsoClass_" ^ Int.to_string (i+1))
                                                                                        ~node_color:(groupindex)
                                                                                        ~vlabel:(fun v -> groupindex v |> Int.to_string) )
let paths = iso_class |> List.map ~f:(fun (c,groups) -> (c,List.mapi gs ~f:(fun i v -> (v,Fixed.FoldSearch.shortest ~cond:(fun maxdist _ _  -> maxdist < 50)  [(c, delete groups i)]
                                                                                            (fun (d,_) ->  match d |> is_group_Tpqr with None -> is_TnBC d && not(List.length v =4 || List.length v = 1)
                                                                                                                                       | Some (u,w) -> not(List.equal Int.equal v u || List.equal Int.equal v w)) ))))


let () = Out_channel.with_file ("./_Clusters/Doubly" ^ name ^ "/" ^ name ^ "AllAffineSubalgebraProof.txt") ~f:(fun file -> 
  paths |> List.iter ~f:(fun (c,ps) -> ["//Matrix\n"
                                       ;gs |> List.length |> Int.to_string |> (fun len -> len ^ "," ^ len ^ "\n")
                                       ; group_mat c |> List.map ~f:(List.map ~f:Int.to_string) |> List.map ~f:(String.concat ~sep:" ") |> String.concat ~sep:"\n" |> (fun s -> s ^ "\n")
                                       ; "//Paths To Double Edge\n" ] |> List.iter  ~f:(Out_channel.output_string file);
                                       ps |> List.iter ~f:(fun (v,p) -> ((v|> List.hd_exn |> groupindex |> Int.to_string)
                                                                        ^":"
                                                                        ^ (match p with Some (_,path) -> (path |> List.map ~f:(fun group -> group |> List.map ~f:fst |> List.hd_exn |> groupindex |> Int.to_string) |> String.concat ~sep:",")
                                                                                      | None -> "<FAILED>")
                                                                        ^ "\n") |> Out_channel.output_string file )
                                       ; Out_channel.output_string file "\n"))
